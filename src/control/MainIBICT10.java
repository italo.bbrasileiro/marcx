package control;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import entity.*;


public class MainIBICT10 
{
	static RegexLibrary regexLib = new RegexLibrary();
	
	public static void main(String[] args) throws Exception 
	{
		MARC acervo = new MARC("kohaIbict6/periodicos.xml");
		ArrayList<Record> recordsEditados = acervo.getRecords();
		
		String campo245, campo780, campo785;
		
		for(Record registro : recordsEditados)
		{
			
			campo245 = registro.getField("245").get(0).getSubfield("a").get(0).getValue();
			
			campo780 = registro.hasField("780") ? 
					registro.getField("780").get(0).getSubfield("t").get(0).getValue() : null;
			
			campo785 = registro.hasField("785") ? 
					registro.getField("785").get(0).getSubfield("t").get(0).getValue() : null;
			

			
			if(fieldHasAnySpecialChar(campo245))
			{
				printDadosDoRegistro(registro);
				continue;
			}
			
			if((campo780 != null) && fieldHasAnySpecialChar(campo780))
			{
				printDadosDoRegistro(registro);
				continue;
			}
			
			if((campo785 != null) && fieldHasAnySpecialChar(campo785))
			{
				printDadosDoRegistro(registro);
				continue;
			}
			
			
		}
	}
	

	
	
	private static boolean fieldHasAnySpecialChar(String valorDoCampo) {
		
		if(valorDoCampo.contains("("))
			return true;
		
//		if(valorDoCampo.contains("/"))
//			return true;
		
		return false;
	}




	private static void printDadosDoRegistro(Record record) 
	{
		System.out.print("biblionumber:;" + record.getField("999").get(0).getSubfield("c").get(0).getValue());
		System.out.println(";titulo:;" + record.getField("245").get(0).getSubfield("a").get(0).getValue());
	}

	private static void criaArquivoXML(ArrayList<Record> records, String outputFile) {
		System.out.println(records.size() + " periodicos criados em " + outputFile);
		XMLHandler.generateXMLFile(records, outputFile);
	}
	
}

