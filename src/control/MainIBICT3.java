package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class MainIBICT3 {
	
	/**
	 * atualiza o link de download nos registros com link.
	 * @param args
	 * @throws Exception
	 */
	static int numAutor = 0, numEditoras = 0, exemplares = 0, 
			numNaoEncontrado = 0, numAlda = 0, numMaria = 0;

	public static void main(String[] args)  throws Exception 
	{
		MARC acervoLivros = new MARC("kohaIBICT/registrosTipoMaterial/kohaIBICTlivros.xml");
		MARC acervoDocEle = new MARC("kohaIBICT/registrosTipoMaterial/kohaIBICTdocumentoEletronico.xml");
		MARC acervoEventos = new MARC("kohaIBICT/registrosTipoMaterial/kohaIBICTeventos.xml");
		MARC acervoInternet = new MARC("kohaIBICT/registrosTipoMaterial/kohaIBICTinternet.xml");
		MARC acervoSoftware = new MARC("kohaIBICT/registrosTipoMaterial/kohaIBICTsoftware.xml");
		MARC acervoTeses = new MARC("kohaIBICT/registrosTipoMaterial/kohaIBICTteses.xml");
		ArrayList<Record> leaderCorrigido = new ArrayList<>();
		
		leaderCorrigido.addAll(corrigeConteudoCampo("leader", acervoLivros, 6, 'a'));
		leaderCorrigido.addAll(corrigeConteudoCampo("leader", acervoSoftware, 6, 'm'));
		leaderCorrigido.addAll(corrigeConteudoCampo("leader", acervoInternet, 6, 'm'));
		// nao eh adicionado de primeira para nao ser duplicado
		corrigeConteudoCampo("leader", acervoTeses, 6, 'a');
		leaderCorrigido.addAll(corrigeConteudoCampo("008", acervoTeses, 24, 'm'));
		corrigeConteudoCampo("leader", acervoEventos, 6, 'a');
		leaderCorrigido.addAll(corrigeConteudoCampo("008", acervoEventos, 29, '1'));
		leaderCorrigido.addAll(corrigeConteudoCampo("008", acervoDocEle, 6, 'm'));
		
		System.out.println(leaderCorrigido.size() + " registros com leader corrigido");
		XMLHandler.generateXMLFile(leaderCorrigido, "kohaIBICT/registrosTipoMaterial/leaderCorrigido.xml");
	}
	
	/**
	 * Insere 'letra' em todos os 'registros', como caractere na 'posicao' indicada.
	 * @param registros
	 * @param posicao
	 * @param letra
	 * @return
	 */
	private static ArrayList<Record> corrigeConteudoCampo(String campo, MARC registros, int posicao, char letra) {
		
		ArrayList<Record> records = registros.getRecords();
		
		StringBuilder valor;
		for(Record record : records)
		{
			valor = new StringBuilder(record.getField(campo).get(0).getValue());
			valor.setCharAt(posicao, letra);
			record.getField(campo).get(0).setValue(valor.substring(0));
		}
		return records;
	}

	/**
	 * Substitui os exemplares pelos existentes no CCN.
	 * 
	 * Esse m�todo percorre toda a lista 'periodicos' e substitui
	 * os exemplares pela correpondencia encontrada no arquivo de exemplares
	 * do CCN.
	 * 
	 * @param periodicos
	 */
	private static void substituiExemplaresPeloCorrespondenteCCN(ArrayList<Record> periodicos) {
		
		HashMap<String, String> exemplaresCCN;
		try {
			exemplaresCCN = getExemplaresFromFile("kohaIBICT/000001-9.col");
		
			String codigoCCN;
			for(Record periodico : periodicos)
			{
				if(periodico.hasField("090"))
				{	
					codigoCCN = periodico.getField("090").get(0).
							getSubfield("a").get(0).getValue();
					if(exemplaresCCN.containsKey(codigoCCN))
					{
						substituiExemplaresDePeriodico(periodico, exemplaresCCN.get(codigoCCN));
					}
				}
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

	/**
	 * Subistitui exemplares pelos encontrados no arquivo do ccn.
	 * 
	 * Os subcampos $x = CCN e $y = 16 devem ser mantidos.
	 * @param periodico
	 * @param exemplares
	 */
	private static void substituiExemplaresDePeriodico(Record periodico, String stringExemplares) 
	{
		periodico.removeAllOccurrencesOfField("952");
		
		String[] exemplares = stringExemplares.split(";");
		
		String exemplarFormatado;
		for(String exemplar : exemplares)
		{
			exemplarFormatado = exemplar.trim();
			if(exemplarFormatado.contains("(") 
					&& Character.isDigit(exemplarFormatado.charAt(0)))
			{
				exemplarFormatado = exemplarFormatado.replace(" ", ", v. ");
				exemplarFormatado = exemplarFormatado.replace("(", ", n. ");
				exemplarFormatado = exemplarFormatado.replace(")", "");
				exemplarFormatado = exemplarFormatado.replace(",,", ",");
			}
				
			Field novoExemplar = periodico.createFieldAndSubfield("952", "h", exemplarFormatado);
			novoExemplar.addSubfield(new Subfield("x", "CCN"));
			novoExemplar.addSubfield(new Subfield("y", "16"));
		}
		
		
	}

	/**
	 * Le o conteudo do arquivo de exemplares do CCN e retorna um hashmap,
	 * que contem como chave o identificador ccn (campo 090 $a) e o conteudo
	 * a lista de exemplares.
	 * @param string
	 * @return
	 * @throws IOException 
	 */
	private static HashMap<String, String> getExemplaresFromFile(String pathArquivo) throws IOException {
		File file = new File(pathArquivo);
		BufferedReader br = new BufferedReader(new FileReader(file));
		HashMap<String, String> exemplares = new HashMap<>();
		
		String linha;
		while ((linha = br.readLine()) != null)
		{
			String codigoCCN, listaExemplares;
			if(linha.contains("CODIGO CCN"))
			{
				codigoCCN = linha.split(":")[1];
				
				while(!linha.contains("COLECAO.....:"))
					linha = br.readLine();
				
				listaExemplares = linha.split(":")[1];
				
				
				if(!exemplares.containsKey(codigoCCN))
				{
					exemplares.put(codigoCCN, listaExemplares);
				}else
				{
					exemplares.put(codigoCCN,
							exemplares.get(codigoCCN) + ";" + listaExemplares);
				}
			}
		}
		return exemplares;
	}

	private static void preencheRegistroComDadosExemplares(Record registroEncontrado, String[] linha) {
		
		String autor = linha.length > 1 ? linha[1] : null;
		String titulo = linha.length > 2 ? linha[2] : null;
		String editora = linha.length > 3 ? linha[3] : null;
		String volume = linha.length > 4 ? linha[4] : null;
		String numero = linha.length > 5 ? linha[5] : null;
		String ano = linha.length > 6 ? linha[6] : null;
		String parte = linha.length > 7 ? linha[7] : null;
		String issn = linha.length > 8 ? linha[8] : null;
		String exemplar = linha.length > 9 ? linha[9] : null;
		String tomboBarCode = linha.length > 10 ? linha[10] : null;
		String ccn = linha.length > 11 ? linha[11] : null;
		
		// adicionar caracteres n. (para numero) e v. (para volume) quando nao houver
		if((numero != null) && (!numero.toLowerCase().contains("n")))
			numero = "n. " + numero;
		if((volume != null) && (!volume.toLowerCase().contains("v")))
			volume = "v. " + volume;
		
		String volNumParte = ano + ", " + volume +  
				", " + numero;
		
		if(parte != null && !parte.isBlank() && !parte.isEmpty())
			volNumParte += ", parte " + parte;
		
		// adiciona issn (se nao tiver)
		if(issn != null)
			if(!registroEncontrado.hasAnyOccurrenceOfFieldAndSubfield("022", "a")
					&& !issn.isEmpty())
			{
				registroEncontrado.createFieldAndSubfield("022", "a", issn);
				numAutor++;
			}
		
		// adiciona titulo (se nao tiver)
		if(titulo != null)
			if(!registroEncontrado.hasAnyOccurrenceOfFieldAndSubfield("245", "a")
					&& !titulo.isEmpty())
			{
				registroEncontrado.createFieldAndSubfield("245", "a", titulo);
			}
		
		// adiciona autor (se nao tiver)
		if(autor != null)
			if(!registroEncontrado.hasAnyOccurrenceOfFieldAndSubfield("100", "a")
					&& !autor.isEmpty())
			{
				registroEncontrado.createFieldAndSubfield("100", "a", autor);
				numAutor++;
			}
		
		// adiciona editora, se nao tiver
		if(editora != null)
			if(!registroEncontrado.hasAnyOccurrenceOfFieldAndSubfield("260", "b")
					&& !editora.isBlank())
			{
				registroEncontrado.createFieldAndSubfield("260", "b", editora);
				numEditoras++;					
			}
		
		// adiciona informacoes do tipo de material (periodico)
		if(!registroEncontrado.hasAnyOccurrenceOfFieldAndSubfield("942", "c"))
				registroEncontrado.createFieldAndSubfield("942", "c", "16");
		
		// criacao dos campo 952 correspondente ao exemplar atual
		Field campo952 = 
				registroEncontrado.createFieldAndSubfield("952", "h", volNumParte);
		exemplares++;
		// adiciona os dados do exemplar
		
		if(exemplar != null)
			campo952.createSubfield("t", exemplar);
		if(tomboBarCode != null && !tomboBarCode.isEmpty())
			campo952.createSubfield("p", tomboBarCode);
		if(ccn != null)
			campo952.createSubfield("x", ccn);
		
		campo952.createSubfield("y", "16");
		
	}

	/**
	 * Retorna o registro correspondente ao issn procurado.
	 * 
	 * 
	 * Percorre a lista de registros 'records' e retorna 
	 * aquele registro que tem valor indicado por 'issn' 
	 * no seu campo 022 $a.
	 * @param records lista de registros
	 * @param issn valor buscado no campo 022
	 * @return
	 */
	private static Record buscaISSN(ArrayList<Record> records, String issn) 
	{
		Record result = null;
		
		// percorre a lista de registros em busca do campo 022.
		for(Record record : records)
		{
			if(!record.hasField("022"))
				continue;
			
			if(record.getField("022").get(0).getSubfield("a").get(0).getValue().contentEquals(issn))
			{
				result = record;
				break;
			}
		}
		return result;
	}
	
	/**
	 * Retorna o registro que tem valor indicado por 'titulo' no campo 245.
	 * @param records lista de registros
	 * @param registro com titulo correspondente
	 * @return
	 */
	private static Record buscaTitulo(ArrayList<Record> records, String titulo) 
	{
		Record result = null;
		
		// percorre a lista de registros em busca do campo 022.
		for(Record record : records)
		{
			if(!record.hasField("245"))
				continue;
			
			if(record.getField("245").get(0).getSubfield("a").get(0).getValue().contentEquals(titulo))
			{
				result = record;
				break;
			}
		}
		return result;
	}

	/**
	 * 
	 * @param record
	 * @param line
	 */
	private static void insertFieldInRecord(Record record, String line) {
		
		String code = line.split("!")[1],
				value = line.split("!")[2];
		// TODO adicionar excecao para campo 001
		
		// get String equals to 'field!!!subfield', which corresponds
		// to the imput 'code'.
		String correspondentName = getCodeCorrespondence(code);
		
		if(correspondentName == null)
		{
			return;
		}
		
		String fieldName = correspondentName.split("!!!")[0], 
				subfieldName = correspondentName.split("!!!")[1];
		String fieldDenomination = null;
		ArrayList<Subfield> subcampos = null;
		// separa registros de controlfield e datafield
		if(fieldName.contentEquals("001") || fieldName.contentEquals("008"))
		{
			fieldDenomination = Constant.controlFieldDenomination;
			switch(fieldName)
			{
			case "001":
				record.addField(new Field(fieldName, value, fieldDenomination));
				break;
			case "008":
				if(!record.hasField("008"))
				{
					String empty40char = "                                        ";
					record.addField(new Field(fieldName, empty40char, fieldDenomination));
				}
				record.getField("008").get(0).setValue(
						editField008value(value, subfieldName, record.getField("008").get(0).getValue()));
				
				
			}
		}else
		{
			fieldDenomination = Constant.dataFieldDenomination;
			// instancia o subcampo
			Subfield subcampo = new Subfield(subfieldName, value, Constant.subfieldDenomination);
			
			if(record.hasField(fieldName) && !record.getField(fieldName).get(0).hasSubfield(subfieldName))
			{
				record.getField(fieldName).get(0).addSubfield(subcampo);
			}else
			{
				subcampos = new ArrayList<Subfield>();
				subcampos.add(subcampo);
				record.addField(new Field(fieldName, "", "", subcampos, fieldDenomination));
			}
		}
	}
	
	/**
	 * Edit the current value of field 008
	 * @param value
	 * @param subfieldName
	 * @param value2
	 * @return
	 */
	private static String editField008value(String valueToInsert, String charRange, String toBeEdited) {
		
		int begin = Integer.parseInt(charRange.split("-")[0]);
		
		toBeEdited = toBeEdited.substring(0, begin) + valueToInsert 
				+ toBeEdited.substring(begin + valueToInsert.length(), toBeEdited.length());
		
		return toBeEdited;
	}

	/**
	 * Recebe o codigo e retorna o campo e subcampo correspondente, separado
	 * por '||||', de acordo com a tabela apresentada pela equipe do CCN.
	 * @param code
	 * @return
	 */
	private static String getCodeCorrespondence(String code)
	{	
		String fieldName = null, subfieldName = null;
		
		switch(code)
		{
		case "S020":
			fieldName = "090";
			subfieldName = "a";
			break;
		case "S070":
			fieldName = "090";
			subfieldName = "c";
			break;
		case "S090":
			fieldName = "090";
			subfieldName = "d";
			break;
		case "S100":
			fieldName = "008";
			subfieldName = "07-10";
			break;
		case "S110":
			fieldName = "008";
			subfieldName = "11-14";
			break;
		case "S120":
			fieldName = "008";
			subfieldName = "15-17";
			break;
		case "S130":
			fieldName = "008";
			subfieldName = "18";
			break;
		case "S140":
			fieldName = "008";
			subfieldName = "21";
			break;
		case "S160":
			fieldName = "008";
			subfieldName = "35-37";
			break;
		case "S180":
			fieldName = "008";
			subfieldName = "24";
			break;
		case "S200":
			fieldName = "245";
			subfieldName = "a";
			break;
		case "S210":
			fieldName = "245";
			subfieldName = "f";
			break;
		case "S220":
			fieldName = "245";
			subfieldName = "c";
			break;
		case "S230":
			fieldName = "245";
			subfieldName = "b";
			break;
		case "S240":
			fieldName = "245";
			subfieldName = "n";
			break;
		case "S250":
			fieldName = "245";
			subfieldName = "p";
			break;
		case "S280":
			fieldName = "210";
			subfieldName = "a";
			break;
		case "S310":
			fieldName = "246";
			subfieldName = "0";
			break;
		case "S320":
			fieldName = "246";
			subfieldName = "a";
			break;
		case "S330":
			fieldName = "246";
			subfieldName = "3";
			break;
		case "S340":
			fieldName = "246";
			subfieldName = "3";
			break;
		case "S411":
			fieldName = "260";
			subfieldName = "a";
			break;
		case "S412":
			fieldName = "260";
			subfieldName = "b";
			break;
		case "S420":
			fieldName = "362";
			subfieldName = "a";
			break;
		case "S440":
			fieldName = "022";
			subfieldName = "a";
			break;
		case "S520":
			fieldName = "550";
			subfieldName = "a";
			break;
		case "S532":
			fieldName = "664";
			subfieldName = "b";
			break;
		case "S533":
			fieldName = "650";
			subfieldName = "a";
			break;
		case "S610":
			fieldName = "759";
			subfieldName = "t";
			break;
		case "S620":
			fieldName = "769";
			subfieldName = "t";
			break;
		case "S710":
			fieldName = "780";
			subfieldName = "0";
			break;
		case "S730":
			fieldName = "780";
			subfieldName = "4";
			break;
		case "S740":
			fieldName = "780";
			subfieldName = "5";
			break;
		case "S750":
			fieldName = "780";
			subfieldName = "6";
			break;
		case "S810":
			fieldName = "785";
			subfieldName = "0";
			break;
		case "S830":
			fieldName = "785";
			subfieldName = "4";
			break;
		case "S840":
			fieldName = "785";
			subfieldName = "5";
			break;
		case "S850":
			fieldName = "785";
			subfieldName = "6";
			break;
		case "S860":
			fieldName = "785";
			subfieldName = "7";
			break;
		case "S870":
			fieldName = "785";
			subfieldName = "8";
			break;
		default:
			System.err.println("Codigo de campo \"" + code + "\" nao encontrado!!!");
//			break;
			return null;
				
		}
		
		
		return fieldName + "!!!" + subfieldName;
	}
}
