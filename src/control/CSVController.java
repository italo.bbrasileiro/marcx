package control;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class CSVController {

	public static void leCSVautoridades(String[] args) throws Exception {
		ArrayList<String[]> linhas = readCSV("xmlIn/enap/autoridadesSemCabecalho.csv", ",");
		ArrayList<Integer> ids = new ArrayList<Integer>();
		ArrayList<Field> fields = new ArrayList<Field>();
		ArrayList<Record> records = new ArrayList<Record>();
		HashMap<String, ArrayList<Field>> recordsMap = new HashMap<String, ArrayList<Field>>();

		// inicializacao de variaveis
		String idAnterior = linhas.get(0)[0];
		String campoAnterior = linhas.get(0)[1];
		String subcampoAnterior = linhas.get(0)[2];
		String valorAnterior = linhas.get(0)[3];

		String oldSubfield = " ";
		Field novoCampo = null;
		Subfield novoSubcampo = null;

		for(String[] linha : linhas)
		{
			String idAtual = linha[0];
			String campoAtual = linha[1];
			String subcampoAtual = linha[2];
			String valorAtual;
			if(linha.length == 4)
				valorAtual = linha[3];
			else
				valorAtual = "";

			// criacao do subcampo
			novoSubcampo = new Subfield(subcampoAtual, valorAtual, Constant.subfieldDenomination);

			// se o hashmap nao tem o id atual
			if(!recordsMap.containsKey(idAtual))
			{	// insercao do subcampo na lista de subcampos do campo atual
				ArrayList<Subfield> subcampos = new ArrayList<Subfield>();
				subcampos.add(novoSubcampo);

				// Criacao do campo e insercao na lista de campos do registro atual
				novoCampo = new Field(campoAtual, "", "", subcampos, Constant.dataFieldDenomination);
				ArrayList<Field> campos = new ArrayList<Field>();
				campos.add(novoCampo);

				recordsMap.put(idAtual, campos);
			}else // se o hashmap tem o id atual
			{
				// se o id atual nao tem o campo atual
				if(!Operation.hasField(recordsMap.get(idAtual), campoAtual))
				{
					ArrayList<Subfield> subcampos = new ArrayList<Subfield>();
					subcampos.add(novoSubcampo);

					// Criacao do campo e insercao na lista de campos do registro atual
					novoCampo = new Field(campoAtual, "", "", subcampos, Constant.dataFieldDenomination);
					recordsMap.get(idAtual).add(novoCampo);
				}else // se o id atual tem o campo atual 
				{
					if(novoCampo.hasSubfield(subcampoAtual))
					{
						ArrayList<Subfield> subcampos = new ArrayList<Subfield>();
						subcampos.add(novoSubcampo);

						// Criacao do campo e insercao na lista de campos do registro atual
						novoCampo = new Field(campoAtual, "", "", subcampos, Constant.dataFieldDenomination);
						recordsMap.get(idAtual).add(novoCampo);
					}else
					{
						novoCampo.addSubfield(novoSubcampo);
					}
				}
			}

			idAnterior = idAtual;
			campoAnterior = campoAtual;
			subcampoAnterior = subcampoAtual;
			valorAnterior = valorAtual;
		}

		// construcao dos registros
		ArrayList<Record> marcRecords = new ArrayList<Record>();
		for(String key : recordsMap.keySet())
		{
			ArrayList<Field> recordFields = recordsMap.get(key);
			Record record = new Record(recordFields);
			record.generateLeaderAndControlFields();
			marcRecords.add(record);
		}


		XMLHandler.generateXMLFile(marcRecords);
		System.out.println("Fim!\n");
	}

	public static ArrayList<String[]> readCSV(String filepath, String separator) throws Exception
	{
		File file = new File(filepath);
//		DataInputStream br = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
		BufferedReader br = new BufferedReader(new FileReader(file, StandardCharsets.UTF_8)); 
		ArrayList<String[]> linhas = new ArrayList<String[]>();
		String st;
		String[] temp;

		while ((st = br.readLine()) != null)
		{
			temp = st.split(separator);
			linhas.add(temp);
		}

		br.close();
		return linhas;
	}
	
	public static void writeCSV(String filepath, ArrayList<String[]> arrayToWrite, String separator) throws Exception
	{
		// Creates a FileOutputStream
		FileOutputStream file = new FileOutputStream(filepath);
		// Creates an OutputStreamWriter
		OutputStreamWriter output = new OutputStreamWriter(file);
		String currentLine;
		
		for(String[] linha : arrayToWrite)
		{
			currentLine = "";
			
			for(String celula : linha)
			{
				currentLine += celula + separator;
			}
			output.append(currentLine + "\n");
			
		}
		
		output.close();
	}

	public static void temp(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		MARC marc = new MARC(Constant.inputFile2);
		ArrayList<Record> records = marc.getRecords();
		ArrayList<Record> recordsSemCampos = new ArrayList<Record>();
		int tamanhoCatalogo = records.size();
		
		System.out.println("START");
		
		for(int i = 0; i < records.size(); i++)
		{
			Record recordAtual = records.get(i);
			
			
			
			Field campo001 = new Field("001", Integer.toString(i + 60000), Constant.controlFieldDenomination);
			recordAtual.addField(campo001);
			
			String tipoAutoridade = "";
			
			if(recordAtual.hasField("100"))
				tipoAutoridade = "PERSO_NAME";
			else if(recordAtual.hasField("110"))
				tipoAutoridade = "CORPO_NAME";
			else if(recordAtual.hasField("111"))
				tipoAutoridade = "MEETI_NAME";
			else if(recordAtual.hasField("130"))
				tipoAutoridade = "UNIF_TITLE";
			else if(recordAtual.hasField("148"))
				tipoAutoridade = "CHRON_TERM";
			else if(recordAtual.hasField("150"))
				tipoAutoridade = "TOPIC_TERM";
			else if(recordAtual.hasField("151"))
				tipoAutoridade = "GEOGR_NAME";
			else if(recordAtual.hasField("155"))
				tipoAutoridade = "GENRE/FORM";
			else if(recordAtual.hasField("180"))
				tipoAutoridade = "ATRIBUTO";
			else
			{
				recordsSemCampos.add(records.remove(i));
				i--;
			}
			
			Subfield subA = new Subfield("a", tipoAutoridade, Constant.subfieldDenomination);
			ArrayList<Subfield> subcampos = new ArrayList<Subfield>();
			subcampos.add(subA);
			
			Field campo942 = new Field("942", "", "", subcampos, Constant.dataFieldDenomination);
			recordAtual.addField(campo942);
			
		}
		
		for(int i = 0; i < records.size(); i++)
		{
			Record recordAtual = records.get(i);
			
			for(Field field : recordAtual.getFields())
			{
				String fieldNodeName = field.getXmlNodeName();
				
				if(fieldNodeName.contentEquals(Constant.dataFieldDenomination))
				{
					String fieldName = field.getFieldName();
					
					if(fieldName.length() < 3)
					{
						if(!fieldName.contentEquals("50") && !fieldName.contentEquals("40"))
							System.out.println("Tag com erro= " + fieldName);
						field.setFieldName("0" + fieldName);
					}
				}
			}
		}

		XMLHandler.generateXMLFile(records);
		XMLHandler.generateXMLFile(recordsSemCampos, "xmlOut/enap/autoridadesSemCampo.xml");
		System.out.println("Fim!\n");
	}

//	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {
//		MARC marc = new MARC(Constant.inputFile);
//		ArrayList<Record> records = marc.getRecords();
//		ArrayList<Record> recordsSemCampos = new ArrayList<Record>();
//		int tamanhoCatalogo = records.size();
//		
//		System.out.println("START");
//		
//		for(int i = 0; i < records.size(); i++)
//		{
//			Record recordAtual = records.get(i);
//			
//			for(Field field : recordAtual.getFields())
//			{
//				String fieldNodeName = field.getXmlNodeName();
//				
//				if(fieldNodeName.contentEquals(Constant.dataFieldDenomination))
//				{
//					String fieldName = field.getFieldName();
//					
//					if(fieldName.length() < 3)
//					{
//						if(!fieldName.contentEquals("50") && !fieldName.contentEquals("40"))
//							System.out.println("Tag com erro= " + fieldName);
//						field.setFieldName("0" + fieldName);
//					}
//				}
//			}
//		}
//
//		XMLHandler.generateXMLFile(records);
////		XMLHandler.generateXMLFile(recordsSemCampos, "xmlOut/enap/autoridadesSemCampo.xml");
//		System.out.println("Fim!\n");
//	}
	
	public static void main(String[] args) throws Exception {
		ArrayList<String[]> linhas = readCSV("xmlIn/enap/exemplaresSemCabecalho.csv", ",");
		MARC marc = new MARC("xmlOut/enap/fazenda_final4.xml");
		String output = "xmlOut/enap/fazenda_final5.xml";
		HashMap<String, String[]> registrosNaoEncontrados = new HashMap<String, String[]>();

		int recordsNotFound = 0, counter = 0;
		//		System.out.println("Registros nao encontrados:");

		for(String[] linha : linhas)
		{
			counter++;
			ArrayList<Record> records = marc.getRecordWithFieldValue("001", linha[0]);

			if(records == null)
			{
				// lista de registros nao encontrados
				//				System.out.println(linha[0]);
				registrosNaoEncontrados.put(linha[0], linha);
			}else if(records.size() > 1)
			{
				//				System.err.print("Registros com identificadores repetidos:" );
				//				for(Record temp : records)
				//				{
				//					System.out.print(temp.getField("001").get(0).getValue());
				//				}
				throw (new IllegalArgumentException());
			}else
			{
				Record record = records.get(0);
				ArrayList<Subfield> subfields = new ArrayList<Subfield>();

				Subfield novoSubcampo;
				if(!linha[1].contentEquals("NULL")) {
					novoSubcampo = new Subfield("p", linha[1], Constant.subfieldDenomination);
					subfields.add(novoSubcampo);
				}
				if(!linha[3].contentEquals("NULL")) {
					novoSubcampo = new Subfield("e", linha[3], Constant.subfieldDenomination);
					subfields.add(novoSubcampo);
				}
				if(!linha[4].contentEquals("NULL")) {
					novoSubcampo = new Subfield("t", linha[4], Constant.subfieldDenomination);
					subfields.add(novoSubcampo);
				}
				if(!linha[5].contentEquals("NULL")) {
					novoSubcampo = new Subfield("5", linha[5], Constant.subfieldDenomination);
					subfields.add(novoSubcampo);
				}
				if(!linha[11].contentEquals("NULL")) {
					novoSubcampo = new Subfield("d", linha[11], Constant.subfieldDenomination);
					subfields.add(novoSubcampo);
				}
				if(!linha[12].contentEquals("NULL")) {
					novoSubcampo = new Subfield("h", linha[12], Constant.subfieldDenomination);
					subfields.add(novoSubcampo);
				}
				if(!linha[14].contentEquals("NULL")) {
					String termo = linha[14];
					if(termo.contentEquals("Não Disponível"))
						termo = "-1";
					else if(termo.contentEquals("Periódicos"))
						termo = "1";
					else if(termo.contentEquals("Obras Raras"))
						termo = "2";
					else if(termo.contentEquals("Em aquisição"))
						termo = "3";
					else if(termo.contentEquals("Preparo Técnico"))
						termo = "4";
					else if(termo.contentEquals("Expositor"))
						termo = "5";
					else if(termo.contentEquals("Setor de Referência"))
						termo = "6";
					else if(termo.contentEquals("Coleção Memória Institucional"))
						termo = "7";

					if(!termo.contentEquals("Disponível no Acervo"))
					{
						novoSubcampo = new Subfield("7", termo, Constant.subfieldDenomination);
						subfields.add(novoSubcampo);
					}
				}
				if(!linha[15].contentEquals("NULL")) {
					novoSubcampo = new Subfield("x", linha[15], Constant.subfieldDenomination);
					subfields.add(novoSubcampo);
				}

				Field campo952 = new Field("952", "", "", subfields, Constant.dataFieldDenomination);
				record.addField(campo952);
			}

			if(counter % 100 == 0)
				System.out.println(counter + "/" + linhas.size());

		}

		int cont = 0, tamanhoCatalogo = marc.getRecords().size();

		// Mudar o tipo de material para a sigla utilizada
		for(Record record : marc.getRecords())
		{
			if(record.getLeaderField().getValue().length() != 24)
			{
				record.getLeaderField().setValue("      am         #a     ");
			}
			
			cont++;
			if(record.hasField("942"))
			{
				Subfield field942c = record.getField("942").get(0).getSubfield("c").get(0);
				switch(field942c.getValue())
				{
				case "Artigos":
					field942c.setValue("ANA");
					break;
				case "Catálogos":
					field942c.setValue("CAT");
					break;
				case "CD-ROMs":
					field942c.setValue("CDROM");
					break;
				case "Ebook Central":
					field942c.setValue("EBOOK");
					break;
				case "Edital":
					field942c.setValue("EDT");
					break;
				case "Folhetos":
					field942c.setValue("FL");
					break;
				case "Livro":
					field942c.setValue("G");
					break;
				case "Livro Consulta":
					field942c.setValue("C");
					break;
				case "Livro Especial":
					field942c.setValue("E");
					break;
				case "Livro Geral":
					field942c.setValue("G");
					break;
				case "Livro Referência":
					field942c.setValue("R");
					break;
				case "Periódico":
					field942c.setValue("S");
					break;
				case "Portaria":
					field942c.setValue("POR");
					break;
				case "Relatórios":
					field942c.setValue("REL");
					break;
				case "TCC  (Graduação)":
					field942c.setValue("ANA");
					break;
				case "TCCP (Pós-Graduação)":
					field942c.setValue("TCCP");
					break;
				case "Tese":
					field942c.setValue("T");
					break;
				case "Vídeo":
					field942c.setValue("VG");
					break;
				default:
					System.err.println("Tipo de material não encontrado:" + field942c.getValue());
					break;
				}
			}else
			{
				System.err.println("Registro sem 942 $c:" + record.getField("001").get(0).getValue());
			}

			if(record.hasField("952"))
			{
				String tipoMaterial = record.getField("942").get(0).getSubfield("c").get(0).getValue();
				String subFieldNodeName = record.getField("942").get(0).getSubfield("c").get(0).getXmlNodeName();
				for(Field field952 : record.getField("952"))
				{
					Subfield subY = new Subfield("y", tipoMaterial, subFieldNodeName);
					field952.addSubfield(subY);
					Subfield subA = new Subfield("a", "002", subFieldNodeName);
					Subfield subB = new Subfield("b", "002", subFieldNodeName);
					field952.addSubfield(subA);
					field952.addSubfield(subB);
				}

			}

			if(cont % 200 == 0)
			{
				System.out.println(cont + "/" + tamanhoCatalogo);
			}
		}

		XMLHandler.generateXMLFile(marc, output);
		System.out.println("Registros não encontrados: " + recordsNotFound);


		//		System.out.println("Print:");
		//
		//		for(String chave : registrosNaoEncontrados.keySet())
		//		{
		//			//System.out.println(registrosNaoEncontrados.get(chave));
		//			for(String coluna : registrosNaoEncontrados.get(chave))
		//			{
		//				System.out.print(coluna + ",");
		//			}
		//			System.out.println();
		//		}

	}















}
