package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class MainCCN {
	
	public static void filtragemDosCampo(String[] args) throws IOException {
		File file = new File("acervoCCN/000001-9.TIT");
		BufferedReader br = new BufferedReader(new FileReader(file)); 
		ArrayList<String[]> linhas = new ArrayList<String[]>();
		HashMap<String, String> campos = new HashMap<>(); 
		String st, campo;
		String[] temp;
		int cont = 0;

		while ((st = br.readLine()) != null)
		{
			cont++;
			if(st.contains("!REC-ID"))
				continue;
			if(!st.startsWith("!"))
				continue;
			
			
			temp = st.split("!");
			campo = temp[1];
			
			if(!campos.containsKey(campo))
				campos.put(campo, null);
		}
		br.close();
		
		for(String campoAtual : campos.keySet())
		{
			System.out.println(campoAtual);
		}
		
	}
}
