package control;

import java.util.ArrayList;

import entity.Field;
import entity.MARC;
import entity.Record;

public class MainMDH {
	
	/**
	 * atualiza o link de download nos registros com link.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args)  throws Exception {
		MARC acervo = new MARC("mmfdh/kohaMDHAutoridades.xml");
		ArrayList<Record> duplicados = new ArrayList<Record>();
		boolean isDuplicated;
		
		for(Record rec : acervo.getRecords())
		{
			isDuplicated = false;
			for(int i = 0; i < rec.getFields().size() - 1; i++)
			{
				String f1 = rec.getFields().get(i).getFieldAsXML();
				for(int j = i + 1; j < rec.getFields().size(); j++)
				{
					String f2 = rec.getFields().get(j).getFieldAsXML();
					if(f1.contentEquals(f2))
					{
						duplicados.add(rec);
						isDuplicated = true;
						break;
					}
				}
				if(isDuplicated)
					break;
			}
		}
		
		System.out.println(duplicados.size() + " records with duplicated fields.");
		// para gerar o xml com as autoridades duplicadas
//		XMLHandler.generateXMLFile(duplicados, "mmfdh/kohaMDHAutoridadesDuplicadas.xml");
		
		for(Record rec : duplicados)
		{
			for(int i = 0; i < rec.getFields().size() - 1; i++)
			{
				String f1 = rec.getFields().get(i).getFieldAsXML();
				for(int j = i + 1; j < rec.getFields().size(); j++)
				{
					String f2 = rec.getFields().get(j).getFieldAsXML();
					if(f1.contentEquals(f2))
					{
						rec.removeField(rec.getFields().get(j));
						j--;
					}
				}
			}
		}
		
		XMLHandler.generateXMLFile(duplicados, "mmfdh/kohaMDHAutoridadesSemDuplicadas.xml");
		System.out.println("Fim");
	}
	
}
