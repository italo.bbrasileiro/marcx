package control;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import entity.Field;
import entity.MARC;
import entity.Record;

public class MainObjetosDigitaisAN2 {
	
	/**
	 * Este metodo le o arquivo principal do acervo e separa os registros por tipo
	 * de campo do objeto digital.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args)  throws Exception {
//		public static void criacaoDeArquivosSeparados(String[] args)  throws Exception {
		MARC acervo1 = new MARC("arquivoNacionalODsNovo/acervoAN_1.xml");
		MARC acervo2 = new MARC("arquivoNacionalODsNovo/acervoAN_2.xml");
		ArrayList<Record> records856 = new ArrayList<Record>(); // registros com link apenas no 856
		ArrayList<Record> records952 = new ArrayList<Record>(); // registros com link apenas no 952
		
		ArrayList<Record> acervoTotal = new ArrayList<Record>();
		acervoTotal.addAll(acervo1.getRecords());
		acervoTotal.addAll(acervo2.getRecords());
		
		System.out.println("Tamanho do acervo:" + acervoTotal.size() + " registros");
		
		BufferedWriter links856, erros856, id856, links952, erros952, id952;
		
		// it is false so the previous content is cleared. "true" to keep the content.
		links856 = new BufferedWriter(new FileWriter("arquivoNacionalODsNovo/campo856/links856.txt", false));
		id856 = new BufferedWriter(new FileWriter("arquivoNacionalODsNovo/campo856/id856.csv", false));
		erros856 = new BufferedWriter(new FileWriter("arquivoNacionalODsNovo/campo856/erros856.txt", false));
		links952 = new BufferedWriter(new FileWriter("arquivoNacionalODsNovo/campo952/links952.txt", false));
		id952 = new BufferedWriter(new FileWriter("arquivoNacionalODsNovo/campo952/id952.csv", false));
		erros952 = new BufferedWriter(new FileWriter("arquivoNacionalODsNovo/campo952/erros952.txt", false));
		
		// itera por todos os registros e separa aqueles com apenas o 
		// campo 856$u, apenas 952 $u ou ambos
		for(Record r : acervoTotal)
		{
			if(r.hasAnyOccurrenceOfFieldAndSubfield("856", "u"))
			{
				for(Field f : r.getField("856"))
				{
					String link = f.getSubfield("u").get(0).getValue()
							.replace("http://biblioteca.an.gov.br/bnweb/", "http://biblioteca/bnweb/");
					
					if(link.endsWith(".pdf") && !link.contains(" ") && !link.contains("|")) 
					{
						links856.append(link + "\n");
						id856.append(r.getField("245").get(0).getSubfield("a").get(0).getValue() +
								";999 $c:;" + r.getField("999").get(0).getSubfield("c")
								.get(0).getValue() + 
								";Link: " + link + "\n");
					}else
					{
						erros856.append(r.getField("245").get(0).getSubfield("a").get(0).getValue() +
								";999 $c:;" + r.getField("999").get(0).getSubfield("c")
								.get(0).getValue() + 
								";Link: " + link + "\n");
					}
				}
			}
			
			if(r.hasAnyOccurrenceOfFieldAndSubfield("952", "u"))
			{
				for(Field f : r.getField("952"))
				{
					if(!f.hasSubfield("u"))
						continue;
					
					String link = f.getSubfield("u").get(0).getValue()
							.replace("http://biblioteca.an.gov.br/bnweb/", "http://biblioteca/bnweb/");
					if(link.endsWith(".pdf") && !link.contains(" ") && !link.contains("|")) 
					{
						links952.append(link + "\n");
						id952.append(r.getField("245").get(0).getSubfield("a").get(0).getValue() +
								";999 $c:;" + r.getField("999").get(0).getSubfield("c")
								.get(0).getValue() + 
								";952 $9:;" + f.getSubfield("9").get(0).getValue() +
								";Link: " + link + "\n");
					}else
					{
						erros952.append(r.getField("245").get(0).getSubfield("a").get(0).getValue() +
								";999 $c:;" + r.getField("999").get(0).getSubfield("c")
								.get(0).getValue() + 
								"952 $9:;" + f.getSubfield("9").get(0).getValue() +
								";Link: " + link + "\n");
					}
				}
			}
		}
		
		links856.close();
		id856.close();
		erros856.close();
		links952.close();
		id952.close();
		erros952.close();
		
		// procura os registros pelo conteudo do link
		ArrayList<String[]> uploads = CSVController.readCSV("arquivoNacionalODsNovo/uploads.csv", ";");
		ArrayList<Record> registrosComLinkEditado = new ArrayList<>();
		BufferedWriter linksComErro;
		
		// it is false so the previous content is cleared. "true" to keep the content.
		linksComErro = new BufferedWriter(new FileWriter("arquivoNacionalODsNovo/linksComErro.csv", false));
		
		for (Record r : acervoTotal)
		{
			if(r.hasAnyOccurrenceOfFieldAndSubfield("856", "u"))
			{
				for(Field f : r.getField("856"))
				{
					String link = f.getSubfield("u").get(0).getValue()
							.replace("http://biblioteca.an.gov.br/bnweb/", "http://biblioteca/bnweb/");
					
					// tratamentos para formatacao de 'link'
					if(link.startsWith("http://"))
						link = link.replace("http://", "");
					
					link = link.replace("/", "-") + ".pdf";
					
					boolean ODencontrado = false;
					String[] chaveOD = null;
					for(String[] od : uploads)
					{
						if(link.contentEquals(od[1]))
						{
							ODencontrado = true;
							chaveOD = od;
							break;
						}
					}
					
					if(ODencontrado)
					{
						String novoLink = "http://koha.an.gov.br/cgi-bin/koha/opac-retrieve-file.pl?id=" + chaveOD[0];
						f.getSubfield("u").get(0).setValue(novoLink);
						if(!registrosComLinkEditado.contains(r))
							registrosComLinkEditado.add(r);
						
					}else
					{
						linksComErro.append(r.getField("245").get(0).getSubfield("a").get(0).getValue() +
								";999 $c:;" + r.getField("999").get(0).getSubfield("c")
								.get(0).getValue() + ";" + link + "\n");
					}
				}
			}
			
			if(r.hasAnyOccurrenceOfFieldAndSubfield("952", "u"))
			{
				for(Field f : r.getField("952"))
				{
					if(!f.hasSubfield("u"))
						continue;
					
					String link = f.getSubfield("u").get(0).getValue()
							.replace("http://biblioteca.an.gov.br/bnweb/", "http://biblioteca/bnweb/");
					
					// tratamentos para formatacao de 'link'
					if(link.startsWith("http://"))
						link = link.replace("http://", "");
					
					link = link.replace("/", "-");
					
					boolean ODencontrado = false;
					String[] chaveOD = null;
					for(String[] od : uploads)
					{
						if(link.contentEquals(od[1]))
						{
							ODencontrado = true;
							chaveOD = od;
							break;
						}
					}
					
					if(ODencontrado)
					{
						String novoLink = "http://koha.an.gov.br/cgi-bin/koha/opac-retrieve-file.pl?id=" + chaveOD[0];
						f.getSubfield("u").get(0).setValue(novoLink);
						if(!registrosComLinkEditado.contains(r))
							registrosComLinkEditado.add(r);
					}else
					{
						linksComErro.append(r.getField("245").get(0).getSubfield("a").get(0).getValue() +
								";999 $c:;" + r.getField("999").get(0).getSubfield("c")
								.get(0).getValue() + 
								";952 $9:;" + f.getSubfield("9").get(0).getValue() + ";" +
								link + "\n");
					}
				}
			}
		}
		
		linksComErro.close();
		
		System.out.println("registros alterados:" + registrosComLinkEditado.size() + " registros");
		XMLHandler.generateXMLFile(registrosComLinkEditado, "arquivoNacionalODsNovo/linksCorrigidos.xml");
	}
	
	
}
