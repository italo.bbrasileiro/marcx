package control;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

/**
 * Esta classe apresenta um conjunto de operacoes que podem ser 
 * realizadas sobre listas de registros, campos ou subcampos.
 * @author Italo Brasileiro
 *
 */
public class Operation {

	public static ArrayList<Record> xmlIBICT()
	{
		ArrayList<Record> result = null;

		return result;
	}
	
	/**
	 * Esta funcao verifica se o arrayList 'fields' apresenta o campo indicado por
	 * 'fieldName'.
	 * @param fields lista de campos avaliada
	 * @param fieldName nome do subcampo buscado
	 * @return
	 */
	public static boolean hasField(ArrayList<Field> fields, String fieldName)
	{
		for(Field field : fields)
		{
			if(field.getFieldName().contentEquals(fieldName))
				return true;
		}
		return false;
	}
	
	public static void mergeMarcFiles(String filepath1, String filepath2) throws Exception 
	{
		MARC marc = new MARC(filepath1);
		MARC marc2 = new MARC(filepath2);
		
		ArrayList<Record> result = new ArrayList<Record>();
		
		result.addAll(marc.getRecords());
		result.addAll(marc2.getRecords());

		XMLHandler.generateXMLFile(result);
		System.out.println("Fim!\n");
	}

	public static void main(String[] args) throws IOException {
		String filepath = "xmlIn/enap/fazenda_2.xml";
		String outputFilepath = "xmlOut/enap/fazenda_2_saida.xml";
		File file = new File(filepath);
		BufferedReader br = new BufferedReader(new FileReader(file));
		BufferedWriter writer;
		ArrayList<String[]> linhas = new ArrayList<String[]>();
		String st;
		String[] temp;
		int i = 0;

		// Comandos usados para abrir o arquivo e limpar qualquer conteudo antigo.
		PrintWriter writer2 = new PrintWriter(outputFilepath);
		writer2.print("");
		writer2.close();

		writer = new BufferedWriter(new FileWriter(outputFilepath, true));
		while ((st = br.readLine()) != null)
		{	
			// Para limpar os espaços vazios do arquivo
			//			boolean hasOnlyNumbersAndSpaces = true;
			//			
			//			for(int c = 0; c < st.length(); c++)
			//			{
			//				if(!Character.isDigit(st.charAt(c)) && st.charAt(c) != " ".charAt(0))
			//					hasOnlyNumbersAndSpaces = false;
			//			}


			while(Character.isDigit(st.charAt(st.length()-1)))
			{
				st = st.substring(0, st.length() - 1);
			}

			st = st.trim();
			//			for(int c = 0; c < st.length(); c++)
			//			{
			//				if(st.charAt(index))
			//			}
			//			String temp3 = st.replace("    ", "");

			//			String[] temp2 = st.split("                                                                                ");
			//			if(!hasOnlyNumbersAndSpaces)
			//				writer.append(st + "\n");

			i++;
			if(i % 100 == 0)
				System.out.println(i);

			// PARA APAGAR AS LINHAS COM ESPAÇO VAZIO
			//			boolean onlySpace = true;
			//			for(int c = 0; c < st.length(); c++)
			//			{
			//				if(st.charAt(c) != " ".charAt(0))
			//					onlySpace = false;
			//			}
			//			
			//			if(onlySpace == false)
			writer.append(st + "\n");
		}

		br.close();
		writer.close();
	}

	/**
	 * Changes the list of values of 'oldValues' to 'newValues', when it
	 * occurs in 'field' and 'subfield' of the 'record'. The replacement also happens if
	 * the field occurs more than once in a record. Return 'false' if no replacement is made.
	 * 
	 * @param record
	 * @param field
	 * @param subfield
	 * @param oldValues
	 * @param newValues
	 * @return 'true' if occurs at least one replacement, 'false' if no replacement occurs.
	 */
	public static boolean swapSubfieldValuesWithList(Record record, String fieldName, String subfieldName, String[] oldValues,
			String[] newValues) {
		
		// teste de comparacao de tamanho das listas. Devem ser iguais
		if(oldValues.length != newValues.length)
		{
			System.err.println("Listas de termos com tamanhos diferentes!");
		}
		
		ArrayList<Field> fieldOccurence = record.getField(fieldName);
		boolean replacementMade = false;
		
		for(Field field : fieldOccurence)
		{
			ArrayList<Subfield> subfieldOccurence = field.getSubfield(subfieldName);
			for(Subfield subfield : subfieldOccurence)
			{
				String value = subfield.getValue();
				
				for(int i = 0; i < oldValues.length; i++)
				{
					if(value.contentEquals(oldValues[i]))
					{
						subfield.setValue(newValues[i]);
						replacementMade = true;
						break;
					}
				}
			}
		}
		
		return replacementMade;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
