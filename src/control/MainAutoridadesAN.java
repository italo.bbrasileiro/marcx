package control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class MainAutoridadesAN {
	
	/**
	 * Cria subcampo 100 $d no RB, a partir das datas de nascimento e morte dos autores
	 * encontrados nos registros de autoridades.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args)  throws Exception 
	{
//		MARC acervo = new MARC("arquivoNacionalAutoridades/acervo.xml");
		MARC acervo1 = new MARC("arquivoNacionalAutoridades/acervo1.xml");
		MARC acervo2 = new MARC("arquivoNacionalAutoridades/acervo2.xml");
		MARC acervo3 = new MARC("arquivoNacionalAutoridades/acervo3.xml");
		MARC acervo4 = new MARC("arquivoNacionalAutoridades/acervo4.xml");
		System.out.println("Lidos");
		MARC autoridades = new MARC("arquivoNacionalAutoridades/autoridades.xml");
		ArrayList<Record> registrosAlterados = new ArrayList<Record>();
		
		
		System.out.println("Leitura do acervo1");
		int cont = 0;
		for(Record rb : acervo1.getRecords())
		{
			if(cont++ % 300 == 0)
				System.out.println(cont + "/" + acervo1.getRecords().size() + " e " + registrosAlterados.size() + " alterados.");
			
			if(rb.hasAnyOccurrenceOfFieldAndSubfield("100", "a"))
			{
				for(Record ra : autoridades.getRecords())
				{
					if(ra.hasAnyOccurrenceOfFieldAndSubfield("100", "a") && ra.hasAnyOccurrenceOfFieldAndSubfield("100", "d"))
					{
						if(ra.getField("100").get(0).getSubfield("a").get(0).getValue().contentEquals(
								rb.getField("100").get(0).getSubfield("a").get(0).getValue()))
						{
							Subfield subd = new Subfield("d", ra.getField("100").get(0).getSubfield("d").get(0).getValue(), Constant.subfieldDenomination);
							rb.getField("100").get(0).getSubfields().add(subd);
							registrosAlterados.add(rb);
							break;
						}
					}
				}
			}
		}
		
		System.out.println("Leitura do acervo2");
		cont = 0;
		for(Record rb : acervo2.getRecords())
		{
			if(cont++ % 300 == 0)
				System.out.println(cont + "/" + acervo2.getRecords().size() + " e " + registrosAlterados.size() + " alterados.");
			if(rb.hasAnyOccurrenceOfFieldAndSubfield("100", "a"))
			{
				for(Record ra : autoridades.getRecords())
				{
					if(ra.hasAnyOccurrenceOfFieldAndSubfield("100", "a") && ra.hasAnyOccurrenceOfFieldAndSubfield("100", "d"))
					{
						if(ra.getField("100").get(0).getSubfield("a").get(0).getValue().contentEquals(
								rb.getField("100").get(0).getSubfield("a").get(0).getValue()))
						{
							Subfield subd = new Subfield("d", ra.getField("100").get(0).getSubfield("d").get(0).getValue(), Constant.subfieldDenomination);
							rb.getField("100").get(0).getSubfields().add(subd);
							registrosAlterados.add(rb);
							break;
						}
					}
				}
			}
		}
		
		System.out.println("Leitura do acervo3");
		cont = 0;
		for(Record rb : acervo3.getRecords())
		{
			if(cont++ % 300 == 0)
				System.out.println(cont + "/" + acervo3.getRecords().size() + " e " + registrosAlterados.size() + " alterados.");
			if(rb.hasAnyOccurrenceOfFieldAndSubfield("100", "a"))
			{
				for(Record ra : autoridades.getRecords())
				{
					if(ra.hasAnyOccurrenceOfFieldAndSubfield("100", "a") && ra.hasAnyOccurrenceOfFieldAndSubfield("100", "d"))
					{
						if(ra.getField("100").get(0).getSubfield("a").get(0).getValue().contentEquals(
								rb.getField("100").get(0).getSubfield("a").get(0).getValue()))
						{
							Subfield subd = new Subfield("d", ra.getField("100").get(0).getSubfield("d").get(0).getValue(), Constant.subfieldDenomination);
							rb.getField("100").get(0).getSubfields().add(subd);
							registrosAlterados.add(rb);
							break;
						}
					}
				}
			}
		}
		
		System.out.println("Leitura do acervo4");
		cont = 0;
		for(Record rb : acervo4.getRecords())
		{
			if(cont++ % 300 == 0)
				System.out.println(cont + "/" + acervo4.getRecords().size() + " e " + registrosAlterados.size() + " alterados.");
			if(rb.hasAnyOccurrenceOfFieldAndSubfield("100", "a"))
			{
				for(Record ra : autoridades.getRecords())
				{
					if(ra.hasAnyOccurrenceOfFieldAndSubfield("100", "a") && ra.hasAnyOccurrenceOfFieldAndSubfield("100", "d"))
					{
						if(ra.getField("100").get(0).getSubfield("a").get(0).getValue().contentEquals(
								rb.getField("100").get(0).getSubfield("a").get(0).getValue()))
						{
							Subfield subd = new Subfield("d", ra.getField("100").get(0).getSubfield("d").get(0).getValue(), Constant.subfieldDenomination);
							rb.getField("100").get(0).getSubfields().add(subd);
							registrosAlterados.add(rb);
							break;
						}
					}
				}
			}
		}
		
		System.out.println(registrosAlterados.size() + " registros alterados.");
		XMLHandler.generateXMLFile(registrosAlterados, "arquivoNacionalAutoridades/registrosAlteradosComData.xml");
		
		
	}
	
	/**
	 * Cria autoridades do tipo "MEETI_NAME" a partir dos campos 111, 6111, e 711
	 * @param args
	 * @throws Exception
	 */
	public static void mainCriaAutoridades(String[] args)  throws Exception 
	{
//		MARC acervo = new MARC("arquivoNacionalAutoridades/acervo.xml");
		MARC acervo1 = new MARC("arquivoNacionalAutoridades/acervo1.xml");
		MARC acervo2 = new MARC("arquivoNacionalAutoridades/acervo2.xml");
		MARC acervo3 = new MARC("arquivoNacionalAutoridades/acervo3.xml");
		MARC acervo4 = new MARC("arquivoNacionalAutoridades/acervo4.xml");
		System.out.println("Lidos");
//		MARC autoridades = new MARC("arquivoNacionalAutoridades/autoridades.xml");
		ArrayList<Record> novasAutoridades = new ArrayList<Record>();
		

		String[] camposAvaliados = {"111", "611", "711"};
		String[] subcamposAvaliados = {"a", "c", "d", "n"};
		HashMap<String, Integer> autoridadesCriadas = new HashMap<String, Integer>();
		
		System.out.println("Leitura do acervo1");
		for(Record r : acervo1.getRecords())
		{
			for(String campo : camposAvaliados)
			{
				if(r.hasField(campo))
				{
					for(Field f : r.getField(campo))
					{
						for(String subcampo : subcamposAvaliados)
						{
							if(f.hasSubfield(subcampo))
							{
								String autoridade = f.getSubfield(subcampo).get(0).getValue();
								autoridadesCriadas.put(autoridade, 1);

							}
						}
						
					}
				}
			}
		}
		
		System.out.println("Leitura do acervo2");
		for(Record r : acervo2.getRecords())
		{
			for(String campo : camposAvaliados)
			{
				if(r.hasField(campo))
				{
					for(Field f : r.getField(campo))
					{
						for(String subcampo : subcamposAvaliados)
						{
							if(f.hasSubfield(subcampo))
							{
								String autoridade = f.getSubfield(subcampo).get(0).getValue();
								autoridadesCriadas.put(autoridade, 1);

							}
						}
						
					}
				}
			}
		}
		
		System.out.println("Leitura do acervo3");
		for(Record r : acervo3.getRecords())
		{
			for(String campo : camposAvaliados)
			{
				if(r.hasField(campo))
				{
					for(Field f : r.getField(campo))
					{
						for(String subcampo : subcamposAvaliados)
						{
							if(f.hasSubfield(subcampo))
							{
								String autoridade = f.getSubfield(subcampo).get(0).getValue();
								autoridadesCriadas.put(autoridade, 1);

							}
						}
						
					}
				}
			}
		}
		
		System.out.println("Leitura do acervo4");
		for(Record r : acervo4.getRecords())
		{
			for(String campo : camposAvaliados)
			{
				if(r.hasField(campo))
				{
					for(Field f : r.getField(campo))
					{
						for(String subcampo : subcamposAvaliados)
						{
							if(f.hasSubfield(subcampo))
							{
								String autoridade = f.getSubfield(subcampo).get(0).getValue();
								autoridadesCriadas.put(autoridade, 1);

							}
						}
						
					}
				}
			}
		}
		
		Iterator<String> it = autoridadesCriadas.keySet().iterator();
		int cont = 0;
		while(it.hasNext())
		{
			if(cont % 200 == 0)
				System.out.println(cont + " de " + autoridadesCriadas.size());
			cont++;
			
			String novaA = it.next();

			Subfield sub = new Subfield("a", novaA, Constant.subfieldDenomination);
			Subfield subTipo = new Subfield("a", "MEETI_NAME", Constant.subfieldDenomination);
			ArrayList<Subfield> subfields = new ArrayList<Subfield>();
			subfields.add(sub);
			
			ArrayList<Subfield> subfields942 = new ArrayList<Subfield>();
			subfields942.add(subTipo);
			
			Field campoPrincipal = new Field("111", "", "", subfields, Constant.dataFieldDenomination);
			Field campoTipo = new Field("942", "", "", subfields942, Constant.dataFieldDenomination);			
			
			ArrayList<Field> fields = new ArrayList<Field>();
			fields.add(campoPrincipal);
			fields.add(campoTipo);
			
			Record novaAutoridade = new Record(fields);
			novaAutoridade.generateLeaderAndControlFields();
			
			novasAutoridades.add(novaAutoridade);
			
		}
		
		
		
		System.out.println(novasAutoridades + " autoridades criadas.");
		XMLHandler.generateXMLFile(novasAutoridades, "arquivoNacionalAutoridades/novasAutoridades.xml");
		
		
	}
	
	/**
	 * atualiza o link de download nos registros com link.
	 * @param args
	 * @throws Exception
	 */
	public static void mainLinkAutoridades(String[] args)  throws Exception 
	{
//		MARC acervo = new MARC("arquivoNacionalAutoridades/acervo.xml");
//		MARC acervo1 = new MARC("arquivoNacionalAutoridades/acervo1.xml");
//		MARC acervo2 = new MARC("arquivoNacionalAutoridades/acervo2.xml");
//		MARC autoridades = new MARC("arquivoNacionalAutoridades/autoridades.xml");
		
		
		final String[] camposParaDesconsiderar = {"leader", "001", "003", "005", "008"};// campos para não considerar na limpeza da autoridade
		ArrayList<Record> autoridadesDuplicadas = new ArrayList<Record>(); // armazena os registros duplicados
		
		// se as duplicadas não foram separadas
//		autoridadesDuplicadas = autoridades.cleanDuplicatedAuthorities(camposParaDesconsiderar);
		
//		// se as duplicadas foram separadas
//		MARC autoridadesDup = new MARC("arquivoNacionalAutoridades/autoridadesDuplicadas.xml");
		MARC autoridades = new MARC("arquivoNacionalAutoridades/autoridadesSemDuplicadas.xml");
		ArrayList<Record> registrosAtualizados = new ArrayList<Record>();
		ArrayList<Record> autoridadesNaoEncontradas = new ArrayList<Record>();
		// armazena lista de autoridades do tipo CORPO_NAME que aparecem em 100, 700 e 600 (que sao PERSO_NAME)
		ArrayList<Record> autoridadesCORPONAMEemPERSO_NAME = new ArrayList<Record>();
		
		// campos de autoridade para avaliar no RB
		String[] camposDeAutoridadeNoRB = {"100", "110", "111", "130", "260", 
				"600", "610", "611", "630", "650", "700", "710", "711", "730"};
		
		// abrir arquivos do acervo
//		MARC acervo1 = new MARC("arquivoNacionalAutoridades/acervo1.xml");
//		MARC acervo2 = new MARC("arquivoNacionalAutoridades/acervo2.xml");
		int contNovosLinks = 0;
		int contadorAutoridadesCORPONAMEemPERSO_NAME = 0; // conta autoridades CORPO_NAME citadas em campos PERSO_NAME
		
		// percorre lista das partes do acervo
		for(int i = 1; i <= 4; i++)
		{
			MARC acervo = new MARC("arquivoNacionalAutoridades/acervo" + i + ".xml");

			// contador de execucao
			int cont = 0;

			// para cada registro no RB
			for(Record registro : acervo.getRecords())
			{	// print execucao
				cont++;
				if(cont % 300 == 0)
					System.out.println(cont + " de " + acervo.getRecords().size());
				
				// para cada campo de autoridade avaliado no RB
				for(String campoAutoridade : camposDeAutoridadeNoRB)
				{
					boolean linkCriado = false;
					// se o registro tem o campo de autoridade avaliado
					if(registro.hasField(campoAutoridade))
					{
						// percorre a lista de ocorrencias do campo de autoridade avaliado
						for(Field f : registro.getField(campoAutoridade))
						{
							// se não tem o link no subcampo 9
							if(!f.hasSubfield("9"))
							{
								// verifica o tipo de autoridade do campo
								String tipoAutoridade = registro.getAuthorityTypeFromRecordField(campoAutoridade);
								
								// percorre a lista de autoridades
								for(Record autoridade : autoridades.getRecords())
								{
									// se o valor principal da autoridade corresponde ao valor do campo do RB
									if(!f.getFieldName().contentEquals("260")
										&& autoridade.getAuthorityMainFieldValue().contentEquals(f.getSubfield("a").get(0).getValue())
										&& autoridade.getAuthorityType().contentEquals(tipoAutoridade)) // e a autoridade corresponde ao mesmo tipo no RB
									{
										// cria o link do subcampo 001 da autoridade com o subcampo 9 do campo no RB
										f.createSubfield("9", autoridade.getField("001").get(0).getValue());
										// contador de links criados
										contNovosLinks++;
										if(!registrosAtualizados.contains(registro))
											registrosAtualizados.add(registro);
										linkCriado = true;
										
										
									} else // se o campo é o 260 $b e autoridade é CORPO_NAME 
										if(f.getFieldName().contentEquals("260") 
											&& f.hasSubfield("b")
											&& autoridade.getAuthorityMainFieldValue().contentEquals(f.getSubfield("b").get(0).getValue())
											&& autoridade.getAuthorityType().contentEquals(tipoAutoridade))
											{
												// cria o link do subcampo 001 da autoridade com o subcampo 9 do campo no RB
												f.createSubfield("9", autoridade.getField("001").get(0).getValue());
												// contador de links criados
												contNovosLinks++;
												if(!registrosAtualizados.contains(registro))
													registrosAtualizados.add(registro);
												linkCriado = true;
											}
									
									// se a autoridade eh do tipo CORPO_NAME e o campo no RB corresponde a PERSO_NAME
									// e as duas possuem o mesmo valor no campo
									if(autoridade.getAuthorityType().contentEquals("CORPO_NAME")
											&& tipoAutoridade.contentEquals("PERSO_NAME")
											&& autoridade.getAuthorityMainFieldValue().contentEquals(f.getSubfield("a").get(0).getValue()))
									{
										if(!autoridadesCORPONAMEemPERSO_NAME.contains(registro))
											autoridadesCORPONAMEemPERSO_NAME.add(registro);
										
										contadorAutoridadesCORPONAMEemPERSO_NAME++;
									}
								}
								
								// se o link nao eh criado depois de percorrer todas as autoridades
								if(!linkCriado)
								{
									// coloca o RB na lista de RBs sem autoridades correspondentes
									if(!autoridadesNaoEncontradas.contains(registro))
										autoridadesNaoEncontradas.add(registro);
								}
							}else
							{
								continue; // se o campo de autoridade atual tem o link no subcampo 9
							}
						}
					}else
					{
						continue; // se o registro atual nao tem o campo de autoridade avaliado
					}
				}
			}
		}
		System.out.println(contNovosLinks + " links criados.");
		XMLHandler.generateXMLFile(registrosAtualizados, "arquivoNacionalAutoridades/autoridadesSemDuplicadasComLink.xml");
		XMLHandler.generateXMLFile(autoridadesNaoEncontradas, "arquivoNacionalAutoridades/autoridadesNaoEncontradas.xml");
		System.out.println(contadorAutoridadesCORPONAMEemPERSO_NAME + " autoridades citadas em " + autoridadesCORPONAMEemPERSO_NAME.size() +  " RBs");
		XMLHandler.generateXMLFile(autoridadesCORPONAMEemPERSO_NAME, "arquivoNacionalAutoridades/autoridadesCORPONAMEemPERSO_NAME.xml");
		
		
//		XMLHandler.generateXMLFile(autoridadesDuplicadas, "arquivoNacionalAutoridades/autoridadesDuplicadas.xml");
//		XMLHandler.generateXMLFile(autoridades.getRecords(), "arquivoNacionalAutoridades/autoridadesSemDuplicadas.xml");
	}

	
	
	
}
