package control;

import java.util.ArrayList;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;

public class MainIBICT6 {
	
	/**
	 * Essa classe foi criada para tratar apenas uma alteracao
	 * nos registros atualizados do CCN.
	 * @param args
	 * @throws Exception
	 */
	
	public static void main(String[] args) throws Exception 
	{
		
		MARC rbsMarc = new MARC("kohaIBICT3/registrosDivulgacao.xml");
		ArrayList<Record> rbs = rbsMarc.getRecords();
		
		for(Record record : rbs)
		{
			record.addField(new Field("599", "a", "Divulgação", Constant.dataFieldDenomination));
		}
			
		System.out.println(rbs.size() + " periodicos CCN criados");
		XMLHandler.generateXMLFile(rbs, "kohaIBICT3/registrosDivulgacaoAtualizados.xml");
	}
}
