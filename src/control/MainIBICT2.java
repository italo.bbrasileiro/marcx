package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

import entity.Constant;
import entity.Field;
import entity.Record;
import entity.Subfield;

public class MainIBICT2 {
	
	/**
	 * atualiza o link de download nos registros com link.
	 * @param args
	 * @throws Exception
	 */
	static int numAutor = 0, numEditoras = 0, exemplares = 0, 
			numNaoEncontrado = 0, numAlda = 0, numMaria = 0;

	/**
	 * Criacao de exemplares baseados na tabela enviada pela Ana do CCN.
	 * 
	 * Os exemplares devem ser criados e transformados em marcxml, para
	 * posterior importacao no Koha.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args)  throws Exception 
	{
		File file = new File("kohaIBICT2/publicacoesDivulgacao.tsv");
		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<Record> periodicos = new ArrayList<Record>();
		String currentLine;
		
		
		
		// Creates a FileOutputStream
		FileOutputStream file2 = new FileOutputStream("kohaIBICT/000001-9-corrigido.txt");
		// Creates an OutputStreamWriter
		OutputStreamWriter output = new OutputStreamWriter(file2);
		
		// it is false so the previous content is cleared. "true" to keep the content.
//		BufferedWriter linhasCorrigidas = new BufferedWriter(new FileWriter("kohaIBICT/000001-9-corrigido.TIT", false));
		
		// mede a quantidade de linhas e junta as linhas 
		currentLine = br.readLine();
		output.write(currentLine);
		while (((currentLine = br.readLine()) != null) && !currentLine.contentEquals(""))
		{
			if(!currentLine.startsWith("!"))
			{
				output.write(currentLine);
				continue;
			}
			
			output.write("\n");
			output.write(currentLine);
		}
		
	    // Closes the writer
		output.close();
		br.close();
		
		file = new File("kohaIBICT/000001-9-corrigido.txt");
		br = new BufferedReader(new FileReader(file));
		Record currentRecord = null;
		ArrayList<Field> fields = null;
		
		// percorre as linhas do arquivo e cria os registros relacionados,
		// separando por campo
		while ((currentLine = br.readLine()) != null)
		{
			// se tem REC-ID, indica o comeco de um novo registro
			// assim, instancia um novo record e adiciona na lista
			// de registros
			if(currentLine.contains("!REC-ID"))
			{
				fields = new ArrayList<Field>();
				currentRecord = new Record(fields);
				// temporario para criacao de leader
				currentRecord.generateLeaderAnd005Field();
				periodicos.add(currentRecord);
				continue;
			}
			// verifica o caso correspondente a linha atual e insere no registro,
			// ou atualiza um campo ja existente
			insertFieldInRecord(currentRecord, currentLine); 
		}
		
		OutputStreamWriter issnNaoEncontrado = new OutputStreamWriter(
				new FileOutputStream("kohaIBICT/issnNaoEncontrado.csv"));

		// leitura dos registros da planilha da Alda
		ArrayList<String[]> csvAlda = CSVController.readCSV("kohaIBICT/planilhaAlda.tsv", "\t");
		ArrayList<String[]> csvMaria = CSVController.readCSV("kohaIBICT/planilhaMariaJoseReduzidaLimpa.csv", "\t");
		ArrayList<String[]> csvISSNnaoEncontrado = new ArrayList<String[]>(); 
		
		// criado para juntar as informa��es contidas nos dois arquivos
		ArrayList<String[]> csvLinhas = new ArrayList<String[]>();
		for(int i = 1; i < csvAlda.size(); i++)
			csvLinhas.add(csvAlda.get(i));
		for(int i = 1; i < csvMaria.size(); i++)
			csvLinhas.add(csvMaria.get(i));
		
		// adiciona o cabe�alho
		csvISSNnaoEncontrado.add(csvAlda.get(0));
		
		// monta os registros de exemplares, baseados nos arquivos csv
		for(int i = 1; i < csvLinhas.size(); i++)
		{	
			Record registroEncontrado = null;
			String [] linha = csvLinhas.get(i);
			String issn = linha.length > 8 ? linha[8] : null;
			
			registroEncontrado = issn != null ? buscaISSN(periodicos, issn) : null;
			
			// se registro nao foi encontrado, salvar em outra lista
			if(registroEncontrado == null)
			{
				csvISSNnaoEncontrado.add(linha);
				numNaoEncontrado++;
			}else // adiciona o campo ao registro
			{
				if(i < csvAlda.size() - 1)
				{
					numAlda++;
				}else
				{
					numMaria++;
				}
				
				preencheRegistroComDadosExemplares(registroEncontrado, linha);
			}
		}
		
		// faz o tratamento do arquivo de exemplares e substitui em
		//'periodicos' aqueles registros encontrados no arquivo de
		// exporta��o do ccn
		substituiExemplaresPeloCorrespondenteCCN(periodicos);
		
		System.out.println(numAutor + " autores adicionados.");
		System.out.println(numEditoras + " editoras adicionadas.");
		System.out.println(exemplares + " exemplares adicionados.");
		System.out.println(numAlda + " exemplares da alda.");
		System.out.println(numMaria + " exemplares da maria.");
				
		// criacao de arquivo com erros
		for(String[] linha : csvISSNnaoEncontrado)
		{
			for (String coluna : linha)
				issnNaoEncontrado.write(coluna + "\t");
		issnNaoEncontrado.write("\n");
		}
		
		issnNaoEncontrado.close();
		
		
		ArrayList<Record> novosRegistros = new ArrayList<Record>();
		ArrayList<String[]> linhasSemISSN = new ArrayList<String[]>();
		
		linhasSemISSN.add(csvAlda.get(0));// adiciona cabecalho
		
		// criacao de registros novos a partir dos arquivos com erros
		for(int l = 1; l < csvISSNnaoEncontrado.size(); l++)
		{
			String[] linha = csvISSNnaoEncontrado.get(l);
			String issn = linha.length > 8 ? linha[8] : null;
			
			if(issn == null || issn.isEmpty())
			{
				linhasSemISSN.add(linha);
			}else
			{
				// cria��o do novo registro
				Record novoRegistro = buscaISSN(novosRegistros, issn); 
				
				if(novoRegistro == null)
				{
					novoRegistro = new Record(new ArrayList<Field>());
					novosRegistros.add(novoRegistro);
					novoRegistro.generateLeaderAnd005Field();
				}				
				
				// preenchimento dos dados do registro
				preencheRegistroComDadosExemplares(novoRegistro, linha);
			}
		}
		
		// criacao dos registros novos para os arquivos sem issn
		ArrayList<Record> novosRegistrosSemISSN = new ArrayList<Record>();
		ArrayList<String[]> linhasSemTitulo = new ArrayList<String[]>();
		linhasSemTitulo.add(csvAlda.get(0));// adiciona cabecalho
		
		for(int l = 1; l < linhasSemISSN.size(); l++)
		{
			String[] linha = linhasSemISSN.get(l);
			String titulo = linha.length > 2 ? linha[2] : null;
			
			if(titulo == null || titulo.isEmpty())
			{
				linhasSemTitulo.add(linha);
			}else
			{
				// cria��o do novo registro
				Record novoRegistro = buscaTitulo(periodicos, titulo);
				if(novoRegistro == null)
					novoRegistro = buscaTitulo(novosRegistrosSemISSN, titulo);
			
				if(novoRegistro == null)
				{
					// cria��o do novo registro
					novoRegistro = new Record(new ArrayList<Field>());
					novosRegistrosSemISSN.add(novoRegistro);
					novoRegistro.generateLeaderAnd005Field();
				}
	
				// preenchimento dos dados do registro
				preencheRegistroComDadosExemplares(novoRegistro, linha);
			}
		
		}
		
		System.out.println(numNaoEncontrado + " ISSNs nao encontrados");
		System.out.println(linhasSemISSN.size() + " linhas sem ISSN");

		br.close();
		
		System.out.println(linhasSemTitulo.size() + " linhas de exemplares sem titulo");
		
		// relatorio para indicar quais registros estao nas tabelas e nao estao
		// nos registros do CCN
		ArrayList<Record> registrosNaoCCN = new ArrayList<>();
		registrosNaoCCN.addAll(novosRegistros);
		registrosNaoCCN.addAll(novosRegistrosSemISSN);
		
		OutputStreamWriter writer;

		String filePath = "kohaIBICT/registrosNaoEncontradosCCN.csv";
		
		// Comandos usados para abrir o arquivo e limpar qualquer conteudo antigo.
		PrintWriter writer2 = new PrintWriter(filePath);
		writer2.print("");
		writer2.close();
					
		writer = new OutputStreamWriter(new FileOutputStream(filePath), StandardCharsets.UTF_8);
		writer.append("titulo;autor;issn;\n");
		for(Record registro : registrosNaoCCN)
		{
			// adiciona autor
			if(registro.hasAnyOccurrenceOfFieldAndSubfield("245", "a"))
				writer.append(registro.getField("245").get(0)
						.getSubfield("a").get(0).getValue());
			writer.append(";");
			if(registro.hasAnyOccurrenceOfFieldAndSubfield("100", "a"))
				writer.append(registro.getField("100").get(0)
						.getSubfield("a").get(0).getValue());
			writer.append(";");
			if(registro.hasAnyOccurrenceOfFieldAndSubfield("022", "a"))
				writer.append(registro.getField("022").get(0)
						.getSubfield("a").get(0).getValue());
			writer.append(";");
			writer.append("\n");
		}
		
		writer.close();
		
		System.out.println(periodicos.size() + " periodicos CCN criados");
		XMLHandler.generateXMLFile(periodicos, "kohaIBICT/periodicosCCN.xml");
		
		System.out.println(novosRegistros.size() + " novos periodicos CCN criados");
		XMLHandler.generateXMLFile(novosRegistros, "kohaIBICT/novosPeriodicosCCN.xml");
		
		System.out.println(novosRegistrosSemISSN.size() + " novos periodicos sem ISSN criados");
		XMLHandler.generateXMLFile(novosRegistrosSemISSN, "kohaIBICT/novosPeriodicosCCNsemISSN.xml");
		
	}
	
	/**
	 * Substitui os exemplares pelos existentes no CCN.
	 * 
	 * Esse m�todo percorre toda a lista 'periodicos' e substitui
	 * os exemplares pela correpondencia encontrada no arquivo de exemplares
	 * do CCN.
	 * 
	 * @param periodicos
	 */
	private static void substituiExemplaresPeloCorrespondenteCCN(ArrayList<Record> periodicos) {
		
		HashMap<String, String> exemplaresCCN;
		try {
			exemplaresCCN = getExemplaresFromFile("kohaIBICT/000001-9.col");
		
			String codigoCCN;
			for(Record periodico : periodicos)
			{
				if(periodico.hasField("090"))
				{	
					codigoCCN = periodico.getField("090").get(0).
							getSubfield("a").get(0).getValue();
					if(exemplaresCCN.containsKey(codigoCCN))
					{
						substituiExemplaresDePeriodico(periodico, exemplaresCCN.get(codigoCCN));
					}
				}
			}
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

	/**
	 * Subistitui exemplares pelos encontrados no arquivo do ccn.
	 * 
	 * Os subcampos $x = CCN e $y = 16 devem ser mantidos.
	 * @param periodico
	 * @param exemplares
	 */
	private static void substituiExemplaresDePeriodico(Record periodico, String stringExemplares) 
	{
		periodico.removeAllOccurrencesOfField("952");
		
		String[] exemplares = stringExemplares.split(";");
		
		String exemplarFormatado;
		for(String exemplar : exemplares)
		{
			exemplarFormatado = exemplar.trim();
			if(exemplarFormatado.contains("(") 
					&& Character.isDigit(exemplarFormatado.charAt(0)))
			{
				exemplarFormatado = exemplarFormatado.replace(" ", ", v. ");
				exemplarFormatado = exemplarFormatado.replace("(", ", n. ");
				exemplarFormatado = exemplarFormatado.replace(")", "");
				exemplarFormatado = exemplarFormatado.replace(",,", ",");
			}
				
			Field novoExemplar = periodico.createFieldAndSubfield("952", "h", exemplarFormatado);
			novoExemplar.addSubfield(new Subfield("x", "CCN"));
			novoExemplar.addSubfield(new Subfield("y", "16"));
		}
		
		
	}

	/**
	 * Le o conteudo do arquivo de exemplares do CCN e retorna um hashmap,
	 * que contem como chave o identificador ccn (campo 090 $a) e o conteudo
	 * a lista de exemplares.
	 * @param string
	 * @return
	 * @throws IOException 
	 */
	private static HashMap<String, String> getExemplaresFromFile(String pathArquivo) throws IOException {
		File file = new File(pathArquivo);
		BufferedReader br = new BufferedReader(new FileReader(file));
		HashMap<String, String> exemplares = new HashMap<>();
		
		String linha;
		while ((linha = br.readLine()) != null)
		{
			String codigoCCN, listaExemplares;
			if(linha.contains("CODIGO CCN"))
			{
				codigoCCN = linha.split(":")[1];
				
				while(!linha.contains("COLECAO.....:"))
					linha = br.readLine();
				
				listaExemplares = linha.split(":")[1];
				
				
				if(!exemplares.containsKey(codigoCCN))
				{
					exemplares.put(codigoCCN, listaExemplares);
				}else
				{
					exemplares.put(codigoCCN,
							exemplares.get(codigoCCN) + ";" + listaExemplares);
				}
			}
		}
		return exemplares;
	}

	private static void preencheRegistroComDadosExemplares(Record registroEncontrado, String[] linha) {
		
		String autor = linha.length > 1 ? linha[1] : null;
		String titulo = linha.length > 2 ? linha[2] : null;
		String editora = linha.length > 3 ? linha[3] : null;
		String volume = linha.length > 4 ? linha[4] : null;
		String numero = linha.length > 5 ? linha[5] : null;
		String ano = linha.length > 6 ? linha[6] : null;
		String parte = linha.length > 7 ? linha[7] : null;
		String issn = linha.length > 8 ? linha[8] : null;
		String exemplar = linha.length > 9 ? linha[9] : null;
		String tomboBarCode = linha.length > 10 ? linha[10] : null;
		String ccn = linha.length > 11 ? linha[11] : null;
		
		// adicionar caracteres n. (para numero) e v. (para volume) quando nao houver
		if((numero != null) && (!numero.toLowerCase().contains("n")))
			numero = "n. " + numero;
		if((volume != null) && (!volume.toLowerCase().contains("v")))
			volume = "v. " + volume;
		
		String volNumParte = ano + ", " + volume +  
				", " + numero;
		
		if(parte != null && !parte.isBlank() && !parte.isEmpty())
			volNumParte += ", parte " + parte;
		
		// adiciona issn (se nao tiver)
		if(issn != null)
			if(!registroEncontrado.hasAnyOccurrenceOfFieldAndSubfield("022", "a")
					&& !issn.isEmpty())
			{
				registroEncontrado.createFieldAndSubfield("022", "a", issn);
				numAutor++;
			}
		
		// adiciona titulo (se nao tiver)
		if(titulo != null)
			if(!registroEncontrado.hasAnyOccurrenceOfFieldAndSubfield("245", "a")
					&& !titulo.isEmpty())
			{
				registroEncontrado.createFieldAndSubfield("245", "a", titulo);
			}
		
		// adiciona autor (se nao tiver)
		if(autor != null)
			if(!registroEncontrado.hasAnyOccurrenceOfFieldAndSubfield("100", "a")
					&& !autor.isEmpty())
			{
				registroEncontrado.createFieldAndSubfield("100", "a", autor);
				numAutor++;
			}
		
		// adiciona editora, se nao tiver
		if(editora != null)
			if(!registroEncontrado.hasAnyOccurrenceOfFieldAndSubfield("260", "b")
					&& !editora.isBlank())
			{
				registroEncontrado.createFieldAndSubfield("260", "b", editora);
				numEditoras++;					
			}
		
		// adiciona informacoes do tipo de material (periodico)
		if(!registroEncontrado.hasAnyOccurrenceOfFieldAndSubfield("942", "c"))
				registroEncontrado.createFieldAndSubfield("942", "c", "16");
		
		// criacao dos campo 952 correspondente ao exemplar atual
		Field campo952 = 
				registroEncontrado.createFieldAndSubfield("952", "h", volNumParte);
		exemplares++;
		// adiciona os dados do exemplar
		
		if(exemplar != null)
			campo952.createSubfield("t", exemplar);
		if(tomboBarCode != null && !tomboBarCode.isEmpty())
			campo952.createSubfield("p", tomboBarCode);
		if(ccn != null)
			campo952.createSubfield("x", ccn);
		
		campo952.createSubfield("y", "16");
		
	}

	/**
	 * Retorna o registro correspondente ao issn procurado.
	 * 
	 * 
	 * Percorre a lista de registros 'records' e retorna 
	 * aquele registro que tem valor indicado por 'issn' 
	 * no seu campo 022 $a.
	 * @param records lista de registros
	 * @param issn valor buscado no campo 022
	 * @return
	 */
	private static Record buscaISSN(ArrayList<Record> records, String issn) 
	{
		Record result = null;
		
		// percorre a lista de registros em busca do campo 022.
		for(Record record : records)
		{
			if(!record.hasField("022"))
				continue;
			
			if(record.getField("022").get(0).getSubfield("a").get(0).getValue().contentEquals(issn))
			{
				result = record;
				break;
			}
		}
		return result;
	}
	
	/**
	 * Retorna o registro que tem valor indicado por 'titulo' no campo 245.
	 * @param records lista de registros
	 * @param registro com titulo correspondente
	 * @return
	 */
	private static Record buscaTitulo(ArrayList<Record> records, String titulo) 
	{
		Record result = null;
		
		// percorre a lista de registros em busca do campo 022.
		for(Record record : records)
		{
			if(!record.hasField("245"))
				continue;
			
			if(record.getField("245").get(0).getSubfield("a").get(0).getValue().contentEquals(titulo))
			{
				result = record;
				break;
			}
		}
		return result;
	}

	/**
	 * 
	 * @param record
	 * @param line
	 */
	private static void insertFieldInRecord(Record record, String line) {
		
		String code = line.split("!")[1],
				value = line.split("!")[2];
		// TODO adicionar excecao para campo 001
		
		// get String equals to 'field!!!subfield', which corresponds
		// to the imput 'code'.
		String correspondentName = getCodeCorrespondence(code);
		
		if(correspondentName == null)
		{
			return;
		}
		
		String fieldName = correspondentName.split("!!!")[0], 
				subfieldName = correspondentName.split("!!!")[1];
		String fieldDenomination = null;
		ArrayList<Subfield> subcampos = null;
		// separa registros de controlfield e datafield
		if(fieldName.contentEquals("001") || fieldName.contentEquals("008"))
		{
			fieldDenomination = Constant.controlFieldDenomination;
			switch(fieldName)
			{
			case "001":
				record.addField(new Field(fieldName, value, fieldDenomination));
				break;
			case "008":
				if(!record.hasField("008"))
				{
					String empty40char = "                                        ";
					record.addField(new Field(fieldName, empty40char, fieldDenomination));
				}
				record.getField("008").get(0).setValue(
						editField008value(value, subfieldName, record.getField("008").get(0).getValue()));
				
				
			}
		}else
		{
			fieldDenomination = Constant.dataFieldDenomination;
			// instancia o subcampo
			Subfield subcampo = new Subfield(subfieldName, value, Constant.subfieldDenomination);
			
			if(record.hasField(fieldName) && !record.getField(fieldName).get(0).hasSubfield(subfieldName))
			{
				record.getField(fieldName).get(0).addSubfield(subcampo);
			}else
			{
				subcampos = new ArrayList<Subfield>();
				subcampos.add(subcampo);
				record.addField(new Field(fieldName, "", "", subcampos, fieldDenomination));
			}
		}
	}
	
	/**
	 * Edit the current value of field 008
	 * @param value
	 * @param subfieldName
	 * @param value2
	 * @return
	 */
	private static String editField008value(String valueToInsert, String charRange, String toBeEdited) {
		
		int begin = Integer.parseInt(charRange.split("-")[0]);
		
		toBeEdited = toBeEdited.substring(0, begin) + valueToInsert 
				+ toBeEdited.substring(begin + valueToInsert.length(), toBeEdited.length());
		
		return toBeEdited;
	}

	/**
	 * Recebe o codigo e retorna o campo e subcampo correspondente, separado
	 * por '||||', de acordo com a tabela apresentada pela equipe do CCN.
	 * @param code
	 * @return
	 */
	private static String getCodeCorrespondence(String code)
	{	
		String fieldName = null, subfieldName = null;
		
		switch(code)
		{
		case "S020":
			fieldName = "090";
			subfieldName = "a";
			break;
		case "S070":
			fieldName = "090";
			subfieldName = "c";
			break;
		case "S090":
			fieldName = "090";
			subfieldName = "d";
			break;
		case "S100":
			fieldName = "008";
			subfieldName = "07-10";
			break;
		case "S110":
			fieldName = "008";
			subfieldName = "11-14";
			break;
		case "S120":
			fieldName = "008";
			subfieldName = "15-17";
			break;
		case "S130":
			fieldName = "008";
			subfieldName = "18";
			break;
		case "S140":
			fieldName = "008";
			subfieldName = "21";
			break;
		case "S160":
			fieldName = "008";
			subfieldName = "35-37";
			break;
		case "S180":
			fieldName = "008";
			subfieldName = "24";
			break;
		case "S200":
			fieldName = "245";
			subfieldName = "a";
			break;
		case "S210":
			fieldName = "245";
			subfieldName = "f";
			break;
		case "S220":
			fieldName = "245";
			subfieldName = "c";
			break;
		case "S230":
			fieldName = "245";
			subfieldName = "b";
			break;
		case "S240":
			fieldName = "245";
			subfieldName = "n";
			break;
		case "S250":
			fieldName = "245";
			subfieldName = "p";
			break;
		case "S280":
			fieldName = "210";
			subfieldName = "a";
			break;
		case "S310":
			fieldName = "246";
			subfieldName = "0";
			break;
		case "S320":
			fieldName = "246";
			subfieldName = "a";
			break;
		case "S330":
			fieldName = "246";
			subfieldName = "3";
			break;
		case "S340":
			fieldName = "246";
			subfieldName = "3";
			break;
		case "S411":
			fieldName = "260";
			subfieldName = "a";
			break;
		case "S412":
			fieldName = "260";
			subfieldName = "b";
			break;
		case "S420":
			fieldName = "362";
			subfieldName = "a";
			break;
		case "S440":
			fieldName = "022";
			subfieldName = "a";
			break;
		case "S520":
			fieldName = "550";
			subfieldName = "a";
			break;
		case "S532":
			fieldName = "664";
			subfieldName = "b";
			break;
		case "S533":
			fieldName = "650";
			subfieldName = "a";
			break;
		case "S610":
			fieldName = "759";
			subfieldName = "t";
			break;
		case "S620":
			fieldName = "769";
			subfieldName = "t";
			break;
		case "S710":
			fieldName = "780";
			subfieldName = "0";
			break;
		case "S730":
			fieldName = "780";
			subfieldName = "4";
			break;
		case "S740":
			fieldName = "780";
			subfieldName = "5";
			break;
		case "S750":
			fieldName = "780";
			subfieldName = "6";
			break;
		case "S810":
			fieldName = "785";
			subfieldName = "0";
			break;
		case "S830":
			fieldName = "785";
			subfieldName = "4";
			break;
		case "S840":
			fieldName = "785";
			subfieldName = "5";
			break;
		case "S850":
			fieldName = "785";
			subfieldName = "6";
			break;
		case "S860":
			fieldName = "785";
			subfieldName = "7";
			break;
		case "S870":
			fieldName = "785";
			subfieldName = "8";
			break;
		default:
			System.err.println("Codigo de campo \"" + code + "\" nao encontrado!!!");
//			break;
			return null;
				
		}
		
		
		return fieldName + "!!!" + subfieldName;
	}
}
