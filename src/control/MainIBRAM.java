package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class MainIBRAM {
	
	/**
	 * Main para a criação de exemplares do Koha BDB
	 * @param args
	 * @throws IOException
	 * @throws TransformerException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, TransformerException {
		
		
		MARC marcIBRAM = new MARC("IBRAM/acervo01.xml");
		
		System.out.println(marcIBRAM.getRecords().size() + " registros no acervo.");
		XMLHandler.generateXMLFile(marcIBRAM, "IBRAM/acervo02.xml");
		HashMap<String, Integer> branches = new HashMap<>();
		
		for (Record record : marcIBRAM.getRecords())
		{
			if(record.hasField("952"))
				for(Field field : record.getField("952"))
				{
					if(field.hasSubfield("a"))
					{
						branches.put(field.getSubfield("a").get(0).getValue(), 0);
					}
						
				}
		}
		
		for (String branch : branches.keySet())
		{
			System.out.println(branch);
		}
	}
	
	
	
	
}
