package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

import entity.Constant;
import entity.Field;
import entity.Record;
import entity.Subfield;

public class MainIBICT5 {
	
	/**
	 * atualiza o link de download nos registros com link.
	 * @param args
	 * @throws Exception
	 */
	static int numAutor = 0, numEditoras = 0, exemplares = 0, 
			numNaoEncontrado = 0, numAlda = 0, numMaria = 0;
	
	public static void main(String[] args) throws Exception {
		
		ArrayList<String[]> pubTemp, pubs = CSVController.readCSV("kohaIBICT2/divulgacao/publicacoes01.tsv", "\t");
		
		for(int i = 2; i <= 17; i++)
		{
			pubTemp = CSVController.readCSV("kohaIBICT2/divulgacao/publicacoes" + (i < 10 ? "0" : "") + i + ".tsv", "\t");
			pubs.addAll(pubTemp.subList(1, pubTemp.size()));
		}
		
		/**
		 *  aqui os registros s�o criados, unicos para cada titulo encontrado na tabela
		 * correspondencia de campos de cada linha dos exemplares:
		 * [0] = 022 $a
		 * [1] = 022 $a (nenhuma linha da tabela tem)
		 * [2] = 008/35-37
		 * [3] = 245 $a
		 * [4] = -
		 * [5] = 245 $c
		 * [6] = 246 $a
		 * [7] = 490 $a
		 * [8] = 770 $t
		 * [9] = 110 $a
		 * [10] = 210 $a
		 * [11] = 246 $a
		 * [12] = 222 $a
		 * [13] = 008/18
		 * [14] = 260 $b
		 * [15] = 260 $a
		 * [16] = 362 $a
		 * [17] = 362 $a
		 * [18] = 362 $a
		 * [19] = 650 $a
		 * [20] = 780 $a e ISSN no $x
		 * [21] = 785 $a e ISSN entra no X
		 * [22] = 952 $h
		 * [23] = 952 $h
		 * [24] = 952 $h
		 * [25] = -
		 */
		String[] exemplar;
		
		String issn, titulo;
		Record record = null;
		ArrayList<Field> campos;
		HashMap<String, Record> records = new HashMap<>();
		for (int r = 1; r < pubs.size(); r++)
		{
			exemplar = pubs.get(r);
			titulo = exemplar[3];
			issn = exemplar[0];
			
			if(!records.containsKey(issn + "---" + titulo))
			{
				campos = new ArrayList<>();
				record = new Record(campos);
				record.generateLeaderAnd005Field();
				records.put(issn + "---" + titulo, record);
				
				// adiciona o campo 008
				String campo008 = new String(new char[40]).replace('\0', ' ');
				record.addField(new Field("008", campo008, Constant.controlFieldDenomination));
				
				// adicao de campos unicos
				if(!exemplar[0].isEmpty() && !exemplar[0].isBlank())
					campos.add(new Field("022", "a", exemplar[0], Constant.dataFieldDenomination));
				
				String campo008Edited;
				if(!exemplar[2].isEmpty() && !exemplar[2].isBlank())
				{
					campo008 = record.getField("008").get(0).getValue();
					campo008Edited = campo008.substring(0, 35) + "por" 
							+ campo008.substring(38);
					record.getField("008").get(0).setValue(campo008Edited);
				}
				
				if(!exemplar[13].isEmpty() && !exemplar[13].isBlank())
				{
					campo008 = record.getField("008").get(0).getValue();
					campo008Edited = campo008.substring(0, 18) + "m" 
							+ campo008.substring(19);
					record.getField("008").get(0).setValue(campo008Edited);
				}

				if(!exemplar[3].isEmpty() && !exemplar[3].isBlank())
				{
					Field campo245 = new Field("245", "a", exemplar[3], Constant.dataFieldDenomination);
					if(!exemplar[5].isEmpty() && !exemplar[5].isBlank())
						campo245.addSubfield(new Subfield("c", exemplar[5]));
					campos.add(campo245);
				}
				if(!exemplar[7].isEmpty() && !exemplar[7].isBlank())
					campos.add(new Field("490", "a", exemplar[7], Constant.dataFieldDenomination));		 
				if(!exemplar[8].isEmpty() && !exemplar[8].isBlank())
					campos.add(new Field("770", "t", exemplar[8], Constant.dataFieldDenomination));
				if(!exemplar[9].isEmpty() && !exemplar[9].isBlank())
					campos.add(new Field("110", "a", exemplar[9], Constant.dataFieldDenomination));
				if(!exemplar[10].isEmpty() && !exemplar[10].isBlank())
					campos.add(new Field("210", "a", exemplar[10], Constant.dataFieldDenomination));
				if(!exemplar[11].isEmpty() && !exemplar[11].isBlank())
					campos.add(new Field("246", "a", exemplar[11], Constant.dataFieldDenomination));
				if(!exemplar[12].isEmpty() && !exemplar[12].isBlank())
					campos.add(new Field("222", "a", exemplar[12], Constant.dataFieldDenomination));
				
				Field campo260 = new Field("260", Constant.dataFieldDenomination);
				if(!exemplar[14].isEmpty() && !exemplar[14].isBlank())
				{
					Subfield subB = new Subfield("b", exemplar[14]);
					campo260.addSubfield(subB);
					campos.add(campo260);
				}
				
				if(!exemplar[15].isEmpty() && !exemplar[15].isBlank())
				{
					Subfield subA = new Subfield("a", exemplar[15]);
					campo260.addSubfield(subA);
					if(!record.hasField("260"))
					{
						campos.add(campo260);
					}
				}
				
				if(!exemplar[16].isEmpty() && !exemplar[16].isBlank())
					campos.add(new Field("362", "a", exemplar[16], Constant.dataFieldDenomination));

				if((!exemplar[17].isEmpty() && !exemplar[17].isBlank()) 
						|| (!exemplar[18].isEmpty() && !exemplar[18].isBlank()) )
					campos.add(new Field("362", "a", exemplar[17] + "-" + exemplar[18], Constant.dataFieldDenomination));
				
				if(!exemplar[19].isEmpty() && !exemplar[19].isBlank())
					campos.add(new Field("650", "a", exemplar[19], Constant.dataFieldDenomination));
				
				if(!exemplar[20].isEmpty() && !exemplar[20].isBlank())
				{
					String[] campo780 = exemplar[20].split(Pattern.quote("\\x"));
					ArrayList<Subfield> subcampos = new ArrayList<>();
					Subfield subT = new Subfield("t", campo780[0].substring(2).trim());
					subcampos.add(subT);
					Subfield subX = new Subfield("x", campo780[1].split(Pattern.quote("ISSN"))[1].trim());
					subcampos.add(subX);
					campos.add(new Field("780", "", "", subcampos, Constant.dataFieldDenomination));
				}
				
				if(!exemplar[21].isEmpty() && !exemplar[21].isBlank())
				{
					String[] campo785 = exemplar[21].split(Pattern.quote("\\x"));
					ArrayList<Subfield> subcampos = new ArrayList<>();
					Subfield subT = new Subfield("t", campo785[0].substring(2).trim());
					subcampos.add(subT);
					Subfield subX;
					if(campo785[1].contains("ISSN"))
						subX = new Subfield("x", campo785[1].split(Pattern.quote("ISSN"))[1].trim());
					else
						subX = new Subfield("x", campo785[1].trim());
					subcampos.add(subX);
					campos.add(new Field("785", "", "", subcampos, Constant.dataFieldDenomination));
				}
				record.addField(new Field("599", "a", "divulgacao", Constant.dataFieldDenomination));
				record.addField(new Field("942", "c", "16", Constant.dataFieldDenomination));
			}
			
			// adicao de campos que repetem
			record = records.get(issn + "---" + titulo);
			
			// o formato a ser salvo no campo 952$h � AAA, v. X, n. X 
			String campo952h = "";
			if(!exemplar[22].isEmpty() && !exemplar[22].isBlank())
				campo952h = exemplar[22] + ", ";
			if(!exemplar[23].isEmpty() && !exemplar[23].isBlank())
				campo952h += exemplar[23] + ", ";
			if(!exemplar[24].isEmpty() && !exemplar[24].isBlank())
				campo952h += "n. " + exemplar[24];
			
			Field campo952 = new Field("952", "h", campo952h, Constant.dataFieldDenomination);
			Subfield subfieldy = new Subfield("y", "16", Constant.subfieldDenomination);
			campo952.addSubfield(subfieldy);
			record.addField(campo952);
		}
		
		ArrayList<Record> rec = new ArrayList<>();
		for(String r : records.keySet())
		{
			rec.add(records.get(r));
		}
		
		CSVController.writeCSV("kohaIBICT2/divulgacao/publicacoes.tsv", pubs, "\t");
		System.out.println("");
		
		System.out.println(records.size() + " periodicos CCN criados");
		XMLHandler.generateXMLFile(rec, "kohaIBICT2/periodicosCCN.xml");
	}
 	
}
