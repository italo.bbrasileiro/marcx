package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class MainIBICT7 
{
	static String yearRegex = "[0-9]{4}([/][0-9]{4})*"; // forma 1993 ou 1992/1993
	static String volumeRegex = "[0-9]+([/][0-9]+)*[(]"; // forma 10 ou 10/11, indica que eh seguido de (
	static String numberRegex = "[(][0-9]+([-|/|,][0-9]+)*[)]";
	static String editionWithWordsRegex = "[(][a-z]+([-|/|,][a-z]+)*[)]";
	static String wordsOrNumbersRegex = "([a-z]+|[0-9]+)+";
	static String wordsAndNumbersRegex = "([0-9]+([ ][a-z]+)*)+";
	static String editionWithWordsOrNumbersRegex = "[(]" + wordsOrNumbersRegex + "([-|/|,]" + wordsOrNumbersRegex + ")*[)]";
	static String editionWithWordsAndNumbersRegex = "[(]" + wordsAndNumbersRegex + "([-|/|,]" + wordsAndNumbersRegex + ")*[)]";
	static String wordOrNumberVolumeRegex = "[0-9]+" + numberRegex + "([,][ ]([0-9]+" + numberRegex + "|[a-z])+)+";
	static String volumeWithNumOrWords = "([0-9]+|[a-z]+)(([ |-]([0-9]+|[a-z]+))+)*";
	
	static ArrayList<Record> records = new ArrayList<>();
	static HashMap<String, Record> recordsOnKoha;
	
	public static void main(String[] args) throws Exception 
	{
		
		BufferedReader br = limpaEInstanciaArquivoDeAcervo();
		
		criaRegistrosDoArquivoDeAcervo(br);
		br = limpaEInstanciaArquivoDeColecao("kohaIBICT3/CCN/arquivosIniciais/000001-9 (cole��es 2022).txt", 
				"kohaIBICT3/CCN/colecaoLimpa.txt");
		criaInformacoesDeColecoes(br);
		adicionaSubcampoYemTodosExemplares();
		
		criaArquivoXML(records, "kohaIBICT3/CCN/registrosCCN.xml");
		
		
		// correcao: gerar marc de registros que nao tiveram exemplares migrados
		corrigeRegistrosCCNSemExemplares();
		
		
	
	}
	
	
	private static void corrigeRegistrosCCNSemExemplares() throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		ArrayList<Record> recordsCorrigidos = new ArrayList<>();
		MARC recordsKoha = new MARC("kohaIBICT3/CCN/correcaoExemplares/bibliotecaIBICT12-04-22.xml");
		String codeCCN;
		Record recordOnKoha;
		int contador = 0;
		
		recordsOnKoha = instanciaRegistrosDoKoha(recordsKoha.getRecords());
		
		for(Record record : records)
		{
			codeCCN = record.getField("090").get(0).getSubfield("a").get(0).getValue();
			recordOnKoha = recordsOnKoha.get(codeCCN);
			
			if(recordOnKoha == null || record.getField("952") == null)
				continue;
			
			if(!recordOnKoha.hasField("952"))
			{
				recordOnKoha.addFields(record.getField("952"));
				recordsCorrigidos.add(recordOnKoha);
			}
			
			if(contador++ % 100 == 0)
				System.out.println(contador + "/" + records.size());
			
		}
		
		
		
		criaArquivoXML(recordsCorrigidos, "kohaIBICT3/CCN/exemplaresCorrigidosCCN.xml");
		
	}

	private static HashMap<String, Record> instanciaRegistrosDoKoha(ArrayList<Record> records) 
	{
		HashMap<String, Record> recordsWithCode = new HashMap<>();
		String code;
		
		for(Record record : records)
		{
			if(!record.hasAnyOccurrenceOfFieldAndSubfield("090", "a"))
				continue;
			
			code = record.getField("090").get(0).getSubfield("a").get(0).getValue();
			
			if(code.isEmpty())
				continue;
			
			if(recordsWithCode.containsKey(code))
				System.err.println("Codigo CCN " + code + " em registros duplicados!!!");
			recordsWithCode.put(code, record);
		}
		
		return recordsWithCode;
	}


	private static Record getCorrespondingRecord(ArrayList<Record> records, String codeCCN) 
	{
		String code;
		
		for(Record record : records)
		{
			if(!record.hasAnyOccurrenceOfFieldAndSubfield("090", "a"))
				continue;
			
			code = record.getField("090").get(0).getSubfield("a").get(0).getValue();
			
			if(code.contentEquals(codeCCN))
				return record;
		}
		return null;
	}


	private static void criaArquivoXML(ArrayList<Record> records, String outputFile) {
		System.out.println(records.size() + " periodicos criados em " + outputFile);
		XMLHandler.generateXMLFile(records, outputFile);
	}

	private static void adicionaSubcampoYemTodosExemplares() {
		for(Record r : records)
		{
			if(r.hasField("952"))
				for(Field field952 : r.getField("952"))
					field952.addSubfield(new Subfield("y", "16"));
		}
	}


	private static void criaInformacoesDeColecoes(BufferedReader br) throws IOException {
		// o indicador da colecao esta no campo 90 $a do registro e no
		// campo !C020! do arquivo do CCN
		String[] CCNfield, editionField;
		Record record = null;
		String currentLine;
		while ((currentLine = br.readLine()) != null)
		{
			if(currentLine.contains("!REC-ID"))
				record = null;
			
			if(currentLine.contains("!C020!"))
			{
				CCNfield = currentLine.split("!");
				record = getCorrespondentRecord(records, CCNfield[2].trim());
			
				if(record == null)
					System.err.println("Registro sem valor " + CCNfield[2]
							+ " no campo 090 $a");
			}
			
			// campo que contem as informacoes de exemplares
			if(currentLine.contains("!C030!"))
			{
				CCNfield = currentLine.split("!");
				editionField = CCNfield[2].split(";");
				
				addItemToRecord(record, editionField);
			}
		}
	}

	private static BufferedReader limpaEInstanciaArquivoDeColecao(String inputPath, String outputPath) throws IOException {
		File file = new File(inputPath);
		BufferedReader br = new BufferedReader(new FileReader(file));
		corrigeLinhasQuebradas(br, outputPath);
		
		// 	reabre o novo arquivo criado, para gerar
		// os registros em formato marcxml
		file = new File(outputPath);
		return new BufferedReader(new FileReader(file));
	}


	private static void criaRegistrosDoArquivoDeAcervo(BufferedReader br) throws IOException 
	{
		String currentLine;
		Record record = null;
		
		while ((currentLine = br.readLine()) != null)
		{
			// se tem REC-ID, indica o comeco de um novo registro
			// assim, instancia um novo record e adiciona na lista
			// de registros
			if(currentLine.contains("!REC-ID"))
			{
				record = new Record(new ArrayList<Field>());
				// temporario para criacao de leader
				record.generateLeaderAnd005Field();
				record.addField(new Field("942", "c", "16", Constant.dataFieldDenomination));
				records.add(record);
			}else
			{
				// verifica o caso correspondente a linha atual e insere no registro,
				// ou atualiza um campo ja existente
				insertFieldInRecord(record, currentLine);
			}
		}
	}


	private static BufferedReader limpaEInstanciaArquivoDeAcervo() throws IOException 
	{
		File file = new File("kohaIBICT3/CCN/arquivosIniciais/000001-9 (t�tulos 2022).txt");
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		// Criacao do segundo arquivo, que corresponde ao
		// primeiro, mas com correcoes das linhas quebradas
	//	FileOutputStream file2 = new FileOutputStream("kohaIBICT3/CCN/t�tulosLimpos.txt");
		corrigeLinhasQuebradas(br, "kohaIBICT3/CCN/t�tulosLimpos.txt");
		
		// reabre o novo arquivo criado, para gerar
		// os registros em formato marcxml
		file = new File("kohaIBICT3/CCN/t�tulosLimpos.txt");
		return new BufferedReader(new FileReader(file));
	}
	
	private static void addItemToRecord(Record record, String[] editionField) 
	{
		for(String edition : editionField)
		{
			record.addFields(parseEditionTo952Field(edition.trim()));
		}
	}

	private static ArrayList<Field> parseEditionTo952Field(String edition) {
		ArrayList<Field> fields = new ArrayList<>(); 
		
		String volumeAndNumber = volumeRegex.substring(0, volumeRegex.length() - 3) + "[ ]*" + numberRegex;
		String simpleForm = yearRegex + "[ ]" + volumeAndNumber;
		String simpleWithListOfVolumes = simpleForm + "([ ]*[,][ ]*" + volumeAndNumber + ")+";
		String simpleWithoutVolume = yearRegex + "[ ]" + numberRegex;
		String yearAndMaybeVolume = yearRegex + "([ ]" + volumeRegex.substring(0, volumeRegex.length() - 3) + ")*";
		String simpleFormWithWordsOrNumber = yearRegex + "[ ]" 
				+ volumeRegex.substring(0, volumeRegex.length() - 3) 
				+ editionWithWordsOrNumbersRegex;
		String simpleFormWithWordsAndNumber = yearRegex + "[ ]" 
				+ volumeRegex.substring(0, volumeRegex.length() - 3)
				+ editionWithWordsAndNumbersRegex;
		String simpleWithListOfVolumesOrWords = simpleForm + "([ ]*[,][ ]" + "(" + volumeAndNumber 
				+ "|[a-z]+)" + ")+";
		String multipleSimpleForms = simpleForm + "([,][ ]" + simpleForm + ")+";
		String yearRange = yearRegex + "[-]" + yearRegex;
		String yearWithWords = yearRegex + "([ ])*" + editionWithWordsRegex;
		String yearWithVolume = yearRegex + "([ ])*" + volumeWithNumOrWords;
		if(edition.matches(simpleForm))
		{
			fields.addAll(fieldsFromType1ofMatch(edition));
		} else if(edition.matches(simpleWithListOfVolumes))
		{
			fields.addAll(fieldsFromType2ofMatch(edition));
		}
		else if (edition.matches(simpleWithoutVolume))
		{
			fields.addAll(fieldsFromType3ofMatch(edition));
		}else if(edition.matches(yearAndMaybeVolume))
		{
			fields.addAll(fieldsFromType4ofMatch(edition));
		}else if(edition.matches(simpleFormWithWordsOrNumber))
		{
			fields.addAll(fieldsFromType5ofMatch(edition));
		}else if(edition.matches(simpleFormWithWordsAndNumber))
		{
			fields.addAll(fieldsFromType6ofMatch(edition));
		}else if(edition.matches(simpleWithListOfVolumesOrWords))
		{
			fields.addAll(fieldsFromType7ofMatch(edition));
		}else if(edition.matches(multipleSimpleForms))
		{
			fields.addAll(fieldsFromType8ofMatch(edition));
		}else if(edition.matches(yearRange))
		{
			fields.addAll(fieldsFromType9ofMatch(edition));
		}else if(edition.matches(yearWithWords))
		{
			fields.addAll(fieldsFromType10ofMatch(edition));
		}else if(edition.matches(yearWithVolume))
		{
			fields.addAll(fieldsFromType11ofMatch(edition));
		}
		else
		{
			System.out.println(edition);
		}
		
		return fields;
	}
	


	private static ArrayList<Field> fieldsFromType1ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year;
		
		volume = getFieldThatMatches(edition, volumeRegex);
		if(volume != null)
		{
			volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
			subfieldHvalue += ", v. " + volume;
		}
		
		number = getFieldThatMatches(edition, numberRegex);
		if(number != null)
		{
			fields.addAll(fillEditionNumbers(number, subfieldHvalue));
		}
		return fields;
	}
	
	


	private static ArrayList<Field> fieldsFromType2ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		
		for(String vol : getListOfVolumes(edition.substring(year.length())))
		{
			volume = getFieldThatMatches(vol.trim(), volumeRegex);
			if(year != null)
				subfieldHvalue = year;

			if(volume != null)
			{
				volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
				subfieldHvalue += ", v. " + volume;
			}
			
			number = getFieldThatMatches(vol, numberRegex);
			if(number != null)
			{
				fields.addAll(fillEditionNumbers(number, subfieldHvalue));
			}
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType3ofMatch(String edition) 
	{
		String year, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year;
		
		number = getFieldThatMatches(edition, numberRegex);
		if(number != null)
		{
			fields.addAll(fillEditionNumbers(number, subfieldHvalue));
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType4ofMatch(String edition) 
	{
		String year, volume, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year;
		
		volume = getFieldThatMatches(edition, volumeRegex);
		if(volume != null)
		{
			volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
			subfieldHvalue += ", v. " + volume;
		}
		
		fields.add(new Field("952", "h", subfieldHvalue, Constant.dataFieldDenomination));
		
		return fields;
	}
	
	
	private static ArrayList<Field> fieldsFromType5ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year + ", ";
		
		volume = getFieldThatMatches(edition, volumeRegex);
		if(volume != null)
		{
			volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
			subfieldHvalue += "v. " + volume;
		}
		
		number = getFieldThatMatches(edition, editionWithWordsOrNumbersRegex);
		if(number != null)
		{
			fields.addAll(fillEditionNumbers(number, subfieldHvalue));
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType6ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year + ", ";
		
		volume = getFieldThatMatches(edition, volumeRegex);
		if(volume != null)
		{
			volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
			subfieldHvalue += "v. " + volume;
		}
		
		number = getFieldThatMatches(edition, editionWithWordsAndNumbersRegex);
		if(number != null)
		{
			fields.addAll(fillEditionNumbers(number, subfieldHvalue));
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType7ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		
		for(String vol : getListOfVolumes(edition.substring(year.length())))
		{
			if(year != null)
				subfieldHvalue = year + ", ";

			volume = getFieldThatMatches(vol.trim(), volumeRegex);
			if(volume != null)
			{
				volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
				subfieldHvalue += "v. " + volume;
			}else
			{
				volume = getFieldThatMatches(vol.trim(), "[a-z]+");
				if(volume != null)
					subfieldHvalue += "v. " + volume;
			}
			
			number = getFieldThatMatches(vol, editionWithWordsOrNumbersRegex);
			if(number != null)
			{
				fields.addAll(fillEditionNumbers(number, subfieldHvalue));
			}else
			{
				fields.add(new Field("952", "h", subfieldHvalue, Constant.dataFieldDenomination));
			}
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType8ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		for(String simpleEdition : edition.split(","))
		{
			year = getFieldThatMatches(simpleEdition, yearRegex);
			if(year != null)
				subfieldHvalue = year + ", ";
			
			volume = getFieldThatMatches(simpleEdition, volumeRegex);
			if(volume != null)
			{
				volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
				subfieldHvalue += "v. " + volume;
			}
			
			number = getFieldThatMatches(simpleEdition, numberRegex);
			if(number != null)
			{
				fields.addAll(fillEditionNumbers(number, subfieldHvalue));
			}
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType9ofMatch(String edition) 
	{
		String year, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		int begin = Integer.parseInt(edition.split("-")[0]);
		int end = Integer.parseInt(edition.split("-")[1]);
		
		for(int i = begin; i <= end; i++)
		{
			year = getFieldThatMatches(edition, yearRegex);
			if(year != null)
				subfieldHvalue = year;
			fields.add(new Field("952", "h", subfieldHvalue, Constant.dataFieldDenomination));
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType10ofMatch(String edition) 
	{
		String year, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year + ", ";
		
		
		
		number = getFieldThatMatches(edition, editionWithWordsRegex);
		if(number != null)
		{
			fields.addAll(fillEditionNumbers(number, subfieldHvalue));
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType11ofMatch(String edition) 
	{
		String year, volume, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year;
		
		String edit = edition.substring(year.length());
		
		volume = getFieldThatMatches(edit, volumeWithNumOrWords);
		if(volume != null)
		{
			fields.addAll(fillVolumeNumbers(volume, subfieldHvalue));
		}
		return fields;
	}
	
	private static String[] getListOfVolumes(String edition) 
	{
		ArrayList<String> volumes = new ArrayList<>();
		boolean insideParenthesis = false;
		int currentFirstChar = 0;
		char letra;
		for(int c = 0; c < edition.length(); c++)
		{
			letra = edition.charAt(c);
			
			if(letra == '(')
				insideParenthesis = true;
			if(letra == ')')
				insideParenthesis = false;
			
			if(letra == ',' && !insideParenthesis)
			{
				volumes.add(edition.substring(currentFirstChar, c));
				currentFirstChar = c + 1;
			}
			
			if(c == (edition.length() - 1))
			{
				volumes.add(edition.substring(currentFirstChar));
			}
		}
		
		String[] vols = new String[volumes.size()];
		for(int i = 0; i < vols.length; i++)
		{
			vols[i] = volumes.get(i);
		}
			
		return vols;
	}


	private static ArrayList<Field> fillEditionNumbers(String number, String subfieldValue) 
	{
		ArrayList<Field> fields = new ArrayList<>();
		number = number.substring(1, number.length() - 1);
		
		for(String num : separateAllNumbers(number))
		{
			if (num.contains("-"))
			{
				fields.addAll(createFieldsFromNumberRange(subfieldValue, num));
			} else			
			{
				fields.add(new Field("952", "h", subfieldValue 
						+ ", n. " + num, Constant.dataFieldDenomination));
			}
		}
		return fields;
	}
	
	private static ArrayList<Field> fillVolumeNumbers(String volume, String subfieldValue) 
	{
		ArrayList<Field> fields = new ArrayList<>();
		
		for(String vol : separateAllNumbers(volume))
		{
			if (vol.contains("-"))
			{
				fields.addAll(createFieldsFromNumberRange(subfieldValue, vol));
			} else			
			{
				fields.add(new Field("952", "h", subfieldValue 
						+ "v. " + vol, Constant.dataFieldDenomination));
			}
		}
		return fields;
	}


	private static String[] separateAllNumbers(String number) {
		if(number.contains(","))
			return number.split(",");
		
		String[] singleOcurrence = new String[1];
		singleOcurrence[0] = number;
		
		return singleOcurrence;
	}


	private static ArrayList<Field> createFieldsFromNumberRange(String subfieldValue, String number) 
	{
		ArrayList<Field> fields = new ArrayList<>();
		int start = Integer.parseInt(number.split("-")[0].trim()), 
				end = Integer.parseInt(number.split("-")[1].trim());
		for(int i = start; i <= end; i++)
		{
			fields.add(new Field("952", "h", subfieldValue + ", n. " + i, Constant.dataFieldDenomination));
		}
		return fields;
	}


	private static String getFieldThatMatches(String textLine, String regex)
	{
		Pattern pattern = Pattern.compile(regex);
		// create matcher for pattern p and given string
		Matcher matcher = pattern.matcher(textLine);
		String valueThatMatches = null;
		
		// if an occurrence if a pattern was found in a given string...
		if (matcher.find()) {
			// ...then you can use group() methods.
			valueThatMatches = matcher.group(0); // whole matched expression
//			System.out.println(m.group(1)); // first expression from round brackets (Testing)
//			System.out.println(m.group(2)); // second one (123)
//			System.out.println(m.group(3)); // third one (Testing)
		}

		return valueThatMatches;
	}

	/**
	 * itera pela lista 'records', e retorna o registro que tem 
	 * o campo 090 $a correspondente ao valor indicado em 'field090a'
	 * @param records
	 * @param filed090a
	 * @return
	 */
	private static Record getCorrespondentRecord(ArrayList<Record> records, String field090a) 
	{
		for(Record r : records)
		{
			if(r.getField("090").get(0).getSubfield("a").get(0).getValue()
					.contentEquals(field090a))
				return r;
		}
		return null;
	}



	private static void corrigeLinhasQuebradas(BufferedReader br, String pathOutput) throws IOException 
	{
		OutputStreamWriter output = new OutputStreamWriter(new FileOutputStream(pathOutput));
		
		String currentLine;
		currentLine = br.readLine(); //le cabecalho
		output.write(currentLine);
		
		while (((currentLine = br.readLine()) != null))
		{
			if(!currentLine.startsWith("!") || currentLine.contentEquals(""))
			{
				output.write(currentLine);
				continue;
			}
			output.write("\n");
			output.write(currentLine);
		}
		
		output.close();
		br.close();
	}

	/**
	 * Avalia o conteudo da linha atual 'line' , busca o campo
	 * correspondente e adiciona no registro 'record'.
	 * @param record
	 * @param line
	 */
	private static void insertFieldInRecord(Record record, String line) {
		
		String code = line.split("!")[1];
		String correspondentName = getCodeCorrespondence(code);
		
		if(correspondentName == null)
			return ;
		
		String fieldName = correspondentName.split("!!!")[0]; 
		
		// separa registros de controlfield e datafield
		if(fieldName.contentEquals("001") || fieldName.contentEquals("008"))
		{
			createControlField(record, line);
		}else
		{
			createDataField(record, line);
		}
	}
	
	private static void createControlField(Record record, String line) 
	{
		String correspondentName = getCodeCorrespondence(line.split("!")[1]);
		
		if(correspondentName == null)
			return ;

		String[] fieldContent = correspondentName.split("!!!");
		String fieldName = fieldContent[0], 
				subfieldName = fieldContent[1];
		
		Field createdField = null;
		switch(fieldName)
		{
		case "001":
			createdField = new Field(fieldName, line.split("!")[2], Constant.controlFieldDenomination);
			record.addField(createdField);
			break;
		case "008":
			if(!record.hasField("008"))
			{
				String empty40char = "                                        ";
				createdField = new Field(fieldName, empty40char, Constant.controlFieldDenomination);
				record.addField(createdField);
			}
			createdField = record.getField("008").get(0); 
			createdField.setValue(
					editField008value(line.split("!")[2], subfieldName, record.getField("008").get(0).getValue()));
		}
	}

	private static void createDataField(Record record, String line) 
	{
		
		String correspondentName = getCodeCorrespondence(line.split("!")[1]);
		
		if(correspondentName == null)
			return ;
		
		String[] fieldContent = correspondentName.split("!!!");
		String fieldName = fieldContent[0], 
				subfieldName = fieldContent[1];
		
		// verifica se ocorrem indicadores
		String ind1 = " ", ind2 = " ";
		if(fieldContent.length > 2)
		{
			ind1 = fieldContent[2];
			ind2 = fieldContent[3];
		}
		
		Field createdField = null;
		ArrayList<Subfield> subcampos;
		// instancia o subcampo
		Subfield subcampo = new Subfield(subfieldName, line.split("!")[2], Constant.subfieldDenomination);
		
		// se o registro tem o campo e n�o tem o subcampo
		if(record.hasField(fieldName) && !record.getField(fieldName).get(0).hasSubfield(subfieldName))
		{
			createdField = record.getField(fieldName).get(0); 
			createdField.addSubfield(subcampo);
		}else
		{
			subcampos = new ArrayList<Subfield>();
			subcampos.add(subcampo);
			createdField = new Field(fieldName, ind1, ind2, subcampos, Constant.dataFieldDenomination);
			record.addField(createdField);
		}
	}


	/**
	 * Edit the current value of field 008
	 * @param value
	 * @param subfieldName
	 * @param value2
	 * @return
	 */
	private static String editField008value(String valueToInsert, String charRange, String toBeEdited) {
		
		int begin = Integer.parseInt(charRange.split("-")[0]);
		
		toBeEdited = toBeEdited.substring(0, begin) + valueToInsert 
				+ toBeEdited.substring(begin + valueToInsert.length(), toBeEdited.length());
		
		return toBeEdited;
	}
	
	/**
	 * Recebe o codigo e retorna o campo e subcampo correspondente, separado
	 * por '!!!', de acordo com a tabela apresentada pela equipe do CCN.
	 * @param code
	 * @return
	 */
	private static String getCodeCorrespondence(String code)
	{	
		String fieldName = null, subfieldName = null;
		
		// recupera o nome do campo e do subcampo
		switch(code)
		{
		case "S020":
			fieldName = "090";
			subfieldName = "a";
			break;
		case "S070":
			fieldName = "090";
			subfieldName = "c";
			break;
		case "S090":
			fieldName = "090";
			subfieldName = "d";
			break;
		case "S100":
			fieldName = "008";
			subfieldName = "07-10";
			break;
		case "S110":
			fieldName = "008";
			subfieldName = "11-14";
			break;
		case "S120":
			fieldName = "008";
			subfieldName = "15-17";
			break;
		case "S130":
			fieldName = "008";
			subfieldName = "18";
			break;
		case "S140":
			fieldName = "008";
			subfieldName = "21";
			break;
		case "S160":
			fieldName = "008";
			subfieldName = "35-37";
			break;
		case "S180":
			fieldName = "599";
			subfieldName = "a";
			break;
		case "S200":
			fieldName = "245";
			subfieldName = "a";
			break;
		case "S210":
			fieldName = "245";
			subfieldName = "b";
			break;
		case "S220":
			fieldName = "245";
			subfieldName = "c";
			break;
		case "S230":
			fieldName = "245";
			subfieldName = "d";
			break;
		case "S240":
			fieldName = "245";
			subfieldName = "s";
			break;
		case "S250":
			fieldName = "245";
			subfieldName = "u";
			break;
		case "S280":
			fieldName = "210";
			subfieldName = "a";
			break;
		case "S310":
			fieldName = "246";
			subfieldName = "a";
			break;
		case "S320":
			fieldName = "246";
			subfieldName = "a";
			break;
		case "S330":
			fieldName = "246";
			subfieldName = "a";
			break;
		case "S340":
			fieldName = "246";
			subfieldName = "a";
			break;
		case "S411":
			fieldName = "260";
			subfieldName = "a";
			break;
		case "S412":
			fieldName = "260";
			subfieldName = "b";
			break;
		case "S420":
			fieldName = "362";
			subfieldName = "a";
			break;
		case "S440":
			fieldName = "022";
			subfieldName = "a";
			break;
		case "S520":
			fieldName = "550";
			subfieldName = "a";
			break;
		case "S532":
			fieldName = "653";
			subfieldName = "b";
			break;
		case "S533":
			fieldName = "650";
			subfieldName = "a";
			break;
		case "S610":
			fieldName = "759";
			subfieldName = "t";
			break;
		case "S620":
			fieldName = "769";
			subfieldName = "t";
			break;
		case "S650":
			fieldName = "779";
			subfieldName = "t";
			break;
		case "S710":
			fieldName = "780";
			subfieldName = "t";
			break;
		case "S730":
			fieldName = "780";
			subfieldName = "t";
			break;
		case "S740":
			fieldName = "780";
			subfieldName = "t";
			break;
		case "S750":
			fieldName = "780";
			subfieldName = "t";
			break;
		case "S760":
			fieldName = "780";
			subfieldName = "t";
			break;
		case "S810":
			fieldName = "785";
			subfieldName = "t";
			break;
		case "S830":
			fieldName = "785";
			subfieldName = "t";
			break;
		case "S840":
			fieldName = "785";
			subfieldName = "t";
			break;
		case "S850":
			fieldName = "785";
			subfieldName = "t";
			break;
		case "S860":
			fieldName = "785";
			subfieldName = "t";
			break;
		case "S870":
			fieldName = "785";
			subfieldName = "t";
			break;
		default:
			System.err.println("Codigo de campo \"" + code + "\" nao encontrado!!!");
//			break;
			return null;
				
		}
		
		// adiciona os indicadores quando preciso
		boolean hasIndicator = true;
		String ind1 = null, ind2 = null;
		switch(code)
		{
		case "S200":
			ind1 = "1";
			ind2 = " ";
			break;
		case "S310":
			ind1 = "3";
			ind2 = "0";
			break;
		case "S320":
			ind1 = "3";
			ind2 = "1";
			break;
		case "S330":
			ind1 = "3";
			ind2 = "3";
			break;
		case "S340":
			ind1 = "3";
			ind2 = "4";
			break;
		case "S420":
			ind1 = "1";
			ind2 = " ";
			break;
		case "S532":
			ind1 = "0";
			ind2 = "0";
			break;
		case "S533":
			ind1 = "0";
			ind2 = "4";
			break;
		case "S710":
			ind1 = "0";
			ind2 = "0";
			break;
		case "S730":
			ind1 = " ";
			ind2 = "4";
			break;
		case "S740":
			ind1 = " ";
			ind2 = "5";
			break;
		case "S750":
			ind1 = " ";
			ind2 = "6";
			break;
		case "S760":
			ind1 = " ";
			ind2 = "7";
			break;
		case "S810":
			ind1 = "0";
			ind2 = "0";
			break;
		case "S830":
			ind1 = " ";
			ind2 = "1";
			break;
		case "S840":
			ind1 = " ";
			ind2 = "5";
			break;
		case "S850":
			ind1 = " ";
			ind2 = "6";
			break;
		case "S860":
			ind1 = " ";
			ind2 = "7";
			break;
		case "S870":
			ind1 = " ";
			ind2 = "8";
			break;
		default:
			hasIndicator = false;
		}
		
		String indicators = "";
		if(hasIndicator)
		{
			indicators = "!!!" + ind1 + "!!!" + ind2;
		}
		
		return fieldName + "!!!" + subfieldName + indicators;
	}
}
