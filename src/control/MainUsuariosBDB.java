package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class MainUsuariosBDB {
	
	/**
	 * Main para a criação de exemplares do Koha BDB
	 * @param args
	 * @throws IOException
	 * @throws TransformerException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public static void main(String[] args) throws 
	IOException, ParserConfigurationException, SAXException, TransformerException {
		
		for(int i = 1; i <= 8; i++)
		{
			criaArquivoLimpoArquivoDeUsuarios("BDB/usuarios/usuarios0" + i + ".xml");
		}
		
		
		HashMap<String, Usuario> usuarios = new HashMap<>();
		String outputPath = "BDB/usuarios/paraImportarNoKoha.csv";
		File file;
		BufferedReader br;
		
		for(int i = 1; i <= 8; i++)
		{
			System.out.println("Lendo arquivo " + i);
			file = new File("BDB/usuarios/usuarios0" + i + "-limpo.xml");
			br = new BufferedReader(new FileReader(file));
			constroiRegistrosDeUsuarios(br, usuarios);
		}
		
		
		
		
		
		criaCSVusuarios(outputPath, usuarios);
		

		
		
		
		
	

	}
	

	private static void criaArquivoLimpoArquivoDeUsuarios(String path) throws IOException {
		OutputStreamWriter writer;
		BufferedReader br = new BufferedReader(new FileReader(path));
		String currentLine, outputPath = path.substring(0, path.length() - 4) + "-limpo.xml";
		
		
		// abre arquivo para escrita
		writer = new OutputStreamWriter(new FileOutputStream(outputPath), StandardCharsets.UTF_8);
		
		// Comandos usados para abrir o arquivo e limpar qualquer conteudo antigo.
		PrintWriter writer2 = new PrintWriter(outputPath);
		writer2.print("");
		writer2.close();
		
		System.out.println("Escrevendo arquivo csv de resultado");
		
		// adiciona a primeira linha
		writer.append(br.readLine().trim());
		String newLineIndicator;
		while ((currentLine = br.readLine()) != null)
		{
			if(currentLine.trim().startsWith("<") && !currentLine.trim().startsWith("</"))
				newLineIndicator = "\n";
			else
				newLineIndicator = "";
			
			writer.append(newLineIndicator + currentLine.trim());
		}
		writer.close();
	}


	private static void criaCSVusuarios(String outputPath, HashMap<String, Usuario> usuarios) throws IOException 
	{
		OutputStreamWriter writer;
		String header = "cardnumber,surname,firstname,title,othernames,initials,streetnumber,streettype,"
				+ "address,address2,city,state,zipcode,country,email,phone,mobile,fax,emailpro,phonepro,"
				+ "B_streetnumber,B_streettype,B_address,B_address2,B_city,B_state,B_zipcode,B_country,"
				+ "B_email,B_phone,dateofbirth,branchcode,categorycode,dateenrolled,dateexpiry,"
				+ "date_renewed,gonenoaddress,lost,debarred,debarredcomment,contactname,contactfirstname,"
				+ "contacttitle,borrowernotes,relationship,sex,password,flags,userid,opacnote,contactnote,"
				+ "sort1,sort2,altcontactfirstname,altcontactsurname,altcontactaddress1,altcontactaddress2,"
				+ "altcontactaddress3,altcontactstate,altcontactzipcode,altcontactcountry,altcontactphone,"
				+ "smsalertnumber,sms_provider_id,privacy,privacy_guarantor_fines,privacy_guarantor_checkouts,"
				+ "checkprevcheckout,updated_on,lastseen,lang,login_attempts,overdrive_auth_token,anonymized,"
				+ "autorenew_checkouts,primary_contact_method,patron_attributes,guarantor_relationship,guarantor_id\r"; 
				
		writer = new OutputStreamWriter(new FileOutputStream(outputPath), StandardCharsets.UTF_8);
		
		// Comandos usados para abrir o arquivo e limpar qualquer conteudo antigo.
		PrintWriter writer2 = new PrintWriter(outputPath);
		writer2.print("");
		writer2.close();
		
		
		writer.append(header + "\n");
		
		for(String key : usuarios.keySet())
		{
			writer.append(usuarios.get(key).getValuesOrderedAsKohaPatronImport() + "\n");
		}
		
		writer.close();
	}

	private static void constroiRegistrosDeUsuarios(BufferedReader br, HashMap<String, Usuario> usuarios) throws IOException {
		String currentLine;
		int cont= 0;
		Usuario usuario = null;
		while ((currentLine = br.readLine()) != null)
		{
			if(cont % 1000000 == 0)
				System.out.println(cont++);
			cont++;
			
			// indica o comeco de um registro de usuario
			if(currentLine.contains("<DATA_RECORD>"))
			{
				// instancia o usuario
				usuario = new Usuario();
				
				// preenche os campos padronizados
				usuario.branchcode = "BDB";
			}
			// indica o fim de um registro de usuario
			else if(currentLine.contains("</DATA_RECORD>"))
			{
				if(!usuarios.containsKey(usuario.firstname + ":::" + usuario.userid))
				{
					usuarios.put(usuario.firstname + ":::" + usuario.userid, usuario);
				}
			}
			else if(!currentLine.contentEquals(""))
			{
				preencheCampoCorrespondente(usuario, currentLine.trim());
			}
		}
	}

	/*
	 * Esse metodo verifica o conteudo de currentLine e preenche o campo correspondente
	 * do usuario atual.
	 */
	private static void preencheCampoCorrespondente(Usuario usuario, String currentLine) {
	
		String campo = currentLine.split(">")[0].split("<")[1];
		String valor = currentLine.split(">")[1].split("<")[0];
		
		if(valor.contentEquals(""))
			return;
		
		String dataFormatada;
		switch(campo)
		{
		case "COD_ID":
		    usuario.cardnumber = valor.replace(",", ".");
		    break;
		case "NOME":
			String[] nome = valor.split(" ");
			usuario.firstname = nome[0];
			usuario.surname = valor.substring(nome[0].length(), valor.length());
			break;
		case "LOGRADOURO":
			usuario.address = valor;
			break;
		case "BAIRRO":
			usuario.address2 = valor;
			break;
		case "CEP":
			usuario.zipcode = valor;
			break;
		case "E_MAIL":
			if(usuario.email == "")
				usuario.email = valor;
			break;
		case "TELEFONE":
			usuario.phone = valor;
			break;
		case "CELULAR":
			usuario.mobile = valor;
			break;
		case "FAX":
			usuario.fax = valor;
			break;
		case "SEXO":
			usuario.sex = valor;
			break;
		case "SENHA":
			usuario.password = valor;
			break;
		case "COD_USUARIO":
			usuario.userid = valor;
			break;
		case "OBSERVACAO":
			usuario.contactnote = valor;
			break;
		case "TIPO_USUARIO":
			usuario.categorycode = valor;
			break;
		case "DATA_NASCIMENTO":
			dataFormatada = formataData(valor);
			usuario.dateofbirth = dataFormatada;
			break;
		case "DATA_CADASTRO":
			dataFormatada = formataData(valor);
			usuario.dateenrolled = dataFormatada;
			break;
		case "DATA_VALIDADE":
			dataFormatada = formataData(valor);
			usuario.dateexpiry = dataFormatada;
			break;
		case "DATA_RENOVACAO":
			dataFormatada = formataData(valor);
			usuario.date_renewed = dataFormatada;
			break;
		default:
			break;
		}
	}
	/**
	 * Transforma a data do formato:
	 * 2006-01-25 20:46:47
	 * para o formato:
	 * 01/25/2006
	 * 
	 * @param valor
	 * @return
	 */
	private static String formataData(String valor) {
		String dataFormatada = null;
		String[] partesData = valor.split(" ")[0].split("-");
		
		dataFormatada = partesData[1] + "/"
				+ partesData[2] + "/" + partesData[0];
		
		return dataFormatada;
	}
	
}

/*
 * Classe que armazena os dados de usuario.
 */
class Usuario
{
	String cardnumber = "";
	String surname = "";
	String firstname = "";
	String title = "";
	String othernames = "";
	String initials = "";
	String streetnumber = "";
	String streettype = "";
	String address = "";
	String address2 = "";
	String city = "";
	String state = "";
	String zipcode = "";
	String country = "";
	String email = "";
	String phone = "";
	String mobile = "";
	String fax = "";
	String emailpro = "";
	String phonepro = "";
	String B_streetnumber = "";
	String B_streettype = "";
	String B_address = "";
	String B_address2 = "";
	String B_city = "";
	String B_state = "";
	String B_zipcode = "";
	String B_country = "";
	String B_email = "";
	String B_phone = "";
	String dateofbirth = "";
	String branchcode = "";
	String categorycode = "";
	String dateenrolled = "";
	String dateexpiry = "";
	String date_renewed = "";
	String gonenoaddress = "";
	String lost = "";
	String debarred = "";
	String debarredcomment = "";
	String contactname = "";
	String contactfirstname = "";
	String contacttitle = "";
	String borrowernotes = "";
	String relationship = "";
	String sex = "";
	String password = "";
	String flags = "";
	String userid = "";
	String opacnote = "";
	String contactnote = "";
	String sort1 = "";
	String sort2 = "";
	String altcontactfirstname = "";
	String altcontactsurname = "";
	String altcontactaddress1 = "";
	String altcontactaddress2 = "";
	String altcontactaddress3 = "";
	String altcontactstate = "";
	String altcontactzipcode = "";
	String altcontactcountry = "";
	String altcontactphone = "";
	String smsalertnumber = "";
	String sms_provider_id = "";
	String privacy = "";
	String privacy_guarantor_fines = "";
	String privacy_guarantor_checkouts = "";
	String checkprevcheckout = "";
	String updated_on = "";
	String lastseen = "";
	String lang = "";
	String login_attempts = "";
	String overdrive_auth_token = "";
	String anonymized = "";
	String autorenew_checkouts = "";
	String primary_contact_method = "";
	String patron_attributes = "";
	String guarantor_relationship = "";
	String guarantor_id = "";
	
	public String getValuesOrderedAsKohaPatronImport()
	{
		String values = "";
		
		values += cardnumber + ",";
		values += surname + ",";
		values += firstname + ",";
		values += title + ",";
		values += othernames + ",";
		values += initials + ",";
		values += streetnumber + ",";
		values += streettype + ",";
		values += address + ",";
		values += address2 + ",";
		values += city + ",";
		values += state + ",";
		values += zipcode + ",";
		values += country + ",";
		values += email + ",";
		values += phone + ",";
		values += mobile + ",";
		values += fax + ",";
		values += emailpro + ",";
		values += phonepro + ",";
		values += B_streetnumber + ",";
		values += B_streettype + ",";
		values += B_address + ",";
		values += B_address2 + ",";
		values += B_city + ",";
		values += B_state + ",";
		values += B_zipcode + ",";
		values += B_country + ",";
		values += B_email + ",";
		values += B_phone + ",";
		values += dateofbirth + ",";
		values += branchcode + ",";
		values += categorycode + ",";
		values += dateenrolled + ",";
		values += dateexpiry + ",";
		values += date_renewed + ",";
		values += gonenoaddress + ",";
		values += lost + ",";
		values += debarred + ",";
		values += debarredcomment + ",";
		values += contactname + ",";
		values += contactfirstname + ",";
		values += contacttitle + ",";
		values += borrowernotes + ",";
		values += relationship + ",";
		values += sex + ",";
		values += password + ",";
		values += flags + ",";
		values += userid + ",";
		values += opacnote + ",";
		values += contactnote + ",";
		values += sort1 + ",";
		values += sort2 + ",";
		values += altcontactfirstname + ",";
		values += altcontactsurname + ",";
		values += altcontactaddress1 + ",";
		values += altcontactaddress2 + ",";
		values += altcontactaddress3 + ",";
		values += altcontactstate + ",";
		values += altcontactzipcode + ",";
		values += altcontactcountry + ",";
		values += altcontactphone + ",";
		values += smsalertnumber + ",";
		values += sms_provider_id + ",";
		values += privacy + ",";
		values += privacy_guarantor_fines + ",";
		values += privacy_guarantor_checkouts + ",";
		values += checkprevcheckout + ",";
		values += updated_on + ",";
		values += lastseen + ",";
		values += lang + ",";
		values += login_attempts + ",";
		values += overdrive_auth_token + ",";
		values += anonymized + ",";
		values += autorenew_checkouts + ",";
		values += primary_contact_method + ",";
		values += patron_attributes + ",";
		values += guarantor_relationship + ",";
		values += guarantor_id;
		
		return values;
	}
}