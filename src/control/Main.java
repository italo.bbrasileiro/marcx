package control;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class Main {
	
	/**
	 * atualiza o link de download nos registros com link.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args)  throws Exception {
		ArrayList<String[]> csv = CSVController.readCSV("arquivoNacionalObjetosDigitais/downloadsApenas952/relatorio.tab", "\t");
		MARC acervo = new MARC("arquivoNacionalObjetosDigitais/952sem856uUmLink.xml");
		ArrayList<Record> regAtualizado = new ArrayList<Record>();
		
		for(int i = 1; i < csv.size(); i++)
		{
			String[] linha = csv.get(i);
//			System.out.println(linha[0]);
			for(Record registro: acervo.getRecords())
			{
				if(registro.getField("999").get(0).getSubfield("c").get(0).getValue().contentEquals(linha[0].split("_")[1]))
				{
					for(Field campo : registro.getField("952"))
					{
						String id952 = linha[0].split("_")[2].split("\\.")[0];
						if(campo.getSubfield("9").get(0).getValue().contentEquals(id952))
						{
							campo.getSubfield("u").get(0).setValue("http://kohaan.ibict.br/cgi-bin/koha/opac-retrieve-file.pl?id=" + linha[1]);
							if(!regAtualizado.contains(registro))
								regAtualizado.add(registro);
							break;
						}
					}
				}
				
			}
		}
		
		XMLHandler.generateXMLFile(regAtualizado, "arquivoNacionalObjetosDigitais/952sem856uUmLink-ODatualizado.xml");
	}
	/**
	 * Separacao dos registros com link do acervo Koha AN
	 * @param args
	 * @throws Exception 
	 */
	public static void mainSeparacao(String[] args) throws Exception {

		// passo 1: Busca de registros com ODs
		
		MARC marcAcervo1 = new MARC("arquivoNacionalObjetosDigitais/kohaANmigradoComLink856.xml");
		MARC marcAcervo2 = new MARC("arquivoNacionalObjetosDigitais/kohaANmigradoComLink952.xml");
		ArrayList<Record> records = new ArrayList<Record>();
		
		ArrayList<Record> recordsno952u = new ArrayList<Record>();
		
		for(Record r : marcAcervo1.getRecords())
		{
			boolean has952u = false;
			if(r.hasField("952"))
				for(Field f : r.getField("952"))
				{
					if(f.hasSubfield("u"))
						has952u = true;
				}
			if(!has952u)
				recordsno952u.add(r);
		}
		
		ArrayList<Record> recordsno856u = new ArrayList<Record>();
		HashMap<String, Integer> tipos = new HashMap<String, Integer>();
		
		for(Record r : marcAcervo2.getRecords())
		{
			boolean has856u = false;
			if(!r.hasField("856"))
			{
				recordsno856u.add(r);
				String tipoRegistro = r.getField("942").get(0).getSubfield("c").get(0).getValue();
				if(!tipos.containsKey(tipoRegistro))
					tipos.put(tipoRegistro, 0);
				tipos.put(tipoRegistro, tipos.get(tipoRegistro) + 1); 
			}
				
		}
		
//		Iterator<String> tiposMaterial = tipos.keySet().iterator();
		
//		while(tiposMaterial.hasNext())
//		{
//			String t = tiposMaterial.next();
//			System.out.println(t + ":" + tipos.get(t));
//		}
		
		// 1.a : campo 952
//		for(Record r : marcAcervo.getRecords())
//		{
//			if(r.hasField("952"))
//				for(Field field : r.getField("952"))
//				{
//					if(field.hasSubfield("u") && !records.contains(r))
//					{
//						records.add(r);
//						break;
//					}
//				}
//		}
		
		// 1.b : campo 856
//		for(Record r : marcAcervo.getRecords())
//		{
//			if(r.hasField("856"))
//				for(Field field : r.getField("856"))
//				{
//					if(field.hasSubfield("u") && !records.contains(r))
//					{
//						records.add(r);
//						break;
//					}
//				}
//		}
		
		// 1.c : merge dos dois arquivos
		records.addAll(marcAcervo1.getRecords());
		
		for(Record r : marcAcervo2.getRecords())
		{
			if(marcAcervo1.recordsWithFieldAndSubfieldValue("999", "c", r.getField("999").get(0).getSubfield("c").get(0).getValue()) == null)
				records.add(r);
		}
		
		ArrayList<Record> maisDeUm952 = new ArrayList<Record>();
		// OBS: separar registros com mais de um 952
		for(Record rec : marcAcervo2.getRecords())
		{
			if(rec.getField("952").size() > 1)
				maisDeUm952.add(rec);
		}
		
		ArrayList<Record> campo952Udiferente = new ArrayList<Record>();
		
		// buscar 952 u diferentes
		for(Record rec : maisDeUm952)
		{
			if(!rec.getField("952").get(0).hasSubfield("u"))
			{
				campo952Udiferente.add(rec);
				continue;
			}
			String atual = rec.getField("952").get(0).getSubfield("u").get(0).getValue();
			for(Field f : rec.getField("952"))
			{
				if((!f.hasSubfield("u")) || !f.getSubfield("u").get(0).getValue().contentEquals(atual))
				{
					campo952Udiferente.add(rec);
					break;
				}
			}
		}
		
		ArrayList<Record> with856uAnd952u = new ArrayList<Record>();
		HashMap<String, Integer> contMateriais = new HashMap<String, Integer>();
		
		for(Record rec : records)
		{
			if(!rec.hasField("856") || !rec.hasField("952"))
				continue;
			
			boolean has952u = false;
			for(Field f : rec.getField("952"))
			{
				if(f.hasSubfield("u"))
					has952u = true;
			}
			
			if(has952u)
			{
				with856uAnd952u.add(rec);
				String tipo = rec.getField("942").get(0).getSubfield("c").get(0).getValue();
				if(!contMateriais.containsKey(tipo))
					contMateriais.put(tipo, 0);
				contMateriais.put(tipo, contMateriais.get(tipo) + 1);
				
			}
		}
		
		Iterator<String> tiposMaterial = contMateriais.keySet().iterator();
		
		while(tiposMaterial.hasNext())
		{
			String t = tiposMaterial.next();
			System.out.println(t + ":" + contMateriais.get(t));
		}
		
		// separar os registros por 952 u com ||
		ArrayList<Record> recComBarras = new ArrayList<Record>();
		ArrayList<Record> recSemBarras = new ArrayList<Record>();
		
		for(Record rec : with856uAnd952u)
		{
			boolean hasBarras = false;
			for(Field f : rec.getField("952"))
			{
				if(f.hasSubfield("u"))
					if(f.getSubfield("u").get(0).getValue().contains("||"))
					{
						recComBarras.add(rec);
						hasBarras = true;
						break;
					}
			}
			if(!hasBarras)
				recSemBarras.add(rec);
		}
		
		ArrayList<Record> linkDiferenteEntre952Ue856U = new ArrayList<Record>();
		ArrayList<Record> linkIguaisEntre952Ue856U = new ArrayList<Record>();

		
		for(Record rec : recSemBarras)
		{
			boolean linksIguais = true;
			String link = rec.getField("856").get(0).getSubfield("u").get(0).getValue();
			
			for(Field f : rec.getField("952"))
			{
				if(!f.hasSubfield("u"))
					continue;
				if(!f.getSubfield("u").get(0).getValue().contentEquals(link))
				{
					linksIguais = false;
					linkDiferenteEntre952Ue856U.add(rec);
					break;
				}
					
			}
			if(linksIguais)
				linkIguaisEntre952Ue856U.add(rec);
				
		}
		
		
		// gerar os links de arquivos com apenas 952$u ocupado.
		System.out.println("Records only 952: " + recordsno856u.size());
		
		
		
		ArrayList<Record> recordsWithNlinks = new ArrayList<Record>();
		ArrayList<Record> recordsWith1link = new ArrayList<Record>();
		
		for(Record recOnly952 : recordsno856u)
		{
			if(recOnly952.hasField("856"))
				System.err.println("Erro, campo 856 existente.");
			
			if(!recOnly952.hasField("999"))
				System.err.println("Registro sem biblionumber");
			
			if(recOnly952.getField("999").size() > 1)
				System.err.println("Registro com mais de um biblionumber");
			
			// separa arquivos com mais de um link no 952$u
			for(Field field952 : recOnly952.getField("952"))
				{
				if(field952.hasSubfield("u"))
					if(field952.getSubfield("u").get(0).getValue().contains("|"))
						if(!recordsWithNlinks.contains(recOnly952))
							recordsWithNlinks.add(recOnly952);
						
				}
			
			
			
			// se não está na lista de registros com mais de um link, adicionar na 
			// lista de registros com apenas um link.
			if(!recordsWithNlinks.contains(recOnly952))
				recordsWith1link.add(recOnly952);
			
		}
		
		// printa o link apenas dos registros de um único link
		// arquivo para download de objetos
		BufferedWriter writer;
		// it is false so the pervious content is cleared. "true" to keep the content.
		writer = new BufferedWriter(new FileWriter("arquivoNacionalObjetosDigitais/linksCampos952.sh", false));
		

		writer.append("#! /bin/sh\n");
		
		for(Record recLink : recordsWith1link)
		{
		// inserir o nome do arquivo : modelo IBICT
					for(Field field952 : recLink.getField("952"))
						if(field952.hasSubfield("u"))
						{
						writer.append("wget -a outputDownload.txt -O ./downloadsApenas952/IBICT_" 
								+ recLink.getField("999").get(0).getSubfield("c").get(0).getValue() 
								+ "_" + field952.getSubfield("9").get(0).getValue() + ".pdf " + field952.getSubfield("u").get(0).getValue() + "\n");
						}
		}
		writer.close();
		
		
		
		XMLHandler.generateXMLFile(records, "arquivoNacionalObjetosDigitais/kohaANmigradoComLinkTodos.xml");
		XMLHandler.generateXMLFile(maisDeUm952, "arquivoNacionalObjetosDigitais/maisDeUm952.xml");
		XMLHandler.generateXMLFile(campo952Udiferente, "arquivoNacionalObjetosDigitais/952diferente.xml");
		XMLHandler.generateXMLFile(recordsno952u, "arquivoNacionalObjetosDigitais/856sem952u.xml");
		XMLHandler.generateXMLFile(recordsno856u, "arquivoNacionalObjetosDigitais/952sem856u.xml");
		XMLHandler.generateXMLFile(with856uAnd952u, "arquivoNacionalObjetosDigitais/com856Ue952U.xml");
		XMLHandler.generateXMLFile(recSemBarras, "arquivoNacionalObjetosDigitais/com856Ue952UsemBarras.xml");
		XMLHandler.generateXMLFile(recComBarras, "arquivoNacionalObjetosDigitais/com856Ue952UcomBarras.xml");
		XMLHandler.generateXMLFile(linkDiferenteEntre952Ue856U, "arquivoNacionalObjetosDigitais/linksDiferentesEntre952Ue856U.xml");
		XMLHandler.generateXMLFile(linkIguaisEntre952Ue856U, "arquivoNacionalObjetosDigitais/linksIguaisEntre952Ue856U.xml");		
		XMLHandler.generateXMLFile(recordsWithNlinks, "arquivoNacionalObjetosDigitais/952sem856uMultiplosLinks.xml");	
		XMLHandler.generateXMLFile(recordsWith1link, "arquivoNacionalObjetosDigitais/952sem856uUmLink.xml");	
		
		
		
		
		
//		MARC marcAcervo = new MARC("arquivoNacionalObjetosDigitais/kohaANmigrado1.xml");
//		BufferedWriter writer;
//
//		writer = new BufferedWriter(new FileWriter("arquivoNacionalObjetosDigitais/campos952.txt", true));
//		
//		for(Record r : marcAcervo.getRecords())
//		{
//			if(r.hasField("952"))
//				for(Field field856 : r.getField("952"))
//				{
//					if(field856.hasSubfield("u"))
//						for(Subfield subU : field856.getSubfield("u"))
//						{
//							writer.append(r.getField("999").get(0).getSubfield("c").get(0).getValue() + 
//									"\t" + r.getField("952").get(0).getSubfield("9").get(0).getValue() + 		
//									"\t" + subU.getValue() + "\n");
//						}
//				}
//		}
//		writer.close();
		
		
		//		MARC marcAcervo = new MARC("arquivoNacionalObjetosDigitais/kohaANmigradoComLink.xml");
//		BufferedWriter writer;
//
//		writer = new BufferedWriter(new FileWriter("arquivoNacionalObjetosDigitais/campos952.txt", true));
//		
//		for(Record r : marcAcervo.getRecords())
//		{
//			if(r.hasField("952"))
//				for(Field field856 : r.getField("952"))
//				{
//					if(field856.hasSubfield("u"))
//						for(Subfield subU : field856.getSubfield("u"))
//						{
//							writer.append(r.getField("999").get(0).getSubfield("c").get(0).getValue() + 
//									"\t" + r.getField("952").get(0).getSubfield("9").get(0).getValue() + 		
//									"\t" + subU.getValue() + "\n");
//						}
//				}
//		}
//		writer.close();
		
//		XMLHandler.generateXMLFile(records, "arquivoNacionalObjetosDigitais/kohaANmigradoComLink2.xml");
//		System.out.println("fim: " + records.size() + " registros.");
	}
	
	/**
	 * Criação de exemplares no Koha do IBICT
	 * @param args
	 * @throws Exception 
	 */
	public static void mainIBICT(String[] args) throws Exception {
		MARC marcAcervo = new MARC("kohaIBICT/ACERVOibict.xml");
		ArrayList<Record> records = marcAcervo.removeField("952");
		
		// colunas correspondentes do cst:
		// [4] 952 $h
		// [5] 952 $h (concatenado com vírgula)
		// [6] 952 $h (concatenado com vírgula)
		// [7] 952 $h (concatenado com vírgula)
		// [9] 952 $t
		// [10] 952 $p
		// [11] 952 $x
		ArrayList<String[]> csv = CSVController.readCSV("kohaIBICT/dadosExemplares.tsv", "\t");
		int cont = 0;
		
		System.out.println(records.size());
		
		for(int i = 2; i < csv.size(); i++)
		{
			String[] exemplar = csv.get(i);
			// busca pelo registro com o kardex indicado em exemlar[8]
			Record record = marcAcervo.getRecordWithKardex(exemplar[8]);
			
			// se o registro não foi encontrado, criar um novo
			if(record == null)
			{
				System.err.println("kardex " + exemplar[8] + " não encontrado.");
				continue;
			}
			
			ArrayList<Subfield> subs = new ArrayList<Subfield>();
			
			subs.add(new Subfield("h", exemplar[4] + "," + exemplar[5] + "," + exemplar[6] + "," + exemplar[7], Constant.subfieldDenomination));
			subs.add(new Subfield("t", exemplar[9], Constant.subfieldDenomination));
			if(exemplar.length > 10)
				subs.add(new Subfield("p", exemplar[10], Constant.subfieldDenomination));
			if(exemplar.length > 11)
				subs.add(new Subfield("x", exemplar[11], Constant.subfieldDenomination));
			
			
			
			Field field952 = new Field("952", " ", " ", subs, Constant.dataFieldDenomination);
			
			record.addField(field952);
			
			if(cont % 200 == 0)
			System.out.println(cont);
			cont++;
		}
		


		
		
		XMLHandler.generateXMLFile(records, "kohaIBICT/ACERVOibict2.xml");
		System.out.println("fim: " + records.size() + " registros.");
	}
	
	/**
	 * Criação de exemplares no Koha do IBICT
	 * @param args
	 * @throws Exception 
	 */
	public static void mainKohaIBICT(String[] args) throws Exception {
		MARC marcAcervo = new MARC("kohaIBICT/ACERVOibict.xml");
		ArrayList<Record> records = marcAcervo.removeField("952");
		
		// colunas correspondentes do cst:
		// [4] 952 $h
		// [5] 952 $h (concatenado com vírgula)
		// [6] 952 $h (concatenado com vírgula)
		// [7] 952 $h (concatenado com vírgula)
		// [9] 952 $t
		// [10] 952 $p
		// [11] 952 $x
		ArrayList<String[]> csv = CSVController.readCSV("kohaIBICT/dadosExemplares.tsv", "\t");
		int cont = 0;
		
		System.out.println(records.size());
		
		for(int i = 2; i < csv.size(); i++)
		{
			String[] exemplar = csv.get(i);
			// busca pelo registro com o kardex indicado em exemlar[8]
			Record record = marcAcervo.getRecordWithKardex(exemplar[8]);
			
			// se o registro não foi encontrado, criar um novo
			if(record == null)
			{
				System.err.println("kardex " + exemplar[8] + " não encontrado.");
				continue;
			}
			
			ArrayList<Subfield> subs = new ArrayList<Subfield>();
			
			subs.add(new Subfield("h", exemplar[4] + "," + exemplar[5] + "," + exemplar[6] + "," + exemplar[7], Constant.subfieldDenomination));
			subs.add(new Subfield("t", exemplar[9], Constant.subfieldDenomination));
			if(exemplar.length > 10)
				subs.add(new Subfield("p", exemplar[10], Constant.subfieldDenomination));
			if(exemplar.length > 11)
				subs.add(new Subfield("x", exemplar[11], Constant.subfieldDenomination));
			
			
			
			Field field952 = new Field("952", " ", " ", subs, Constant.dataFieldDenomination);
			
			record.addField(field952);
			
			if(cont % 200 == 0)
			System.out.println(cont);
			cont++;
		}
		


		
		
		XMLHandler.generateXMLFile(records, "kohaIBICT/ACERVOibict2.xml");
		System.out.println("fim: " + records.size() + " registros.");
	}
	
	/**
	 * Limpeza dos resgistros de autoridade do MMFDH.
	 * Problema: apresentam subcampos duplicados.
	 * @param args
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException
	 */
	public static void mainLimpezaMMFDH(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		MARC marcAcervo = new MARC("LimpezaAutoridadesMMFDH/autoridadesMMFDH2.xml");
		ArrayList<Record> records = marcAcervo.getRecords();
		int cont = 0;
		
		System.out.println(records.size());
		for(Record r : records)
		{
			if(cont % 200 == 0)
				System.out.println(cont);
			cont++;
		}
		
		System.out.println("passou");
		
		
		XMLHandler.generateXMLFile(records, "LimpezaAutoridadesMMFDH/autoridadesMMFDH2Limpas.xml");
		System.out.println("fim");
	}
	
	public static void mainBDB(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		MARC marcAcervo = new MARC("BDB/mrc_bdb.xml");
		ArrayList<Record> records = marcAcervo.getRecords();
		ArrayList<Record> autoridades = new ArrayList<Record>();
		int cont = 0;
		
		System.out.println(records.size());
		for(Record r : records)
		{
			r.mergeMultipleOccurrencesOfField("082");
			
//			if(cont % 1000 == 0)
//				System.out.println(cont);
//			cont++;
		}
		
		System.out.println(records.size());
		for(Record r : records)
		{
			if(!r.getLeaderField().getFieldName().contentEquals(Constant.leaderDenomination))
				System.out.println("AQUI");
		}
		
		// armazena a lista de autoridades a ser adicionada na lista final.
		ArrayList<Record> tempAuthorities;
		
		System.out.println("parte 1");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("100", "100");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 2");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("110", "110");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 3");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("111", "111");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 4");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("130", "130");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 5");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("653", "150");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 6");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("650", "150");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 7");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("700", "100");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 8");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("710", "110");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 9");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("711", "111");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 10");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("730", "130");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		
		// remove autoridades que duplicaram por conta de origens diferentes
		ArrayList<Record> autoridades2 = new ArrayList<Record>();
		autoridades2.addAll(removeDuplicated(autoridades));
		
		String[] tiposAutoridades = {"CORPO_NAME", "GEOGR_NAME", "MEETI_NAME", "PERSO_NAME", "TOPIC_TERM", "UNIF_TITLE"};
		
		for(String tipo : tiposAutoridades)
		{
			ArrayList<Record> autTemp = new ArrayList<Record>();
			for(Record aut : autoridades2)
			{
				if(aut.getField("942").get(0).getSubfield("c").get(0).getValue().contentEquals(tipo))
					autTemp.add(aut);
			}
			XMLHandler.generateXMLFile(autTemp, "BDB/autoridadesBDB" + tipo + ".xml");
		}
		
		XMLHandler.generateXMLFile(records, "BDB/bdb2.xml");
//		XMLHandler.generateXMLFile(autoridades, "BDB/autoridadesBDB2.xml");
		System.out.println("fim");
	}
	
	/**
	 * Remove as autoridades duplicadas (aquelas que sao identicas) da lista autoridades.
	 * @param autoridades
	 * @param tempAuthorities
	 * @return
	 */
	private static ArrayList<Record> removeDuplicated(ArrayList<Record> autoridades) {
		
		ArrayList<Record> result = new ArrayList<Record>();
		String[] fieldsToAvoid = {Constant.leaderDenomination, "001", "003", "005", "008"};
		boolean inserir;
		String conteudoAutAtual;
		
		for(int i = 0; i < autoridades.size(); i++)
		{
			inserir = true;
			
			for(Record aut : result)
			{
				if(aut.getAuthorityMainFieldValue().contentEquals(autoridades.get(i).getAuthorityMainFieldValue()))
				{
					conteudoAutAtual = autoridades.get(i).getRecordAsXMLWithoutIndicatedFields(fieldsToAvoid);
					if(aut.getRecordAsXMLWithoutIndicatedFields(fieldsToAvoid).contentEquals(conteudoAutAtual)) 
					{
						inserir = false;
						break;
					}
				}
			}
			if(inserir)
				result.add(autoridades.get(i));
			
			if(i % 300 == 0)
				System.out.println(i + "/" + autoridades.size());
		}
		
		return result;
	}

	public ArrayList<Record> cleanDuplicatedAuthorities(ArrayList<Record> autoridades)
	{
		ArrayList<Record> autoridadesNaoDuplicadas = new ArrayList<Record>();
		
		for (Record autAvaliada : autoridades)
		{
			for (Record aut : autoridades)
			{
				if(areEqualsAuthorities(autAvaliada, aut))
					break;
			}
		}
		
		return autoridadesNaoDuplicadas;
	}
	
	
	private boolean areEqualsAuthorities(Record autAvaliada, Record aut) {
		
		ArrayList<String> fieldsAutAvaliada = autAvaliada.getFieldNameList();
		ArrayList<String> fieldsAut = autAvaliada.getFieldNameList();
		
		return false;
	}

	public static void mainAN(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		MARC marcAcervo = new MARC("arquivoNacional/kohaAN1.xml");
		ArrayList<Record> records = marcAcervo.getRecords();
		
		String[] campos001 = {"106738",
				"108039",
				"108040",
				"108064",
				"108075",
				"108076",
				"108078",
				"108080",
				"108082",
				"108415",
				"108845",
				"110906",
				"110911",
				"110914",
				"110918",
				"110919",
				"110928",
				"110932",
				"110935",
				"110939",
				"110954",
				"110957",
				"110976",
				"110980",
				"111042",
				"111052",
				"111053",
				"111127",
				"111129",
				"111161",
				"111163",
				"111167",
				"111223",
				"111226",
				"111250",
				"111252",
				"111263",
				"111266",
				"111313",
				"111316",
				"111356",
				"111998",
				"112121",
				"112133",
				"112207",
				"114062",
				"114063",
				"114064",
				"117602",
				"118895",
				"121531",
				"121532",
				"121539",
				"121927"};
		
		for(Record r : records)
		{
			for(String campo001 : campos001)
			{
				if(r.getField("001").get(0).getValue().contentEquals(campo001))
				{
					String c999 = r.getField("999").get(0).getSubfield("c").get(0).getValue(); 
					System.out.println(c999 + "," + campo001 + ".jpg");
				}
			}
		}
		
		System.out.println("fim");
	}
	
	/**
	 * Limpeza de autoridades do MMFDH
	 * @param args
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException
	 */
	public static void main1(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException
	{
		// abre arquivo com as autoridades
		MARC marcAutoridades = new MARC(Constant.inputAutoridadeMMFDH);
		ArrayList<Record> recordsAutoridades = marcAutoridades.getRecords();
		System.out.println("Marc de autoridades carregado");
		
		// abre arquivo do catalogo
		MARC marcAcervo = new MARC(Constant.inputCatalogoMMFDH);
		ArrayList<Record> recordsAcervo = marcAcervo.getRecords();
		System.out.println("Marc de acervo carregado");
		
		// abre arquivo do catalogo
		MARC marcCorrecoes = new MARC(Constant.inputCorrecoes);
		ArrayList<Record> recordsCorrecoes = marcCorrecoes.getRecords();
		System.out.println("Marc de acervo carregado");
		
		// Armazena listas de autoridades separadas por tipo
		HashMap<String, ArrayList<Record>> tipoAutoridades = new HashMap<String, ArrayList<Record>>();
		ArrayList<Record> erros = new ArrayList<Record>();
		
		
		// print todos os IDs de autoridade para remocao do Koha do MMFDH
//		System.out.println("**************");
//		for(Record rem : recordsAutoridades)
//		{
//			System.out.println(rem.getField("001").get(0).getValue());
//		}
		
		
		System.out.println(recordsAutoridades.size() + " autoridades");
		
		// substituicao das correcoes feitas pela Ingrid
		for(Record correto : recordsCorrecoes)
		{
			String idCorreto = correto.getField("001").get(0).getValue();
			
			// percorre lista de autoridades e remove as incorretas
			for(int r = 0; r < recordsAutoridades.size(); r++)
			{
				Record aut = recordsAutoridades.get(r);
				
				if(aut.getField("001").get(0).getValue().contentEquals(idCorreto))
				{
					recordsAutoridades.remove(r);
					r--;
					break;
				}
			}
		}
		// em seguida, adiciona as correcoes
		recordsAutoridades.addAll(recordsCorrecoes);
		System.out.println("correcoes substituidas"); //---------------------------------------------------------------------------------------------------------
		
		// percorre lista de autoridades
		for(int r = 0; r < recordsAutoridades.size(); r++)
		{
			Record autoridadeAtual = recordsAutoridades.get(r);
			
			// realizacao de testes e verificacoes
			if(apresentaErros(autoridadeAtual))
			{
				erros.add(autoridadeAtual);
				continue;
			}
			
			
			// recebe a string de tipo da autoridade em questao
			String tipo = autoridadeAtual.getField("942").get(0).getSubfield("a").get(0).getValue();
			
			// autoridade sem sao inseridas na lista de erros
			if(tipo.contentEquals(""))
			{
				erros.add(autoridadeAtual);
				continue;
			}
			
			// adiciona a autoridade na lista do tipo correspondente, criando as separacoes
			if(!tipoAutoridades.containsKey(tipo))
				tipoAutoridades.put(tipo, new ArrayList<Record>());
			tipoAutoridades.get(tipo).add(autoridadeAtual);
			
//			String tipo = autoridadeAtual.getField("942").get(index)
		}
		System.out.println("Autoridades separadas por tipo"); //---------------------------------------------------------------------------------------------------------
		
		// armazena autoridades com $a vazio
		ArrayList<Record> aVazio = new ArrayList<Record>();
		
		// percorre os grupos de autoridades para verificar erros
		for(String tipo : tipoAutoridades.keySet())
		{
			String campoPrincipal = campoPrincipalDoTipo(tipo);
			
			// coloca na lista de erros as autoridades que nao apresentam o campo obrigatorio (150, 100, 111, dependendo da autoridade...) preenchido
			for( int a = 0; a < tipoAutoridades.get(tipo).size(); a++)
			{
				Record autoridadeAtual = tipoAutoridades.get(tipo).get(a);
				
				// verifica a existencia de erros nos campos obrigatorios
				if(!temCampoObrigatorio(autoridadeAtual, tipo))
				{
					// remove a autoridade de sua lista correspondente e insere na lista com erros
					erros.add(tipoAutoridades.get(tipo).remove(a));
					a--;
					continue;
				}
				
				
				// remove os espacos em branco e caracteres especiais
				autoridadeAtual.getField(campoPrincipal).get(0).getSubfield("a").get(0).setValue(limpaCaracteres(autoridadeAtual.getField(campoPrincipal).get(0).getSubfield("a").get(0).getValue()));
				
				// separa autoridades com $a vazio
				if(autoridadeAtual.getField(campoPrincipal).get(0).getSubfield("a").get(0).getValue().contentEquals(""))
				{
					aVazio.add(tipoAutoridades.get(tipo).remove(a));
					a--;
					continue;
				}
			}
		}
		System.out.println("Verificacao de erros realizada, remocao de espacos em branco e caracteres especiais"); //---------------------------------------------------------------------------------------------------------
		
		// armazena a lista de autoridades por tipo, sem duplicadas
		HashMap<String, ArrayList<Record>> tipoAutoridades2 = new HashMap<String, ArrayList<Record>>();
		// armazena as autoridades duplicadas que serao removidas do arquivo principal de autoridades
		ArrayList<Record> duplicadasRemovidas = new ArrayList<Record>();
		
		// percorre os grupos de autoridades e remove as duplicadas
		for(String tipo : tipoAutoridades.keySet())
		{
			//pula as autoridades do tipo "CORPO_NAME" para tratamento posterior
			if(tipo.contentEquals("CORPO_NAME"))
				continue;
			
			// armazena as autoridades por nome, para encontrar as duplicadas
			HashMap<String, ArrayList<Record>> autoridadesPorNome = new HashMap<String, ArrayList<Record>>();
			String campoPrincipal = campoPrincipalDoTipo(tipo);
			
			// inicializa o tipo atual no hash que armazena as autoridades sem duplicadas
			tipoAutoridades2.put(tipo, new ArrayList<Record>());
			
			// coloca na lista de erros as autoridades que nao apresentam o campo obrigatorio (150, 100, 111, dependendo da autoridade...) preenchido
			for( int a = 0; a < tipoAutoridades.get(tipo).size(); a++)
			{
				Record autoridadeAtual = tipoAutoridades.get(tipo).get(a);
				
				String nome = autoridadeAtual.getField(campoPrincipal).get(0).getSubfield("a").get(0).getValue().toUpperCase();
				
				// constroi o hash 'autoridadesPorNome'
				if(!autoridadesPorNome.containsKey(nome))
					autoridadesPorNome.put(nome, new ArrayList<Record>());
				autoridadesPorNome.get(nome).add(autoridadeAtual);
			}
			
			// percorre cada lista de nome de autoridade e trata aquelas com mais de um elemento,
			// mantendo a autoridade com maior quantidade de subcampos preenchidos
			for(String nome : autoridadesPorNome.keySet())
			{
				if(autoridadesPorNome.get(nome).size() != 1)
				{
					// verifica qual deve ser mantida
					// lista de autoridades duplicadas
					ArrayList<Record> duplicadas = autoridadesPorNome.get(nome);
					
					// variaveis para auxiliar na escolha da autoridade a ser mantida
					Record autoridadeMantida = null;
					int maxSubCampos = 0;
					
					// percorre a lista e verifica aquela que tem mais campos
					for(Record aut : duplicadas)
					{
						int totalSubcampos = 0;
						for(Field campo : aut.getFields())
						{
							totalSubcampos += campo.getSubfields().size();
						}
						
						// mantem autoridade com mais subcampos
						if(totalSubcampos > maxSubCampos)
						{
							if(autoridadeMantida!= null)
								duplicadasRemovidas.add(autoridadeMantida);
							autoridadeMantida = aut;
							maxSubCampos = totalSubcampos;
						}else
						{
							duplicadasRemovidas.add(aut);
						}
					}
					
					// verifica erro
					if(autoridadeMantida == null)
						System.err.println("ERROOO na autoridade mantida dentre as duplicadas");
						// adiciona a autoridadeMantida na lista de autoridades
					tipoAutoridades2.get(tipo).add(autoridadeMantida);
					
					// atualiza no catalogo os ids daquelas que vao sair
					
				}else // adiciona caso encontre apenas uma autoridade por nome
				{
					tipoAutoridades2.get(tipo).add(autoridadesPorNome.get(nome).get(0));
				}
			}
			
		}
		System.out.println("Autoridades duplicadas removidas");  //---------------------------------------------------------------------------------------------------------
		
		// Armazena as diferentes autoridades do tipo corpo_name, considerando multiplos campos $b
		HashMap<String, ArrayList<HashMap<String, Record>>> camposNaoRepetidos = new HashMap<String, ArrayList<HashMap<String,Record>>>();
		ArrayList<Record> autoridadesNaoRepetidas = new ArrayList<Record>();
		ArrayList<Record> autoridadesRepetidas = new ArrayList<Record>();
		String campoPrincipal = campoPrincipalDoTipo("CORPO_NAME");
		HashMap<String, Record> paraRemoverDoHashDeCampos = null;
		
		// realiza correcao das autoridades do tipo "CORPO_NAME", que tambem consideram o campo b
		for( int a = 0; a < tipoAutoridades.get("CORPO_NAME").size(); a++)
		{
			Record autoridade = tipoAutoridades.get("CORPO_NAME").get(a);
			
			String valorPrincipal = autoridade.getField(campoPrincipal).get(0).getSubfield("a").get(0).getValue();
			
			// insere a autoridade do campo principal, caso nao exista
			if(!camposNaoRepetidos.containsKey(valorPrincipal))
				camposNaoRepetidos.put(valorPrincipal, new ArrayList<HashMap<String,Record>>());
			
			// constroi hash com os valores nos subcampos $b
			HashMap<String, Record> camposBAutoridadeAtual = new HashMap<String, Record>();
			
			// se nao tem campo B, define o hash como null
			if(!autoridade.getField(campoPrincipal).get(0).hasSubfield("b"))
				camposBAutoridadeAtual.put(null, autoridade);
			else {
				for(Subfield subB : autoridade.getField(campoPrincipal).get(0).getSubfield("b"))
				{
					camposBAutoridadeAtual.put(subB.getValue(), autoridade);
				}
			}
			
			// confere a existencia dos subcampos b
			boolean encontrado = false;
			Record recordEncontrado = null;
			
			// percorre todas as autoridades inseridas no hash em busca da autoridade atual
			for(HashMap<String, Record> camposBAutoridadeNaoRepetida : camposNaoRepetidos.get(valorPrincipal))
			{
				// se for encontrado um conjunto igual, para a busca
				if(subcamposBAvaliadosEhIgualSubcamposBatualComRegistro(camposBAutoridadeNaoRepetida, camposBAutoridadeAtual))
				{
					encontrado = true;
					Iterator<String> it = camposBAutoridadeNaoRepetida.keySet().iterator();
					recordEncontrado = camposBAutoridadeNaoRepetida.get(it.next());
					paraRemoverDoHashDeCampos = camposBAutoridadeNaoRepetida;
					break;
				}
			}
			
			// se for encontrado conjunto igual, adiciona ao grupo de autoridades repetidas
			// se nao for encontrado, adiciona ao grupo de campos nao repetidos e a lista de autoridades nao repetidas
			if(!encontrado)
			{
				camposNaoRepetidos.get(valorPrincipal).add(camposBAutoridadeAtual);
				autoridadesNaoRepetidas.add(autoridade);
			}else
			{
				if(autoridade.getTotalNumberOfSubfields() > recordEncontrado.getTotalNumberOfSubfields())
				{
					autoridadesNaoRepetidas.remove(recordEncontrado);
					autoridadesNaoRepetidas.add(autoridade);
					autoridadesRepetidas.add(recordEncontrado);
					
					// troca a referencia dos subcampos $b pelo novo registro
					camposNaoRepetidos.get(valorPrincipal).remove(paraRemoverDoHashDeCampos);
					camposNaoRepetidos.get(valorPrincipal).add(camposBAutoridadeAtual);
				}else
				{
					autoridadesRepetidas.add(autoridade);
				}
			}
		}
		System.out.println("Autoridades 'CORPO_NAME' corrigidas"); //---------------------------------------------------------------------------------------------------------

		// corrige IDs de autoridades no acervo, exceto as autoridades do tipo 'corpo_name'
		// tambem trata a limpeza de caracteres
		ArrayList<Record> autoridadeNaoEncontrada = corrigeIDdeAutoridadesNoAcervo(tipoAutoridades2, recordsAcervo);
		System.out.println("Acervo corrigido");
		
		// corrige as autoridades do tipo 'corpo_name' considerando os diferentes campos $b
		corrigeIDdeAutoridadesCorpoNameNoAcervo(autoridadesNaoRepetidas, recordsAcervo);
		System.out.println("Autoridades CORPO_NAME usadas no acervo corrigidas");
		
		// adicao de virgula em PERSO_NAME com $d
		for(Record perso : tipoAutoridades2.get("PERSO_NAME"))
		{
			// se PERSO_NAME tem campo $d, adicionar virgula ao final de $a
			if(perso.hasField("100") && perso.getField("100").get(0).hasSubfield("d"))
			{
				String autID = perso.getField("001").get(0).getValue();
				String novoNome = perso.getField("100").get(0).getSubfield("a").get(0).getValue() + ",";
				
				// seta o nome da autoridade com virgula
				perso.getField("100").get(0).getSubfield("a").get(0).setValue(novoNome);
				
				// percorre catalogo e busca referencias a autoridade alterada
				for(Record rec : recordsAcervo)
				{
					if (rec.hasField("650"))
					{
						for(Field campo650 : rec.getField("650"))
						{
							if(campo650.hasSubfield("9"))
							{
								if(campo650.getSubfield("9").get(0).getValue().contentEquals(autID))
									campo650.getSubfield("a").get(0).setValue(novoNome);
							}
						}
					}
				}
			}
		}
		System.out.println("Virgulas inseridas em autoridades 'PERSO_NAME'"); //---------------------------------------------------------------------------------------------------------
		
		ArrayList<Record> topicTermMaiuscula = new ArrayList<Record>();
		String tipoTopicTerm = "TOPIC_TERM";
		
		// adicao das autoridades CORPO_NAME na lista de autoridades a gerar arquivo
		tipoAutoridades2.put("CORPO_NAME", autoridadesNaoRepetidas);
		
		// separar autoridades do tipo "TOPIC_TERM" que estao maiusculas
		for(int r = 0; r < tipoAutoridades2.get(tipoTopicTerm).size(); r++)
		{
			Record auto = tipoAutoridades2.get(tipoTopicTerm).get(r);
			if(auto.getField(campoPrincipalDoTipo(tipoTopicTerm)).get(0).getSubfield("a").get(0).getValue().contentEquals(
					auto.getField(campoPrincipalDoTipo(tipoTopicTerm)).get(0).getSubfield("a").get(0).getValue().toUpperCase()))
			{
				topicTermMaiuscula.add(tipoAutoridades2.get(tipoTopicTerm).remove(r));
				r--;
			}
		}
		System.out.println("Maiusculas e minusculas separadas"); //---------------------------------------------------------------------------------------------------------
		
		// cria arquivos de autoridades para cada tipo diferente. Se usar tipoAutoridades, pega as duplicadas, e tipoAutoridades2 nao considera as duplicadas
		for(String tipo : tipoAutoridades2.keySet())
		{	
			System.out.println(tipoAutoridades2.get(tipo).size() + " autoridades do tipo " + tipo);
			XMLHandler.generateXMLFile(tipoAutoridades2.get(tipo), Constant.outputAutoridadeMMFDH + tipo + ".xml");
		}
		
		// realiza correcoes de ID no acervo
		correcaoDeIDAcervoNoCorpoName(recordsAcervo);
		correcaoDeIDAcervoWithCorpoName(recordsAcervo);
		
		
		System.out.println(erros.size() + " autoridades com erro");
		XMLHandler.generateXMLFile(erros, Constant.outputAutoridadeMMFDH + "erros.xml");
		System.out.println(duplicadasRemovidas.size() + " autoridades duplicadas removidas");
		XMLHandler.generateXMLFile(duplicadasRemovidas, Constant.outputAutoridadeMMFDH + "duplicadasRemovidas.xml");
		System.out.println(aVazio.size() + " autoridades de valor vazio");
		XMLHandler.generateXMLFile(aVazio, Constant.outputAutoridadeMMFDH + "valorVazio.xml");
		System.out.println(topicTermMaiuscula.size() + " autoridades topic_term maiusculas");
		XMLHandler.generateXMLFile(topicTermMaiuscula, Constant.outputAutoridadeMMFDH + "TOPIC_TERM_maiuscula.xml");
		System.out.println(autoridadeNaoEncontrada.size() + " autoridades do acervo nao encontradas");
		XMLHandler.generateXMLFile(autoridadeNaoEncontrada, Constant.outputAutoridadeMMFDH + "autoridadeNaoEncontrada.xml");
		System.out.println(recordsAcervo.size() + " registros do acervo salvos");
		XMLHandler.generateXMLFile(recordsAcervo, Constant.outputAutoridadeMMFDH + "acervoCorrigido5.xml");
		System.out.println(autoridadesRepetidas.size() + " autoridades CORPO_NAME repetidas");
		XMLHandler.generateXMLFile(autoridadesRepetidas, Constant.outputAutoridadeMMFDH + "corpoNameRepetidas.xml");

		System.out.println("Fim!\n");
		
		// lista de IDs para remover
//		System.out.println("Autoridades (ID) para remover:");
//		System.out.println("4626");
//		System.out.println("8533");
//		System.out.println("5068");
//		System.out.println("3");
//		System.out.println("1963");
//		System.out.println("3651");
//		System.out.println("4165");
//		for(Record rem : duplicadasRemovidas)
//		{
//			System.out.println(rem.getField("001").get(0).getValue());
//		}
		
	}
	
	public static void correcaoDeIDAcervoWithCorpoName(ArrayList<Record> recordsAcervo) throws ParserConfigurationException, SAXException, IOException, TransformerException
	{
		// abre arquivo com as autoridades
		MARC marcAutoridades = new MARC(Constant.inputAutoridadeAtualizadaCorpoNameMMFDH3);
		ArrayList<Record> recordsAutoridades = marcAutoridades.getRecords();
		System.out.println("Marc de autoridades carregado");

		ArrayList<Record> autoridadesCorpoName = new ArrayList<Record>();
		
		// corrige IDs de autoridades no acervo
		// tambem trata a limpeza de caracteres
		// corrige as autoridades do tipo 'corpo_name' considerando os diferentes campos $b
		for(Record aut : recordsAutoridades)
		{
			if(aut.getField("942").get(0).getSubfield("a").get(0).getValue().contentEquals("CORPO_NAME"))
				autoridadesCorpoName.add(aut);
		}
		
		corrigeIDdeAutoridadesCorpoNameNoAcervo(autoridadesCorpoName, recordsAcervo);
		System.out.println("Autoridades CORPO_NAME usadas no acervo corrigidas");

		
		XMLHandler.generateXMLFile(recordsAcervo, Constant.outputAutoridadeMMFDH + "acervoCorrigido4.xml");
		
	}
	
	/**
	 * Limpeza de autoridades do MMFDH
	 * @param recordsAcervo 
	 * @param args
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException
	 */
	public static void correcaoDeIDAcervoNoCorpoName(ArrayList<Record> recordsAcervo) throws ParserConfigurationException, SAXException, IOException, TransformerException
	{
		// abre arquivo com as autoridades
		MARC marcAutoridades = new MARC(Constant.inputAutoridadeAtualizadaMMFDH);
		ArrayList<Record> recordsAutoridades = marcAutoridades.getRecords();
		System.out.println("Marc de autoridades carregado");
		
		// corrige IDs de autoridades no acervo
		// tambem trata a limpeza de caracteres
		corrigeIDdeAutoridadesNoAcervo(recordsAutoridades, recordsAcervo);
		System.out.println("Acervo corrigido");
		
		XMLHandler.generateXMLFile(recordsAcervo, Constant.outputAutoridadeMMFDH + "acervoCorrigido2.xml");
		
	}
	


	private static boolean subcamposBAvaliadosEhIgualSubcamposBatualComRegistro(HashMap<String, Record> avaliado, HashMap<String, Record> atual)
	{
//		// se as duas forem null ao mesmo tempo, retornar true
//		if(avaliado==null && atual==null)
//			return true;
//		
//		// se uma for null e a outra nao, retornar falso
//		if(avaliado==null)
//			return false;
//		if(atual==null)
//			return false;
		
		// se as listas possuem tamanhos diferentes, nao sao iguais
		if(atual.keySet().size() != avaliado.keySet().size())
			return false;
		
		// realiza a comparacao em dois sentidos
		for(String at : atual.keySet())
			if(!avaliado.containsKey(at))
				return false;
		
		for(String at : avaliado.keySet())
			if(!atual.containsKey(at))
				return false;
		
		return true;
	}
	
	private static boolean subcamposBAvaliadosEhIgualSubcamposBatual(HashMap<String, String> avaliado, HashMap<String, String> atual)
	{
		// se as duas forem null ao mesmo tempo, retornar true
		if(avaliado==null && atual==null)
			return true;
		
		// se uma for null e a outra nao, retornar falso
		if(avaliado==null)
			return false;
		if(atual==null)
			return false;
		
		// se as listas possuem tamanhos diferentes, nao sao iguais
		if(atual.keySet().size() != avaliado.keySet().size())
			return false;
		
		// realiza a comparacao em dois sentidos
		for(String at : atual.keySet())
			if(!avaliado.containsKey(at))
				return false;
		
		for(String at : avaliado.keySet())
			if(!atual.containsKey(at))
				return false;
		
		return true;
	}
	
	/**
	 * Le a autoridade atual e remove caracteres ponto, virgula e espacos em branco, no comeco e no final
	 * Considera como escecao o caso de abreviaturas
	 * @param tipo
	 * @return
	 */
	private static String limpaCaracteres(String valor) {
		
		// remove espacos em branco
		valor = valor.strip();
		
		// se for vazio, retornar
		if(valor.contentEquals(""))
			return valor;
		
		// excecao do caso de abreviatura
		// se o ultimo caractere for ponto, precedido de letra, precedida de espaco ou ponto, eh sigla
		if(valor.length() > 2)
			if(valor.charAt(valor.length() - 1) == '.' && Character.isLetter(valor.charAt(valor.length() - 2)) && (valor.charAt(valor.length() - 3) == '.' || valor.charAt(valor.length() - 3) == ' '))
				return valor;
		
		// limpa no final
		while(valor.charAt(valor.length() - 1) == ' '
				|| valor.charAt(valor.length() - 1) == '.'
				|| valor.charAt(valor.length() - 1) == '-'
				|| valor.charAt(valor.length() - 1) == ',')
		{
			valor = valor.substring(0, valor.length() - 1).strip();
		}
		
		// limpa no comeco
				while(valor.charAt(0) == ' '
						|| valor.charAt(0) == '.'
						|| valor.charAt(0) == '-'
						|| valor.charAt(0) == ',')
				{
					valor = valor.substring(1).strip();
				}
		
		return valor;
	}

	/**
	 * Corrige o ID das autoridades nos registros do acervo e retorna aquelas que nao tem autoridade
	 * correspondente
	 * @param tipoAutoridades
	 * @param recordsAcervo
	 * @return
	 */
	private static void corrigeIDdeAutoridadesNoAcervo(ArrayList<Record> recordAutoridades,
			ArrayList<Record> recordsAcervo) {

		int count = 0;
		
		for(Record acervo : recordsAcervo)
		{
			// print para controle de execucao
//			count++;
//			if(count % 500 == 0)
//				System.out.println(count + "/" + recordsAcervo.size());
				
			// percorre a lista de autoridades nos campos de autoridades dos RB
			String[] camposParaVerificarAutoridades = {"100", "110", "111", "130", "600", "610", "650", "651", "700", "710"};
			
			for(String campoParaVerificar : camposParaVerificarAutoridades)
			{
				if(acervo.hasField(campoParaVerificar))
				{
					for(Field campoX : acervo.getField(campoParaVerificar))
					{
						// se o campo nao tem subcampo a ou nao tem subcampo 9, adiciona a lista de registros com autoridade errada  
						if(!campoX.hasSubfield("a"))
							continue;
						
						// recebe o valor do campo e limpa
						String valor = limpaCaracteres(campoX.getSubfield("a").get(0).getValue());
						// armazena de volta o valor do campo, com as correcoes de caracteres especiais
						campoX.getSubfield("a").get(0).setValue(valor);
						
						// procura 'valor' na lista de autoridades
						for(Record aut : recordAutoridades)
						{
							// o tratamento de autoridades "CORPO_NAME" é feito separado
							String tipo = aut.getField("942").get(0).getSubfield("a").get(0).getValue();
							if(tipo.contentEquals("CORPO_NAME"))
								continue;
							
							String campoPrincipal = campoPrincipalDoTipo(tipo);
							String nomeAutoridade = aut.getField(campoPrincipal).get(0).getSubfield("a").get(0).getValue();
							
							// se o nome da autoridade no acervo corresponde ao nome da autoridade no banco de autoridades, setar o id correto
							if(valor.toUpperCase().contentEquals(nomeAutoridade.toUpperCase()))
							{
								if(!campoX.hasSubfield("9"))
								{
									Subfield subf = new Subfield("9", aut.getField("001").get(0).getValue(), aut.getField("942").get(0).getSubfield("a").get(0).getXmlNodeName());
									campoX.addSubfield(subf);
								}else
								{
									campoX.getSubfield("9").get(0).setValue(aut.getField("001").get(0).getValue());
								}
							}				
						}
					}
				}
			}	
		}
	}
	
	private static void corrigeIDdeAutoridadesCorpoNameNoAcervo(ArrayList<Record> autoridadesCorpoName,
			ArrayList<Record> recordsAcervo) {
		
		String campoPrincipalDoTipo = campoPrincipalDoTipo("CORPO_NAME");
		int cont = 0;
		
		for(Record acervo : recordsAcervo)
		{
			String[] camposParaVerificarAutoridades = {"110", "610", "710"};
			
			for(String campoParaVerificar : camposParaVerificarAutoridades)
			{
				if(!acervo.hasField(campoParaVerificar))
					continue;
				
				for(Field campoAtual : acervo.getField(campoParaVerificar))
				{
					if(!campoAtual.hasSubfield("a"))
						continue;
					
					String acervoCampoA = limpaCaracteres(campoAtual.getSubfield("a").get(0).getValue());
					// constroi hash com os valores nos subcampos $b
					
					
					for(Record autoridadeCN : autoridadesCorpoName)
					{
						String autoCampoA = limpaCaracteres(autoridadeCN.getField(campoPrincipalDoTipo).get(0).getSubfield("a").get(0).getValue());
						HashMap<String, String> acervoB, autB;
						
						if(acervoCampoA.contentEquals(autoCampoA))
						{
							// se o subcampo $a do RA e do RB sao iguais, compara os campos B
							// constroi a lista de subcampos $b do RB
							if(!campoAtual.hasSubfield("b"))
								acervoB = null;
							else {
								acervoB = new HashMap<String, String>();
							
								for(Subfield subB : campoAtual.getSubfield("b"))
								{
									acervoB.put(subB.getValue(), "");
								}
							}
	
							// constroi a lista de subcampos $b do RB
							if(!autoridadeCN.getField(campoPrincipalDoTipo).get(0).hasSubfield("b"))
								autB = null;
							else {
								autB = new HashMap<String, String>();
							
								for(Subfield subB : autoridadeCN.getField(campoPrincipalDoTipo).get(0).getSubfield("b"))
								{
									autB.put(subB.getValue(), "");
								}
							}
							
							// compara as duas listas de subcampos
							if(subcamposBAvaliadosEhIgualSubcamposBatual(acervoB, autB))
							{
								// se as duas listas de subcampos forem iguais, altera o ID no acervo de acordo com o ID da autoridade
								if(!campoAtual.hasSubfield("9"))
									campoAtual.addSubfield(new Subfield("9", autoridadeCN.getField("001").get(0).getValue(), autoridadeCN.getField("942").get(0).getSubfield("a").get(0).getXmlNodeName()));
								campoAtual.getSubfield("9").get(0).setValue(autoridadeCN.getField("001").get(0).getValue());
								cont++;
							}
							
						}
					}
				}
			}
		}
		System.out.println(cont + " registros corrigidos");
	}
	
	/**
	 * Corrige o ID das autoridades nos registros do acervo e retorna aquelas que nao tem autoridade
	 * correspondente
	 * @param tipoAutoridades
	 * @param recordsAcervo
	 * @return
	 */
	private static ArrayList<Record> corrigeIDdeAutoridadesNoAcervo(HashMap<String, ArrayList<Record>> tipoAutoridades,
			ArrayList<Record> recordsAcervo) {
		
		// armazena a lista de registros com erros
		ArrayList<Record> registroDeAutoridadesComErro = new ArrayList<Record>();
		boolean encontrado = false;
		int count = 0;
		
		for(Record acervo : recordsAcervo)
		{
			// print para controle de execucao
			count++;
			if(count % 500 == 0)
				System.out.println(count + "/" + recordsAcervo.size());
			
//			if(!acervo.hasField("650") && !acervo.hasField("651"))
//				continue;
				
			encontrado = false;
			// percorre a lista de autoridades nos campos de autoridades dos RB
			String[] camposParaVerificarAutoridades = {"100", "110", "111", "130", "600", "610", "650", "651", "700", "710"};
			
			for(String campoParaVerificar : camposParaVerificarAutoridades)
			{
				if(acervo.hasField(campoParaVerificar))
				{
					for(Field campoX : acervo.getField(campoParaVerificar))
					{
						// se o campo 650 nao tem subcampo a ou nao tem subcampo 9, adiciona a lista de registros com autoridade errada  
						if(!campoX.hasSubfield("a"))
							continue;
						
						// recebe o valor do campo e limpa
						String valor = limpaCaracteres(campoX.getSubfield("a").get(0).getValue());
						// armazena de volta o valor do campo, com as correcoes de caracteres especiais
						campoX.getSubfield("a").get(0).setValue(valor);
						
						// procura 'valor' na lista de autoridades
						for(String tipo : tipoAutoridades.keySet())
						{
							String campoPrincipal = campoPrincipalDoTipo(tipo);
							for(Record autoridade : tipoAutoridades.get(tipo))
							{
								String nomeAutoridade = autoridade.getField(campoPrincipal).get(0).getSubfield("a").get(0).getValue();
								// se o nome da autoridade no acervo corresponde ao nome da autoridade no banco de autoridades, setar o id correto
								if(valor.toUpperCase().contentEquals(nomeAutoridade.toUpperCase()))
								{
									encontrado = true;
									if(!campoX.hasSubfield("9"))
									{
										Subfield subf = new Subfield("9", autoridade.getField("001").get(0).getValue(), autoridade.getField("942").get(0).getSubfield("a").get(0).getXmlNodeName());
										campoX.addSubfield(subf);
									}else
									{
										campoX.getSubfield("9").get(0).setValue(autoridade.getField("001").get(0).getValue());
									}
								}
							}
						}
					}
				}
			}
			
			// se nao foi encontrado, adiciona na lista de registros com erro
			if(!encontrado)
				registroDeAutoridadesComErro.add(acervo);
		}
		return registroDeAutoridadesComErro;
	}

	/**
	 * Retorna o campo principal correspondente ao tipo da autoridade
	 * @param tipo
	 * @return
	 */
	private static String campoPrincipalDoTipo(String tipo) {
		switch(tipo)
		{
			case "CORPO_NAME":
				return "110";
			case "GEOGR_NAME":
				return "151";
			case "MEETI_NAME":
				return "111";
			case "PERSO_NAME":
				return "100";
			case "TOPIC_TERM":
				return "150";
			case "UNIF_TITLE":
				return "130";
			default:
				System.err.println("Tipo de autoridade nao encontrado");
				return "";
		}
	}

	/**
	 * Verifica se a autoridadeAtual respeita as regras de seu campo especifico 
	 * @param autoridadeAtual
	 * @param tipo
	 * @return
	 */
	private static boolean temCampoObrigatorio(Record autoridadeAtual, String tipo) {
		
		switch(tipo)
		{
			case "CORPO_NAME":
				if(temErroEmCampoEspecifico(autoridadeAtual, "110", "a"))
					return false;
				break;
			case "GEOGR_NAME":
				if(temErroEmCampoEspecifico(autoridadeAtual, "151", "a"))
					return false;
				break;
			case "MEETI_NAME":
				if(temErroEmCampoEspecifico(autoridadeAtual, "111", "a"))
					return false;
				break;
			case "PERSO_NAME":
				if(temErroEmCampoEspecifico(autoridadeAtual, "100", "a"))
					return false;
				break;
			case "TOPIC_TERM":
				if(temErroEmCampoEspecifico(autoridadeAtual, "150", "a"))
					return false;
				break;
			case "UNIF_TITLE":
				if(temErroEmCampoEspecifico(autoridadeAtual, "130", "a"))
					return false;
				break;
			default:
				System.err.println("Tipo de autoridade nao encontrado");
				return false;
		}
		
		return true;
	}

	/**
	 * Verifica se a autoridade correspondente tem erros em seu campo obrigatorio
	 * @param autoridadeAtual
	 * @param campo
	 * @param subcampo
	 * @return
	 */
	private static boolean temErroEmCampoEspecifico(Record autoridadeAtual, String campo, String subcampo) {
		
		// verifica se o campo existe
		if(!autoridadeAtual.hasField(campo))
			return true;
		
		// verifica se tem mais de um campo obrigatorio
		if(autoridadeAtual.getField(campo).size() != 1)
			return true;
		
		// verifica se o subcampo existe
		if(!autoridadeAtual.getField(campo).get(0).hasSubfield(subcampo))
			return true;
		
		// verifica se tem mais do que um subcampo obrigatorio
		if(autoridadeAtual.getField(campo).get(0).getSubfield(subcampo).size() != 1)
			return true;
		
			
		return false;
	}

	/**
	 * Este metodo recebe como entrada um autoridade e percorre uma lista de verificacoes em busca de erros.
	 * @param autoridade
	 * @return
	 */
	public static boolean apresentaErros(Record autoridade)
	{
		// realizacao de testes e verificacoes
		if(!autoridade.hasField("942"))
		{
			System.err.println("Autoridade de id " + autoridade.getField("001").get(0).getValue() + "nao possui tipo definido");
			return true;
		}
		if(autoridade.getField("942").size() > 1)
		{
			System.err.println("Autoridade de id " + autoridade.getField("001").get(0).getValue() + "apresenta mais de um tipo definido");
			return true;
		}
		if(autoridade.getField("942").get(0).getSubfield("a").size() != 1)
		{
			System.err.println("Autoridade de id " + autoridade.getField("001").get(0).getValue() + "apresenta problemas no subcampo $a");
			return true;
		}
		
		return false;
	}
	
	// experimentos mmfdh
	public static void experimentoMMFDH(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException
	{

		MARC marc = new MARC(Constant.inputFile2);
		ArrayList<Record> records = marc.getRecords();
		HashMap<String, ArrayList<Record>> allAuthorities = new HashMap<String, ArrayList<Record>>();
		ArrayList<Record> duplicated = new ArrayList<Record>();
		ArrayList<Record> error = new ArrayList<Record>();
		ArrayList<Record> autoridades180 = new ArrayList<Record>();
		ArrayList<Record> topicTerm = new ArrayList<Record>();

		System.out.println(records.size());
		int cont = 0;
		//		HashMap<String, Integer> camposMarc = new HashMap<String, Integer>();
		//
		for(int r = 0; r < records.size(); r++)
		{
			String autoridade = "";
			
			if(records.get(r).hasField("150") && records.get(r).getField("150").get(0).hasSubfield("a"))
			{
				autoridade = records.get(r).getField("150").get(0).getSubfield("a").get(0).getValue();
			}else if(records.get(r).hasField("100") && records.get(r).getField("100").get(0).hasSubfield("a"))
			{
				autoridade = records.get(r).getField("100").get(0).getSubfield("a").get(0).getValue();
			}else if(records.get(r).hasField("110") && records.get(r).getField("110").get(0).hasSubfield("a"))
			{
				autoridade = records.get(r).getField("110").get(0).getSubfield("a").get(0).getValue();
			}else if(records.get(r).hasField("111") && records.get(r).getField("111").get(0).hasSubfield("a"))
			{
				autoridade = records.get(r).getField("111").get(0).getSubfield("a").get(0).getValue();
			}else if(records.get(r).hasField("151") && records.get(r).getField("151").get(0).hasSubfield("a"))
			{
				autoridade = records.get(r).getField("151").get(0).getSubfield("a").get(0).getValue();
			}else if(records.get(r).hasField("147") && records.get(r).getField("147").get(0).hasSubfield("a"))
			{
				autoridade = records.get(r).getField("147").get(0).getSubfield("a").get(0).getValue();
			}else if(records.get(r).hasField("130") && records.get(r).getField("130").get(0).hasSubfield("a"))
			{
				autoridade = records.get(r).getField("130").get(0).getSubfield("a").get(0).getValue();
			}else 
			{
				// criar lista com autoridades problematicas
				System.err.println("ERRO");
				error.add(records.get(r));
				continue;
			}
			
			if(autoridade.contentEquals(""))
			{
				error.add(records.get(r));
				continue;
			}
			
			if(records.get(r).hasField("942") && records.get(r).getField("942").get(0).getSubfield("a").get(0).getValue().toUpperCase().contentEquals("TOPIC_TERM"))
			{
				topicTerm.add(records.get(r));
			}
			
			if(!allAuthorities.containsKey(autoridade.toUpperCase()))
				allAuthorities.put(autoridade.toUpperCase(), new ArrayList<Record>());
			allAuthorities.get(autoridade.toUpperCase()).add(records.get(r));
		}

		for(String chave : allAuthorities.keySet())
		{
			if(allAuthorities.get(chave).size() > 1)
			{
				duplicated.addAll(allAuthorities.get(chave));
			}
		}
		
		System.out.println(duplicated.size());
		XMLHandler.generateXMLFile(duplicated, Constant.outputFile);
		XMLHandler.generateXMLFile(error, Constant.outputFile2);
		XMLHandler.generateXMLFile(autoridades180, Constant.outputFile3);
		XMLHandler.generateXMLFile(topicTerm, Constant.outputFile4);
		System.out.println("Fim!\n");
	}

	public static void correcaoEnapFinal(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException
	{

		MARC marc = new MARC(Constant.inputFile2);
		ArrayList<Record> records = marc.getRecords();
		ArrayList<Record> recordsComErros = new ArrayList<Record>();
		ArrayList<Record> recordsCorrigidos = new ArrayList<Record>();
		boolean erro = false;

		System.out.println("Tamanho:" + records.size());

		for(int r = 0; r < records.size(); r++)
		{
			Record record = records.get(r);
			erro = false;

			if(record.hasField("008"))
			{
				String field008 = record.getField("008").get(0).getValue();
				if(field008.length() != 40)
				{
					erro = true;
				}

				if(field008.indexOf("por") != 35 
						&& field008.indexOf("eng") != 35
						&& field008.indexOf("spa") != 35
						&& field008.indexOf("ita") != 35
						&& field008.indexOf("fre") != 35
						&& field008.indexOf("ger") != 35)
				{
					erro = true;
				}
			}else
			{
				erro = true;
			}

			if(erro) 
			{
				recordsComErros.add(records.remove(r));
				r--;
			}
		}

		for(int r = 0; r < recordsComErros.size(); r++)
		{
			Record recordAtual = recordsComErros.get(r);
			if(recordsComErros.get(r).hasField("008"))
			{
				Field field008 = recordsComErros.get(r).getField("008").get(0);
				String value008 = field008.getValue();

				// correcao 1: campo 008 com tamanho 38
				if(value008.length() == 38)
				{
					field008.setValue(value008 + " d");
					recordsComErros.get(r).getField("008").get(0).setValue(field008.getValue());

					recordsCorrigidos.add(recordsComErros.remove(r));
					r--;
				}

				// correcao 2: campo 008 com tamanho 6
				else if(value008.length() == 6)
				{
					if(recordsComErros.get(r).hasField("041"))
						field008.setValue(value008 + "                       000 0 "+ recordsComErros.get(r).getField("041").get(0).getSubfield("a").get(0).getValue() + " d");
					else
						field008.setValue(value008 + "                       000 0 eng d");

					if(field008.getValue().length() == 40)
					{
						recordsCorrigidos.add(recordsComErros.remove(r));
						r--;
					}
					else 
					{
						System.err.println("ERRO");
					}
				}

				// correcao 3: campo 008 com idioma POR
				else if(value008.regionMatches(35, "POR", 0, 3))
				{
					field008.setValue(field008.getValue().replace("POR", "por"));
					recordsCorrigidos.add(recordsComErros.remove(r));
					r--;
				}

				// correcao 4: campo 008 de tamanho 40 mas sem o idioma
				else if(value008.length() == 40 && (value008.charAt(35) == " ".charAt(0)))
				{
					if(recordsComErros.get(r).hasField("041"))
						field008.setValue(value008.substring(0, 35) + recordsComErros.get(r).getField("041").get(0).getSubfield("a").get(0).getValue() + value008.substring(38, 40));
					else
						field008.setValue(value008.substring(0, 35) + "eng" + value008.substring(38, 40));

					if(field008.getValue().length() == 40)
					{
						recordsCorrigidos.add(recordsComErros.remove(r));
						r--;
					}
					else 
					{
						System.err.println("ERRO");
					}
				}

				// correcao 5: campo 008 com idioma POR
				else if(value008.regionMatches(35, "POR", 0, 3))
				{
					field008.setValue(field008.getValue().replace("POR", "por"));
					recordsCorrigidos.add(recordsComErros.remove(r));
					r--;
				}

				// correcao 6: campo 008 com tamanho 36
				else if(value008.length() == 36)
				{
					field008.setValue(value008.substring(0, 27) + "    " + value008.substring(27, 36));

					if(field008.getValue().length() == 40)
					{
						recordsCorrigidos.add(recordsComErros.remove(r));
						r--;
					}
					else 
					{
						System.err.println("ERRO");
					}

				}

				// correcao 7: campo 008 com tamanho 32
				else if(value008.length() == 32)
				{
					field008.setValue(value008.substring(0, 27) + "        " + value008.substring(27, 32));

					if(field008.getValue().length() == 40)
					{
						recordsCorrigidos.add(recordsComErros.remove(r));
						r--;
					}
					else 
					{
						System.err.println("ERRO");
					}

				}

				// correcao 8: campo 008 com tamanho 28
				else if(value008.length() == 28)
				{
					field008.setValue(value008.substring(0, 15) + "            " + value008.substring(15, 28));

					if(field008.getValue().length() == 40)
					{
						recordsCorrigidos.add(recordsComErros.remove(r));
						r--;
					}
					else 
					{
						System.err.println("ERRO");
					}

				}

				// correcao 9: campo 008 com tamanho 24
				else if(value008.length() == 24)
				{
					field008.setValue(value008.substring(0, 22) + "                " + value008.substring(22, 24));

					if(field008.getValue().length() == 40)
					{
						recordsCorrigidos.add(recordsComErros.remove(r));
						r--;
					}
					else 
					{
						System.err.println("ERRO");
					}

				}

				// correcao 10: campo 008 com tamanho 34
				else if(value008.length() == 34)
				{
					field008.setValue(value008.substring(0, 31) + "    por d");

					if(field008.getValue().length() == 40)
					{
						recordsCorrigidos.add(recordsComErros.remove(r));
						r--;
					}
					else 
					{
						System.err.println("ERRO: tamanho " + field008.getValue().length());
					}
				}

				// correcao 11: campo 008 com tamanho 33
				else if(value008.length() == 33)
				{
					field008.setValue(value008.substring(0, 17) + "       " + value008.substring(17, 33));

					if(field008.getValue().length() == 40)
					{
						recordsCorrigidos.add(recordsComErros.remove(r));
						r--;
					}
					else 
					{
						System.err.println("ERRO: tamanho " + field008.getValue().length());
					}

				}

				// correcao 12: campo 008 com tamanho 26
				else if(value008.length() == 26)
				{
					field008.setValue(value008.substring(0, 16) + "              " + value008.substring(16, 26));

					if(field008.getValue().length() == 40)
					{
						recordsCorrigidos.add(recordsComErros.remove(r));
						r--;
					}
					else 
					{
						System.err.println("ERRO: tamanho " + field008.getValue().length());
					}

				}



			}
		}

		for(int r=0; r < recordsComErros.size(); r++)
		{

			if(recordsComErros.get(r).hasField("008"))
				System.err.println(recordsComErros.get(r).getField("008").get(0).getValue());
			else
			{
				System.err.println("Sem campo 008! Campo criado");
				Field newField008 = new Field("008", "070713s2000dfba   f     00     0 0 por d", Constant.controlFieldDenomination);
				recordsComErros.get(r).addField(newField008);

				recordsCorrigidos.add(recordsComErros.remove(r));
				r--;
			}
		}

		// verificacao das correcoes
		for(Record record : recordsCorrigidos)
		{
			if(record.getField("008").get(0).getValue().length() != 40)
			{
				System.err.println("ERRO AQUI:" + record.getField("008").get(0).getValue());
			}
		}

		System.out.println("TESTES DE ERROS:");

		int j = 0;
		// verificacao de erros em 'records'
		for(Record record : records)
		{
			if(!record.hasField("008"))
			{
				j++;
				continue;
			}

			if(record.getField("008").get(0).getValue().length() != 40)
			{
				System.err.println("ERRO AQUI:" + record.getField("008").get(0).getValue());
				j++;
			}
		}

		records.addAll(recordsCorrigidos);

		int i = 0;

		// correcao 13: espacos vazios no idioma
		for(Record record : records)
		{
			if(record.getField("008").get(0).getValue().charAt(35) == " ".charAt(0) 
					&& record.getField("008").get(0).getValue().charAt(36) == " ".charAt(0)
					&& record.getField("008").get(0).getValue().charAt(37) == " ".charAt(0))
			{
				if(record.hasField("041"))
					record.getField("008").get(0).setValue(record.getField("008").get(0).getValue().substring(0, 35) + record.getField("041").get(0).getSubfield("a").get(0).getValue() + record.getField("008").get(0).getValue().substring(38, 40));
				else
					record.getField("008").get(0).setValue(record.getField("008").get(0).getValue().substring(0, 35) + "por" + record.getField("008").get(0).getValue().substring(38, 40));
			}
		}

		// verificacao tamanho geral
		for(Record record : records)
		{
			if(record.hasField("008"))
			{
				if(record.getField("008").get(0).getValue().length() != 40)
				{
					System.err.println("ERRO no tamanho do campo:" + record.getField("008").get(0).getValue());
					i++;
				}

				String field008 = record.getField("008").get(0).getValue();
				if(field008.indexOf("por") != 35 
						&& field008.indexOf("eng") != 35
						&& field008.indexOf("spa") != 35
						&& field008.indexOf("ita") != 35
						&& field008.indexOf("fre") != 35
						&& field008.indexOf("ger") != 35)
				{

					System.out.println("Erro de idioma:" + field008);
					i++;
					erro = true;
				}


			}else
			{
				System.err.println("Sem 008!");
				i++;
			}
		}

		System.out.println("Com erros:" + i);


		XMLHandler.generateXMLFile(records, Constant.outputFilePath3);
		System.out.println("Registros com erro: " + recordsComErros.size());
		System.out.println("Registros corrigidos: " + recordsCorrigidos.size());
		System.out.println("Fim!\n");
		System.out.println("Erros: " + i);
		System.out.println("Erros em 'records' = " + j);
	}

	// Busca de registros para Rebeca
	public static void temp3(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException
	{

		MARC marc = new MARC(Constant.inputFile3);
		ArrayList<Record> records = marc.getRecords();
		ArrayList<Record> recordsFinal = new ArrayList<Record>();

		int i = 0;

		for(int r = records.size() - 1; r >= 0; r--)
		{
			Record record = records.get(r);

			if(record.hasField("008"))
				if(record.getField("008").get(0).getValue().contains("por"))
				{
					recordsFinal.add(record);
					i++;
				}
			if(i == 50)
				break;
		}

		XMLHandler.generateXMLFile(recordsFinal, Constant.outputFilePath2);
		System.out.println(recordsFinal.size());
		System.out.println("Fim!\n");
	}

	public static void correcaoEnapEncoding(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException
	{

		MARC marc = new MARC(Constant.inputFile2);
		ArrayList<Record> records = marc.getRecords();
		int i = 0;

		for(Record record : records)
		{
			// contar registros avaliados
			i++;
			if(i % 100 == 0)
				System.out.println(i + "/" + records.size());

			// 1 - corrigir campo leader
			record.getLeaderField().setValue("00415nz  a2200121n  4500");

			// 2 - corrigir 008
			if(record.hasField("008"))
			{
				Field field008 = record.getField("008").get(0);
				field008.setValue(field008.getValue().replace("#", " "));
			}

			// 3 - inserir em 952 $o o valor de 090 $a + " " + $b
			if(record.hasField("090") && record.hasField("952"))
			{
				Field field090 = record.getField("090").get(0);
				if(field090.hasSubfield("a") && field090.hasSubfield("b"))
				{
					Subfield subfield090a = field090.getSubfield("a").get(0);
					Subfield subfield090b = field090.getSubfield("b").get(0);
					String callnumber = subfield090a.getValue() + " " + subfield090b.getValue();

					Subfield subfield952o = new Subfield("o", callnumber, Constant.subfieldDenomination);
					ArrayList<Field> fields952 = record.getField("952");

					for (Field field952 : fields952)
					{
						field952.addSubfield(subfield952o);
					}

				}


			}
		}

		XMLHandler.generateXMLFile(records, Constant.outputFilePath3);
		System.out.println("Fim!\n");
	}

	public static void mainErroIBICT(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException
	{
		MARC marc = new MARC(Constant.inputFile2);
		ArrayList<Record> records = marc.getRecords();

		for(Record record : records)
		{
			if(!record.hasField("080") || !record.hasField("952"))
				continue;
			else 
			{
				Field campo080 = record.getField("080").get(0);
				if(!campo080.hasSubfield("b"))
					continue;
				else
				{
					String cutter = campo080.getSubfield("b").get(0).getValue();
					String callnumber = "";

					if(campo080.hasSubfield("a"))
						callnumber = campo080.getSubfield("a").get(0).getValue();

					ArrayList<Field> fields952 = record.getField("952");

					for(Field field952 : fields952)
					{
						Subfield subfieldo = new Subfield("o", callnumber + " " + cutter, Constant.subfieldDenomination);
						field952.addSubfield(subfieldo);
					}
				}
			}
		}

		XMLHandler.generateXMLFile(records);
		System.out.println("Fim!\n");
	}

	// main - abrir e fechar XML
	@SuppressWarnings("resource")
	public static void criaListaCutter(String[] args) throws Exception 
	{
		MARC marc = new MARC(Constant.inputFile2);
		ArrayList<Record> records = marc.getRecords();
		BufferedWriter writer;

		writer = new BufferedWriter(new FileWriter(Constant.outputFilePath, true));
		// Comandos usados para abrir o arquivo e limpar qualquer conteudo antigo.
		PrintWriter writer2 = new PrintWriter(Constant.outputFilePath);
		writer2.print("");
		writer2.close();

		for(Record record : records)
		{
			if(!record.hasField("001") || !record.hasField("090"))
				continue;

			String campo001 = record.getField("001").get(0).getValue();
			String subcampo090 = record.getField("090").get(0).getSubfield("a").get(0).getValue();

			// A String a ser escrita no arquivo deve ser incluida no 'writer' pelo metodo 'append'.
			writer.append(campo001 + ":::" + subcampo090 + "\n");
		}

		writer.close();
		//XMLHandler.generateXMLFile(records);
		System.out.println("Fim!\n");
	}

	public static ArrayList<String[]> readFile(String filepath, String separator) throws Exception
	{
		File file = new File(filepath);
		BufferedReader br = new BufferedReader(new FileReader(file)); 
		ArrayList<String[]> linhas = new ArrayList<String[]>();
		String st;
		String[] temp;

		while ((st = br.readLine()) != null)
		{
			temp = st.split(separator);
			linhas.add(temp);
		}

		br.close();
		return linhas;
	}

	public static void temp2(String[] args) throws Exception 
	{
		//		MARC marcPHL = new MARC(Constant.inputFile); // registros PHL
		MARC marcIBICT = new MARC(Constant.inputFile2); // catalogo IBICT
		ArrayList<Record> recordsIBICT = marcIBICT.getRecords();
		ArrayList<Record> recordsFinal = new ArrayList<Record>();
		ArrayList<String[]> linhas = readFile("xmlIn/ibict2/listaCutter.xml", ":::");
		HashMap<String, String> listaCutter = new HashMap<String, String>();

		for(String[] linha : linhas)
		{
			if(listaCutter.containsKey(linha[0])) 
			{
				System.err.println("Campo 001 duplicado:" + linha[0]);
			}else
			{
				listaCutter.put(linha[0], linha[1]);
			}
		}

		for(int r = 0; r < recordsIBICT.size(); r++)
		{
			Record record = recordsIBICT.get(r);

			if(!record.hasField("001"))
			{
				recordsFinal.add(record);
				continue;
			}

			String identificador = record.getField("001").get(0).getValue();

			if(!listaCutter.containsKey(identificador))
			{
				System.out.println("identificador não encontrado: " + identificador);
			}else
			{
				String cutter = listaCutter.get(identificador);
				if(record.hasField("080"))
				{
					record.getField("080").get(0).addSubfield(new Subfield("b", cutter, Constant.subfieldDenomination));
				}else
				{
					Subfield novo080b = new Subfield("b", cutter, Constant.subfieldDenomination);
					ArrayList<Subfield> subcampo80a = new ArrayList<Subfield>();
					subcampo80a.add(novo080b);

					Field novo80 = new Field("080", "", "", subcampo80a, Constant.dataFieldDenomination);
					record.addField(novo80);
				}
			}

			recordsFinal.add(record);
		}

		//		for(Record recordIBICT : recordsIBICT)
		//		{
		//			String valor = recordIBICT.getField("001").get(0).getValue();
		//			ArrayList<Record> recordAtual = marcPHL.getRecordWithFieldValue("001", valor);
		//
		//			if(recordAtual == null)
		//				System.err.println("Registro não encontrado: campo 001 = " + valor + " ");
		//			else if(recordAtual.size() > 1)
		//				System.err.println("ERRO. Mais de um campo 001.");
		//
		//
		//			String cutter = recordAtual.get(0).getField("090").get(0).getSubfield("a").get(0).getValue();
		//			Subfield subf = new Subfield("b", cutter, Constant.subfieldDenomination);
		//			recordIBICT.getField("080").get(0).addSubfield(subf);
		//
		//			recordsFinal.add(recordIBICT);
		//		}


		//			if(marc.hasRecordsWithSameValueInField(records, "001"))
		//			{
		//				System.out.println("ERRO");
		//			}
		//			
		//			for(Record record : records)
		//			{
		//				if(!record.hasField("001"))
		//					System.out.println("ERRO");

		//				if(record.hasField("001") && record.hasField("090"))
		//				{
		//					ArrayList<Field> campos = new ArrayList<Field>();
		//					
		//					campos.add(record.getLeaderField());
		//					campos.add(record.getField("001").get(0));
		//					campos.add(record.getField("005").get(0));
		//					campos.add(record.getField("008").get(0));
		//					if(record.hasField("020"))
		//						campos.add(record.getField("020").get(0));
		//					campos.add(record.getField("090").get(0));
		//					campos.add(record.getField("245").get(0));

		//					Record novo = new Record(campos);
		//					String[] oldS = {"a"};
		//					String[] newS = {"b"};
		//					novo.swapFieldAndSubfields("090", "080", oldS, newS);
		//					recordsFinal.add(novo);
		//				}
		//			}

		XMLHandler.generateXMLFile(recordsFinal);
		System.out.println("Fim!\n");
	}


	// main - abrir e fechar XML
	public static void alterarCampos(String[] args) throws Exception 
	{
		MARC marc = new MARC(Constant.inputFile2);
		ArrayList<Record> records = marc.getRecords();
		ArrayList<Record> recordsFinal = new ArrayList<Record>();
		HashMap<String, Integer> materiais = new HashMap<String, Integer>();
		marc.removeField("930");

		String[] oldValues = {"Livros", "Teses", "Periódicos"};
		String[] newValues = {"Livro", "Tese", "Periódico"};


		for(Record record : records)
		{
			if(record.hasField("942"))
			{
				// TODO testar o metodo abaixo, implementado para substituir o procedimento atual
				//Operation.swapSubfieldValuesWithList(record, "942", "c", oldValues, newValues);
				for(Field field942 : record.getField("942"))
				{
					for(Subfield subF : field942.getSubfield("c"))
					{
						if(subF.getValue().contentEquals("Livros"))
							subF.setValue("Livro");
						else if(subF.getValue().contentEquals("Teses"))
							subF.setValue("Tese");
						else if(subF.getValue().contentEquals("Periódicos"))
							subF.setValue("Periódico");
					}
				}
			}
		}

		//			marc.removeField("999");
		//		for(Record record : records)
		//		{

		//			if(record.hasField("999"))
		//			{
		//				for(Field field999 : record.getField("999"))
		//				{
		//					String[] list = {"f"};
		//					field999.removeSubfieldsExceptTheList(list);
		//				}
		//				
		//				String[] old = {"f"};
		//				String[] newF = {"c"};
		//				
		//				record.swapFieldAndSubfields("999", "942", old, newF);
		//			}

		//				if(record.hasField("999"))
		//				{
		//					for(Field field999 : record.getField("999"))
		//					{
		//						if(field999.hasSubfield("f"))
		//						{
		//							for(Subfield subcampoF : field999.getSubfield("f"))
		//							{
		//								if(!materiais.containsKey(subcampoF.getValue()))
		//									materiais.put(subcampoF.getValue(), 1);
		//								else
		//									materiais.put(subcampoF.getValue(), materiais.get(subcampoF.getValue()));
		//							}
		//						}
		//					}
		//				}
		//		}

		//			for(String material : materiais.keySet())
		//			{
		//				System.out.println(material);
		//			}

		XMLHandler.generateXMLFile(records);
		System.out.println("Fim!\n");
	}

	// main - abrir e fechar XML
	//	public static void main(String[] args) throws Exception 
	//	{
	//		MARC marc = new MARC(Constant.inputFile);
	//		ArrayList<Record> records = marc.getRecords();
	//		ArrayList<Record> recordsFinal = new ArrayList<Record>();
	//		
	//		for(Record record : records)
	//		{
	//			if(record.hasField("150"))
	//			{
	//				recordsFinal.add(record);
	//			}
	//		}
	//		
	//		XMLHandler.generateXMLFile(recordsFinal);
	//		System.out.println("Fim!\n");
	//	}

	// main - abrir e fechar XML
	public static void openClose(String[] args) throws Exception 
	{
		MARC marc = new MARC(Constant.inputFile2);
		ArrayList<Record> records = marc.getRecords();

		XMLHandler.generateXMLFile(records);
		System.out.println("Fim!\n");
	}

	// criar autoridades
	//	public static void main(String[] args) throws Exception 
	//	{
	//		MARC marc = new MARC(Constant.inputFile);
	//		ArrayList<Record> records = marc.getRecords();
	//		ArrayList<Record> recordsResult = new ArrayList<Record>();
	//		
	//		for(Record record : records)
	//		{
	//			if(!record.hasField("100") && !record.hasField("110")
	//					&& !record.hasField("111") && !record.hasField("600")
	//					&& !record.hasField("610") && !record.hasField("611")
	//					&& !record.hasField("650") && !record.hasField("700")
	//					&& !record.hasField("710") && !record.hasField("711"))
	//			{
	//				continue;
	//			}
	//			
	//			ArrayList<Field> camposAutoridades = new ArrayList<Field>();
	//			
	//			for(Field field : record.getFields())
	//			{
	//				if(field.getFieldName().contentEquals("leader") || field.getFieldName().contentEquals("003")
	//						|| field.getFieldName().contentEquals("008")
	//						|| field.getFieldName().contentEquals("005") || field.getFieldName().contentEquals("100")
	//						|| field.getFieldName().contentEquals("110") || field.getFieldName().contentEquals("111")
	//						|| field.getFieldName().contentEquals("600")
	//						|| field.getFieldName().contentEquals("610") || field.getFieldName().contentEquals("611")
	//						|| field.getFieldName().contentEquals("650") || field.getFieldName().contentEquals("700")
	//						|| field.getFieldName().contentEquals("710") || field.getFieldName().contentEquals("711"))
	//				{
	//					camposAutoridades.add(field);
	//				}
	//				
	//				if(field.getFieldName().contentEquals("700"))
	//					field.setFieldName("100");
	//				if(field.getFieldName().contentEquals("710"))
	//					field.setFieldName("110");
	//				if(field.getFieldName().contentEquals("711"))
	//					field.setFieldName("111");
	//				if(field.getFieldName().contentEquals("600"))
	//					field.setFieldName("100");
	//				if(field.getFieldName().contentEquals("610"))
	//					field.setFieldName("110");
	//				if(field.getFieldName().contentEquals("611"))
	//					field.setFieldName("111");
	//				if(field.getFieldName().contentEquals("650"))
	//					field.setFieldName("150");
	//			}
	//			
	//			Record recordAutoridade = new Record(camposAutoridades);
	//			
	//			recordsResult.add(recordAutoridade);
	//		}
	//		
	//		XMLHandler.generateXMLFile(recordsResult);
	//		System.out.println("Fim!\n");
	//	}

	// Main - contar materiais
	//	public static void main(String[] args) throws Exception 
	//	{
	//		//CSVController.readCSV(Constant.inputCSVFilePath, Constant.csvSeparator);
	//		
	//		MARC marc = new MARC(Constant.inputFile);
	//		ArrayList<Record> records = marc.getRecords();
	//		ArrayList<Record> result = new ArrayList<Record>();
	//		HashMap<String, Integer> materiais = new HashMap<String, Integer>();
	//
	//		for(Record record : records)
	//		{
	//			if(record.hasField("942"))
	//			{
	//				ArrayList<Field> fields = record.getField("942");
	//				for(Field field : fields)
	//				{	
	//					if(field.hasSubfield("c"))
	//					{
	//						ArrayList<Subfield> subfields = field.getSubfield("c");
	//						
	//						for(Subfield subfield : subfields)
	//						{
	//							if(!materiais.containsKey(subfield.getValue()))
	//							{
	//								materiais.put(subfield.getValue(), 0);
	//							}
	//							materiais.put(subfield.getValue(), materiais.get(subfield.getValue()) + 1);
	//						}
	//						
	//					}
	//				}
	//			}
	//		}
	//		
	//		for(String material : materiais.keySet())
	//		{
	//			System.out.println(material + ": " + materiais.get(material));
	//		}
	//
	//		XMLHandler.generateXMLFile(records);
	//		System.out.println("Fim!\n");
	//	}

	// main mudar campo e subcampo
	//	public static void main(String[] args) throws Exception 
	//	{
	//		//CSVController.readCSV(Constant.inputCSVFilePath, Constant.csvSeparator);
	//		
	//		MARC marc = new MARC(Constant.inputFile);
	//		ArrayList<Record> records = marc.getRecords();
	//
	//		for(Record record : records)
	//		{
	//			if(record.hasField("990"))
	//			{
	//				ArrayList<Field> fields = record.getField("990");
	//				for(Field field : fields)
	//				{	
	//					if(field.hasSubfield("a"))
	//					{
	//						ArrayList<Subfield> subfields = field.getSubfield("a");
	//						
	//						for(Subfield subfield : subfields)
	//						{
	//							subfield.swapSubfieldName("c");
	//						}
	//						
	//					}
	//					field.swapFieldName("942");
	//				}
	//			}
	//		}
	//
	//		XMLHandler.generateXMLFile(records);
	//		System.out.println("Fim!\n");
	//	}

	//	public static void main(String[] args) throws Exception 
	//	{
	//		//CSVController.readCSV(Constant.inputCSVFilePath, Constant.csvSeparator);
	//		
	//		MARC marc = new MARC(Constant.inputFilePath);
	//		ArrayList<Record> records = marc.getRecords();
	//		ArrayList<Record> result = new ArrayList<Record>();
	//
	//		for(Record record : records)
	//		{
	//			if(record.hasField("990"))
	//			{
	//				ArrayList<Field> fields = record.getField("990");
	//				for(Field field : fields)
	//				{	
	//					if(field.hasSubfield("a"))
	//					{
	//						ArrayList<Subfield> subfields = field.getSubfield("a");
	//						
	//						for(Subfield subfield : subfields)
	//						{
	//							subfield.swapSubfieldName("c");
	//						}
	//						
	//					}
	//					field.swapFieldName("942");
	//				}
	//			}
	//		}
	//
	//		XMLHandler.generateXMLFile(records);
	//		System.out.println("Fim!\n");
	//	}

	// main - merge files
	//		public static void main(String[] args) throws Exception 
	//		{
	//			MARC marc = new MARC(Constant.inputFilePath1);
	//			MARC marc2 = new MARC(Constant.inputFilePath2);
	//			
	//			ArrayList<Record> result = new ArrayList<Record>();
	//			
	//			result.addAll(marc.getRecords());
	//			result.addAll(marc2.getRecords());
	//
	//			XMLHandler.generateXMLFile(result);
	//			System.out.println("Fim!\n");
	//		}

	//Main - migracao do koha da SNJ
	//	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	//	{
	//		MARC marc = new MARC(Constant.inputFilePath);
	//		ArrayList<Record> records = marc.getRecords();
	//		ArrayList<Record> result = new ArrayList<Record>();
	//		int total942BK = 0, total942PER = 0, total952BK = 0, total952PER = 0;
	//
	//		for(Record record : records)
	//		{
	//			if(record.hasField("952"))
	//			{
	//				ArrayList<Field> fields = record.getField("952");
	//				for(Field field : fields)
	//				{	
	//					if(field.hasSubfield("a"))
	//					{
	//						ArrayList<Subfield> subfields = field.getSubfield("a");
	//						for(Subfield subfield : subfields)
	//						{
	//							subfield.setValue("MMFDH");
	//						}
	//					}
	//				}
	//			}
	//			
	//			if(record.hasField("952"))
	//			{
	//				ArrayList<Field> fields = record.getField("952");
	//				for(Field field : fields)
	//				{	
	//					if(field.hasSubfield("b"))
	//					{
	//						ArrayList<Subfield> subfields = field.getSubfield("b");
	//						for(Subfield subfield : subfields)
	//						{
	//							subfield.setValue("MMFDH");
	//						}
	//					}
	//				}
	//			}
	//		}
	//
	//		XMLHandler.generateXMLFile(records);
	//		System.out.println("Fim!\n");
	//	}

	// Main - Migracao do koha do ibict
	//	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	//	{
	//		MARC marc16 = new MARC(Constant.inputFilePath);
	////		MARC marckardex = new MARC(Constant.inputKardexFilePath);
	//		ArrayList<Record> records = marc16.getRecords();
	////		ArrayList<Record> recordsKardex = marckardex.getRecords();
	//		ArrayList<Record> result = new ArrayList<Record>();
	////		ArrayList<Record> result = marc16.getRecordWithField("022");
	//		HashMap<String, ArrayList<Record>> kardexGroup = new HashMap<String, ArrayList<Record>>();
	//		
	//		// agrupa os registros por issn
	//		for(Record record : records)
	//		{
	//			String kardexValue = record.getKardexValue();
	//			if(!kardexGroup.containsKey(kardexValue))
	//			{
	//				kardexGroup.put(kardexValue, new ArrayList<Record>());
	//			}else
	//				System.out.println();
	//			kardexGroup.get(kardexValue).add(record);
	//		}
	//		
	//		// monta o xml com apenas o primeiro registro de cada issn. Dos outros registros sao usados
	//		// apenas o campo 952.
	//		Iterator<String> it = kardexGroup.keySet().iterator();
	//		while(it.hasNext())
	//		{	
	//			String issnAtual = it.next();
	//			ArrayList<Record> recordsAtual = kardexGroup.get(issnAtual);
	//			Record primeiroRegistro = recordsAtual.get(0);
	//			
	//			if(recordsAtual.size() > 1)
	//			{
	//				for(int i = 1; i < recordsAtual.size(); i++)
	//				{
	//					primeiroRegistro.addFields(recordsAtual.get(i).getField("952"));
	//				}
	//			}
	//			result.add(primeiroRegistro);
	//		}
	//		
	//		// Para a criacao de varios arquivos.
	////		it = kardexGroup.keySet().iterator();
	////		while(it.hasNext())
	////		{
	////			String issnAtual = it.next();
	////			XMLHandler.generateXMLFile(kardexGroup.get(issnAtual), ("arquivos/Registros_do_catalogo_sem_ISSN_no_kardex_agrupados_por_ISSN/catalogo_final_issn" + issnAtual + ".xml"));
	////		}







	//Registros com o campo
	//		for(Record record : records)
	//		{
	//			if(record.hasField("952"))
	//				result.add(record);
	//		}

	//		for(Record recordkardex : recordsKardex)
	//		{
	//			if(recordkardex.getField("005").get(0).getValue().contentEquals("20120717112715.0"))
	//				System.out.println();
	//				
	//			if(!marc16.containsKardex(recordkardex.getKardex().getSubfield("a").get(0).getValue()))
	//			{
	//				result.add(recordkardex);
	//			}
	//		}

	// merge dos registros com kardex
	//		for(Record recordK : recordsKardex)
	//		{	
	//			String valorKardex = recordK.getKardexValue();
	//			for(Record record : records)
	//			{
	//				if(valorKardex.contentEquals(record.getKardexValue()))
	//				{
	//					recordK.addFields(record.getField("952"));
	//				}
	//			}
	//			result.add(recordK);
	//		}


	//	XMLHandler.generateXMLFile(result);
	//	System.out.println("Total: " + result.size() + " registros adicionados ao marcXML.");
	//	System.out.println("Fim!");
	//}

	//	/**
	//	 * Metodo 'main' para as conversoes do XML do senado.
	//	 * @param args
	//	 * @throws ParserConfigurationException
	//	 * @throws SAXException
	//	 * @throws IOException
	//	 * @throws TransformerException
	//	 */
	//	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {
	//		String path = Constant.inputFilePath;
	//
	//		Document xmlFile = readMarcXMLFile(path);
	//
	//		NodeList registros = getAllMarcRecords(xmlFile);
	//
	//		// iterate over the records list 
	//		for(int i = 0; i < registros.getLength(); i++) {
	//			Node record = registros.item(i);
	//
	//			// list with all the fields in 'record'
	////			NodeList fields = record.getChildNodes();
	//
	//
	//
	//			// -------------------------- Operacoes marc para criacao ------------------
	//			// ---------------------- dos registros do senado para o Koha -------------
	//			ArrayList<Node> campos;
	//			
	//			//Operacoes no campo 779
	//			if(isFieldDuplicated(record, "779")) {
	//				campos = getDuplicatedFieldInstances(record, "779");
	//				for(Node campo : campos) {
	//					// a) Apagar subcampos
	//					Operation.removeSubfield(record, campo, "a");
	//					Operation.removeSubfield(record, campo, "l");
	//
	//					// b) concatenar subcampos
	//					String[] sub1 = {"v", "i", "k", "d", "y"};
	//					Operation.concatenateSubfields(campo, sub1, "g");
	//					
	//					// c) mudar campos e subcampos
	//					String[] sub2 = {"b", "j", "n"};
	//					String[] newSub2 = {"w", "d", "t"};
	//					Operation.swapFieldAndSubfields(campo, sub2, "773", newSub2);
	//
	//				}
	//			}else if(hasField(record, "779")){
	//				Node campo779 = getField(record.getChildNodes(), "779");
	//
	//				// a) Apagar subcampos
	//				Operation.removeSubfield(record, campo779, "a");
	//				Operation.removeSubfield(record, campo779, "l");
	//
	//				//b) Concatenar subcampos
	//				String[] sub1 = {"v", "i", "k", "d", "y"};
	//				Operation.concatenateSubfields(campo779, sub1, "g");
	//				
	//				// c) mudar campos e subcampos
	//				String[] sub2 = {"b", "j", "n"};
	//				String[] newSub2 = {"w", "d", "t"};
	//				Operation.swapFieldAndSubfields(campo779, sub2, "773", newSub2);
	//			}
	//			
	//			
	//			
	//			//Operacoes no campo 890
	//			if(isFieldDuplicated(record, "890")) {
	//				campos = getDuplicatedFieldInstances(record, "890");
	//				for(Node campo : campos) {
	//					// a) Apagar subcampos
	//					Operation.removeSubfield(record, campo, "B");
	//					Operation.removeSubfield(record, campo, "9");
	//					Operation.removeSubfield(record, campo, "6");
	//					Operation.removeSubfield(record, campo, "8");
	//					Operation.removeSubfield(record, campo, "f");
	//					Operation.removeSubfield(record, campo, "F");
	//					Operation.removeSubfield(record, campo, "g");
	//					Operation.removeSubfield(record, campo, "i");
	//					Operation.removeSubfield(record, campo, "j");
	//					Operation.removeSubfield(record, campo, "k");
	//
	//					// b) mudar subcampos
	//					Operation.swapSubfield(campo, "h", "t");
	//					Operation.swapSubfield(campo, "7", "z");
	//					
	//					// c) concatenar subcampos
	//					String[] sub3 = {"a", "b", "c", "d"};
	//					Operation.concatenateSubfields(campo, sub3, "h");
	//					
	//					// d) concatenar subcampos
	//					String[] sub4 = {"p", "P"};
	//					Operation.concatenateSubfields(campo, sub4, "7");
	//				
	//					// e) mudar campos e subcampos
	//					String[] sub5 = {"m", "1", "2", "3", "5", "4"};
	//					String[] newSub5 = {"y", "a", "8", "o", "p", "x"};
	//					Operation.swapFieldAndSubfields(campo, sub5, "952", newSub5);
	//				}
	//			}else if(hasField(record, "890")){
	//				Node campo890 = getField(record.getChildNodes(), "890");
	//				
	//				// a) Apagar subcampos
	//				Operation.removeSubfield(record, campo890, "B");
	//				Operation.removeSubfield(record, campo890, "9");
	//				Operation.removeSubfield(record, campo890, "6");
	//				Operation.removeSubfield(record, campo890, "8");
	//				Operation.removeSubfield(record, campo890, "f");
	//				Operation.removeSubfield(record, campo890, "F");
	//				Operation.removeSubfield(record, campo890, "g");
	//				Operation.removeSubfield(record, campo890, "i");
	//				Operation.removeSubfield(record, campo890, "j");
	//				Operation.removeSubfield(record, campo890, "k");
	//
	//				// b) mudar subcampos
	//				Operation.swapSubfield(campo890, "h", "t");
	//				Operation.swapSubfield(campo890, "7", "z");
	//				
	//				// c) concatenar subcampos
	//				String[] sub3 = {"a", "b", "c", "d"};
	//				Operation.concatenateSubfields(campo890, sub3, "h");
	//				
	//				// d) concatenar subcampos
	//				String[] sub4 = {"p", "P"};
	//				Operation.concatenateSubfields(campo890, sub4, "7");
	//			
	//				// e) mudar campos e subcampos
	//				String[] sub5 = {"m", "1", "2", "3", "5", "4"};
	//				String[] newSub5 = {"y", "a", "8", "o", "p", "x"};
	//				Operation.swapFieldAndSubfields(campo890, sub5, "952", newSub5);
	//				
	//			}
	//			
	//			// Operacao no campo 988:
	//			// a) mudar camo e subcampos
	//			if(isFieldDuplicated(record, "988")) {
	//				campos = getDuplicatedFieldInstances(record, "988");
	//				for(Node campo : campos) {
	//					String[] sub6 = {"a"};
	//					String[] newSub6 = {"c"};
	//					Operation.swapFieldAndSubfields(campo, sub6, "942", newSub6);
	//				}
	//			}else if(hasField(record, "988")){
	//				Node campo988 = getField(record.getChildNodes(), "988");
	//				
	//				String[] sub6 = {"a"};
	//				String[] newSub6 = {"c"};
	//				Operation.swapFieldAndSubfields(campo988, sub6, "942", newSub6);
	//			}
	//		}
	//		writeMarcXMLFile(xmlFile, Constant.outputFilePath);
	//	}
}
