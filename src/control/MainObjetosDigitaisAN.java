package control;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import entity.Field;
import entity.MARC;
import entity.Record;

public class MainObjetosDigitaisAN {
	
	/**
	 * Este metodo le o arquivo principal do acervo e separa os registros por tipo
	 * de campo do objeto digital.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args)  throws Exception {
//		public static void criacaoDeArquivosSeparados(String[] args)  throws Exception {
		MARC acervo1 = new MARC("arquivoNacionalODs/acervoAN_1.xml");
		MARC acervo2 = new MARC("arquivoNacionalODs/acervoAN_2.xml");
		ArrayList<Record> withoutLinks = new ArrayList<Record>(); // registros com link apenas no 856
		ArrayList<Record> only856 = new ArrayList<Record>(); // registros com link apenas no 856
		ArrayList<Record> only952 = new ArrayList<Record>(); // registros com link apenas no 952
		ArrayList<Record> both856and952 = new ArrayList<Record>(); // registros com link no 856 e no 952
		ArrayList<Record> singleLinkIn952u = new ArrayList<Record>(); // registros com link no 856 e 952, apenas um no 952
		ArrayList<Record> multipleLinkIn952u = new ArrayList<Record>(); // registros com link no 856 e 952, multiplos no 952
		ArrayList<Record> sameLinkIn952uAnd856u = new ArrayList<Record>(); // registros com um link no 952, que apresentam links iguais entre 856 e 952
		ArrayList<Record> differentLinkIn952uAnd856u = new ArrayList<Record>(); // registros com um link no 952, que apresentam links diferentes entre 856 e 952
		ArrayList<Record> only952singleLink = new ArrayList<Record>(); // registros com link apenas no 952, apenas um link
		ArrayList<Record> only952multipleLink = new ArrayList<Record>(); // registros com link apenas no 952, multiplos links
		
		ArrayList<Record> acervoTotal = new ArrayList<Record>();
		acervoTotal.addAll(acervo1.getRecords());
		acervoTotal.addAll(acervo2.getRecords());
		
		System.out.println("Tamanho do acervo:" + acervoTotal.size() + " registros");
		
		// itera por todos os registros e separa aqueles com apenas o 
		// campo 856$u, apenas 952 $u ou ambos
		for(Record r : acervoTotal)
		{
			// se nao tem 856 
			if(!r.hasAnyOccurrenceOfFieldAndSubfield("856", "u"))
			{	// se tem 952, adiciona em only952
				if(r.hasAnyOccurrenceOfFieldAndSubfield("952", "u"))
					only952.add(r);
				else
					// se nao tem link em nenhum campo (856 ou 952), salvar aqui
					withoutLinks.add(r);
			}else
			{	// se tem 856 e nao tem 952, adiciona em oly856 
				if(!r.hasAnyOccurrenceOfFieldAndSubfield("952", "u"))
					only856.add(r);
				else	// adiciona na lista com os dois campos presentes
					both856and952.add(r);
			}
				
		}
		
		// separa os registros que apresentam mais de um link em algum 952$u
		for(Record rec : both856and952)
		{
			boolean multipleLinkIn952 = false;
			// percorre a lista dos 952 e separa os registros com mais de um link no subcampo u
			for(Field f : rec.getField("952"))
			{
				if(f.hasSubfield("u"))
				{
					if(f.getSubfield("u").get(0).getValue().contains("|"))
					{
						multipleLinkIn952u.add(rec);
						multipleLinkIn952 = true;
						break;
					}
				}
			}
			if(!multipleLinkIn952)
				singleLinkIn952u.add(rec);
		}
		
		// separa os registros com multiplos links e so um link, dentre
		// aqueles com link apenas no 952.
		for(Record r : only952)
		{
			boolean multipleLink = false;
			for(Field f : r.getField("952"))
			{
				if(f.hasSubfield("u"))
				{
					if(f.getSubfield("u").get(0).getValue().contains("|"))
					{
						only952multipleLink.add(r);
						multipleLink = true;
						break;
					}
				}
			}
			if(!multipleLink)
				only952singleLink.add(r);
		}
		
				
		// separa os registros que apresentam links diferentes entre os subcampos 856$u e 952$u
		boolean sameLink856and952;
		for(Record rec : singleLinkIn952u)
		{
			sameLink856and952 = false;
			String link = rec.getField("856").get(0).getSubfield("u").get(0).getValue();
			
			for(Field f : rec.getField("952"))
			{
				if(f.hasSubfield("u"))
				{
					if(f.getSubfield("u").get(0).getValue().contentEquals(link))
					{
						sameLinkIn952uAnd856u.add(rec);
						sameLink856and952 = true;
						break;
					}
				}
			}
			if(!sameLink856and952)
				differentLinkIn952uAnd856u.add(rec);
		}
		
		write856LinksToOutput("only856", only856);
		write952LinksToOutput("only952singleLink", only952singleLink);
		
		//renameFiles856("arquivoNacionalODs/filesOnly856/ods856editado/", "only856",  only856);
		renameFiles952("arquivoNacionalODs/filesOnly952singleLink/ods952editado/", "only952singleLink", only952singleLink);
		
		System.out.println("withoutLinks:" + withoutLinks.size() + " registros");
		XMLHandler.generateXMLFile(withoutLinks, "arquivoNacionalODs/withoutLinks.xml");
		System.out.println("only856:" + only856.size() + " registros");
		XMLHandler.generateXMLFile(only856, "arquivoNacionalODs/only856.xml");
		System.out.println("only952:" + only952.size() + " registros");
		XMLHandler.generateXMLFile(only952, "arquivoNacionalODs/only952.xml");
		System.out.println("856 e 952:" + both856and952.size() + " registros");
		XMLHandler.generateXMLFile(both856and952, "arquivoNacionalODs/both856and952.xml");
		System.out.println("um link no 952:" + singleLinkIn952u.size() + " registros");
		XMLHandler.generateXMLFile(singleLinkIn952u, "arquivoNacionalODs/singleLinkIn952u.xml");
		System.out.println("multiplos links no 952:" + multipleLinkIn952u.size() + " registros");
		XMLHandler.generateXMLFile(multipleLinkIn952u, "arquivoNacionalODs/multipleLinkIn952u.xml");
		System.out.println("only952singleLink:" + only952singleLink.size() + " registros");
		XMLHandler.generateXMLFile(only952singleLink, "arquivoNacionalODs/only952singleLink.xml");
		System.out.println("only952multipleLink:" + only952multipleLink.size() + " registros");
		XMLHandler.generateXMLFile(only952multipleLink, "arquivoNacionalODs/only952multipleLink.xml");
		System.out.println("mesmo link no 952 e 856:" + sameLinkIn952uAnd856u.size() + " registros");
		XMLHandler.generateXMLFile(sameLinkIn952uAnd856u, "arquivoNacionalODs/sameLinkIn952uAnd856u.xml");
		System.out.println("diferentes links no 952 e 856:" + differentLinkIn952uAnd856u.size() + " registros");
		XMLHandler.generateXMLFile(differentLinkIn952uAnd856u, "arquivoNacionalODs/differentLinkIn952uAnd856u.xml");
//		System.out.println("856 e 952:" + both856and952.size() + " registros");
//		XMLHandler.generateXMLFile(both856and952, "arquivoNacionalObjetosDigitais/novosArquivos/both856and952.xml");


	}
	
	/**
	 * Creates a new file name format: "type_999$c_name".
	 * @param path
	 * @param listType
	 * @param records
	 */
	private static void renameFiles856(String path, String listType, ArrayList<Record> records) {
		File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {

            if (listOfFiles[i].isFile()) {

                File f = new File(path + listOfFiles[i].getName()); 
                
                Record r = getCorresponding856Record(f.getName(), records);
                
                f.renameTo(new File(path + listType + "_" + r.getField("999").get(0).
                		getSubfield("c").get(0).getValue() + "_" + f.getName()));
            }
        }

        System.out.println("Arquivos '" + listType + "' renomeados. ");
		
	}
	
	/**
	 * Creates a new file name format: "type_999$c_name".
	 * @param path
	 * @param listType
	 * @param records
	 */
	private static void renameFiles952(String path, String listType, ArrayList<Record> records) {
		File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {

            if (listOfFiles[i].isFile()) {

                File f = new File(path + listOfFiles[i].getName()); 
                
                String chave = getCorresponding952recordAndSubfield(f.getName(), records);
                
                f.renameTo(new File(path + listType + "_" + chave + "_" + f.getName()));
            }
        }

        System.out.println("Arquivos '" + listType + "' renomeados. ");
		
	}

	private static Record getCorresponding856Record(String nome, ArrayList<Record> records) {
		
		for(Record r : records)
		{
			for (Field f : r.getField("856"))
			{
				String[] temp = f.getSubfield("u").get(0).getValue().split("/");
				String nomeAtual = temp[temp.length - 1];
				
				if (nome.contentEquals(nomeAtual))
					return r;
			}
		}
		
 		System.out.println("arquivo nao encontrado:  " + nome);
		return null;
	}
	
	private static String getCorresponding952recordAndSubfield(String nome, ArrayList<Record> records) {
			
			for(Record r : records)
			{
				for (Field f : r.getField("952"))
				{
					if(!f.hasSubfield("u"))
						continue;
					 
					String[] temp = f.getSubfield("u").get(0).getValue().split("/");
					String nomeAtual = temp[temp.length - 1];
					
					if (nome.contentEquals(nomeAtual))
						return r.getField("999").get(0).getSubfield("c").get(0).getValue() + "_"
								+ f.getSubfield("9").get(0).getValue();
				}
			}
			
	 		System.out.println("arquivo nao encontrado:  " + nome);
			return null;
		}

	private static void write952LinksToOutput(String title, ArrayList<Record> registros) throws IOException {
		BufferedWriter writer, writerExcept, IDwriter;

		// it is false so the previous content is cleared. "true" to keep the content.
		writer = new BufferedWriter(new FileWriter("arquivoNacionalODs/files" + title 
				+ "/links" + title + ".txt", false));
		IDwriter = new BufferedWriter(new FileWriter("arquivoNacionalODs/files" + title
				+ "/identifier" + title + ".txt", false));
		writerExcept = new BufferedWriter(new FileWriter("arquivoNacionalODs/files" + title
				+ "/error" + title + ".txt", false));
		
		IDwriter.append("id 001 -------- if 952 $9 ---------- link\n");

		for(Record r : registros)
		{
			if(!r.hasField("952"))
				System.err.println("###Registro sem campo 952");
			else
			{
				for(int i = 0; i < r.getField("952").size(); i++)
				{
					Field f = r.getField("952").get(i);
					if(f.hasSubfield("u"))
					{
						// realiza alteracao no conteudo do link para novo link do bnweb
						String link = f.getSubfield("u").get(0).getValue().
								replace("http://biblioteca.an.gov.br/bnweb/", "http://biblioteca/bnweb/");
						
						if(link.endsWith(".pdf") && !link.contains(" ")) 
						{
							writer.append(link + "\n");
							IDwriter.append("999 $c: " + r.getField("999").get(0).getSubfield("c")
									.get(0).getValue() + 
									"952 $9:" + f.getSubfield("9").get(0).getValue() +
									"\tLink: " + link + "\n");
						}else
						{
							writerExcept.append("999 $c: " + r.getField("999").get(0).getSubfield("c")
									.get(0).getValue() + 
									"952 $9:" + f.getSubfield("9").get(0).getValue() +
									"\tLink: " + link + "\n");
						}
					}
				}
			}
		}
		writer.close();
		IDwriter.close();
		writerExcept.close();
	}
	
	private static void write856LinksToOutput(String title, ArrayList<Record> registros) throws IOException {
		BufferedWriter writer, writerExcept, IDwriter;

		// it is false so the previous content is cleared. "true" to keep the content.
		writer = new BufferedWriter(new FileWriter("arquivoNacionalODs/files" + title + "/links" + title + ".txt", false));
		IDwriter = new BufferedWriter(new FileWriter("arquivoNacionalODs/files" + title + "/identifier" + title + ".txt", false));
		writerExcept = new BufferedWriter(new FileWriter("arquivoNacionalODs/files" + title + "/error" + title + ".txt", false));
		
		IDwriter.append("id 001 ---------- link\n");

		for(Record r : registros)
		{
			if(!r.hasField("856"))
				System.err.println("----Registro sem campo 856");
			else
			{
				for(int i = 0; i < r.getField("856").size(); i++)
				{
					Field f = r.getField("856").get(i);
					if(f.hasSubfield("u"))
					{
						// realiza alteracao no conteudo do link para novo link do bnweb
						String link = f.getSubfield("u").get(0).getValue().
								replace("http://biblioteca.an.gov.br/bnweb/", "http://biblioteca/bnweb/");
						
						if(link.endsWith(".pdf") && !link.contains(" ")) 
						{
							writer.append(link + "\n");
							IDwriter.append("999 $c: " + r.getField("999").get(0).getSubfield("c")
									.get(0).getValue() + 
									"\tLink: " + link + "\n");
						}else
						{
							writerExcept.append("999 $c: " + r.getField("999").get(0).getSubfield("c")
									.get(0).getValue() + 
									"\tLink: " + link + "\n");
						}
					}
				}
			}
		}
		writer.close();
		IDwriter.close();
		writerExcept.close();
	}



	/**
	 * Percorre toda a lista de registros definida em 'registros'
	 * e salva todos os links de download disponiveis no campo 856.
	 * O download é feito por um script, com wget. O nome do arquivo
	 * baixado eh definido como o biblionumber do registro em questao.
	 * @param registros
	 * @param filename
	 * @throws IOException 
	 */
	public static void createFileForODdownloadFromField856(ArrayList<Record> registros, String path) throws IOException
	{
		BufferedWriter writer;
		BufferedWriter writerExcept;
		// it is false so the pervious content is cleared. "true" to keep the content.
		writer = new BufferedWriter(new FileWriter(path + "linksCampos856.sh", false));
		writerExcept = new BufferedWriter(new FileWriter(path + "linkExceptions856.txt", false));
		

		writer.append("#! /bin/sh\n");
		
		for(Record r : registros)
		{
			if(!r.hasField("856"))
				System.err.println("----Registro sem campo 856");
			else
			{
				for(int i = 0; i < r.getField("856").size(); i++)
				{
					Field f = r.getField("856").get(i);
					if(f.hasSubfield("u"))
					{
						String link = f.getSubfield("u").get(0).getValue();
						if(link.endsWith(".pdf"))
						{
							writer.append("wget -a outputDownload.txt -O ./ods856/IBICT2_" 
								+ r.getField("999").get(0).getSubfield("c").get(0).getValue() + "_" + i
								+ ".pdf " + link + "\n");
						}else
						{
							writerExcept.append("999 $c: " + r.getField("999").get(0).getSubfield("c").get(0).getValue() + 
									"\tLink: " + link + "\n");
						}
					}
				}
			}
		}
		writer.close();
		writerExcept.close();
	}
	
	/**
	 * Percorre toda a lista de registros definida em 'registros'
	 * e salva todos os links de download disponiveis no campo 952.
	 * O download é feito por um script, com wget. O nome do arquivo
	 * baixado eh definido como o biblionumber do registro em questao
	 * + numero do campo 952$9 do exemplar.
	 * @param registros
	 * @param filename
	 * @throws IOException 
	 */
	public static void createFileForODdownloadFromField952(ArrayList<Record> registros, String path) throws IOException
	{
		BufferedWriter writer;
		BufferedWriter writerExcept;
		// it is false so the pervious content is cleared. "true" to keep the content.
		writer = new BufferedWriter(new FileWriter(path + "linksCampos952.sh", false));
		writerExcept = new BufferedWriter(new FileWriter(path + "linkExceptions952.txt", false));
		

		writer.append("#! /bin/sh\n");
		
		for(Record r : registros)
		{
			if(!r.hasField("952"))
				System.err.println("Registro sem campo 952");
			else
			{
				for(Field f : r.getField("952"))
				{
					if(f.hasSubfield("u"))
					{
						String link = f.getSubfield("u").get(0).getValue();
						if(link.endsWith(".pdf"))
						{
						writer.append("wget -a outputDownload.txt -O ./ods952/IBICT2_" 
								+ r.getField("999").get(0).getSubfield("c").get(0).getValue() 
								+ "_" + f.getSubfield("9").get(0).getValue() + ".pdf " + link + "\n");
						}else
						{
							writerExcept.append("999 $c: " + r.getField("999").get(0).getSubfield("c").get(0).getValue() + 
									"\t952 $9: " + f.getSubfield("9").get(0).getValue() + "\tLink: " + link + "\n");
						}
					}
				}
			}
		}
		writer.close();
		writerExcept.close();
	}
	/**
	 * atualiza o link de download nos registros com link.
	 * @param args
	 * @throws Exception
	 */
	public static void mainAtualizaLink(String[] args)  throws Exception {
		ArrayList<String[]> csv = CSVController.readCSV("arquivoNacionalObjetosDigitais/downloadsApenas952/relatorio.tab", "\t");
		MARC acervo = new MARC("arquivoNacionalObjetosDigitais/952sem856uUmLink.xml");
		ArrayList<Record> regAtualizado = new ArrayList<Record>();
		
		for(int i = 1; i < csv.size(); i++)
		{
			String[] linha = csv.get(i);
//			System.out.println(linha[0]);
			for(Record registro: acervo.getRecords())
			{
				if(registro.getField("999").get(0).getSubfield("c").get(0).getValue().contentEquals(linha[0].split("_")[1]))
				{
					for(Field campo : registro.getField("952"))
					{
						String id952 = linha[0].split("_")[2].split("\\.")[0];
						if(campo.getSubfield("9").get(0).getValue().contentEquals(id952))
						{
							campo.getSubfield("u").get(0).setValue("http://kohaan.ibict.br/cgi-bin/koha/opac-retrieve-file.pl?id=" + linha[1]);
							if(!regAtualizado.contains(registro))
								regAtualizado.add(registro);
							break;
						}
					}
				}
				
			}
		}
		
		XMLHandler.generateXMLFile(regAtualizado, "arquivoNacionalObjetosDigitais/952sem856uUmLink-ODatualizado.xml");
	}
	/**
	 * Separacao dos registros com link do acervo Koha AN
	 * @param args
	 * @throws Exception 
	 */
	public static void mainSeparacao(String[] args) throws Exception {

		// passo 1: Busca de registros com ODs
		
		MARC marcAcervo1 = new MARC("arquivoNacionalObjetosDigitais/kohaANmigradoComLink856.xml");
		MARC marcAcervo2 = new MARC("arquivoNacionalObjetosDigitais/kohaANmigradoComLink952.xml");
		ArrayList<Record> records = new ArrayList<Record>();
		
		ArrayList<Record> recordsno952u = new ArrayList<Record>();
		
		for(Record r : marcAcervo1.getRecords())
		{
			boolean has952u = false;
			if(r.hasField("952"))
				for(Field f : r.getField("952"))
				{
					if(f.hasSubfield("u"))
						has952u = true;
				}
			if(!has952u)
				recordsno952u.add(r);
		}
		
		ArrayList<Record> recordsno856u = new ArrayList<Record>();
		HashMap<String, Integer> tipos = new HashMap<String, Integer>();
		
		for(Record r : marcAcervo2.getRecords())
		{
			boolean has856u = false;
			if(!r.hasField("856"))
			{
				recordsno856u.add(r);
				String tipoRegistro = r.getField("942").get(0).getSubfield("c").get(0).getValue();
				if(!tipos.containsKey(tipoRegistro))
					tipos.put(tipoRegistro, 0);
				tipos.put(tipoRegistro, tipos.get(tipoRegistro) + 1); 
			}
				
		}
		
//		Iterator<String> tiposMaterial = tipos.keySet().iterator();
		
//		while(tiposMaterial.hasNext())
//		{
//			String t = tiposMaterial.next();
//			System.out.println(t + ":" + tipos.get(t));
//		}
		
		// 1.a : campo 952
//		for(Record r : marcAcervo.getRecords())
//		{
//			if(r.hasField("952"))
//				for(Field field : r.getField("952"))
//				{
//					if(field.hasSubfield("u") && !records.contains(r))
//					{
//						records.add(r);
//						break;
//					}
//				}
//		}
		
		// 1.b : campo 856
//		for(Record r : marcAcervo.getRecords())
//		{
//			if(r.hasField("856"))
//				for(Field field : r.getField("856"))
//				{
//					if(field.hasSubfield("u") && !records.contains(r))
//					{
//						records.add(r);
//						break;
//					}
//				}
//		}
		
		// 1.c : merge dos dois arquivos
		records.addAll(marcAcervo1.getRecords());
		
		for(Record r : marcAcervo2.getRecords())
		{
			if(marcAcervo1.recordsWithFieldAndSubfieldValue("999", "c", r.getField("999").get(0).getSubfield("c").get(0).getValue()) == null)
				records.add(r);
		}
		
		ArrayList<Record> maisDeUm952 = new ArrayList<Record>();
		// OBS: separar registros com mais de um 952
		for(Record rec : marcAcervo2.getRecords())
		{
			if(rec.getField("952").size() > 1)
				maisDeUm952.add(rec);
		}
		
		ArrayList<Record> campo952Udiferente = new ArrayList<Record>();
		
		// buscar 952 u diferentes
		for(Record rec : maisDeUm952)
		{
			if(!rec.getField("952").get(0).hasSubfield("u"))
			{
				campo952Udiferente.add(rec);
				continue;
			}
			String atual = rec.getField("952").get(0).getSubfield("u").get(0).getValue();
			for(Field f : rec.getField("952"))
			{
				if((!f.hasSubfield("u")) || !f.getSubfield("u").get(0).getValue().contentEquals(atual))
				{
					campo952Udiferente.add(rec);
					break;
				}
			}
		}
		
		ArrayList<Record> with856uAnd952u = new ArrayList<Record>();
		HashMap<String, Integer> contMateriais = new HashMap<String, Integer>();
		
		for(Record rec : records)
		{
			if(!rec.hasField("856") || !rec.hasField("952"))
				continue;
			
			boolean has952u = false;
			for(Field f : rec.getField("952"))
			{
				if(f.hasSubfield("u"))
					has952u = true;
			}
			
			if(has952u)
			{
				with856uAnd952u.add(rec);
				String tipo = rec.getField("942").get(0).getSubfield("c").get(0).getValue();
				if(!contMateriais.containsKey(tipo))
					contMateriais.put(tipo, 0);
				contMateriais.put(tipo, contMateriais.get(tipo) + 1);
				
			}
		}
		
		Iterator<String> tiposMaterial = contMateriais.keySet().iterator();
		
		while(tiposMaterial.hasNext())
		{
			String t = tiposMaterial.next();
			System.out.println(t + ":" + contMateriais.get(t));
		}
		
		// separar os registros por 952 u com ||
		ArrayList<Record> recComBarras = new ArrayList<Record>();
		ArrayList<Record> recSemBarras = new ArrayList<Record>();
		
		for(Record rec : with856uAnd952u)
		{
			boolean hasBarras = false;
			for(Field f : rec.getField("952"))
			{
				if(f.hasSubfield("u"))
					if(f.getSubfield("u").get(0).getValue().contains("||"))
					{
						recComBarras.add(rec);
						hasBarras = true;
						break;
					}
			}
			if(!hasBarras)
				recSemBarras.add(rec);
		}
		
		ArrayList<Record> linkDiferenteEntre952Ue856U = new ArrayList<Record>();
		ArrayList<Record> linkIguaisEntre952Ue856U = new ArrayList<Record>();

		
		for(Record rec : recSemBarras)
		{
			boolean linksIguais = true;
			String link = rec.getField("856").get(0).getSubfield("u").get(0).getValue();
			
			for(Field f : rec.getField("952"))
			{
				if(!f.hasSubfield("u"))
					continue;
				if(!f.getSubfield("u").get(0).getValue().contentEquals(link))
				{
					linksIguais = false;
					linkDiferenteEntre952Ue856U.add(rec);
					break;
				}
					
			}
			if(linksIguais)
				linkIguaisEntre952Ue856U.add(rec);
				
		}
		
		
		// gerar os links de arquivos com apenas 952$u ocupado.
		System.out.println("Records only 952: " + recordsno856u.size());
		
		
		
		ArrayList<Record> recordsWithNlinks = new ArrayList<Record>();
		ArrayList<Record> recordsWith1link = new ArrayList<Record>();
		
		for(Record recOnly952 : recordsno856u)
		{
			if(recOnly952.hasField("856"))
				System.err.println("Erro, campo 856 existente.");
			
			if(!recOnly952.hasField("999"))
				System.err.println("Registro sem biblionumber");
			
			if(recOnly952.getField("999").size() > 1)
				System.err.println("Registro com mais de um biblionumber");
			
			// separa arquivos com mais de um link no 952$u
			for(Field field952 : recOnly952.getField("952"))
				{
				if(field952.hasSubfield("u"))
					if(field952.getSubfield("u").get(0).getValue().contains("|"))
						if(!recordsWithNlinks.contains(recOnly952))
							recordsWithNlinks.add(recOnly952);
						
				}
			
			
			
			// se não está na lista de registros com mais de um link, adicionar na 
			// lista de registros com apenas um link.
			if(!recordsWithNlinks.contains(recOnly952))
				recordsWith1link.add(recOnly952);
			
		}
		
		// printa o link apenas dos registros de um único link
		// arquivo para download de objetos
		BufferedWriter writer;
		// it is false so the pervious content is cleared. "true" to keep the content.
		writer = new BufferedWriter(new FileWriter("arquivoNacionalObjetosDigitais/linksCampos952.sh", false));
		

		writer.append("#! /bin/sh\n");
		
		for(Record recLink : recordsWith1link)
		{
		// inserir o nome do arquivo : modelo IBICT
					for(Field field952 : recLink.getField("952"))
						if(field952.hasSubfield("u"))
						{
						writer.append("wget -a outputDownload.txt -O ./downloadsApenas952/IBICT_" 
								+ recLink.getField("999").get(0).getSubfield("c").get(0).getValue() 
								+ "_" + field952.getSubfield("9").get(0).getValue() + ".pdf " + field952.getSubfield("u").get(0).getValue() + "\n");
						}
		}
		writer.close();
		
		
		
		XMLHandler.generateXMLFile(records, "arquivoNacionalObjetosDigitais/kohaANmigradoComLinkTodos.xml");
		XMLHandler.generateXMLFile(maisDeUm952, "arquivoNacionalObjetosDigitais/maisDeUm952.xml");
		XMLHandler.generateXMLFile(campo952Udiferente, "arquivoNacionalObjetosDigitais/952diferente.xml");
		XMLHandler.generateXMLFile(recordsno952u, "arquivoNacionalObjetosDigitais/856sem952u.xml");
		XMLHandler.generateXMLFile(recordsno856u, "arquivoNacionalObjetosDigitais/952sem856u.xml");
		XMLHandler.generateXMLFile(with856uAnd952u, "arquivoNacionalObjetosDigitais/com856Ue952U.xml");
		XMLHandler.generateXMLFile(recSemBarras, "arquivoNacionalObjetosDigitais/com856Ue952UsemBarras.xml");
		XMLHandler.generateXMLFile(recComBarras, "arquivoNacionalObjetosDigitais/com856Ue952UcomBarras.xml");
		XMLHandler.generateXMLFile(linkDiferenteEntre952Ue856U, "arquivoNacionalObjetosDigitais/linksDiferentesEntre952Ue856U.xml");
		XMLHandler.generateXMLFile(linkIguaisEntre952Ue856U, "arquivoNacionalObjetosDigitais/linksIguaisEntre952Ue856U.xml");		
		XMLHandler.generateXMLFile(recordsWithNlinks, "arquivoNacionalObjetosDigitais/952sem856uMultiplosLinks.xml");	
		XMLHandler.generateXMLFile(recordsWith1link, "arquivoNacionalObjetosDigitais/952sem856uUmLink.xml");
		
		
		
		
		
//		MARC marcAcervo = new MARC("arquivoNacionalObjetosDigitais/kohaANmigrado1.xml");
//		BufferedWriter writer;
//
//		writer = new BufferedWriter(new FileWriter("arquivoNacionalObjetosDigitais/campos952.txt", true));
//		
//		for(Record r : marcAcervo.getRecords())
//		{
//			if(r.hasField("952"))
//				for(Field field856 : r.getField("952"))
//				{
//					if(field856.hasSubfield("u"))
//						for(Subfield subU : field856.getSubfield("u"))
//						{
//							writer.append(r.getField("999").get(0).getSubfield("c").get(0).getValue() + 
//									"\t" + r.getField("952").get(0).getSubfield("9").get(0).getValue() + 		
//									"\t" + subU.getValue() + "\n");
//						}
//				}
//		}
//		writer.close();
		
		
		//		MARC marcAcervo = new MARC("arquivoNacionalObjetosDigitais/kohaANmigradoComLink.xml");
//		BufferedWriter writer;
//
//		writer = new BufferedWriter(new FileWriter("arquivoNacionalObjetosDigitais/campos952.txt", true));
//		
//		for(Record r : marcAcervo.getRecords())
//		{
//			if(r.hasField("952"))
//				for(Field field856 : r.getField("952"))
//				{
//					if(field856.hasSubfield("u"))
//						for(Subfield subU : field856.getSubfield("u"))
//						{
//							writer.append(r.getField("999").get(0).getSubfield("c").get(0).getValue() + 
//									"\t" + r.getField("952").get(0).getSubfield("9").get(0).getValue() + 		
//									"\t" + subU.getValue() + "\n");
//						}
//				}
//		}
//		writer.close();
		
//		XMLHandler.generateXMLFile(records, "arquivoNacionalObjetosDigitais/kohaANmigradoComLink2.xml");
//		System.out.println("fim: " + records.size() + " registros.");
	}
	
	public static void mainTeste(String[] args)  throws Exception {
		MARC acervo1 = new MARC("arquivoNacionalVerif/kohaAN15-03_01.xml");
		MARC acervo2 = new MARC("arquivoNacionalVerif/kohaAN15-03_02.xml");
		
//		String campo = "100", subcampo1 = "d", subcampo2 = "c", subcampo3 = "z", subcampo4 = "w";
//		System.out.println("Campo "+ campo + ": " + (acervo1.countRecordsWithField(campo) + acervo2.countRecordsWithField(campo)) + " registros");
//		System.out.println("Campo " + campo + subcampo1 + ": " + (acervo1.countRecordsWithFieldAndSubfield(campo, subcampo1) 
//				+ acervo2.countRecordsWithFieldAndSubfield(campo, subcampo1)) + " registros");
//		System.out.println("Campo " + campo + subcampo2 + ": " + (acervo1.countRecordsWithFieldAndSubfield(campo, subcampo2) 
//				+ acervo2.countRecordsWithFieldAndSubfield(campo, subcampo2)) + " registros");
//		System.out.println("Campo " + campo + subcampo3 + ": " + (acervo1.countRecordsWithFieldAndSubfield(campo, subcampo3) 
//				+ acervo2.countRecordsWithFieldAndSubfield(campo, subcampo3)) + " registros");
//		System.out.println("Campo " + campo + subcampo4 + ": " + (acervo1.countRecordsWithFieldAndSubfield(campo, subcampo4) 
//				+ acervo2.countRecordsWithFieldAndSubfield(campo, subcampo4)) + " registros");
		
		HashMap<String, Integer> quantItens = new HashMap<String, Integer>();
		for(Record r : acervo1.getRecords())
		{
			if(r.hasField("952"))
				for(Field f : r.getField("952"))
				{
					String tipo = f.getSubfield("y").get(0).getValue();
					if(!quantItens.containsKey(tipo))
						quantItens.put(tipo, 0);
					quantItens.put(tipo, quantItens.get(tipo) + 1);
				}
		}
		
		for(Record r : acervo2.getRecords())
		{
			if(r.hasField("952"))
				for(Field f : r.getField("952"))
				{
					String tipo = f.getSubfield("y").get(0).getValue();
					if(!quantItens.containsKey(tipo))
						quantItens.put(tipo, 0);
					quantItens.put(tipo, quantItens.get(tipo) + 1);
				}
		}
//		
		Iterator<String> it = quantItens.keySet().iterator();
		while(it.hasNext())
		{
			String tipo = it.next();
			System.out.println(tipo + ": " + quantItens.get(tipo));
		}
//		System.out.println("only856:" + sem90.size() + " registros sem cutter.");
//		XMLHandler.generateXMLFile(sem90, "arquivoNacionalVerif/acervoAN1sem090.xml");
//		XMLHandler.generateXMLFile(r773, "arquivoNacionalVerif/acervoAN1maisDeUm773.xml");
//		XMLHandler.generateXMLFile(r100, "arquivoNacionalVerif/acervoAN1maisDeUm100.xml");
	}
}
