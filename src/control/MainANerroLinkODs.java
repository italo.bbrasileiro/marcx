package control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class MainANerroLinkODs {
	
	/**
	 * Cria subcampo 100 $d no RB, a partir das datas de nascimento e morte dos autores
	 * encontrados nos registros de autoridades.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args)  throws Exception 
	{
		MARC acervo01 = new MARC("arquivoNacional2/acervo01.xml");
		MARC acervo02 = new MARC("arquivoNacional2/acervo02.xml");
		MARC acervo03 = new MARC("arquivoNacional2/acervo03.xml");
		MARC acervo04 = new MARC("arquivoNacional2/acervo04.xml");
		System.out.println("Lidos");
		
		ArrayList<Record> registrosAlterados = new ArrayList<Record>();
		
		ArrayList<Record> acervo = new ArrayList<>();
		
		acervo.addAll(acervo01.getRecords());
		acervo.addAll(acervo02.getRecords());
		acervo.addAll(acervo03.getRecords());
		acervo.addAll(acervo04.getRecords());
		
		System.out.println("Leitura do acervo1");
		
		String url;
		for(Record rec : acervo)
		{
			if(rec.hasAnyOccurrenceOfFieldAndSubfield("856", "u"))
			{
				for(Field campo856 : rec.getField("856"))
				{
					url = campo856.getSubfield("u").get(0).getValue(); 
					if(url.startsWith("/cgi-bin/koha/"))
					{
						url = url.replace("/cgi-bin/koha/", "http://koha.an.gov.br/cgi-bin/koha/");
						campo856.getSubfield("u").get(0).setValue(url);
						if(!registrosAlterados.contains(rec))
						{
							registrosAlterados.add(rec);
						}
					}
				}
			}
			
			if(rec.hasAnyOccurrenceOfFieldAndSubfield("952", "u"))
			{
				for(Field campo952 : rec.getField("952"))
				{
					if(!campo952.hasSubfield("u"))
						continue;
					
					url = campo952.getSubfield("u").get(0).getValue(); 
					if(url.startsWith("/cgi-bin/koha/"))
					{
						url = url.replace("/cgi-bin/koha/", "http://koha.an.gov.br/cgi-bin/koha/");
						campo952.getSubfield("u").get(0).setValue(url);
						if(!registrosAlterados.contains(rec))
						{
							registrosAlterados.add(rec);
						}
					}
				}
			}
		}
		
		System.out.println(registrosAlterados.size() + " registros alterados.");
		XMLHandler.generateXMLFile(registrosAlterados, "arquivoNacional2/urlCorrigida.xml");
	}
	
}
