package control;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import entity.*;


public class MainIBICT9 
{
	static RegexLibrary regexLib = new RegexLibrary();
	
	public static void main(String[] args) throws Exception 
	{
		MARC acervo = new MARC("kohaIbict5/acervo.xml");
		ArrayList<Record> recordsEditados = acervo.getRecords();
		
		for(Record registro : recordsEditados)
		{
			adicionaCampo035(registro);
			if(!registro.hasField("952"))
				continue;
			
			for(Field field952 : registro.getField("952"))
			{					
				if(!field952.hasSubfield("h"))
					continue;

				if(!registro.hasField("853"))
					adicionaCampo853aoRegistro(field952);
				
				adicionaCampos863CorrespondenteAosExemplares(field952);
			}
		}
		criaArquivoXML(recordsEditados, "kohaIbict5/acervoFinal.xml");
	}
	

	private static boolean adicionaCampo853aoRegistro(Field field952) 
	{	
		Field field853 = generateField853(field952);
		
		if(field853 != null)
		{
			field952.getCorrespondingRecord().addField(field853);
			return true;
		}else
			printDadosDoRegistro(field952.getCorrespondingRecord());
		return false;
	
	}
	
	private static void printDadosDoRegistro(Record record) 
	{
		System.out.print("biblionumber:;" + record.getField("999").get(0).getSubfield("c").get(0).getValue());
		System.out.println(";titulo:;" + record.getField("245").get(0).getSubfield("a").get(0).getValue());
	}


	private static Field generateField853(Field fieldExample952) 
	{
		String fieldRegex = RegexLibrary.getCorrespondingRegex(fieldExample952.getSubfield("h").get(0).getValue());
		return RegexLibrary.generateField853ForRegex(fieldRegex, fieldExample952);
	}



	private static void adicionaCampos863CorrespondenteAosExemplares(Field field952) 
	{
			Field field863 = generateField863basedOn952(field952);
			if(field863 != null)
				field952.getCorrespondingRecord().addField(field863);
	}


	private static void adicionaCampo035(Record registro) {
		registro.addField(new Field("035", "a", "000001-9", Constant.dataFieldDenomination));
	}


	private static Field generateField863basedOn952(Field field952) {
		String fieldRegex = RegexLibrary.getCorrespondingRegex(field952.getSubfield("h").get(0).getValue());
		return RegexLibrary.generateField863ForRegex(fieldRegex, field952);
	}
	
	private static void criaArquivoXML(ArrayList<Record> records, String outputFile) {
		System.out.println(records.size() + " periodicos criados em " + outputFile);
		XMLHandler.generateXMLFile(records, outputFile);
	}
	
}

