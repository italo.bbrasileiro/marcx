package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class MainBDB2 {
	
	static ArrayList<Record> recordsInicial;
	static HashMap<String, ArrayList<Record>> recordsByISBNValue;
	static HashMap<String, String> isbnsInExtract;
	static ArrayList<Record> withoutISBN;
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		String outputFinalResult =  "BDB2/output/registrosFinalCompleto.xml";
		ArrayList<Record> registros = getRecordsFromFile(outputFinalResult);
		
		int cont952p = 0;
		for(Record record : registros)
		{
			if(record.hasAnyOccurrenceOfFieldAndSubfield("952", "p"))
				for(Field field952 : record.getField("952"))
				{
					if(field952.hasSubfield("p"))
						cont952p++;
				}
		}
		
		System.out.println("Total de ocorrências do 952 $p" + cont952p);
		
	}
	
	public static void mainOld(String[] args) throws 
	IOException, ParserConfigurationException, SAXException, TransformerException 
	{
		String outputFinalResult =  "BDB2/output/registrosFinalCompleto.xml";
		
		isbnsInExtract = getISBNsInExtract();
		constroiRegistrosMarc();
		separateRecordsWithAndWithoutISBN();
		mergeMultipleInstancesOf082();
		criaExemplaresComDadosDoAcervo();
		removeTomboDosRegistros();
		inserirInformacaoDeBaixa();
		darBaixaEmRegistrosSemExemplaresDisponiveis();
		
		// contagem de quantos 952 $x
		int contX = 0;
		for(Record record : recordsInicial)
		{
			if (record.hasField("952"))
				for(Field field : record.getField("952"))
				{
					if(field.hasSubfield("x"))
						contX++;
				}
		}
		System.out.println("Total de entradas do campo 952 $x:" + contX);
		
		createXMLFile(recordsInicial, outputFinalResult);
	}

	private static void darBaixaEmRegistrosSemExemplaresDisponiveis() {
		boolean semExemplares;
		
		for(Record record : recordsInicial)
		{
			semExemplares = true;
			if(record.hasField("952"))
				for(Field field : record.getField("952"))
				{
					if(!field.hasSubfield("0"))
					{
						semExemplares = false;
						break;
					}
				}
			if(semExemplares)
				record.getField("942").get(0).addSubfield(new Subfield("n", "1"));
		}
	}

	private static void removeTomboDosRegistros() 
	{
		for(Record record : recordsInicial)
		{
			if(record.hasAnyOccurrenceOfFieldAndSubfield("901", "a"))
				for(Field campo901 : record.getField("901"))
					record.removeField(campo901);
		}
	}

	private static void inserirInformacaoDeBaixa() throws IOException {
		HashMap<String, String> tombosEcodDoc = getAllTombosECodDoc();
		int totalTombos = tombosEcodDoc.keySet().size(),
				contTombos = 0;
		
		Field field;
		for(String tombo : tombosEcodDoc.keySet())
		{
			if(contTombos++ % 200 == 0)
				System.out.println(contTombos + "/" + totalTombos);
			field = getFieldWithTombo(tombo);
			if(field != null)
			{
				field.addSubfield(new Subfield("0", "1")); // TODO add subcampo do status de baixa
				if(tombosEcodDoc.get(tombo) != null)
				{
					if(field.hasSubfield("p"))
						field.getSubfield("p").get(0).setValue(tombosEcodDoc.get(tombo));
					else
						field.addSubfield(new Subfield("p", tombosEcodDoc.get(tombo)));
				}
			}
		}
	}

	private static HashMap<String, String> getAllTombosECodDoc() throws IOException {
		HashMap<String, String> tombos = new HashMap<>();
		
		tombos.putAll(getTombosArquivo1());
		tombos.putAll(getTombosArquivosIguais("BDB2/baixas/arquivo2.csv"));
		tombos.putAll(getTombosArquivosIguais("BDB2/baixas/arquivo3.csv"));
		tombos.putAll(getTombosArquivosIguais("BDB2/baixas/arquivo4.csv"));
		tombos.putAll(getTombosArquivosIguais("BDB2/baixas/arquivo5.csv"));
		tombos.putAll(getTombosArquivosIguais("BDB2/baixas/arquivo6.csv"));
		
		return tombos;
	}

	private static HashMap<String, String> getTombosArquivosIguais(String filePath) throws IOException {
		String line, tombo, cod_doc, match;
		File file = new File(filePath);
		BufferedReader br = new BufferedReader(new FileReader(file, StandardCharsets.UTF_8)); 
		HashMap <String, String> tombos = new HashMap<>();
		
		while ((line = br.readLine()) != null)
		{
			if(line.matches("(.*)[0-9]*[/][0-9]{4}[ ]*[L][0-9]*(.*)") 
					|| line.matches("(.*)[0-9]*[/][0-9]{2}[ ]*[L][0-9]*(.*)"))
			{
				match = extractTomboFromLine2(line);
				tombo = match.split(" ")[0];
				cod_doc = match.substring(match.indexOf("L"));
				tombos.put(tombo, cod_doc);
				
			}
		}
		
		return tombos;
	}
	
	private static HashMap<String, String> getTombosArquivo1() throws IOException {
		String filePath = "BDB2/baixas/arquivo1.csv", line;
		File file = new File(filePath);
		BufferedReader br = new BufferedReader(new FileReader(file, StandardCharsets.UTF_8)); 
		HashMap <String, String> tombos = new HashMap<>();
		
		while ((line = br.readLine()) != null)
		{
			if(line.matches("(.*)[0-9]*[/][0-9]{4}(.*)") 
					|| line.matches("(.*)[0-9]*[/][0-9]{2}(.*)"))
				tombos.put(extractTomboFromLine(line), null);
		}
		
		return tombos;
	}

	private static String extractTomboFromLine(String line) {
		String tombo = null;
		Pattern p = null;
		Matcher m;
		
		if(line.matches("(.*)[0-9]*[/][0-9]{4}(.*)"))
		{
			p = Pattern.compile("[0-9]*[/][0-9]{4}");
		}
		else if (line.matches("(.*)[0-9]*[/][0-9]{2}(.*)"))
		{
			p = Pattern.compile("[0-9]*[/][0-9]{2}");
		}
		m = p.matcher(line);
		
		if(m.find())
		{
			tombo = m.group(0);
		}
		return tombo;
	}
	
	private static String extractTomboFromLine2(String line) {
		String tombo = null;
		Pattern p = null;
		Matcher m;
		
		if(line.matches("(.*)[0-9]*[/][0-9]{4}[ ]*[L][0-9]*(.*)"))
		{
			p = Pattern.compile("[0-9]*[/][0-9]{4}[ ]*[L][0-9]*");
		}
		else if (line.matches("(.*)[0-9]*[/][0-9]{2}[ ]*[L][0-9]*(.*)"))
		{
			p = Pattern.compile("[0-9]*[/][0-9]{2}[ ]*[L][0-9]*");
		}
		m = p.matcher(line);
		
		if(m.find())
		{
			tombo = m.group(0);
		}
		return tombo;
	}

	private static Field getFieldWithTombo(String tombo) {
		for(Record record : recordsInicial)
		{
			if(record.hasField("952"))
			{
				for(Field field : record.getField("952"))
				{	
					if(field.hasSubfield("x"))
						if(field.getSubfield("x").get(0).getValue().contentEquals(tombo))
							return field;
				}
			}
		}
		return null;
	}

	private static void criaExemplaresComDadosDoAcervo() throws IOException 
	{
		criaExemplaresComISBNeApenasUmaEntrada();
		criaExemplaresComISBNeMultiplasEntradas();
		buscaECriaExemplaresSemISBN();
	}

	private static void buscaECriaExemplaresSemISBN() throws IOException 
	{
		HashMap<String, ArrayList<Record>> recordsWithSameTitleAndAuthor = new HashMap<>();
		ArrayList<Record> registrosSemISBNUnificados = new ArrayList<>();
		String title, author = "";
		for(Record record : withoutISBN)
		{
			title = record.getField("245").get(0).getSubfield("a").get(0).getValue();
			if(record.hasAnyOccurrenceOfFieldAndSubfield("100", "a"))
				author = record.getField("100").get(0).getSubfield("a").get(0).getValue();
			String titleAndAuthor = title + "!!!!!" + author;
			
			if(!recordsWithSameTitleAndAuthor.containsKey(titleAndAuthor))
				recordsWithSameTitleAndAuthor.put(titleAndAuthor, new ArrayList<>());
			recordsWithSameTitleAndAuthor.get(titleAndAuthor).add(record);
		}
		
		for(String key : recordsWithSameTitleAndAuthor.keySet())
			registrosSemISBNUnificados.add(criaExemplaresSemISBN(recordsWithSameTitleAndAuthor.get(key)));
		
		createXMLFile(registrosSemISBNUnificados, "BDB2/output/registrosSemISBNUnificados.xml");
	}

	private static Record criaExemplaresSemISBN(ArrayList<Record> records) throws IOException 
	{
		createExemplarForRecordWithoutISBN(records.get(0));
		for(int rec = 1; rec < records.size(); rec++)
		{
			createExemplarInPreviousRecordWithouISBN(records.get(0), records.get(rec));
			recordsInicial.remove(records.get(rec));
		}
		return records.get(0);
	}

	private static void createExemplarInPreviousRecordWithouISBN(Record previousRecord, Record recordToAdd) {
		Field field952 = new Field("952", "", "", new ArrayList<>(), Constant.dataFieldDenomination);
		
		field952.addSubfield(new Subfield("a", "BDB"));
		field952.addSubfield(new Subfield("b", "BDB"));
		createField952oFrom082ab(recordToAdd, field952);
		createField952xFrom901a(recordToAdd, field952);
		createField952yFrom942c(recordToAdd, field952);
		
		previousRecord.addField(field952);
		
	}

	private static void criaExemplaresComISBNeMultiplasEntradas() throws IOException {
		ArrayList<Record> multipleISBNrecords = new ArrayList<>();
		
		for(String isbnKey : recordsByISBNValue.keySet())
		{
			if(recordsByISBNValue.get(isbnKey).size() > 1)
			{
				criaMultiplosExemplares(recordsByISBNValue.get(isbnKey));
				multipleISBNrecords.add(recordsByISBNValue.get(isbnKey).get(0));
			}
		}
		createXMLFile(multipleISBNrecords, "BDB2/output/registrosFinalMultiplosISBN.xml");
	}

	private static void criaMultiplosExemplares(ArrayList<Record> records) throws IOException {
		
		createExemplarForRecord(records.get(0));
		for(int rec = 1; rec < records.size(); rec++)
		{
			createExemplarInPreviousRecord(records.get(0), records.get(rec));
			recordsInicial.remove(records.get(rec));
		}
	}

	private static void createExemplarInPreviousRecord(Record previousRecord, Record recordToAdd) {
		Field field952 = new Field("952", "", "", new ArrayList<>(), Constant.dataFieldDenomination);
		
		field952.addSubfield(new Subfield("a", "BDB"));
		field952.addSubfield(new Subfield("b", "BDB"));
		createField952oFrom082ab(recordToAdd, field952);
		createField952xFrom901a(recordToAdd, field952);
		createField952yFrom942c(recordToAdd, field952);
		
		String recordISBN = previousRecord.getField("020").get(0).getSubfield("a").get(0).getValue();
		if(isTheISBNinExtract(recordISBN))
		{
			String extractRecord = isbnsInExtract.get(recordISBN);
			createField952eFromExtract(extractRecord, field952);
			createField952pFromExtract(extractRecord, field952);
		}
		previousRecord.addField(field952);
	}

	private static void criaExemplaresComISBNeApenasUmaEntrada() throws IOException {
		ArrayList<Record> singleISBNrecords = new ArrayList<>();
		
		Record record;
		for(String isbnKey : recordsByISBNValue.keySet())
		{
			if(recordsByISBNValue.get(isbnKey).size() == 1)
			{
				record = recordsByISBNValue.get(isbnKey).get(0);
				createExemplarForRecord(record);
				singleISBNrecords.add(record);
			}
		}
		
		createXMLFile(singleISBNrecords, "BDB2/output/registrosFinalApenasUmISBN.xml");
		
	}

	private static HashMap<String, String> getISBNsInExtract() throws IOException {
		String currentRecord = "", isbn = "";
		String extractFilePath = "BDB2/DDOC_EXTRACT.xml";
		File file = new File(extractFilePath);
		BufferedReader br = new BufferedReader(new FileReader(file, StandardCharsets.UTF_8)); 
		HashMap<String, String> isbns = new HashMap<>();
		String line;
		
		br.readLine(); // pula primeira linha de cabecalho
		while ((line = br.readLine()) != null)
		{
			// se a tag corresponde ao comeco do registro, cria novo array
			// para armazenar os campos
			if(line.contains("<DATA_RECORD>"))
			{
				currentRecord = "" + line;
				isbn = "";
				continue;
			}
			
			currentRecord += line;
			if(line.contains("<ISBN>"))
			{
				isbn = getXMLvalue(line);
			}
			
			// se a tag corresponde ao final do registro, busca o registro 
			// que apresenta correspondencia e pula para a proxima linha
			if(line.contains("</DATA_RECORD>"))
			{
				if(!isbns.containsKey(isbn))
					isbns.put(isbn, currentRecord);
			}
		}
		
		br.close();
		return isbns;
	}

	private static void createExemplarForRecord(Record record) throws IOException 
	{
		Field field952 = new Field("952", "", "", new ArrayList<>(), Constant.dataFieldDenomination);
		
		field952.addSubfield(new Subfield("a", "BDB"));
		field952.addSubfield(new Subfield("b", "BDB"));
		createField952oFrom082ab(record, field952);
		createField952xFrom901a(record, field952);
		createField952yFrom942c(record, field952);
		
		String recordISBN = record.getField("020").get(0).getSubfield("a").get(0).getValue();
		if(isTheISBNinExtract(recordISBN))
		{
			String extractRecord = isbnsInExtract.get(recordISBN);
			createField952eFromExtract(extractRecord, field952);
			createField952pFromExtract(extractRecord, field952);
			
		}
		record.addField(field952);
	}
	
	private static void createExemplarForRecordWithoutISBN(Record record) throws IOException 
	{
		Field field952 = new Field("952", "", "", new ArrayList<>(), Constant.dataFieldDenomination);
		
		field952.addSubfield(new Subfield("a", "BDB"));
		field952.addSubfield(new Subfield("b", "BDB"));
		createField952oFrom082ab(record, field952);
		createField952xFrom901a(record, field952);
		createField952yFrom942c(record, field952);
		
		record.addField(field952);
	}

	private static void createField952eFromExtract(String extractRecord, Field field952) 
	{
		String xmlValue = extractRecord.split("<FORMAQUISICAO>")[1];
		xmlValue = xmlValue.split("</FORMAQUISICAO>")[0];
		
		field952.addSubfield(new Subfield("e", xmlValue));
	}
	
	private static void createField952pFromExtract(String extractRecord, Field field952) 
	{
		String xmlValue = extractRecord.split("<COD_DOC>")[1];
		xmlValue = xmlValue.split("</COD_DOC>")[0];
		
		field952.addSubfield(new Subfield("p", xmlValue));
	}

	private static boolean isTheISBNinExtract(String recordISBN) {
		for(String isbnKey : isbnsInExtract.keySet()) 
		{
			if(isbnKey.contentEquals(recordISBN))
				return true;
		}
		return false;
	}

	private static String getXMLvalue(String currentLine) {
		return currentLine.split(">")[1].split("</")[0];
	}
	
	private static void createField952yFrom942c(Record record, Field field952) 
	{
		String field942c = getValueFromSubfield(record, "942", "c");
		
		if(!field942c.contentEquals(""))
			field952.addSubfield(new Subfield("y", field942c));
		
	}

	private static void createField952xFrom901a(Record record, Field field952) 
	{
		String field901a = getValueFromSubfield(record, "901", "a");
		
		if(!field901a.contentEquals(""))
			field952.addSubfield(new Subfield("x", field901a));
	}

	private static void createField952oFrom082ab(Record record, Field field952) 
	{
		String field082a = getValueFromSubfield(record, "082", "a");
		String field082b = getValueFromSubfield(record, "082", "b");
		
		if(!field082a.contentEquals("") || !field082b.contentEquals(""))
		field952.addSubfield(new Subfield("o", (field082a + " " + field082b).trim()));
	}

	private static String getValueFromSubfield(Record record, String field, String subfield) {
		String value = "";
		if(record.hasAnyOccurrenceOfFieldAndSubfield(field, subfield))
			value = record.getField(field).get(0).getSubfield(subfield).get(0).getValue();
		return value;
	}

	private static void mergeMultipleInstancesOf082() 
	{	
		Field field082;
		
		for(Record record : recordsInicial)
		{
			if(record.hasField("082"))
				if(record.getField("082").size() > 1)
				{
					field082 = mergeAll082Subfields(record.getField("082"));
					record.removeAllOccurrencesOfField("082");
					record.addField(field082);
				}
		}
	}

	private static Field mergeAll082Subfields(ArrayList<Field> fields082) {
		Field field082 = new Field("082", Constant.dataFieldDenomination);
		
		for(Field field : fields082)
		{
			field082.addSubfields(field.getSubfields());
		}
		return field082;
	}

	private static void separateRecordsWithAndWithoutISBN() throws IOException {
		ArrayList<Record> withISBN = new ArrayList<>();
		withoutISBN = new ArrayList<>();
		
		for(Record record : recordsInicial)
		{
			if(record.hasField("020") && record.getField("020").get(0).hasSubfield("a"))
					withISBN.add(record);
			else
				withoutISBN.add(record);
		}
		
		createXMLFile(withISBN, "BDB2/output/comISBN.xml");
		createXMLFile(withoutISBN, "BDB2/output/semISBN.xml");
		
		verificaOsISBNDuplicados(withISBN);
	}

	private static void createXMLFile(ArrayList<Record> recordList, String filePath) {
		System.out.println(recordList.size() + " registros no arquivo " + filePath);
		XMLHandler.generateXMLFile(recordList, filePath);
	}

	private static void verificaOsISBNDuplicados(ArrayList<Record> records) throws IOException 
	{
		recordsByISBNValue = new HashMap<>();
		String isbn = "";
		
		for (Record record : records)
		{
			isbn = record.getField("020").get(0).getSubfield("a").get(0).getValue();
			
			if (!recordsByISBNValue.containsKey(isbn))
				recordsByISBNValue.put(isbn, new ArrayList<>());

			recordsByISBNValue.get(isbn).add(record);
		}
		writeFileISBNsFromHash(recordsByISBNValue);
	}

	private static void writeFileISBNsFromHash(HashMap<String, ArrayList<Record>> recordsByISBNValue2) throws IOException {
		OutputStreamWriter writer;
		String filePath = "BDB2/output/ISBNandTotal.xml";
		
		cleanFile(filePath);
		writer = new OutputStreamWriter(new FileOutputStream(filePath), StandardCharsets.UTF_8);

		for(String isbn : recordsByISBNValue2.keySet()) 
			writer.append(isbn + "\t" + recordsByISBNValue2.get(isbn).size() + "\n");

		writer.close();
	}

	private static void cleanFile(String filePath) throws FileNotFoundException {
		PrintWriter writer2 = new PrintWriter(filePath);
		writer2.print("");
		writer2.close();
	}

	private static void constroiRegistrosMarc() throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		recordsInicial = new ArrayList<Record>();
		recordsInicial.addAll(getRecordsFromFile("BDB2/acervoBDBatualizado_01.xml"));
		recordsInicial.addAll(getRecordsFromFile("BDB2/acervoBDBatualizado_02.xml"));
		recordsInicial.addAll(getRecordsFromFile("BDB2/acervoBDBatualizado_03.xml"));
		recordsInicial.addAll(getRecordsFromFile("BDB2/acervoBDBatualizado_04.xml"));
		
		System.out.println(recordsInicial.size() + " registros no total.");
	}


	private static ArrayList<Record> getRecordsFromFile(String filePath) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		MARC marc = new MARC(filePath);
		return marc.getRecords();
	}

}
