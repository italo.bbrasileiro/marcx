package control;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class MainIBICT {
	
	/**
	 * atualiza o link de download nos registros com link.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args)  throws Exception {
		ArrayList<String[]> csv = CSVController.readCSV("kohaIBICT/dadosExemplares.tsv", "\t");
//		MARC acervo = new MARC("kohaIBICT/acervoIBICT.xml");
		ArrayList<Record> periodicosRemovidos = new ArrayList<Record>();
		ArrayList<Record> periodicosRemovidosComS = new ArrayList<Record>();
		ArrayList<Record> periodicosNaoRemovidos = new ArrayList<Record>();
		ArrayList<Record> novosPeriodicos = new ArrayList<Record>();
		
		for(int i = 2; i < csv.size(); i++)
		{
			String[] linha = csv.get(i);
			
			ArrayList<Field> fields = new ArrayList<Field>();
			Record rec = new Record(fields);
			
			rec.generateLeaderAndControlFields();
			rec.createFieldAndSubfield("100", "a", linha[1]);
			rec.createFieldAndSubfield("700", "a", linha[1]);
			rec.createFieldAndSubfield("245", "a", linha[2]);
			rec.createFieldAndSubfield("260", "b", linha[3]);
			rec.createFieldAndSubfield("022", "a", linha[8]);
			rec.createFieldAndSubfield("942", "c", "16");
			
			Field f952 = rec.createFieldAndSubfield("952", "h", linha[4] + ", " + linha[5] + ", " + linha[6] + ", " + linha[7]);
			if(linha.length > 9) // nem sempre essas informacoes estao preenchidas
				f952.createSubfield("t", linha[9]);
			if(linha.length > 10)
				f952.createSubfield("p", linha[10]);
			if(linha.length > 11)
				f952.createSubfield("x", linha[11]);
			
			novosPeriodicos.add(rec);
		}
		
//		for(Record r : acervo.getRecords())
//		{
//			if(r.hasField("942"))
//			{
//				if(r.getField("942").get(0).getSubfield("c").get(0).getValue().contentEquals("16"))
//				{
//					periodicosRemovidos.add(r);
//					continue;
//				}
//				if(r.getField("942").get(0).getSubfield("c").get(0).getValue().contentEquals("S"))
//				{
//					periodicosRemovidosComS.add(r);
//					continue;
//				}
//			}else
//			{
//				System.err.println("Registro sem campo 942");
//			}
//			periodicosNaoRemovidos.add(r);
//		}
		
		

		
//		System.out.println(periodicosRemovidos.size() + " periodicos removidos");
//		System.out.println(periodicosNaoRemovidos.size() + " periodicos nao removidos");
//		XMLHandler.generateXMLFile(periodicosRemovidos, "kohaIBICT/acervoIBICTperiodicosRemovidos.xml");
//		System.out.println(periodicosRemovidosComS.size() + " periodicos removidos com S");
//		XMLHandler.generateXMLFile(periodicosRemovidosComS, "kohaIBICT/periodicosRemovidosComS.xml");
//		XMLHandler.generateXMLFile(periodicosNaoRemovidos, "kohaIBICT/acervoIBICTperiodicosNaoRemovidos.xml");
		System.out.println(novosPeriodicos.size() + " novos periodicos criados");
		XMLHandler.generateXMLFile(novosPeriodicos, "kohaIBICT/novosPeriodicos.xml");
	}
}
