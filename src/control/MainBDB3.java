package control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import entity.Field;
import entity.MARC;
import entity.Record;

public class MainBDB3 {
	
	static ArrayList<Record> recordsInicial;
	static HashMap<String, ArrayList<Record>> recordsByISBNValue;
	static HashMap<String, String> isbnsInExtract;
	static ArrayList<Record> withoutISBN;
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		String arquivoAutoridades =  "BDB3/autoridades.xml";
		ArrayList<Record> registros = getRecordsFromFile(arquivoAutoridades);
		
		alterarOrdemNomesAutores(registros);
		criaArquivoXML(registros, "BDB3/autoridadesEditadas.xml");
	}
	
	private static void criaArquivoXML(ArrayList<Record> records, String outputFile) {
		System.out.println(records.size() + " periodicos criados em " + outputFile);
		XMLHandler.generateXMLFile(records, outputFile);
	}

	private static void alterarOrdemNomesAutores(ArrayList<Record> registros) 
	{
		String nomeAutor;
		
		for(Record registro : registros)
		{
			nomeAutor = registro.getField("100").get(0).getSubfield("a").get(0).getValue();
			if(nomeAutor.contains(","))
				trocaOrdemNome(registro, nomeAutor);
			else
			{
				registro.getField("100").get(0).setFieldName("110");
				registro.getField("942").get(0).getSubfield("a").get(0).setValue("CORPO_NAME");
			}
		}
	}

	private static void trocaOrdemNome(Record registro, String nomeAutor) {
		String[] parteNome;
		
		parteNome = nomeAutor.split(",");
		nomeAutor = parteNome[1].trim() + ", " + parteNome[0].trim();
		registro.getField("100").get(0).getSubfield("a").get(0).setValue(nomeAutor);
	}

	private static ArrayList<Record> getRecordsFromFile(String filePath) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		MARC marc = new MARC(filePath);
		return marc.getRecords();
	}

}
