package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class MainIBICT8 
{
	static String yearRegex = "[0-9]{4}([/][0-9]{4})*"; // forma 1993 ou 1992/1993
	static String volumeRegex = "[0-9]+([/][0-9]+)*[(]"; // forma 10 ou 10/11, indica que eh seguido de (
	static String numberRegex = "[(][0-9]+([-|/|,][0-9]+)*[)]";
	static String editionWithWordsRegex = "[(][a-z]+([-|/|,][a-z]+)*[)]";
	static String wordsOrNumbersRegex = "([a-z]+|[0-9]+)+";
	static String wordsAndNumbersRegex = "([0-9]+([ ][a-z]+)*)+";
	static String editionWithWordsOrNumbersRegex = "[(]" + wordsOrNumbersRegex + "([-|/|,]" + wordsOrNumbersRegex + ")*[)]";
	static String editionWithWordsAndNumbersRegex = "[(]" + wordsAndNumbersRegex + "([-|/|,]" + wordsAndNumbersRegex + ")*[)]";
	static String wordOrNumberVolumeRegex = "[0-9]+" + numberRegex + "([,][ ]([0-9]+" + numberRegex + "|[a-z])+)+";
	static String volumeWithNumOrWords = "([0-9]+|[a-z]+)(([ |-]([0-9]+|[a-z]+))+)*";
	
	static ArrayList<Record> records;
	static ArrayList<Record> recordsOutput = new ArrayList<>();
	static ArrayList<Record> recordsMadeByHand = new ArrayList<>();
	static int contRemoved;
	
	static ArrayList<Record> recordsToRemove = new ArrayList<>();
	
	public static void main(String[] args) throws Exception 
	{
		MARC acervo = new MARC("kohaIBICT4/output/registrosCCN.xml");
		ArrayList<Record> recordsAcervo = acervo.getRecords();
		
		MARC editados = new MARC("kohaIBICT4/output/registrosEditados.xml");
		ArrayList<Record> recordsEditados = editados.getRecords();
		
		String biblionumber;
		for(Record recordEd : recordsEditados)
		{
			biblionumber = recordEd.getField("999").get(0).getSubfield("c").get(0).getValue();
			removeBiblionumberAtOldRecords(biblionumber, recordsAcervo);
			recordsAcervo.add(recordEd);
		}
		
		recordsAcervo.removeAll(recordsToRemove);
		System.out.println(contRemoved);
		
		criaArquivoXML(recordsAcervo, "kohaIBICT4/output/registrosCCNmerged.xml");
	}
	
	private static void removeBiblionumberAtOldRecords(String biblionumber, ArrayList<Record> recordsAcervo) 
	{
		String currentBB;
		for(Record recordAcervo : recordsAcervo)
		{
			Field field = recordAcervo.getField("999").get(0);
			Subfield subfield = field.getSubfield("c").get(0);
			currentBB = subfield.getValue();
			
			if(currentBB.contentEquals(biblionumber))
			{
				contRemoved++;
				System.out.println(contRemoved);
				recordsToRemove.add(recordAcervo);
				break;
			}
		}
		
	}

	public static void mainOld(String[] args) throws Exception 
	{
		MARC acervoIBICT = new MARC("kohaIBICT4/input/acervoIBICT9-6.xml");
		records = acervoIBICT.getRecords();
		
		removeTodosExemplaresPeriodicos();
		
		BufferedReader br = instanciaArquivoDeColecao("kohaIBICT4/input/colecoesCCN.txt");
		
		System.out.println("Lista de cole��es nao inseridas:");
		criaInformacoesDeColecoes(br);
		adicionaSubcampoYemTodosExemplares();
		
		criaArquivoXML(recordsOutput, "kohaIBICT4/output/registrosCCN.xml");
		criaArquivoXML(recordsMadeByHand, "kohaIBICT4/output/registrosParaEditar.xml");
		
		geraListaBiblionumberParaApagar();
	}
	
	private static void geraListaBiblionumberParaApagar() throws IOException 
	{
		String outputPath = "kohaIBICT4/output/listaBib.txt";
		OutputStreamWriter writer;
		writer = new OutputStreamWriter(new FileOutputStream(outputPath), StandardCharsets.UTF_8);

		// Comandos usados para abrir o arquivo e limpar qualquer conteudo antigo.
		PrintWriter writer2 = new PrintWriter(outputPath);
		writer2.print("");
		writer2.close();
		
		String bbnumber;
		for(Record rec : recordsOutput)
		{
			bbnumber = rec.getField("999").get(0).getSubfield("c").get(0).getValue();
			writer.append(bbnumber + "\n");
		}
		writer.close();
	}

	private static void removeTodosExemplaresPeriodicos() 
	{
		for(Record record : records)
		{
			if(!record.getField("942").get(0).getSubfield("c").get(0).getValue().contentEquals("16"))
				continue;
			record.removeAllOccurrencesOfField("952");
		}
	}


	private static void criaArquivoXML(ArrayList<Record> records, String outputFile) {
		System.out.println(records.size() + " periodicos criados em " + outputFile);
		XMLHandler.generateXMLFile(records, outputFile);
	}

	private static void adicionaSubcampoYemTodosExemplares() {
		for(Record r : recordsOutput)
		{
			if(r.hasField("952"))
				for(Field field952 : r.getField("952"))
					field952.addSubfield(new Subfield("y", "16"));
		}
	}


	private static void criaInformacoesDeColecoes(BufferedReader br) throws IOException {
		// o indicador da colecao esta no campo 90 $a do registro e no
		// campo !C020! do arquivo do CCN
		String[] CCNfield, editionField;
		Record record = null;
		String currentLine;
		while ((currentLine = br.readLine()) != null)
		{
			if(currentLine.contains("!REC-ID"))
				record = null;
			
			if(currentLine.contains("!C020!"))
			{
				CCNfield = currentLine.split("!");
				record = getCorrespondentRecord(records, CCNfield[2].trim());
				if(!recordsOutput.contains(record))
					recordsOutput.add(record);
			
				if(record == null)
					System.err.println("Registros sem valor " + CCNfield[2]
							+ " no campo 090 $a");
			}
			
			// campo que contem as informacoes de exemplares
			if(currentLine.contains("!C030!"))
			{
				CCNfield = currentLine.split("!");
				editionField = CCNfield[2].split(";");
				
				addItemToRecord(record, editionField);
			}
		}
	}

	private static BufferedReader instanciaArquivoDeColecao(String inputPath) throws IOException {
		File file = new File(inputPath);
		return new BufferedReader(new FileReader(file));
	}

	
	private static void addItemToRecord(Record record, String[] editionField) 
	{
		for(String edition : editionField)
		{
			record.addFields(parseEditionTo952Field(record, edition.trim()));
		}
	}

	private static ArrayList<Field> parseEditionTo952Field(Record record, String edition) {
		ArrayList<Field> fields = new ArrayList<>(); 
		
		String volumeAndNumber = volumeRegex.substring(0, volumeRegex.length() - 3) + "[ ]*" + numberRegex;
		String simpleForm = yearRegex + "[ ]" + volumeAndNumber;
		String simpleWithListOfVolumes = simpleForm + "([ ]*[,][ ]*" + volumeAndNumber + ")+";
		String simpleWithoutVolume = yearRegex + "[ ]" + numberRegex;
		String yearAndMaybeVolume = yearRegex + "([ ]" + volumeRegex.substring(0, volumeRegex.length() - 3) + ")*";
		String simpleFormWithWordsOrNumber = yearRegex + "[ ]" 
				+ volumeRegex.substring(0, volumeRegex.length() - 3) 
				+ editionWithWordsOrNumbersRegex;
		String simpleFormWithWordsAndNumber = yearRegex + "[ ]" 
				+ volumeRegex.substring(0, volumeRegex.length() - 3)
				+ editionWithWordsAndNumbersRegex;
		String simpleWithListOfVolumesOrWords = simpleForm + "([ ]*[,][ ]" + "(" + volumeAndNumber 
				+ "|[a-z]+)" + ")+";
		String multipleSimpleForms = simpleForm + "([,][ ]" + simpleForm + ")+";
		String yearRange = yearRegex + "[-]" + yearRegex;
		String yearWithWords = yearRegex + "([ ])*" + editionWithWordsRegex;
		String yearWithVolume = yearRegex + "([ ])*" + volumeWithNumOrWords;
		String volumeAndEditionInWords = "[0-9]+[(][a-z]+([,][a-z]+)*[)]";
		String yearWithVolumeAndWords = yearRegex + "[ ]" + volumeAndEditionInWords + "([,][ ]" + volumeAndEditionInWords + ")*";
		String simpleFormWithNesp = simpleForm + " nesp";
		if(edition.matches(simpleForm))
		{
			fields.addAll(fieldsFromType1ofMatch(edition));
		} else if(edition.matches(simpleWithListOfVolumes))
		{
			fields.addAll(fieldsFromType2ofMatch(edition));
		}
		else if (edition.matches(simpleWithoutVolume))
		{
			fields.addAll(fieldsFromType3ofMatch(edition));
		}else if(edition.matches(yearAndMaybeVolume))
		{
			fields.addAll(fieldsFromType4ofMatch(edition));
		}else if(edition.matches(simpleFormWithWordsOrNumber))
		{
			fields.addAll(fieldsFromType5ofMatch(edition));
		}else if(edition.matches(simpleFormWithWordsAndNumber))
		{
			fields.addAll(fieldsFromType6ofMatch(edition));
		}else if(edition.matches(simpleWithListOfVolumesOrWords))
		{
			fields.addAll(fieldsFromType7ofMatch(edition));
		}else if(edition.matches(multipleSimpleForms))
		{
			fields.addAll(fieldsFromType8ofMatch(edition));
		}else if(edition.matches(yearRange))
		{
			fields.addAll(fieldsFromType9ofMatch(edition));
		}else if(edition.matches(yearWithWords))
		{
			fields.addAll(fieldsFromType10ofMatch(edition));
		}else if(edition.matches(yearWithVolume))
		{
			fields.addAll(fieldsFromType11ofMatch(edition));
		}else if(edition.matches(yearWithVolumeAndWords))
		{
			fields.addAll(fieldsFromType12ofMatch(edition));
		}else if(edition.matches(simpleFormWithNesp))
		{
			fields.addAll(fieldsFromType13ofMatch(edition));
		}
		else
		{
			System.out.println("Codigo CCN:" + record.getField("090").get(0).getSubfield("a").get(0).getValue() + " " + edition);
			if(!recordsMadeByHand.contains(record))
				recordsMadeByHand.add(record);
		}
		
		return fields;
	}
	


	private static ArrayList<Field> fieldsFromType1ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year;
		
		volume = getFieldThatMatches(edition, volumeRegex);
		if(volume != null)
		{
			volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
			subfieldHvalue += ", v. " + volume;
		}
		
		number = getFieldThatMatches(edition, numberRegex);
		if(number != null)
		{
			fields.addAll(fillEditionNumbers(number, subfieldHvalue));
		}
		return fields;
	}
	
	


	private static ArrayList<Field> fieldsFromType2ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		
		for(String vol : getListOfVolumes(edition.substring(year.length())))
		{
			volume = getFieldThatMatches(vol.trim(), volumeRegex);
			if(year != null)
				subfieldHvalue = year;

			if(volume != null)
			{
				volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
				subfieldHvalue += ", v. " + volume;
			}
			
			number = getFieldThatMatches(vol, numberRegex);
			if(number != null)
			{
				fields.addAll(fillEditionNumbers(number, subfieldHvalue));
			}
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType3ofMatch(String edition) 
	{
		String year, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year;
		
		number = getFieldThatMatches(edition, numberRegex);
		if(number != null)
		{
			fields.addAll(fillEditionNumbers(number, subfieldHvalue));
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType4ofMatch(String edition) 
	{
		String year, volume, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year;
		
		volume = getFieldThatMatches(edition, volumeRegex);
		if(volume != null)
		{
			volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
			subfieldHvalue += ", v. " + volume;
		}
		
		fields.add(new Field("952", "h", subfieldHvalue, Constant.dataFieldDenomination));
		
		return fields;
	}
	
	
	private static ArrayList<Field> fieldsFromType5ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year + ", ";
		
		volume = getFieldThatMatches(edition, volumeRegex);
		if(volume != null)
		{
			volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
			subfieldHvalue += "v. " + volume;
		}
		
		number = getFieldThatMatches(edition, editionWithWordsOrNumbersRegex);
		if(number != null)
		{
			fields.addAll(fillEditionNumbers(number, subfieldHvalue));
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType6ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year + ", ";
		
		volume = getFieldThatMatches(edition, volumeRegex);
		if(volume != null)
		{
			volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
			subfieldHvalue += "v. " + volume;
		}
		
		number = getFieldThatMatches(edition, editionWithWordsAndNumbersRegex);
		if(number != null)
		{
			fields.addAll(fillEditionNumbers(number, subfieldHvalue));
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType7ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		
		for(String vol : getListOfVolumes(edition.substring(year.length())))
		{
			if(year != null)
				subfieldHvalue = year + ", ";

			volume = getFieldThatMatches(vol.trim(), volumeRegex);
			if(volume != null)
			{
				volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
				subfieldHvalue += "v. " + volume;
			}else
			{
				volume = getFieldThatMatches(vol.trim(), "[a-z]+");
				if(volume != null)
					subfieldHvalue += "v. " + volume;
			}
			
			number = getFieldThatMatches(vol, editionWithWordsOrNumbersRegex);
			if(number != null)
			{
				fields.addAll(fillEditionNumbers(number, subfieldHvalue));
			}else
			{
				fields.add(new Field("952", "h", subfieldHvalue, Constant.dataFieldDenomination));
			}
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType8ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		for(String simpleEdition : edition.split(","))
		{
			year = getFieldThatMatches(simpleEdition, yearRegex);
			if(year != null)
				subfieldHvalue = year + ", ";
			
			volume = getFieldThatMatches(simpleEdition, volumeRegex);
			if(volume != null)
			{
				volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
				subfieldHvalue += "v. " + volume;
			}
			
			number = getFieldThatMatches(simpleEdition, numberRegex);
			if(number != null)
			{
				fields.addAll(fillEditionNumbers(number, subfieldHvalue));
			}
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType9ofMatch(String edition) 
	{
		String year, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		int begin = Integer.parseInt(edition.split("-")[0]);
		int end = Integer.parseInt(edition.split("-")[1]);
		
		for(int i = begin; i <= end; i++)
		{
			year = getFieldThatMatches(edition, yearRegex);
			if(year != null)
				subfieldHvalue = year;
			fields.add(new Field("952", "h", subfieldHvalue, Constant.dataFieldDenomination));
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType10ofMatch(String edition) 
	{
		String year, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year + ", ";
		
		
		
		number = getFieldThatMatches(edition, editionWithWordsRegex);
		if(number != null)
		{
			fields.addAll(fillEditionNumbers(number, subfieldHvalue));
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType11ofMatch(String edition) 
	{
		String year, volume, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year;
		
		String edit = edition.substring(year.length());
		
		volume = getFieldThatMatches(edit, volumeWithNumOrWords);
		if(volume != null)
		{
			fields.addAll(fillVolumeNumbers(volume, subfieldHvalue));
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType12ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		String edit = edition.substring(year.length());
		
		for(String vol : getListOfVolumes(edit))
		{
			volume = getFieldThatMatches(vol, volumeWithNumOrWords);
			
			subfieldHvalue = year + ", v. " + volume;
			
			number = getFieldThatMatches(vol, editionWithWordsRegex);
			if(number != null)
			{
				fields.addAll(fillEditionNumbers(number, subfieldHvalue));
			}
		}
		return fields;
	}
	
	private static ArrayList<Field> fieldsFromType13ofMatch(String edition) 
	{
		String year, volume, number, subfieldHvalue = "";
		ArrayList<Field> fields = new ArrayList<>();
		
		year = getFieldThatMatches(edition, yearRegex);
		if(year != null)
			subfieldHvalue = year;
		
		volume = getFieldThatMatches(edition, volumeRegex);
		if(volume != null)
		{
			volume = volume.substring(0, volume.length() - 1); // remove o ( ao final
			subfieldHvalue += ", v. " + volume;
		}
		
		number = getFieldThatMatches(edition, numberRegex);
		if(number != null)
		{
			fields.addAll(fillEditionNumbers(number, subfieldHvalue));
		}
		
		fields.add(new Field("952", "h", subfieldHvalue +  ", nesp", Constant.dataFieldDenomination));
		
		return fields;
	}
	
	private static String[] getListOfVolumes(String edition) 
	{
		ArrayList<String> volumes = new ArrayList<>();
		boolean insideParenthesis = false;
		int currentFirstChar = 0;
		char letra;
		for(int c = 0; c < edition.length(); c++)
		{
			letra = edition.charAt(c);
			
			if(letra == '(')
				insideParenthesis = true;
			if(letra == ')')
				insideParenthesis = false;
			
			if(letra == ',' && !insideParenthesis)
			{
				volumes.add(edition.substring(currentFirstChar, c));
				currentFirstChar = c + 1;
			}
			
			if(c == (edition.length() - 1))
			{
				volumes.add(edition.substring(currentFirstChar));
			}
		}
		
		String[] vols = new String[volumes.size()];
		for(int i = 0; i < vols.length; i++)
		{
			vols[i] = volumes.get(i);
		}
			
		return vols;
	}


	private static ArrayList<Field> fillEditionNumbers(String number, String subfieldValue) 
	{
		ArrayList<Field> fields = new ArrayList<>();
		number = number.substring(1, number.length() - 1);
		
		for(String num : separateAllNumbers(number))
		{
			if (num.contains("-"))
			{
				fields.addAll(createFieldsFromNumberRange(subfieldValue, num));
			} else			
			{
				fields.add(new Field("952", "h", subfieldValue 
						+ ", n. " + num, Constant.dataFieldDenomination));
			}
		}
		return fields;
	}
	
	private static ArrayList<Field> fillVolumeNumbers(String volume, String subfieldValue) 
	{
		ArrayList<Field> fields = new ArrayList<>();
		
		for(String vol : separateAllNumbers(volume))
		{
			if (vol.contains("-"))
			{
				fields.addAll(createFieldsFromNumberRange(subfieldValue, vol));
			} else			
			{
				fields.add(new Field("952", "h", subfieldValue 
						+ "v. " + vol, Constant.dataFieldDenomination));
			}
		}
		return fields;
	}


	private static String[] separateAllNumbers(String number) {
		if(number.contains(","))
			return number.split(",");
		
		String[] singleOcurrence = new String[1];
		singleOcurrence[0] = number;
		
		return singleOcurrence;
	}


	private static ArrayList<Field> createFieldsFromNumberRange(String subfieldValue, String number) 
	{
		ArrayList<Field> fields = new ArrayList<>();
		int start = Integer.parseInt(number.split("-")[0].trim()), 
				end = Integer.parseInt(number.split("-")[1].trim());
		for(int i = start; i <= end; i++)
		{
			fields.add(new Field("952", "h", subfieldValue + ", n. " + i, Constant.dataFieldDenomination));
		}
		return fields;
	}


	private static String getFieldThatMatches(String textLine, String regex)
	{
		Pattern pattern = Pattern.compile(regex);
		// create matcher for pattern p and given string
		Matcher matcher = pattern.matcher(textLine);
		String valueThatMatches = null;
		
		// if an occurrence if a pattern was found in a given string...
		if (matcher.find()) {
			// ...then you can use group() methods.
			valueThatMatches = matcher.group(0); // whole matched expression
//			System.out.println(m.group(1)); // first expression from round brackets (Testing)
//			System.out.println(m.group(2)); // second one (123)
//			System.out.println(m.group(3)); // third one (Testing)
		}

		return valueThatMatches;
	}

	/**
	 * itera pela lista 'records', e retorna o registro que tem 
	 * o campo 090 $a correspondente ao valor indicado em 'field090a'
	 * @param records
	 * @param filed090a
	 * @return
	 */
	private static Record getCorrespondentRecord(ArrayList<Record> records, String field090a) 
	{
		for(Record r : records)
		{
			if(!r.hasAnyOccurrenceOfFieldAndSubfield("090", "a"))
				continue;
			
			if(r.getField("090").get(0).getSubfield("a").get(0).getValue()
					.contentEquals(field090a))
				return r;
		}
		return null;
	}
}
