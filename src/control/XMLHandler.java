package control;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class XMLHandler {

	/**
	 * Le o arquivo marcxml e transforma em um objeto do tipo
	 * 'Document', que sera utilizado para as operacoes do programa.
	 * @param pathFile Diretorio e nome do arquivo xml a ser lido. 
	 * @return Document contendo o objeto do arquivo xml lido.
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException
	 */
	public static Document readMarcXMLFile(String pathFile) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		// create the document builder object
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document document = builder.parse(new File(pathFile));
		document.getDocumentElement().normalize();

		System.out.println("XML ready!");

		return document;
	}

	/**
	 * Instancia os objetos a partir de um Document
	 * criado de um marcxml.
	 * @param xml Document criado a partir de um marcXML.
	 * @return
	 */
	public static ArrayList<Record> instantiateXMLfields(Document xml)
	{
		ArrayList<Record> recordsArray = new ArrayList<Record>();
		NodeList records = xml.getElementsByTagName(Constant.recordDenomination);

		// Percorre a lista de registros
		for(int r = 0; r < records.getLength(); r++)
		{	
			Node record = records.item(r);

			// Lista de campos do registro
			NodeList fields = record.getChildNodes();
			ArrayList<Field> fieldsArray = new ArrayList<Field>();

			// Percorre a lista de campos do registro para instanciar objetos
			// do tipo 'Field'. 
			for(int f = 0; f < fields.getLength(); f++)
			{
				Node field = fields.item(f);
				String fieldName = getFieldTag(field);
				String fieldContent = field.getTextContent();
				String nodeName= field.getNodeName().toString();
				
//				if(fieldName != null && fieldName.contentEquals("001") && fieldContent.contentEquals("503151426012733"))
//					System.out.println();

				if(nodeName.contentEquals(Constant.dataFieldDenomination))
				{	// Percorre a lista de subcampos para instanciar objetos
					// do tipo "Subfield"
					NodeList subfields = field.getChildNodes();
					String ind1 = getIndicator1(field);
					String ind2 = getIndicator2(field);

					ArrayList<Subfield> subfieldsArray = new ArrayList<Subfield>();
					for(int s = 0; s < subfields.getLength(); s++)
					{
						Node subfield = subfields.item(s);
						String subfieldName = getXMLSubfieldTag(subfield);
						String subfieldValue = subfield.getTextContent().replaceAll("&(?!amp;)", "&amp;");
						subfieldValue = subfieldValue.replaceAll("<", "&lt;");
						subfieldValue = subfieldValue.replaceAll(">", "&gt;");
						String xmlNodeName = subfield.getNodeName();
						
						if(subfieldName != null)
						{
							Subfield subc = new Subfield(subfieldName, subfieldValue, xmlNodeName);
							subfieldsArray.add(subc);
						}
					}
					Field campo = new Field(fieldName, ind1, ind2, subfieldsArray, nodeName);
					linkFieldAndSubfield(campo);
					fieldsArray.add(campo);
				}else if(nodeName.contentEquals(Constant.controlFieldDenomination))
				{
					Field campo = new Field(fieldName, fieldContent, nodeName);
					fieldsArray.add(campo);
				}else if(nodeName.contentEquals(Constant.leaderDenomination))
				{
					Field campo = new Field(Constant.leaderDenomination, fieldContent, nodeName);
					fieldsArray.add(campo);
				}
			}
			Record temp = new Record(fieldsArray);
			recordsArray.add(temp);
		}
		return recordsArray;
	}

	/**
	 * Esse metodo realiza a ligacao do objeto 'Field' de todos os
	 * 'Subfield' do
	 * com o seu campo mae correspondente.
	 * @param campo
	 */
	private static void linkFieldAndSubfield(Field field) {
		for(Subfield subfield : field.getSubfields()) 
		{
			subfield.setField(field);
		}

	}

	/**
	 * Recupera o indicador1 do campo 'field'.
	 * @param field Campo avaliado.
	 * @return String com o valor do ind1.
	 */
	private static String getIndicator1(Node field) 
	{
		String ind1 = "";

		NamedNodeMap at = field.getAttributes();

		if(at == null)
			System.err.println("Campo sem atributos!");
		else
		{
			for(int a = 0; a < at.getLength(); a++)
			{
				Node temp = at.item(a);
				String st2 = temp.getTextContent();
				String st3 = temp.getNodeName();

				if(st3 == Constant.indicator1Denomination)
				{
					ind1 = st2;
					return ind1;
				}
			}
		}
		return null;
	}

	/**
	 * Recupera o indicador2 do campo 'field'.
	 * @param field Campo avaliado.
	 * @return String com o valor de ind2.
	 */
	private static String getIndicator2(Node field) 
	{
		String ind2 = "";

		NamedNodeMap at = field.getAttributes();

		if(at == null)
			System.err.println("Campo sem atributos!");
		else
		{
			for(int a = 0; a < at.getLength(); a++)
			{
				Node temp = at.item(a);
				String st2 = temp.getTextContent();
				String st3 = temp.getNodeName();

				if(st3 == Constant.indicator2Denomination)
				{
					ind2 = st2;
					return ind2;
				}
			}
		}
		return null;
	}

	/**
	 * Busca dentre a lista de atributos do campo o valor inserido
	 * no campo 'tag'.
	 * @param field Campo no qual eh feito a busca da tag
	 * @return String contendo a tag do campo ou null
	 */
	public static String getFieldTag(Node field) {

		//se 'field' nao tem atributos, retorna null
		if(!field.hasAttributes()) {
			return null;
		}else {
			NamedNodeMap atributos = field.getAttributes();
			for(int a = 0; a < atributos.getLength(); a++) {
				Node atributo = atributos.item(a);
				if(atributo.getNodeName().contains("tag"))
					return atributo.getTextContent();
			}
		}
		return null;
	}

	/**
	 * Busca dentre a lista de atributos do campo o valor inserido
	 * no campo 'Constant.recordSubfieldDenomination()' do XML.
	 * @param field Campo no qual eh feito a busca da tag
	 * @return String contendo a tag do campo ou null
	 */
	public static String getXMLSubfieldTag(Node subfield) {
		NamedNodeMap atributos = subfield.getAttributes();

		if(atributos == null)
			return null;

		Node atributo = getXMLAttribute(atributos, Constant.subfieldTagDenomination);
		if(atributo != null)
			return atributo.getTextContent();

		return null;
	}

	/**
	 * Busca o valor do subcampo especificado por 'searchedSubfield'.
	 * @param field Campo no qual eh feito a busca.
	 * @param searchedSubfield subcampo procurado.
	 * @return String contendo o valor do campo procurado.
	 */
	public static String getSubfieldValue(Node subfield) {
		if(subfield != null)
			return subfield.getTextContent();
		return null;
	}



	/**
	 * Busca um atributo dentro de uma lista de atributos.
	 * Recebe uma lista 'NamedNodeMap' de atributos e a String 'at' correspondente ao atributo procurado.
	 * @param atributos Lista de atributos
	 * @param at atributo procurado
	 * @return Node atributo encontrado ou null
	 */
	public static Node getXMLAttribute(NamedNodeMap atributos, String at) {

		//percorre a lista de atributos
		for(int a = 0; a <atributos.getLength(); a++) {
			//atributo = item atual
			Node atributo = atributos.item(a);
			if(atributo.getNodeName().contains(at)) {
				return atributo;
			}
		}
		return null;
	}

	/**
	 * Este metodo recebe uma lista de registros e cria um arquivo marcxml a partir desses registros.
	 * Os valores para a cria��o das tags sao buscadas na classe "Constant", de acordo com os valores presentes
	 * nos campos com denominacao das tags.
	 * @param records
	 */
	public static void generateXMLFile(ArrayList<Record> records)
	{	
		if(records == null || records.size() == 0)
		{
			System.err.println("Nao eh possivel gerar o .xml. Lista de registros vazia.");
			return;
		}
		BufferedWriter writer;

		try {
			writer = new BufferedWriter(new FileWriter(Constant.outputFilePath, true));

			// Comandos usados para abrir o arquivo e limpar qualquer conteudo antigo.
			PrintWriter writer2 = new PrintWriter(Constant.outputFilePath);
			writer2.print("");
			writer2.close();

			// A String a ser escrita no arquivo deve ser incluida no 'writer' pelo metodo 'append'.
			writer.append(Constant.xmlHeader + "\n");


			// A lista de campos sera percorrida em ordem numerica
			for(Record record : records)
			{
				ArrayList<Field> fields = record.getFieldsOrdered();
				writer.append("<" + Constant.recordDenomination + ">\n");
				
				for(Field field : fields)
				{	
					if(field.getXmlNodeName().contentEquals(Constant.dataFieldDenomination))
					{
						writer.append("\t<" + field.getXmlNodeName() + " " + Constant.indicator1Denomination
								+ "=\"" + field.getIndic1() + "\" " + Constant.indicator2Denomination
								+ "=\"" + field.getIndic2() + "\" " + Constant.fieldTagDenomination
								+ "=\"" + field.getFieldName() + "\">\n");
						
						//preenche a lista de subcampos
						for(Subfield subfield : field.getSubfields())
						{
							writer.append("\t\t<" + subfield.getXmlNodeName()
							+ " " + Constant.subfieldTagDenomination + "=\""
							+ subfield.getSubfieldName() + "\">" + subfield.getValue().trim()
							+ "</" + subfield.getXmlNodeName() + ">\n");
						}
						
						writer.append("\t</" + field.getXmlNodeName() + ">\n");
						
					}else if(field.getXmlNodeName().contentEquals(Constant.controlFieldDenomination))
					{
						writer.append("\t<" + field.getXmlNodeName() + " " 
								+ Constant.fieldTagDenomination + "=\"" + field.getFieldName()
								+ "\">" + field.getValue() + "</" + field.getXmlNodeName() + ">\n");
						
					}else if(field.getXmlNodeName().contentEquals(Constant.leaderDenomination))
					{
						writer.append("\t<" + field.getXmlNodeName() + ">" + record.getLeaderField().getValue() + "</" + field.getXmlNodeName() + ">\n");
						
					}else
					{
						System.err.println("Formato de campo desconhecido: " + field.getXmlNodeName());
					}
					
				}
				writer.append("</" + Constant.recordDenomination + ">\n");
			}
			writer.append("</" + Constant.collectionDenomination + ">");
			writer.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}
	
	/**
	 * Este metodo recebe uma lista de registros e cria um arquivo marcxml a partir desses registros.
	 * Os valores para a criacao das tags sao buscadas na classe "Constant", de acordo com os valores presentes
	 * nos campos com denominacao das tags. O arquivo eh salvado com no diretorio e nomes definidos em filePath.
	 * @param records
	 */
	public static void generateXMLFile(ArrayList<Record> records, String filePath)
	{	
		if(records == null || records.size() == 0)
		{
			System.err.println("Nao eh possivel gerar o .xml. Lista de registros vazia.");
			return;
		}
		//BufferedWriter writer;
		OutputStreamWriter writer;

		try {
//			writer = new BufferedWriter(new FileWriter(filePath, true));
			writer = new OutputStreamWriter(new FileOutputStream(filePath), StandardCharsets.UTF_8);

			// Comandos usados para abrir o arquivo e limpar qualquer conteudo antigo.
			PrintWriter writer2 = new PrintWriter(filePath);
			writer2.print("");
			writer2.close();

			// A String a ser escrita no arquivo deve ser incluida no 'writer' pelo metodo 'append'.
			writer.append(Constant.xmlHeader + "\n");

			// A lista de campos sera percorrida em ordem numerica
			for(Record record : records)
			{
				ArrayList<Field> fields = record.getFieldsOrdered();
				writer.append("<" + Constant.recordDenomination + ">\n");
				
				for(Field field : fields)
				{	
					if(field.getXmlNodeName().contentEquals(Constant.dataFieldDenomination))
					{
						writer.append("\t<" + field.getXmlNodeName() + " " + Constant.indicator1Denomination
								+ "=\"" + field.getIndic1() + "\" " + Constant.indicator2Denomination
								+ "=\"" + field.getIndic2() + "\" " + Constant.fieldTagDenomination
								+ "=\"" + field.getFieldName() + "\">\n");
						
						//preenche a lista de subcampos
						for(Subfield subfield : field.getSubfieldsOrdered())
						{
							writer.append("\t\t<" + subfield.getXmlNodeName()
							+ " " + Constant.subfieldTagDenomination + "=\""
							+ subfield.getSubfieldName() + "\">" + convertSpecialChar(subfield.getValue())
							+ "</" + subfield.getXmlNodeName() + ">\n");
						}
						
						writer.append("\t</" + field.getXmlNodeName() + ">\n");
						
					}else if(field.getXmlNodeName().contentEquals(Constant.controlFieldDenomination))
					{
						writer.append("\t<" + field.getXmlNodeName() + " " 
								+ Constant.fieldTagDenomination + "=\"" + field.getFieldName()
								+ "\">" + convertSpecialChar(field.getValue()) + "</" + field.getXmlNodeName() + ">\n");
						
					}else if(field.getXmlNodeName().contentEquals(Constant.leaderDenomination)
							|| field.getXmlNodeName().contentEquals("marc:leader"))
					{
						writer.append("\t<" + field.getXmlNodeName() + ">" 
								+ convertSpecialChar(record.getLeaderField().getValue()) + "</" + field.getXmlNodeName() + ">\n");
						
					}else
					{
						System.err.println("Formato de campo desconhecido: " + field.getXmlNodeName());
					}
					
				}
				writer.append("</" + Constant.recordDenomination + ">\n");
			}
			writer.append("</" + Constant.collectionDenomination + ">");
			writer.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}
	
	private static String convertSpecialChar(String value) {
		
		value = value.replace("&", "&#38;");
		value = value.replace("<", "&#60;");
		value = value.replace(">", "&#62;");
		value = value.replace("'", "&#39;");
		value = value.replace("\"", "&#34;");
		
		return value;
	}

	public static void generateXMLFile(MARC marc, String filePath)
	{
		generateXMLFile(marc.getRecords(), filePath);
	}
}
