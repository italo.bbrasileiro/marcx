package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class MainBDB {
	
	/**
	 * Main para a criação de exemplares do Koha BDB
	 * @param args
	 * @throws IOException
	 * @throws TransformerException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public static void main(String[] args) throws 
	IOException, ParserConfigurationException, SAXException, TransformerException {
		
		String currentLine, xmlTag, xmlValue;
		HashMap<String, Integer> filledTags = new HashMap<String, Integer>();
		// Esse primeiro trecho foi criado para limpar o arquivo enviado pela equipe da BDB
		// e remover linhas com lixo
		/**
		File file = new File("BDB/exemplares/DDOC_EXTRACT_Modificado.xml");
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		FileOutputStream file2 = new FileOutputStream("BDB/exemplares/DDOC_EXTRACT_Modificado_Limpo.xml");
		// Creates an OutputStreamWriter
		OutputStreamWriter output = new OutputStreamWriter(file2);
		
		currentLine = br.readLine();
		while (currentLine != null)
		{
			if(currentLine.trim().endsWith(">"))
			{
				currentLine += "\n";
				output.write(currentLine);
			}else
			{
				currentLine += " " + br.readLine();
				continue;
			}
			
			currentLine = br.readLine();
		}
		
		output.close();
		**/
		
		MARC marcBDB1 = new MARC("BDB/acervoBDBatualizado_01.xml");
		MARC marcBDB2 = new MARC("BDB/acervoBDBatualizado_02.xml");
		MARC marcBDB3 = new MARC("BDB/acervoBDBatualizado_03.xml");
		MARC marcBDB4 = new MARC("BDB/acervoBDBatualizado_04.xml");
		File file3 = new File("BDB/exemplares/DDOC_EXTRACT_Modificado_Limpo.xml");
		BufferedReader br = new BufferedReader(new FileReader(file3));
		
		ArrayList<Record> recordsInicial = new ArrayList<>();
		recordsInicial.addAll(marcBDB1.getRecords());
		recordsInicial.addAll(marcBDB2.getRecords());
		recordsInicial.addAll(marcBDB3.getRecords());
		recordsInicial.addAll(marcBDB4.getRecords());
		System.out.println(recordsInicial.size() + " registros no total.");
		// percorre as linhas do arquivo de exemplares e cria os registros relacionados,
		// separando por campo
		ArrayList<Field> campos = new ArrayList<>();
		ArrayList<Record> oldRecord = null;
		ArrayList<Record> semCorrespondencia = new ArrayList<>();
		ArrayList<Record> maisDeUmRegistro = new ArrayList<>();
		String titulo = "", isbn = ""; // armazena o titulo buscado
		
		// marcadores para contar progresso
		long numLinhas = br.lines().count(), linhaAtual = 0, countExemplares = 0;
		br = new BufferedReader(new FileReader(file3));
		Field campoTitulo = null;
		boolean isTitulo = false;
		br.readLine(); // pula a primeira linha, com cabecalho do xml
		System.out.println(numLinhas);
		
		// aqui eh feito o merge dos registros existentes. Usar ISBN
		ArrayList<Record> registrosUnicos = new ArrayList<>();
		HashMap<String, Record> hashIsbn = new HashMap<>();
		Record existingISBN;
		for(Record record : recordsInicial)
		{
			// se nao tem ISBN, adiciona na lista de registros unicos
			if(!record.hasAnyOccurrenceOfFieldAndSubfield("020", "a"))
			{
				registrosUnicos.add(record);
				continue;
			}else
			{
				String currentISBN = record.getField("020").get(0).getSubfield("a").get(0).getValue().toLowerCase();
				// se nao contem, adiciona na lista de isbns unicos
				if(!hashIsbn.containsKey(currentISBN))
				{
					hashIsbn.put(currentISBN, record);
					registrosUnicos.add(record);
				}else // se contem, faz o merge com os campos não inseridos
				{
					existingISBN = hashIsbn.get(currentISBN);
					existingISBN.mergeNewFieldsFromRecord(record);
				}
			}
		}

		System.out.println("Depois do merge por ISBN, o acervo apresenta " + registrosUnicos.size() + " registros.");
		
		// aqui eh feito o merge dos registros existentes. Usar Titulo (245$a) e edicao (250$a)
		ArrayList<Record> registrosUnicos2 = new ArrayList<>();
		HashMap<String, Record> hashTitulo = new HashMap<>();
		Record existingTitulo;
		String currentTitle, currentEdition, concatTitleEdition;
		for(Record record : registrosUnicos)
		{
			// se nao tem ISBN, adiciona na lista de registros unicos
			if(!record.hasAnyOccurrenceOfFieldAndSubfield("245", "a"))
			{
				registrosUnicos2.add(record);
				continue;
			}else
			{
				currentTitle = record.getField("245").get(0).getSubfield("a").get(0).getValue().toLowerCase();
				if(record.hasAnyOccurrenceOfFieldAndSubfield("250", "a"))
					currentEdition = record.getField("250").get(0).getSubfield("a").get(0).getValue().toLowerCase();
				else
					currentEdition = "";
				concatTitleEdition = currentTitle + "---" + currentEdition;
				// se nao contem, adiciona na lista de isbns unicos
				if(!hashTitulo.containsKey(concatTitleEdition))
				{
					hashTitulo.put(concatTitleEdition, record);
					registrosUnicos2.add(record);
				}else // se contem, faz o merge com os campos não inseridos
				{
					existingTitulo = hashTitulo.get(concatTitleEdition);
					existingTitulo.mergeNewFieldsFromRecord(record);
				}
			}
		}

		System.out.println("Depois do merge por titulo e edicao, o acervo apresenta " + registrosUnicos2.size() + " registros.");
		
		while ((currentLine = br.readLine()) != null)
		{
			linhaAtual++;
			if(linhaAtual % 10000 == 0)
			{
				System.out.println(linhaAtual + "/" + numLinhas);
			}
			// se a tag corresponde ao final do registro, busca o registro 
			// que apresenta correspondencia e pula para a proxima linha
			if(currentLine.contains("</DATA_RECORD>"))
			{
				
				// busca campo que apresente correspondencia
				if(!isbn.contentEquals(""))
					oldRecord = MARC.getRecordWithFieldAndSubfieldValue(registrosUnicos2, "020", "a", isbn);
				
				if(isbn.contentEquals("") || (oldRecord == null))
					oldRecord = MARC.getRecordWithFieldAndSubfieldValue(registrosUnicos2, "245", "a", titulo);
					
				// verifica se mais de um registro com o mesmo titulo foi encontrado
				if(oldRecord == null)
				{
//					System.out.println("Nenhum registro encontrado para: " + titulo);
					semCorrespondencia.add(new Record(campos));
					continue;
				}
				else if(oldRecord.size() > 1)
				{
//					System.err.println("Mais de um registro com o mesmo título: " + titulo);
					maisDeUmRegistro.add(new Record(campos));
					continue;
				}
				
				// aqui eh tratada a insercao de campos no registro correspondente
				campos = mergeDuplicatedFields(campos);
				inserirCamposNoRegistro(campos, oldRecord.get(0));
				countExemplares++;

				continue;
			}
			
			// se a tag corresponde ao comeco do registro, cria novo array
			// para armazenar os campos
			if(currentLine.contains("DATA_RECORD"))
			{
				// limpa campos usados para busca
				titulo = ""; isbn = "";
				
				campos = new ArrayList<>();
				continue;
			}

			
			xmlTag = getXMLtag(currentLine);
			xmlValue = getXMLvalue(currentLine);
			
			
			
			if(xmlValue != null && !xmlValue.isEmpty())
			{
				// trata aqui as concatenacoes de campo
				// armazena o titulo
				if(xmlTag.contentEquals("TITULO"))
				{
					titulo = xmlValue.toLowerCase();
					isTitulo = true;
				}
				// armazena o isbn
				if(xmlTag.contentEquals("ISBN"))
					isbn = xmlValue.toLowerCase();
				
				if(xmlTag.contentEquals("ARTIGO_TITULO"))
				{
					campoTitulo.getSubfield("a").get(0).setValue(xmlValue + " " + 
							campoTitulo.getSubfield("a").get(0).getValue());
					titulo = xmlValue.toLowerCase() + " " + titulo;
					continue;
				}
				
				Field campo = createFieldFromTag(xmlTag, xmlValue);
				if(isTitulo)
				{
					campoTitulo = campo;
					isTitulo = false;
				}
				if(campo != null)
				{
					campos.add(campo);
				}
			}
		
		}
		
		// completa os registros com as informacoes que faltam
		for(Record record : semCorrespondencia)
		{
			record.generateLeaderAndControlFields();
		}
		
		for(Record record : maisDeUmRegistro)
		{
			record.generateLeaderAndControlFields();
		}
		
		
		
		System.out.println(countExemplares + " exemplares adicionados");
		System.out.println(registrosUnicos2.size() + " registros a criar");
		XMLHandler.generateXMLFile(registrosUnicos2, "BDB/acervoBDBatualizadoComExemplares.xml");
		System.out.println(semCorrespondencia.size() + " registros sem correspondência");
		XMLHandler.generateXMLFile(semCorrespondencia, "BDB/semCorrespondencia.xml");
		System.out.println(maisDeUmRegistro.size() + " registros com mais de uma ocorrência");
		XMLHandler.generateXMLFile(maisDeUmRegistro, "BDB/maisDeUmaCorrespondencia.xml");
	}
	

	private static ArrayList<Field> mergeDuplicatedFields(ArrayList<Field> campos) {
		HashMap<String, Field> camposUnicos = new HashMap<>();
		ArrayList<Field> camposFinal = new ArrayList<>();
		String nomeCampo;
		Field campoExistente;
		
		for(Field campo : campos)
		{
			nomeCampo = campo.getFieldName();
			if(!camposUnicos.containsKey(nomeCampo))
			{
				camposUnicos.put(nomeCampo, campo);
			}else // se o campo já existe, adiciona os subcampos
			{
				campoExistente = camposUnicos.get(nomeCampo);
				campoExistente.addSubfields(campo.getSubfields());
			}
		}
		
		for(String campo : camposUnicos.keySet())
		{
			camposFinal.add(camposUnicos.get(campo));
		}
		
		return camposFinal;
	}


	/**
	 * Percorre a lista de campos do registro anterior e insere os campos novos e de exemplares.
	 * Aqui tambem sao tratados as excecoes de campos que devem ser unicos ou duplicados.
	 * @param campos
	 * @param record
	 */
	private static void inserirCamposNoRegistro(ArrayList<Field> campos, Record record) {
		
		for(Field campo : campos)
		{
			if(record.hasField(campo.getFieldName())
					&& !campo.getFieldName().contentEquals("952"))
			{
				
				continue;
			}
			else
				record.addField(campo);
		}
		
	}

	private static String getXMLvalue(String currentLine) {
		return currentLine.split(">")[1].split("</")[0];
	}

	private static String getXMLtag(String currentLine) {
		return currentLine.split(">")[0].split("<")[1];
	}

	/**
	 * Função main usada para criar os registros (sem exemplares). 
	 * 
	 * @param args
	 * @throws ParserConfigurationExceptionsubfieldName
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException
	 */
	public static void mainCriaRegistros(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		// arquivo de teste
		MARC marcAcervo = new MARC("BDB/mrc_bdb.xml"); // acervo da BDB
		
		// lista de registros do acervo
		ArrayList<Record> records = marcAcervo.getRecords();
		// lista vazia para preencher com as autoridades que serao criadas
		ArrayList<Record> autoridades = new ArrayList<Record>();
		int cont = 0; // contador
		
		// converte os registros para a tag xml mais simples
		Constant.convertoXMLOutputType(records);// see definition in Constant class
		
		System.out.println(records.size());
		
		// percorre a lista de registros
		for(Record r : records)
		{	
			// merge das múltiplas ocorrencias do campo 082
			r.mergeMultipleOccurrencesOfField("082");
			
//			if(cont % 1000 == 0)
//				System.out.println(cont);
//			cont++;
		}
		
		System.out.println(records.size());
		for(Record r : records)
		{
			if(!r.getLeaderField().getFieldName().contentEquals(Constant.leaderDenomination))
				System.out.println("AQUI");
			
			// move o campo  001 para o subcampo 901 $a e remove campo 001, para
			// ser preenchido automaticamente pelo Koha na importacao
			r.createFieldAndSubfield("901", "a", r.getField("001").get(0).getValue());
			r.removeField(r.getField("001").get(0));
			r.createFieldAndSubfield("942", "c", "LIV");
			
			// altera todos os campos 653 para 650
			if(r.hasField("653"))
				for(Field f : r.getField("653"))
				{
					f.setFieldName("650");
				}
			
			// problema a corrigir nos campos 300 sem valor
			// o campo 300 nao esta duplicado, apenas uma ocorrencia
			if(r.hasField("300"))
			{
				if(r.getField("300").get(0).getSubfields().size() < 1)
				{
					r.removeField(r.getField("300").get(0));
				}
			}
		}
		
		// armazena a lista de autoridades a ser adicionada na lista final.
		ArrayList<Record> tempAuthorities;
		
		System.out.println("parte 1");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("100", "100");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 2");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("110", "110");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 3");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("111", "111");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 4");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("130", "130");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 5");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("653", "150");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 6");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("650", "150");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 7");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("700", "100");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 8");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("710", "110");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 9");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("711", "111");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		System.out.println("parte 10");
		tempAuthorities = marcAcervo.createAuthoritiesFromField("730", "130");
		autoridades.addAll(removeDuplicated(tempAuthorities));
		
		// remove autoridades que duplicaram por conta de origens diferentes
		ArrayList<Record> autoridades2 = new ArrayList<Record>();
		autoridades2.addAll(removeDuplicated(autoridades));
		
		
		String[] tiposAutoridades = {"CORPO_NAME", "GEOGR_NAME", "MEETI_NAME", "PERSO_NAME", "TOPIC_TERM", "UNIF_TITLE"};
		
		for(String tipo : tiposAutoridades)
		{
			ArrayList<Record> autTemp = new ArrayList<Record>();
			for(Record aut : autoridades2)
			{
				if(aut.getField("942").get(0).getSubfield("a").get(0).getValue().contentEquals(tipo))
					autTemp.add(aut);
			}
			System.out.println(autTemp.size() + " autoridades do tipo " + tipo);
			XMLHandler.generateXMLFile(autTemp, "BDB/autoridadesBDB_" + tipo + ".xml");
		}
		
		System.out.println(records.size() + " registros no total");
		XMLHandler.generateXMLFile(records, "BDB/bdb_corrigido.xml");
//		XMLHandler.generateXMLFile(autoridades, "BDB/autoridadesBDB2.xml");
		System.out.println("fim");
	}
	
	/**
	 * Remove as autoridades duplicadas (aquelas que sao identicas) da lista autoridades.
	 * @param autoridades
	 * @param tempAuthorities
	 * @return
	 */
	private static ArrayList<Record> removeDuplicated(ArrayList<Record> autoridades) {
		
		ArrayList<Record> result = new ArrayList<Record>();
		String[] fieldsToAvoid = {Constant.leaderDenomination, "001", "003", "005", "008"};
		boolean inserir;
		String conteudoAutAtual;
		
		for(int i = 0; i < autoridades.size(); i++)
		{
			inserir = true;
			
			for(Record aut : result)
			{
				if(aut.getAuthorityMainFieldValue().contentEquals(autoridades.get(i).getAuthorityMainFieldValue()))
				{
					conteudoAutAtual = autoridades.get(i).getRecordAsXMLWithoutIndicatedFields(fieldsToAvoid);
					if(aut.getRecordAsXMLWithoutIndicatedFields(fieldsToAvoid).contentEquals(conteudoAutAtual)) 
					{
						inserir = false;
						break;
					}
				}
			}
			if(inserir)
				result.add(autoridades.get(i));
			
			if(i % 2000 == 0)
				System.out.println(i + "/" + autoridades.size());
		}
		
		return result;
	}

	/**
	 * 
	 * @param xmlTag
	 * @param xmlValue
	 * @return
	 */
	private static Field createFieldFromTag(String xmlTag, String xmlValue) {
		
		String campo = "", subfield = "", ind1 = "", ind2 = "";
		
		switch(xmlTag)
		{
		case "ANCESTRAL1":
			campo = "901";
			subfield = "a";
			break;
		case "ANCESTRAL2":
			campo = "901";
			subfield = "b";
			break;
		case "ANO":
			campo = "100";
			subfield = "d";
			break;
		case "ANO_FALE":
			campo = "100";
			subfield = "d";
			break;
		case "ANO_NASC":
			campo = "100";
			subfield = "d";
			break;
		case "APROVADO":
			campo = "901";
			subfield = "c";
			break;
		case "ARTIGO_SUBTITULO": // TODO concatenar ao campo de subtitulo
			campo = "245";
			ind2 = xmlValue;
			break;
		case "ARTIGO_TITULO":
			campo = "245";
			ind2 = xmlValue;
			break;
		case "ASS_CODIGO":
			campo = "697";
			subfield = "a";
			break;
		case "ASS_DESCRICAO":
			campo = null;
			subfield = null;
			break;
		case "ASS_DESCRICAOBUSCA":
			campo = null;
			subfield = null;
			break;
		case "ASS_INDIC_NCORPORATIVO":
			campo = null;
			subfield = null;
			break;
		case "ASS_INDIC_NEVENTO":
			campo = null;
			subfield = null;
			break;
		case "ASS_INDIC_NPESSOAL":
			campo = null;
			subfield = null;
			break;
		case "ASS_NOMECORPORATIVO":
			campo = "610";
			subfield = "a";
			break;
		case "ASS_NOMEDEEVENTO":
			campo = "611";
			subfield = "a";
			break;
		case "ASS_NOMEGEOGRAFICO":
			campo = "651";
			subfield = "a";
			break;
		case "ASS_NOMEPESSOAL":
			campo = "600";
			subfield = "a";
			break;
		case "ASS_TERMOCRONOLOGICO":
			campo = "648";
			subfield = "a";
			break;
		case "ASS_TERMOLIVRE":
			campo = "697";
			subfield = "a";
			break;
		case "ASS_TERMOTOPICO":
			campo = "650";
			subfield = "a";
			break;
		case "ASS_TITULOUNIFORME":
			campo = "630";
			subfield = "a";
			break;
		case "ASSUNTO":
			campo = "650";
			subfield = "a";
			break;
		case "ASSUNTO_BUSCA":
			campo = "650";
			subfield = "a";
			break;
		case "AUT_JURISDICAO":
			campo = null;
			subfield = null;
			break;
		case "AUT_NIVELCODIFIC":
			campo = null;
			subfield = null;
			break;
		case "AUT_NOMECORPOR":
			campo = null;
			subfield = null;
			break;
		case "AUT_NOMEEVENTO":
			campo = null;
			subfield = null;
			break;
		case "AUT_NOMEPESSOAL": // TODO deve ser concatenado com o campo seguinte
			campo = null;
			subfield = null;
			break;
		case "AUT_NOMESOBRENOME": // TODO deve ser concatenado com o campo anterior
			campo = null;
			subfield = null;
			break;
		case "AUT_NOTAUTORIA":
			campo = "545";
			subfield = "a";
			break;
		case "AUT_QUANTDOCS":
			campo = null;
			subfield = null;
			break;
		case "AUT_SITUACAO":
			campo = null;
			subfield = null;
			break;
		case "AUT_STATUS":
			campo = null;
			subfield = null;
			break;
		case "AUT_TITULOUNIF":
			campo = null;
			subfield = null;
			break;
		case "AUTOR_PRINCIPAL":
			campo = null;
			subfield = null;
			break;
		case "BAIRRO":
			campo = null;
			subfield = null;
			break;
		case "CAS_QUANTDOCS":
			campo = "901";
			subfield = "f";
			break;
		case "CAS_SITUACAO":
			campo = "901";
			subfield = "g";
			break;
		case "CEP":
			campo = null;
			subfield = null;
			break;
		case "CGC":
			campo = "901";
			subfield = "h";
			break;
		case "CLASSIFICACAO":
			campo = "083";
			subfield = "a";
			break;
		case "COD_ASSUNTO":
			campo = "901";
			subfield = "i";
			break;
		case "COD_AUTOR":
			campo = "901";
			subfield = "j";
			break;
		case "COD_BIB":
			campo = "901";
			subfield = "k";
			break;
		case "COD_CATEG":
			campo = "901";
			subfield = "l";
			break;
		case "COD_DOC":
			campo = "035";
			subfield = "a";
			break;
		case "COD_EMPRESA":
			campo = "901";
			subfield = "m";
			break;
		case "COD_FORMAQUISICAO":
			campo = "952";
			subfield = "e";
			break;
		case "COD_GENERO":
			campo = "901";
			subfield = "n";
			break;
		case "COD_LOCAL1":
			campo = "901";
			subfield = "o";
			break;
		case "COD_LOCAL2":
			campo = "901";
			subfield = "p";
			break;
		case "COD_LOCAL3":
			campo = "901";
			subfield = "q";
			break;
		case "COD_LOCALIDADE":
			campo = "260";
			subfield = "a";
			break;
		case "COD_ORIGEM":
			campo = "901";
			subfield = "r";
			break;
		case "COD_RESP":
			campo = "901";
			subfield = "s";
			break;
		case "COD_SECAO":
			campo = "901";
			subfield = "t";
			break;
		case "COD_SUPORTE":
			campo = "901";
			subfield = "u";
			break;
		case "COMPLEMENTO":
			campo = "901";
			subfield = "v";
			break;
		case "CONTATO":
			campo = "901";
			subfield = "w";
			break;
		case "CONTROLEANTIGO":
			campo = "901";
			subfield = "x";
			break;
		case "CUTTER":
			campo = "084";
			subfield = "0";
			break;
		case "DATA":
			campo = "901";
			subfield = "y";
			break;
		case "DATA_INS":
			campo = "901";
			subfield = "z";
			break;
		case "DATA_TOMBAMENTO":
			campo = "902";
			subfield = "a";
			break;
		case "DAU_AUT_INTERROG":
			campo = "902";
			subfield = "b";
			break;
		case "DAU_GERARSECUND":
			campo = null;
			subfield = null;
			break;
		case "DAU_JURISDICAO":
			campo = null;
			subfield = null;
			break;
		case "DAU_NOMECORPOR":
			campo = null;
			subfield = null;
			break;
		case "DAU_NOMEEVENTO":
			campo = null;
			subfield = null;
			break;
		case "DAU_NOMEPESSOAL":
			campo = null;
			subfield = null;
			break;
		case "DAU_RESP_FORMATO":
			campo = "902";
			subfield = "c";
			break;
		case "DAU_RESP_INTERROG":
			campo = "902";
			subfield = "d";
			break;
		case "DAU_RESP_ORDEM":
			campo = "902";
			subfield = "e";
			break;
		case "DAU_SITUACAO":
			campo = "902";
			subfield = "f";
			break;
		case "DAU_TITULOUNIF":
			campo = null;
			subfield = null;
			break;
		case "DCA_SITUACAO":
			campo = "902";
			subfield = "g";
			break;
		case "DCE_SITUACAO":
			campo = "902";
			subfield = "h";
			break;
		case "DDD":
			campo = "902";
			subfield = "i";
			break;
		case "DESCRICAO":
			campo = "902";
			subfield = "j";
			break;
		case "DIMENSAO":
			campo = "300";
			subfield = "c";
			break;
		case "E_MAIL":
			campo = "902";
			subfield = "k";
			break;
		case "EDICAO":
			campo = "250";
			subfield = "a";
			break;
		case "EMP_CODCAT":
			campo = "902";
			subfield = "l";
			break;
		case "ENVIA_AVISO":
			campo = "902";
			subfield = "m";
			break;
		case "EXEMPLAR":
			campo = "952";
			subfield = "t";
			break;
		case "EXTENSAO":
			campo = "300";
			subfield = "a";
			break;
		case "FANTASIA":
			campo = "902";
			subfield = "n";
			break;
		case "FANTASIA_BUSCA":
			campo = "902";
			subfield = "o";
			break;
		case "FAX":
			campo = "902";
			subfield = "p";
			break;
		case "FONE":
			campo = "902";
			subfield = "q";
			break;
		case "FORMAQUISICAO":
			campo = "902";
			subfield = "r";
			break;
		case "FRA_SITUACAO":
			campo = "902";
			subfield = "s";
			break;
		case "GENERO":
			campo = "902";
			subfield = "t";
			break;
		case "IDI_CODIGOCAT":
			campo = "902";
			subfield = "u";
			break;
		case "IE":
			campo = "902";
			subfield = "v";
			break;
		case "ILL_CODIGO":
			campo = "300";
			subfield = "b";
			break;
		case "ILUSTRACAO":
			campo = "300";
			subfield = "b";
			break;
		case "ISBN":
			campo = "020";
			subfield = "a";
			break;
		case "LOC_REFBIB":
			campo = "902";
			subfield = "w";
			break;
		case "LOCALIZACAO":
			campo = "902";
			subfield = "x";
			break;
		case "LOGRADOURO":
			campo = "902";
			subfield = "y";
			break;
		case "MARC":
			campo = "902";
			subfield = "z";
			break;
		case "NOME":
			campo = "100"; // TODO concatenar com campos de nome
			subfield = "a";
			break;
		case "NOMECORPORATIVO":
			campo = null;
			subfield = null;
			break;
		case "NOMEDEEVENTO":
			campo = null;
			subfield = null;
			break;
		case "NOMEGEOGRAFICO":
			campo = null;
			subfield = null;
			break;
		case "NOMEPESSOAL":
			campo = null;
			subfield = null;
			break;
		case "NOTAS":
			campo = "500";
			subfield = "a";
			break;
		case "OBSERVACAO":
			campo = "903";
			subfield = "a";
			break;
		case "ORDEM":
			campo = "903";
			subfield = "b";
			break;
		case "PAGINAS":
			campo = "300";
			subfield = "a";
			break;
		case "RAZAO":
			campo = "903";
			subfield = "c";
			break;
		case "RAZAO_BUSCA":
			campo = "903";
			subfield = "d";
			break;
		case "SIGLA":
			campo = "903";
			subfield = "e";
			break;
		case "SITE":
			campo = "903";
			subfield = "f";
			break;
		case "SOBRENOME":
			campo = "100"; // TODO tem que unir com campos de nome
			subfield = "a";
			break;
		case "STATUS":
			campo = "903";
			subfield = "g";
			break;
		case "SUBTITULO":
			campo = "903";
			subfield = "h";
			break;
		case "SUP_DEFAULT":
			campo = "903";
			subfield = "i";
			break;
		case "SUP_SITUACAO":
			campo = "903";
			subfield = "j";
			break;
		case "SUPORTE":
			campo = "338";
			subfield = "b";
			break;
		case "TERMO1":
			campo = null;
			subfield = null;
			break;
		case "TERMO2":
			campo = null;
			subfield = null;
			break;
		case "TERMO3":
			campo = null;
			subfield = null;
			break;
		case "TERMO4":
			campo = null;
			subfield = null;
			break;
		case "TERMOCRONOLOGICO":
			campo = null;
			subfield = null;
			break;
		case "TERMOLIVRE":
			campo = "697";
			subfield = "a";
			break;
		case "TERMOTOPICO":
			campo = null;
			subfield = null;
			break;
		case "TIPO":
			campo = null;
			subfield = null;
			break;
		case "TIPO_ASSUNTO":
			campo = "697";
			subfield = "a";
			break;
		case "TIPO_BASE":
			campo = null;
			subfield = null;
			break;
		case "TIPO_CADASTRO":
			campo = null;
			subfield = null;
			break;
		case "TIPO_DOC": // TODO deve ocupar o campo 952 $y tbm
			campo = "942";
			subfield = "c";
			break;
		case "TIPOAUTOR":
			campo = null;
			subfield = null;
			break;
		case "TIPODESC":
			campo = null;
			subfield = null;
			break;
		case "TIPOSUBDIVISAO":
			campo = null;
			subfield = null;
			break;
		case "TITULO":
			campo = "245";
			subfield = "a";
			break;
		case "TITULOUNIFORME":
			campo = null;
			subfield = null;
			break;
		case "TOMBO":
			campo = "903";
			subfield = "k";
			break;
		case "TOMO":
			campo = "903";
			subfield = "l";
			break;
		case "TPE_CODIGO":
			campo = "903";
			subfield = "m";
			break;
		case "USU_CODIGO":
			campo = "903";
			subfield = "n";
			break;
		case "VALOR_AQUISICAO":
			campo = "903";
			subfield = "o";
			break;
		case "VOLUME":
			campo = "903";
			subfield = "p";
			break;
		case "VOLUMECOLECAO":
			campo = "903";
			subfield = "q";
			break;
		default:
			System.err.println("Tag não encontrada:" + xmlTag);
			break;
		}
		
		if(campo == null)
			return null;
			
		ArrayList<Subfield> subfields = new ArrayList<>();
		if(!subfield.contentEquals(""))
			subfields.add(new Subfield(subfield, xmlValue));
		
		return new Field(campo, ind1, ind2, subfields, Constant.dataFieldDenomination);
	}
	
}
