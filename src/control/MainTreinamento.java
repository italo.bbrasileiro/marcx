package control;

import java.util.ArrayList;

import entity.Field;
import entity.MARC;
import entity.Record;
import entity.Subfield;

public class MainTreinamento 
{
	static ArrayList<Record> records;
	
	public static void main(String[] args) throws Exception 
	{
		MARC acervo = new MARC("kohaTreinamento/naoSaoDaBC.xml");
		records = acervo.getRecords();
		
		atualizaBibliotecaDeOrigem();
		
		criaArquivoXML(records, "kohaTreinamento/movidosParaBC.xml");
	}
	
	private static void atualizaBibliotecaDeOrigem() 
	{
		for(Record record : records)
			for(Field field952 : record.getField("952"))
				modificaBibliotecaDeRegistro(field952); 
	}

	private static void modificaBibliotecaDeRegistro(Field field952) 
	{
		Subfield subfieldA = null, subfieldB = null; 

		if(field952.hasSubfield("a"))
			subfieldA = field952.getSubfield("a").get(0);
		
		if(field952.hasSubfield("b"))
			subfieldB = field952.getSubfield("b").get(0);
		
		resetSubfieldValue("a", subfieldA);
		resetSubfieldValue("b", subfieldB);
	}
	
	public static void resetSubfieldValue(String name, Subfield subcampo)
	{
		if(subcampo != null)
			subcampo.setValue("BC");
		else
			subcampo = new Subfield("b", "BC");
	}

	private static void criaArquivoXML(ArrayList<Record> records, String outputFile) {
		System.out.println(records.size() + " periodicos criados em " + outputFile);
		XMLHandler.generateXMLFile(records, outputFile);
	}
	
}
