package entity;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexLibrary
{
	static final String regex1 = "[0-9]{4}[,][ ][v][.][ ][0-9]+[,][ ][n][.][ ][0-9]+"; // 1967, v.1, n. 4
	static final String regex2 = "[0-9]{4}[,][ ][n][.][ ][0-9]+"; // 1967, n. 4
	static final String regex3 = "[0-9]{4}[,][ ][v][.][ ][0-9]+"; // 1967, v. 4
	static final String regex4 = "[0-9]{4}"; // 1967
	static final String regex5 = "[0-9]{3}"; //685, registros da revista Epoca com apenas informacao do numero
	static final String regex6 = "[n][.][ ][a-z]+"; // n. especial e algumas variações

	public static String getCorrespondingRegex(String editionInformation) 
	{
		if(matches(editionInformation, regex1))
			return regex1;
		if(matches(editionInformation, regex2))
			return regex2;
		if(matches(editionInformation, regex3))
			return regex3;
		if(matches(editionInformation, regex4))
			return regex4;
		if(matches(editionInformation, regex5))
			return regex5;
		if(matches(editionInformation, regex6))
			return regex6;
		// para testes de regex não registrados
		else
			System.out.println("Entradas em 952 $h com problemas:" + editionInformation);
		
		return "";
	}
	
	public static Field generateField853ForRegex(String fieldRegex, Field fieldExample952) 
	{
		switch(fieldRegex)
		{
		case regex1:
			return (new Field("853", "2", "0", subfieldFor853Regex1(), Constant.dataFieldDenomination));
		case regex2:
			return (new Field("853", "2", "0", subfieldFor853Regex1(), Constant.dataFieldDenomination));
		case regex3:
			return (new Field("853", "2", "0", subfieldFor853Regex1(), Constant.dataFieldDenomination));
		case regex4:
			return (new Field("853", "2", "0", subfieldFor853Regex1(), Constant.dataFieldDenomination));
		case regex5:
			return (new Field("853", "2", "0", subfieldFor853Regex1(), Constant.dataFieldDenomination));
		case regex6:
			return (new Field("853", "2", "0", subfieldFor853Regex1(), Constant.dataFieldDenomination));
		default:
			return null;
		}
	}
	
	public static Field generateField863ForRegex(String fieldRegex, Field field952) 
	{
		switch(fieldRegex)
		{
		case regex1:
			return (new Field("863", "4", "1", subfieldFor863Regex1(field952), Constant.dataFieldDenomination));
		case regex2:
			return (new Field("863", "4", "1", subfieldFor863Regex2(field952), Constant.dataFieldDenomination));
		case regex3:
			return (new Field("863", "4", "1", subfieldFor863Regex3(field952), Constant.dataFieldDenomination));
		case regex4:
			return (new Field("863", "4", "1", subfieldFor863Regex4(field952), Constant.dataFieldDenomination));
		case regex5:
			return (new Field("863", "4", "1", subfieldFor863Regex5(field952), Constant.dataFieldDenomination));
		case regex6:
			return (new Field("863", "4", "1", subfieldFor863Regex5(field952), Constant.dataFieldDenomination));
		default: // TODO parei aqui
			System.out.println(fieldRegex);
		}
		
		return null;
	}

	private static ArrayList<Subfield> subfieldFor853Regex1() {
		ArrayList<Subfield> subfields = new ArrayList<>();
		
		subfields.add(new Subfield("8", "1"));
		subfields.add(new Subfield("a", "v."));
		subfields.add(new Subfield("b", "n."));
		subfields.add(new Subfield("d", "pt"));
		subfields.add(new Subfield("i", "(ano)"));
		subfields.add(new Subfield("j", "(mês)"));
		
		return subfields;
	}
	
	private static ArrayList<Subfield> subfieldFor863Regex1(Field field952) 
	{
		ArrayList<Subfield> subfields = new ArrayList<>();
		// year definition
		String editionInfo = field952.getSubfield("h").get(0).getValue();
		String year = editionInfo.substring(0, 4);
		subfields.add(new Subfield("i", year));
		
		// volume definition
		String volume = editionInfo.split(",")[1].trim();
		volume = volume.split(" ")[1];
		subfields.add(new Subfield("a", volume));
		
		// number definition
		String number = editionInfo.split(",")[2].trim();
		number = number.split(" ")[1];
		subfields.add(new Subfield("b", number));
		
		return subfields;
	}
	
	private static ArrayList<Subfield> subfieldFor863Regex2(Field field952) 
	{
		ArrayList<Subfield> subfields = new ArrayList<>();
		// year definition
		String editionInfo = field952.getSubfield("h").get(0).getValue();
		String year = editionInfo.substring(0, 4);
		subfields.add(new Subfield("i", year));
		
		// number definition
		String number = editionInfo.split(",")[1].trim();
		number = number.split(" ")[1];
		subfields.add(new Subfield("b", number));
		
		return subfields;
	}
	
	private static ArrayList<Subfield> subfieldFor863Regex3(Field field952) 
	{
		ArrayList<Subfield> subfields = new ArrayList<>();
		// year definition
		String editionInfo = field952.getSubfield("h").get(0).getValue();
		String year = editionInfo.substring(0, 4);
		subfields.add(new Subfield("i", year));
		
		// number definition
		String volume = editionInfo.split(",")[1].trim();
		volume = volume.split(" ")[1];
		subfields.add(new Subfield("a", volume));
		
		return subfields;
	}
	
	private static ArrayList<Subfield> subfieldFor863Regex4(Field field952) 
	{
		ArrayList<Subfield> subfields = new ArrayList<>();
		// year definition
		String year = field952.getSubfield("h").get(0).getValue();
		subfields.add(new Subfield("i", year));
		
		return subfields;
	}
	
	private static ArrayList<Subfield> subfieldFor863Regex5(Field field952) 
	{
		ArrayList<Subfield> subfields = new ArrayList<>();
		
		// number definition
		String number = field952.getSubfield("h").get(0).getValue();
		subfields.add(new Subfield("b", number));
		
		return subfields;
	}

	private static boolean matches(String textLine, String regex)
	{
		Pattern pattern = Pattern.compile(regex);
		// create matcher for pattern p and given string
		Matcher matcher = pattern.matcher(textLine);
		
		// if an occurrence if a pattern was found in a given string...
		if (matcher.find()) 
			return true;
		return false;
	}
	
	private static String getFieldThatMatches(String textLine, String regex)
	{
		Pattern pattern = Pattern.compile(regex);
		// create matcher for pattern p and given string
		Matcher matcher = pattern.matcher(textLine);
		String valueThatMatches = null;
		
		// if an occurrence if a pattern was found in a given string...
		if (matcher.find()) {
			// ...then you can use group() methods.
			valueThatMatches = matcher.group(0); // whole matched expression
//			System.out.println(m.group(1)); // first expression from round brackets (Testing)
//			System.out.println(m.group(2)); // second one (123)
//			System.out.println(m.group(3)); // third one (Testing)
		}

		return valueThatMatches;
	}
}