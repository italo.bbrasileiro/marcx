package entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public class Record {
	ArrayList<Field> fields;

	public Record(ArrayList<Field> fields) 
	{
		this.fields = fields;			

		for(Field field : fields)
			field.setCorrespondingRecord(this);
	}

	/**
	 * Adiciona o campo 'field' a lista de campos
	 * do registro atual.
	 * @param field
	 * @return
	 */
	public boolean addField(Field field)
	{
		field.setCorrespondingRecord(this);
		return this.fields.add(field);
	}
	
	public void addField(ArrayList<Field> fields)
	{
		for(Field field : fields)
			addField(field);
	}

	/**
	 * Adiciona os campos 'fields' a lista de campos
	 * do registro atual.
	 * @param fields arrayList com os campos a serem adicionados.
	 */
	public void addFields(ArrayList<Field> fields)
	{	
		for(Field field : fields)
			this.addField(field);
	}

	/**
	 * Metodo 'main' para a correcao dos registros de periodicos.
	 * @param args
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws TransformerException
	 */
	public void corrigeISSN(String fieldName, String subfieldName)
	{
		// O ISSN eh usado em um campo nao duplicavel
		Field issnField = getField(fieldName).get(0);

		if(issnField == null)
		{
			System.err.println("Registro sem ISSN!");
			return;
		}

		Subfield issnSubfield = issnField.getSubfield(subfieldName).get(0);
		String valor = issnSubfield.getValue();

		if(!valor.contains("-") && valor.length()==8)
		{
			valor = valor.substring(0, 4) + "-" + valor.substring(4, 8);
			issnSubfield.setValue(valor);
		}else if(valor.length() != 9 || !valor.contains("-"))
		{
			System.out.println("Erro: " + valor);
		}
	}
	
	/**
	 * Cria uma instancia do campo indicado por 'fieldName', contendo apenas o subcampo
	 * indicado por subfieldName, contendo o valor 'subfieldValue'
	 * @param fieldName nome do campo a ser criado
	 * @param subfieldName nome do subcampo a ser criado
	 * @param subfieldValue valor a ser inserido no subcampo
	 */
	public Field createFieldAndSubfield(String fieldName, String subfieldName, String subfieldValue)
	{
		Subfield sub = new Subfield(subfieldName, subfieldValue, Constant.subfieldDenomination);
		ArrayList<Subfield> subs = new ArrayList<Subfield>();
		
		subs.add(sub);
		Field field = new Field(fieldName, "", "", subs, Constant.dataFieldDenomination);
		this.addField(field);
		
		return field;
	}
	
	/**
	 * 
	 * @param valueToInsert
	 * @param charRange range of index to edit, separated with "-". Ex: "15-18".
	 */
	public void editField008(String valueToInsert, String charRange) {
		
		int begin = Integer.parseInt(charRange.split("-")[0]);
		String currentValue = this.getField("008").get(0).getValue();
		
		currentValue = currentValue.substring(0, begin) + valueToInsert 
				+ currentValue.substring(begin + valueToInsert.length(), currentValue.length());
		
		this.getField("008").get(0).setValue(currentValue);
	}
	
	
	public void generateLeader(String leaderValue)
	{
		Field newField = new Field(Constant.leaderDenomination, leaderValue, Constant.leaderDenomination);
		fields.add(newField);
	}
	
	
	public void generateLeaderAndControlFields()
	{
		generateLeader("00415nz  a2200121n  4500");
		generateControlFields();
	}
	
	public void generateControlFields()
	{
		Date data = new Date();
		String data005 = Integer.toString(data.getYear() + 1900);
		String mes = Integer.toString(data.getMonth()+1);
		if(mes.length() == 1)
			mes = "0".concat(mes);
		data005 = data005.concat(mes);
		String dia = Integer.toString(data.getDate());
		if(dia.length() == 1)
			dia = "0".concat(dia);
		data005 = data005.concat(dia);
		String hora = Integer.toString(data.getHours());
		if(hora.length() == 1)
			hora = "0".concat(hora);
		data005 = data005.concat(hora);
		String minuto = Integer.toString(data.getMinutes());
		if(minuto.length() == 1)
			minuto = "0".concat(minuto);
		data005 = data005.concat(minuto);
		String segundo = Integer.toString(data.getSeconds());
		if(segundo.length() == 1)
			segundo = "0".concat(segundo);
		data005 = data005.concat(segundo);
		data005 = data005.concat(".0");
		
		Field newField = new Field("003", "OSt", Constant.controlFieldDenomination);
		fields.add(newField);
		newField = new Field("005", data005, Constant.controlFieldDenomination);
		fields.add(newField);
		newField = new Field("008", "                                        ", Constant.controlFieldDenomination);
		fields.add(newField);
		
	}
	
	
	
	public void generateLeaderAnd005Field()
	{
		Date data = new Date();
		String data005 = Integer.toString(data.getYear() + 1900);
		String mes = Integer.toString(data.getMonth()+1);
		if(mes.length() == 1)
			mes = "0".concat(mes);
		data005 = data005.concat(mes);
		String dia = Integer.toString(data.getDate());
		if(dia.length() == 1)
			dia = "0".concat(dia);
		data005 = data005.concat(dia);
		String hora = Integer.toString(data.getHours());
		if(hora.length() == 1)
			hora = "0".concat(hora);
		data005 = data005.concat(hora);
		String minuto = Integer.toString(data.getMinutes());
		if(minuto.length() == 1)
			minuto = "0".concat(minuto);
		data005 = data005.concat(minuto);
		String segundo = Integer.toString(data.getSeconds());
		if(segundo.length() == 1)
			segundo = "0".concat(segundo);
		data005 = data005.concat(segundo);
		data005 = data005.concat(".0");
		
		generateLeader("00415nz  a2200121n  4500");
		Field newField = new Field("005", data005, Constant.controlFieldDenomination);
		fields.add(newField);
	}
	
	/**
	 * Avalia o registro (deve ser de autoridades), verifica qual o tipo da autoridade
	 * pelo campo 942 $c, e retorna o valor do subcampo $a do campo principal da autoridade.
	 * @return
	 */
	public String getAuthorityMainFieldValue()
	{
		String value = null;
		String mainField = null;
		 switch(this.getField("942").get(0).getSubfield("a").get(0).getValue())
		 {
		 case "CORPO_NAME":
			 mainField = "110";
			 break;
		 case "GEOGR_NAME":
			 mainField = "151";
			 break;
		 case "MEETI_NAME":
			 mainField = "111";
			 break;
		 case "PERSO_NAME":
			 mainField = "100";
			 break;
		 case "TOPIC_TERM":
			 mainField = "150";
			 break;
		 case "UNIF_TITLE":
			 mainField = "130";
			 break;
			 default:
				 System.err.println("No 942 $a defined");
		 }
		value = this.getField(mainField).get(0).getSubfield("a").get(0).getValue();
		return value;
	}
	
	/**
	 * Retorna o tipo da autoridade atual, referente ao conteudo do campo 942 $a.
	 * @return
	 */
	public String getAuthorityType()
	{
		if(this.getField("942") == null || this.getField("942").isEmpty() || !this.getField("942").get(0).hasSubfield("a"))
		{
			System.err.println("Autoridade " + this.getField("001").get(0).getValue() + " sem tipo definido no 942, ou sem subcampo $a");
			return null;
		}
		else
			return this.getField("942").get(0).getSubfield("a").get(0).getValue();
	}
	
	/**
	 * Avalia o campo field e retorna o tipo de autoridade relacionada ao campo.
	 * @param field
	 * @return
	 */
	public String getAuthorityTypeFromRecordField(String field)
	{
		switch(field)
		{
		case "100":
			return "PERSO_NAME";
		case "110":
			return "CORPO_NAME";
		case "111":
			return "MEETI_NAME";
		case "130":
			return "UNIF_TITLE";
		case "260":
			return "CORPO_NAME";
		case "600":
			return "PERSO_NAME";
		case "610":
			return "CORPO_NAME";
		case "611":
			return "MEETI_NAME";
		case "630":
			return "UNIF_TITLE";
		case "650":
			return "TOPIC_TERM";
		case "700":
			return "PERSO_NAME";
		case "710":
			return "CORPO_NAME";
		case "711":
			return "MEETI_NAME";
		case "730":
			return "UNIF_TITLE";
		default:
			return null;
		}
	}
	
	/**
	 * Retorna uma lista com todos os identificadores
	 * de todos os datafield do registro. Eh retornado um
	 * elemento para cada campo duplicado, caso ocorra.
	 * @return
	 */
	public ArrayList<String> getDataFieldNameList()
	{
		ArrayList<String> nameList = new ArrayList<String>();
		HashMap<String, String> temp = new HashMap<String, String>();

		for (Field field : this.fields)
		{
			temp.put(field.getFieldName(), "");
		}

		Iterator<String> it = temp.keySet().iterator();

		while(it.hasNext())
		{
			nameList.add(it.next());
		}

		return nameList;
	}
	
	/**
	 * Busca por um campo especifico, indicado por fieldName.
	 * Caso nao encontre, retorna null.
	 * @param fieldName campo procurado.
	 * @return Field campo procurado ou null. 
	 */
	public ArrayList<Field> getField(String fieldName)
	{
		ArrayList<Field> f = null;
		for(Field field : fields)
		{
			if(field.getFieldName().contentEquals(fieldName))
			{
				if(f == null)
					f = new ArrayList<Field>();
				f.add(field);
			}
		}
		return f;
	}
	
	/**
	 * Retorna uma lista com todos os identificadores
	 * de todos os campos do registro. Eh retornado um
	 * elemento para cada campo duplicado, caso ocorra.
	 * @return
	 */
	public ArrayList<String> getFieldNameList()
	{
		ArrayList<String> nameList = new ArrayList<String>();
		HashMap<String, String> temp = new HashMap<String, String>();

		for (Field field : this.fields)
		{
			temp.put(field.getFieldName(), "");
		}

		Iterator<String> it = temp.keySet().iterator();

		while(it.hasNext())
		{
			nameList.add(it.next());
		}

		return nameList;
	}
	
	/**
	 * Retorna uma lista com todos os identificadores
	 * de todos os campos do registro. Eh retornado um
	 * unico elemento no caso de campos duplicados.
	 * @return
	 */
	public ArrayList<String> getFieldNameListIncludingDuplicateds()
	{
		ArrayList<String> nameList = new ArrayList<String>();

		for (Field field : this.fields)
		{
			nameList.add(field.getFieldName());
		}
		return nameList;
	}
	
	public ArrayList<Field> getFields() {
		return fields;
	}
	
	/**
	 * Retorna a lista de campos desse registro ordenada.
	 * @return
	 */
	public ArrayList<Field> getFieldsOrdered() {
		Field menorCampo = null;
		int menorValorCampo;
		ArrayList<Field> orderedFields = new ArrayList<Field>();

		orderedFields.add(getLeaderField());
		ArrayList<Field> temp = getField("001");
		if(temp == null)
		{
//			System.err.println("Registro sem campo 001");
		}else
		{
			menorCampo = temp.get(0);
			orderedFields.add(menorCampo);
		}

		// Insercao dos elementos de record.getField na lista
		// ordered field, ate que as duas tenham o mesmo tamanho.
		while(getFields().size() != orderedFields.size())
		{
			menorValorCampo  = Integer.MAX_VALUE;
			for(Field field : getFields())
			{	
				if((!field.getFieldName().contentEquals("leader")) 
						&& (!field.getFieldName().contentEquals("marc:leader"))
						&& Integer.parseInt(field.getFieldName()) <= menorValorCampo 
						&& !orderedFields.contains(field))
				{
					menorCampo = field;
					menorValorCampo = Integer.parseInt(field.getFieldName());
				}
			}
			orderedFields.add(menorCampo);
		}
		return orderedFields;
	}
	
	/**
	 * Retorna o campo que contem o kardex do registro.
	 * @return {@link Field} correspondente ao kardex. If no kardex is found, return null.
	 * 
	 */
	public Field getKardex()
	{
		ArrayList<Field> fields = getField(Constant.kardexField);
		if(fields == null || fields.size() == 0)
			return null;

		Field kardex = fields.get(0);

		return kardex;
	}
	
	/**
	 * Retorna o valor do campo que contem o kardex do registro.
	 * @return {@link Field} correspondente ao kardex.
	 */
	public String getKardexValue()
	{
		ArrayList<Field> fields = getField(Constant.kardexField);
		if(fields == null || fields.size() == 0)
			System.out.println();

		Field kardex = fields.get(0);

		return kardex.getSubfield("a").get(0).getValue();
	}

	/**
	 *  Retorna o objeto Field com o valor do campo 'leader' do registro.
	 *  
	 * @return Field correspodente ao campo 'leader'.
	 */
	public Field getLeaderField() {

		for(Field field : fields)
		{
			if(field.getFieldName().contentEquals(Constant.leaderDenomination))
			{
				return field;
			}
		}
		System.err.println("Registro sem leader");
		return null;
	}
	
	

	/**
	 * Utilizado para gerar uma string com o registro formatado em XML.
	 * @return
	 */
	public String getRecordAsXML()
	{
		String result = "";
		result += "<" + Constant.recordDenomination + ">\n";
		
		// print leader field
		Field leader = this.getLeaderField();
		result += "\t<" + leader.getXmlNodeName() + ">" + leader.getValue() +  "</" + leader.getXmlNodeName() + ">\n";
		
		// percorre a lista de campos em ordem crescente e printa cada um deles
		for(Field field : this.getFieldsOrdered())
		{
			if(field.getFieldName().contentEquals(leader.getFieldName()))
				continue;
				
			result += "\t<" + field.getXmlNodeName();
			if(field.getXmlNodeName().contentEquals(Constant.dataFieldDenomination))
			{
				result += " " + Constant.indicator1Denomination + "=\"" + field.getIndic1()
					+ "\" " + Constant.indicator2Denomination + "=\"" + field.getIndic2()
					+ "\"";
			}
			result += " " + Constant.fieldTagDenomination + "=\"" + field.getFieldName() + "\">";
			
			if(field.getXmlNodeName().contentEquals(Constant.controlFieldDenomination))
			{
				result += field.getValue() + "</" + field.getXmlNodeName() + ">\n";
			}else
			{
				if(field.getSubfields().size() > 0)
				{
					for(Subfield subfield : field.getSubfields())
					{
						result += "\n\t\t<" + subfield.getXmlNodeName() + " " + Constant.subfieldTagDenomination + "=\""
								+ subfield.getSubfieldName() + "\">" + subfield.getValue() + "</" + subfield.getXmlNodeName() + ">\n";
					}
				}
				result += "\t</" + field.getXmlNodeName() + ">\n";
			}
		}
		
		result += "</" + Constant.recordDenomination + ">";
		
		return result;
	}
	
	/**
	 * Utilizado para gerar uma string com o registro formatado em XML, sem os campos indicados em "fields".
	 * @return
	 */
	public String getRecordAsXMLWithoutIndicatedFields(String[] fields)
	{
		String result = "";
		result += "<" + Constant.recordDenomination + ">\n";
		boolean useField = true;
		
		// print leader field
		for(String fieldToAvoid : fields)
			if(fieldToAvoid.contentEquals(Constant.leaderDenomination))
				useField = false;
				
		Field leader = this.getLeaderField();
		if (useField) {
			result += "\t<" + leader.getXmlNodeName() + ">" + leader.getValue() +  "</" + leader.getXmlNodeName() + ">\n";
		}
		
		// percorre a lista de campos em ordem crescente e printa cada um deles
		for(Field field : this.getFieldsOrdered())
		{
			if(field.getFieldName().contentEquals(leader.getFieldName()))
				continue;
				
			useField = true;
			for(String fieldToAvoid : fields)
				if(field.getFieldName().contentEquals(fieldToAvoid))
					useField = false;
			
			if(!useField)
				continue;
			
			result += "\t<" + field.getXmlNodeName();
			if(field.getXmlNodeName().contentEquals(Constant.dataFieldDenomination))
			{
				result += " " + Constant.indicator1Denomination + "=\"" + field.getIndic1()
					+ "\" " + Constant.indicator2Denomination + "=\"" + field.getIndic2()
					+ "\"";
			}
			result += " " + Constant.fieldTagDenomination + "=\"" + field.getFieldName() + "\">";
			
			if(field.getXmlNodeName().contentEquals(Constant.controlFieldDenomination))
			{
				result += field.getValue() + "</" + field.getXmlNodeName() + ">\n";
			}else
			{
				if(field.getSubfields().size() > 0)
				{
					for(Subfield subfield : field.getSubfields())
					{
						result += "\n\t\t<" + subfield.getXmlNodeName() + " " + Constant.subfieldTagDenomination + "=\""
								+ subfield.getSubfieldName() + "\">" + subfield.getValue() + "</" + subfield.getXmlNodeName() + ">\n";
					}
				}
				result += "\t</" + field.getXmlNodeName() + ">\n";
			}
		}
		
		result += "</" + Constant.recordDenomination + ">";
		
		return result;
	}
	
	public int getTotalNumberOfSubfields()
	{
		int total = 0;
		for(Field field : getFields())
		{
			total += field.getSubfields().size();
		}
		return total;
	}
	
	/**
	 * Esta funcao verifica se o registro atual apresenta o campo indicado por
	 * 'fieldName'.
	 * @param fieldName campo procurado.
	 * @return
	 */
	public boolean hasField(String fieldName)
	{
		for(Field field : fields)
		{
			if(field.getFieldName().contentEquals(fieldName))
				return true;
		}
		return false;
	}
	
	/**
	 * Return true only if the current record has at least one occurence of
	 * the mentioned field with the subfield.
	 * @param field
	 * @param subfield
	 * @return
	 */
	public boolean hasAnyOccurrenceOfFieldAndSubfield(String field, String subfield)
	{
		if(!hasField(field))
			return false;
		
		for(Field f : getField(field))
		{
			if(f.hasSubfield(subfield))
				return true;
		}
		
		return false;
	}

	/**
	 * Verifica se o campo 'fieldName' esta duplicado dentro do registro atual.
	 * @param fieldName Nome do campo verificado.
	 * @return true se for duplicado, false caso contrario.
	 */
	public boolean isFieldDuplicated(String fieldName)
	{	
		boolean primeiraInstancia = false;
		if(!hasField(fieldName))
			return false;

		for(Field field : fields)
		{	
			if(field.getFieldName().contentEquals(fieldName) && primeiraInstancia)
				return true;

			if(field.getFieldName().contentEquals(fieldName))
				primeiraInstancia = true;

		}
		return false;
	}
	
	/**
	 * Compara Adiciona campos/subcampos de secondaryRecord no registro atual, 
	 * quando nao existem.
	 * Adiciona duplicado apenas o campo 952.
	 * @param recordToKeep
	 * @param recordToVerify
	 */
	public void mergeNewFieldsFromRecord(Record secondaryRecord)
	{
		String fieldName, subfieldName;
		for(Field field : secondaryRecord.getFields())
		{
			fieldName = field.getFieldName();
			if(!this.hasField(fieldName) || fieldName.contentEquals("952"))
			{
				this.addField(field);
				continue;
			}else
			{
				if(!field.hasAnySubfield())
					continue;
				else
				{
					for(Subfield subfield : field.getSubfields())
					{
						subfieldName = subfield.getSubfieldName();
						if(!this.hasAnyOccurrenceOfFieldAndSubfield(fieldName, subfieldName))
						{
							this.getField(fieldName).get(0).addSubfield(subfield);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Used to create a single field, as a result of merging all
	 * subfields in all occurrences of the mentioned field. 
	 * @param field
	 */
	public void mergeMultipleOccurrencesOfField(String field)
	{
		if (hasField(field) && getField(field).size() > 1)
		{
			Field temp = getField(field).get(0);
			for (int i = 1; i < getField(field).size(); i++)
			{
				Field currentField = getField(field).get(i);
				temp.addSubfields(currentField.getSubfields());
				removeField(currentField);
			}
		}
	}
	
	/**
	 * Remove o campo especificado em 'field'
	 * da lista de campos do registro atual.
	 * @param field
	 * @return
	 */
	public void removeAllOccurrencesOfField(String fieldName)
	{
		for(int f = 0; f < fields.size(); f++)
		{
			Field field = this.fields.get(f);
			if(field.getFieldName().contentEquals(fieldName))
			{
				fields.remove(field);
				f--;
			}
		}
	}
	
	/**
	 * Remove o campo especificado em 'field'
	 * da lista de campos do registro atual.
	 * @param field
	 * @return
	 */
	public boolean removeField(Field field)
	{
		return this.fields.remove(field);
	}

	/**
	 * Troca o nome do campo e dos seus subcampos, na mesma ordem apresentada nos
	 * vetores 'oldSubfields' e 'nemSubfields'.
	 * @param oldFieldName Antigo nome do campo.
	 * @param newFieldName Novo nome do campo.
	 * @param oldSubfields Vetor com antigos nomes dos subcampos.
	 * @param newSubfields Vetor com novos nomes de subcampos.
	 */
	public void swapFieldAndSubfields(String oldFieldName, String newFieldName, String[] oldSubfields, String[] newSubfields)
	{
		ArrayList<Field> oldFields = getField(oldFieldName);

		for(Field field : oldFields)
		{
			field.setFieldName(newFieldName);
			for(int s = 0; s < oldSubfields.length; s++)
			{
				ArrayList<Subfield> oldS = field.getSubfield(oldSubfields[s]);
				if(oldS != null && oldS.size() > 0)
				{
					for(Subfield subfield : oldS)
					{
						subfield.setSubfieldName(newSubfields[s]);
					}
				}
			}
		}
	}
	
	public void setFields(ArrayList<Field> fields) {
		this.fields = fields;
	}

	public String getTitle() 
	{	return getField("245").get(0).getSubfield("a").get(0).getValue();	}
}
