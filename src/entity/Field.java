package entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Field {
	String fieldName;
	ArrayList<Subfield> subfields;
	String indic1;
	String indic2;
	String value;
	String xmlNodeName; // nome do no usado no xml. Pode ser usado o nome apresentado em Constant.dataFieldDenomination ou Constant.controlFieldDenomination
	Record correspondingRecord;
	
	public Field(String fieldName, String nodeName) {
		this.fieldName = fieldName;
		this.indic1 = " ";
		this.indic2 = " ";
		this.xmlNodeName = getXmlNodeName(fieldName);
		subfields = new ArrayList<Subfield>();
	}
	
	public Field(String leaderValue) {
		this.xmlNodeName = Constant.leaderDenomination;
		value = leaderValue;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.getFieldName();
	}
	
	public Field(String fieldName, String value, String nodeName) {
		this.fieldName = fieldName;
		this.xmlNodeName = getXmlNodeName(fieldName);
		this.value = value;
		subfields = new ArrayList<Subfield>();
	}
	
	public Field(String fieldName, String ind1, String ind2, ArrayList<Subfield> subfields, String nodeName) {
		this.fieldName = fieldName;
		this.subfields = subfields;
		this.xmlNodeName = getXmlNodeName(fieldName);
		this.indic1 = ind1;
		this.indic2 = ind2;
	}
	
	public Field(String fieldName, String subfieldName, String subfieldValue, String nodeName) {
		this.fieldName = fieldName;
		this.indic1 = " ";
		this.indic2 = " ";
		
		if(this.subfields == null)
			this.subfields = new ArrayList<>();
		
		Subfield subfield = new Subfield(subfieldName, subfieldValue);
		subfields.add(subfield);
		
		this.xmlNodeName = getXmlNodeName(fieldName);
	}
	
	public void addIndicators(String ind1, String ind2)
	{
		this.setIndic1(ind1);
		this.setIndic1(ind2);
	}
	
	/**
	 * Adiciona o subcampo 'subfield' a lista de subcampos
	 * do campo atual.
	 * @param subfield
	 * @return
	 */
	public boolean addSubfield(Subfield subfield)
	{
		if(this.subfields.add(subfield))
		{
			subfield.setField(this);
			return true;
		}
		return false;
	}
	
	/**
	 * Adiciona a lista de subcampos 'subfields' a lista de subcampos
	 * do campo atual.
	 * @param subfield
	 * @return
	 */
	public boolean addSubfields(ArrayList<Subfield> subfields)
	{
		for(Subfield s : subfields)
		{
			if(!addSubfield(s))
			{
				return false;
			}
			s.setField(this);
		}
		return true;
	}
	
	/**
	 * Define como " " o valor dos indicadores 'ind1' e 'ind2' do campo atual.
	 */
	public void cleanFieldIndicators() {
		indic1 = " ";
		indic2 = " ";
	}
	
	/**
	 * Concatena os subcampos da lista 'subfields' e armazena o resultado dentro
	 * do subcampo 'destinationSubfield'. Se o subcampo apresenta valor duplicado, o
	 * primeiro deles 
	 * @param subfields
	 * @param destinationSubfield
	 * @return
	 */
	public boolean concatenateSubfields(String[] subfields, Subfield destinationSubfield)
	{
		String result = "";
		
		for(String sf : subfields)
		{
			if(hasSubfield(sf))
				result += getSubfield(sf).get(0).getValue() + ", ";
		}
		
		// TODO verificar remocao de virgula e espaco final
		if(result.endsWith(", "))
			result = result.substring(0, result.length() - 2);
		
		if(result.contentEquals(""))
			return false;
		return true; 
	}
	
	/**
	 * Create subfield inside the current field.
	 * @param subfield
	 * @param value
	 */
	public void createSubfield(String subfield, String value)
	{
		Subfield novoSubcampo = new Subfield(subfield, value, Constant.subfieldDenomination);
		novoSubcampo.setField(this);
		this.subfields.add(novoSubcampo);
	}
	
	/**
	 * Retorna todo o campo no formato xml em texto, com todos os subcampos existentes,
	 * separados pelo separador indicado
	 * @return
	 */
	public String getFieldAsXML()
	{
		String fieldText = "";
		
		// percorre a lista de campos em ordem crescente e printa cada um deles
		if(getFieldName().contentEquals(Constant.leaderDenomination))
			fieldText+="\t<" + getXmlNodeName() + ">" + getValue() + "</" + getXmlNodeName() + ">\n";
		else
		{
			fieldText += "\t<" + getXmlNodeName();
			if(getXmlNodeName().contentEquals(Constant.dataFieldDenomination))
			{
				fieldText += " " + Constant.indicator1Denomination + "=\"" + getIndic1()
					+ "\" " + Constant.indicator2Denomination + "=\"" + getIndic2()
					+ "\"";
			}
			fieldText += " " + Constant.fieldTagDenomination + "=\"" + getFieldName() + "\">";
			
			if(getXmlNodeName().contentEquals(Constant.controlFieldDenomination))
			{
				fieldText += getValue() + "</" + getXmlNodeName() + ">\n";
			}
			if(getSubfields().size() > 0)
			{
				for(Subfield subfield : getSubfields())
				{
					fieldText += "\n\t\t<" + subfield.getXmlNodeName() + " " + Constant.subfieldTagDenomination + "=\""
							+ subfield.getSubfieldName() + "\">" + subfield.getValue() + "</" + subfield.getXmlNodeName() + ">\n";
				}
			}
			fieldText += "\t</" + getXmlNodeName() + ">\n";
		}
		return fieldText;
	}
	
	public String getFieldName() {
		return fieldName;
	}
	
	public String getIndic1() {
		return indic1;
	}
	
	public String getIndic2() {
		return indic2;
	}
	
	/**
	 * Retorna uma lista de subcampos indicados por 'subfieldName' contidos
	 * dentro do campo atual.
	 * @param subfieldName
	 * @return
	 */
	public ArrayList<Subfield> getSubfield(String subfieldName)
	{
		ArrayList<Subfield> subf = null;
		
		for(Subfield subfield : this.subfields)
		{
			if(subfield.getSubfieldName().contentEquals(subfieldName))
			{
				if(subf == null)
					subf = new ArrayList<Subfield>();
				subf.add(subfield);
			}
		}
		return subf;
	}
	
	public ArrayList<Subfield> getSubfields() {
		return subfields;
	}
	
	public String getValue() {
		return value;
	}
	
	public String getXmlNodeName() {
		return xmlNodeName;
	}
	
	/**
	 * Verifica se o campo correspondente apresenta
	 * o subcampo indicado por 'subfieldName'.
	 * @param subfieldName Nome do subcampo procurado.
	 * @return
	 */
	public boolean hasSubfield(String subfieldName)
	{
		if(subfields == null || subfields.isEmpty())
			return false;
		
		for(Subfield subfield : subfields)
		{
			if(subfield.getSubfieldName().contentEquals(subfieldName))
				return true;
		}
		return false;
	}
	
	/**
	 * Verifica se o campo correspondente apresenta
	 * algum subcampo.
	 * @param subfieldName Nome do subcampo procurado.
	 * @return
	 */
	public boolean hasAnySubfield()
	{
		if(subfields == null || subfields.isEmpty() || (subfields.size() == 0))
			return false;
		
		return true;
	}
	

	
	private boolean hasSubfieldOnList(String[] list, String subfield)
	{
		for(String s : list)
		{
			if(s.contentEquals(subfield))
				return true;
		}
		return false;
	}
	
	/**
	 * Verifica se existe outro subcampo (diferente de searchedSubfield) 
	 * detro do campo atual com o mesmo nome. 
	 * @param searchedSubfield Campo avaliado.
	 * @return true se o campo for duplicado.
	 */
	public boolean isSubfieldDuplicated(Subfield searchedSubfield)
	{
		for(Subfield subfield : this.subfields)
		{
			if(!subfield.equals(searchedSubfield) && subfield.getSubfieldName().contentEquals(searchedSubfield.getSubfieldName()))
				return true;
		}
		return false;
	}
	

	
	/**
	 * Remove o subcampo especificado em 'subfield' da lista de subcampos
	 * do campo atual.
	 * @param subfield
	 * @return
	 */
	public boolean removeSubfield(Subfield subfield)
	{
		return subfields.remove(subfield);
	}
	
	/**
	 * Remove o subcampo especificado em 'subfield' da lista de subcampos
	 * do campo atual.
	 * @param subfield
	 * @return
	 */
	public void removeSubfieldsExceptTheList(String[] subfieldsNotToRemove)
	{
		for(int i = 0; i < this.getSubfields().size(); i++)
		{
			if(!hasSubfieldOnList(subfieldsNotToRemove, this.getSubfields().get(i).getSubfieldName()))
			{
				removeSubfield(this.getSubfields().get(i));
				i--;
			}
		}
	}
	
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setIndic1(String indic1) {
		this.indic1 = indic1;
	}

	public void setIndic2(String indic2) {
		this.indic2 = indic2;
	}
	
	public void setSubfields(ArrayList<Subfield> subfields) {
		this.subfields = subfields;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public void setXmlNodeName(String nodeName) {
		this.xmlNodeName = nodeName;
	}
	
	/**
	 * Troca o nome do campo atual pelo nome indicado em 'newFieldName'.
	 * @param newName novo nome para o campo.
	 */
	public void swapFieldName(String newFieldName) {
		this.fieldName = newFieldName;
	}

	public ArrayList<Subfield> getSubfieldsOrdered() 
	{
		HashMap<String, ArrayList<Subfield>> subfieldsNames = new HashMap<>();
		
		String subfieldName;
		// TODO os subcampos podem se repetir!!!
		for(Subfield subfield : getSubfields())
		{
			subfieldName = subfield.getSubfieldName();
			if(!subfieldsNames.containsKey(subfieldName))
				subfieldsNames.put(subfieldName, new ArrayList<>());
			subfieldsNames.get(subfieldName).add(subfield);
			
		}
		
		ArrayList<String> subcamposOrdenados = new ArrayList<>();
		for(String nome : subfieldsNames.keySet())
		{
			subcamposOrdenados.add(nome);
		}
		
		Collections.sort(subcamposOrdenados, String.CASE_INSENSITIVE_ORDER);
		
		ArrayList<Subfield> orderedSubfields = new ArrayList<Subfield>();
		for(String key : subcamposOrdenados)
		{
			orderedSubfields.addAll(subfieldsNames.get(key));
		}
		return orderedSubfields;
	}

	public Record getCorrespondingRecord() {
		return correspondingRecord;
	}

	public void setCorrespondingRecord(Record correspondingRecord) {
		this.correspondingRecord = correspondingRecord;
	}

	
	private String getXmlNodeName(String field) 
	{
		if(field.contentEquals("leader"))
				return Constant.leaderDenomination;
		
		if(field.contentEquals("001") || field.contentEquals("003") 
				|| field.contentEquals("005") || field.contentEquals("008"))
			return Constant.controlFieldDenomination;
		
		return Constant.dataFieldDenomination;
	}
}
