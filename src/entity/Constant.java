package entity;

import java.util.ArrayList;

public class Constant {

	//path to input xml
//	public static final String inputFile = "xmlIn/sophia/autoridadesSophia.xml";
	//public static final String inputFile = "xmlIn/ibict2/registrosComCutter2.xml";
	public static final String inputFile2 = "xmlIn/kohaAutoridadesMMFDH.xml";
//	public static final String inputFile2 = "xmlIn/enap2/fazenda_final5-corrigido.xml";
	public static final String inputFile3 = "xmlIn/registrosSNJ/registros_bib_SNJ.xml";
//	public static final String inputFilePath1 = "xmlIn/enap/fazenda_1.xml";
//	public static final String inputFilePath2 = "xmlIn/enap/fazenda_2.xml";
//	public static final String inputCSVFilePath = "xmlIn/enap/exemplares.csv";
	public static final String inputAutoridadeMMFDH = "mmfdh/input/autoridadesMMFDH.xml";
	public static final String inputAutoridadeAtualizadaMMFDH = "mmfdh/input/autoridadesAtualizadas.xml";
	public static final String inputAutoridadeAtualizadaCorpoNameMMFDH = "mmfdh/input/autoridadesCorpoNameAtualizadas.xml";
	public static final String inputAutoridadeAtualizadaCorpoNameMMFDH3 = "mmfdh/input/autoridadesCORPO_NAME3.xml";
	public static final String inputCatalogoMMFDH = "mmfdh/input/catalogoMMFDH.xml";
	public static final String inputCatalogoCorpoNameMMFDH = "mmfdh/input/acervoCorpoNameAtualizado.xml";
	public static final String inputCatalogoMMFDH3 = "mmfdh/input/acervoMMFDH3.xml";
	public static final String inputCorrecoes = "mmfdh/input/correcoesIngrid.xml";
	
	//path to output xml
	//public static final String outputFilePath = "xmlOut/enap/fazenda_final.xml";
//	public static final String outputFilePath = "xmlOut/sophia/autoridades_final.xml";
	public static final String outputFilePath = "xmlIn/ibict2/registrosComCutter3.xml";
	public static final String outputFilePath2 = "xmlIn/registrosSNJ/registros_bib_SNJ3.xml";
	public static final String outputFilePath3 = "xmlIn/enap2/fazenda_final5-008corrigido.xml";
	public static final String outputFilePath4 = "xmlIn/problemaAutoridadesSophia/autoridades_final2.xml";
	public static final String outputFile = "xmlIn/kohaAutoridadesDuplicadas.xml";
	public static final String outputFile2 = "xmlIn/kohaAutoridadesErro.xml";
	public static final String outputFile3 = "xmlIn/kohaAutoridadesCom180.xml";
	public static final String outputFile4 = "xmlIn/kohaAutoridadesTopicTerm.xml";
	public static final String outputAutoridadeMMFDH = "mmfdh/output/";
	
	// modelo 1
//	public static String collectionDenomination = "marc:collection";
//	public static String recordDenomination = "marc:record";
//	public static String leaderDenomination = "marc:leader";
//	public static String controlFieldDenomination = "marc:controlfield";
//	public static String dataFieldDenomination = "marc:datafield";
//	public static String subfieldDenomination = "marc:subfield";
//	public static String fieldTagDenomination = "tag";
//	public static String subfieldTagDenomination = "code";
//	public static String indicator1Denomination = "ind1";
//	public static String indicator2Denomination = "ind2";
	
	
	
	// modelo 2
	public static  String collectionDenomination = "collection";
	public static  String recordDenomination = "record";
	public static  String leaderDenomination = "leader";
	public static  String controlFieldDenomination = "controlfield";
	public static  String dataFieldDenomination = "datafield";
	public static  String subfieldDenomination = "subfield";
	public static  String fieldTagDenomination = "tag";
	public static  String subfieldTagDenomination = "code";
	public static  String indicator1Denomination = "ind1";
	public static  String indicator2Denomination = "ind2";
	
	//Header IBICT
//	public static final String xmlHeader = "<?xml version=\"1.0\" "
//			+ "encoding=\"utf-8\" "
//			+ "standalone=\"no\"?>"
//			+ "<" + collectionDenomination + " xmlns:marc=\"http://www.loc.gov/MARC21/slim\" "
//			+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
//			+ "xsi:schemaLocation=\"http://www.loc.gov/MARC21/slim "
//			+ "http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd\">";
	
	// Header SNJ
//	public static final String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
//			+ "\n<" + collectionDenomination 
//			+ "\n	xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
//			+ "\n	xsi:schemaLocation=\"http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd\""
//			+ "\n	xmlns=\"http://www.loc.gov/MARC21/slim\">\n";
	
	// Header SNJ 2
//	public static String xmlHeader ="<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
//			+ "\n<" + collectionDenomination 
//			+ " xmlns:marc=\"http://www.loc.gov/MARC21/slim\""
//			+ " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd\">";
//	
	// Header ibict
	public static String xmlHeader ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "\n<" + collectionDenomination 
			+ "\n  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
			+ "\n  xsi:schemaLocation=\"http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd\""
			+ "\n  xmlns=\"http://www.loc.gov/MARC21/slim\">\n";
	
	// outras constantes
	public static final String kardexField = "022";
	public static final String csvSeparator = ",";
	
	
	 /**
	  * Convert the xml files to the following pattern:
	  * 
	  */
	
	public static void convertoXMLOutputType(ArrayList<Record> records) {
		collectionDenomination = "collection";
		recordDenomination = "record";
		leaderDenomination = "leader";
		controlFieldDenomination = "controlfield";
		dataFieldDenomination = "datafield";
		subfieldDenomination = "subfield";
		fieldTagDenomination = "tag";
		subfieldTagDenomination = "code";
		indicator1Denomination = "ind1";
		indicator2Denomination = "ind2";
		
		// updates the xml tag in the header
		xmlHeader ="<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
				+ "\n<" + collectionDenomination 
				+ " xmlns:marc=\"http://www.loc.gov/MARC21/slim\""
				+ " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd\">";
		
		for (Record r : records)
		{
			r.getField("marc:leader").get(0).setFieldName(leaderDenomination);
			r.getField("leader").get(0).setXmlNodeName(leaderDenomination);
			for(Field f : r.getFields())
			{
				String xmlNodeName = f.getXmlNodeName();
				if (!xmlNodeName.contentEquals("leader") && !xmlNodeName.contentEquals("marc:leader"))
					switch (xmlNodeName)
					{
					case "marc:controlfield":
						f.setXmlNodeName(controlFieldDenomination);
						break;
					case "marc:datafield":
						f.setXmlNodeName(dataFieldDenomination);
						break;
					default:
						System.err.println("tag não encontrada");
						break;
					}
				
				// troca as tags dos subcampos
				for (Subfield s : f.getSubfields())
				{
					s.setXmlNodeName(subfieldDenomination);
				}
			}
		}
	}

}

