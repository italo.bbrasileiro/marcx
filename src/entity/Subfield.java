package entity;

public class Subfield {
	String subfieldName;
	String value;
	String xmlNodeName;
	Field field;
//TODO variavel para verificar se tem repeticao do subcampo dentro do seu respectivo campo. 	
//	boolean isRepeatable;
	
	public Subfield(String subfieldName, String value, String xmlNodeName) {
		this.subfieldName = subfieldName;
		this.value = value;
		this.xmlNodeName = xmlNodeName;
	}
	
	public Subfield(String subfieldName, String value) {
		this.subfieldName = subfieldName;
		this.value = value;
		this.xmlNodeName = Constant.subfieldDenomination;
	}

	/**
	 * Altera o nome do subcampo 'oldSubfield' para 'newSubfield', dentro
	 * do campo 'field'.
	 * @param field Campo a ter os subcampos alterados.
	 * @param oldSubfield nome do subcampo a ser alterado.
	 * @param newSubfield novo nome do subcampo.
	 */
	public void swapSubfieldName(String newSubfieldName) {
		subfieldName = newSubfieldName;
	}
	
	//---------- getters and setters ------------
	public String getSubfieldName() {
		return subfieldName;
	}

	public void setSubfieldName(String subfieldName) {
		this.subfieldName = subfieldName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}

	public String getXmlNodeName() {
		return xmlNodeName;
	}

	public void setXmlNodeName(String xmlNodeName) {
		this.xmlNodeName = xmlNodeName;
	}
	
	
}
