package entity;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import control.XMLHandler;

public class MARC {

	ArrayList<Record> records;

	public MARC(ArrayList<Record> records) {
		this.records = records;
	}

	public MARC(String filePath) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		Document xmlCatalogo = XMLHandler.readMarcXMLFile(filePath);
		this.records = XMLHandler.instantiateXMLfields(xmlCatalogo);
	}
	
	
	
	/**
	 * Remove a lista de autoridades duplicadas entre os registros do marc atual
	 * e as retorna em uma nova lista.
	 * @param autoridades
	 * @return
	 */
	public ArrayList<Record> cleanDuplicatedAuthorities(String[] camposParaDesconsiderar)
	{
		int totalAutoridades = 0;
		ArrayList<Record> autoridades = this.getRecords();
		ArrayList<Record> autoridadesDuplicadas = new ArrayList<Record>(); // armazena os registros duplicados
		
		// percorre lista de autoridades
		for (int i = 0; i < autoridades.size() - 1; i++) 
		{
			totalAutoridades = autoridades.size();
			if(i % 100 == 0)
				System.out.println(i + " de " + totalAutoridades);
			Record autoridade = autoridades.get(i);
			
			// reitera pela lista de autoridades e busca duplicadas
			for (int j = i + 1; j < totalAutoridades; j++) 
			{
				Record autoridadeComparada = autoridades.get(j);
				
				// verifica se as autoridades sao iguais, primeiro pelo valor do campo principal, depois
				// pelo tipo da autoridade e por ultimo pelo quantidade de campos do registro
				if(autoridade.getAuthorityMainFieldValue().contentEquals(autoridadeComparada.getAuthorityMainFieldValue())
						&& autoridade.getAuthorityType().contentEquals(autoridadeComparada.getAuthorityType())
						&& autoridade.getFields().size() == autoridadeComparada.getFields().size())
				{
					String texto = autoridade.getRecordAsXMLWithoutIndicatedFields(camposParaDesconsiderar);
					String textoComparado = autoridadeComparada.getRecordAsXMLWithoutIndicatedFields(camposParaDesconsiderar);
					
					// se o xml das duas eh identico, remover a autoridade da lista principal e adicionar 
					// na lista de autoridades duplicadas.
					if(texto.contentEquals(textoComparado))
					{
						autoridadesDuplicadas.add(autoridades.remove(j));
						break;
					}
				}
			}
		}
		return autoridadesDuplicadas;
	}
	
	/**
	 * Verifica se o marc atual contem o kardex indicado pelo campo 'kardex'.
	 * @param kardex procurado no registro.
	 * @return true se o registro contem o kardex, false se nao contem.
	 */
	public boolean containsKardex(String kardex)
	{
		for(Record record : records)
		{
			String kardexAtual = record.getKardex().getSubfield("a").get(0).getValue();
			
			if(kardexAtual.contentEquals(kardex))
				return true;
		}
		return false;
	}
	
	public int countRecordsWithField(String field)
	{
		int count = 0;
		
		for(Record record : records)
		{
			if(record.hasField(field))
				count++;
		}
		
		return count;
	}
	
	public int countRecordsWithFieldAndSubfield(String field, String subfield)
	{
		int count = 0;
		
		for(Record record : records)
		{
			if(record.hasField(field))
				for(Field f : record.getField(field))
					if(f.hasSubfield(subfield))
					{
						count++;
						continue;
					}
			else
				continue;
		}
		
		return count;
	}
	
	/**
	 * Searches all records with the indicated 'recordField',
	 * and creates an authority in the corresponding authority field,
	 * with the correct type.
	 * @param recordField
	 * @param authorityCorrespondingField
	 * @return
	 */
	public ArrayList<Record> createAuthoritiesFromField(String recordField, String authorityCorrespondingField)
	{
		ArrayList<Record> authorityRecords = new ArrayList<Record>();
		ArrayList<Record> recordsWithIndicatedField = getRecordWithField(recordField);
		
		if(recordsWithIndicatedField != null && recordsWithIndicatedField.size() > 0)
			for(Record r : recordsWithIndicatedField)
			{
				for(Field f : r.getField(recordField)) {
				Field campo = new Field(authorityCorrespondingField, " ", " ", f.getSubfields(), Constant.dataFieldDenomination);
				ArrayList<Field> fields = new ArrayList<Field>();
				
				fields.add(campo);
				
				
				// add authority type
				Subfield authorityType = new Subfield("a", getAuthorityType(authorityCorrespondingField), Constant.subfieldDenomination);
				ArrayList<Subfield> subfieldAuthorityType = new ArrayList<Subfield>();
				subfieldAuthorityType.add(authorityType);
				Field authority942 = new Field("942", " ", " ", subfieldAuthorityType, Constant.dataFieldDenomination);
				fields.add(authority942);
						
				Record newAuthority = new Record(fields);
				newAuthority.generateLeaderAndControlFields();
						
				authorityRecords.add(newAuthority);
				}
			}
		
		
				
		return authorityRecords;
	}
	
	/*
	 * Verifica o campo utilizado pela autoridade e retorna o tipo correspondente.
	 */
	private String getAuthorityType(String authorityCorrespondingField) {
		switch(authorityCorrespondingField)
		{
		case "110":
			return "CORPO_NAME";
		case "151":
			return "GEOGR_NAME";
		case "111":
			return "MEETI_NAME";
		case "100":
			return "PERSO_NAME";
		case "150":
			return "TOPIC_TERM";
		case "130":
			return "UNIF_TITLE";
			
		}
		return null;
	}
	
	public ArrayList<Record> getRecords() {
		return records;
	}

	/**
	 * Busca o conjunto de registros que apresentam o campo indicado por 'fieldName'.
	 * @param fieldName nome do campo buscado.
	 * @return {@link ArrayList}<{@link Record}> com registros que tem o campo indicado por 'fieldName'.
	 */
	public ArrayList<Record> getRecordWithField(String fieldName)
	{
		ArrayList<Record> records = null;

		for(Record record : this.records)
		{
			ArrayList<Field> fields = record.getField(fieldName);
			if(fields == null)
				continue;
			else
			{
				for(Field field : fields)
				{
					if(field.getFieldName().contentEquals(fieldName))
					{
						if(records == null)
							records = new ArrayList<Record>();
						records.add(record);
						break;
					}
				}
			}
		}
		return records;
	}

	/**
	 * Busca o conjunto de registros que nao apresentam o campo indicado por 'fieldName'.
	 * @param fieldName nome do campo buscado.
	 * @return {@link ArrayList}<{@link Record}> com registros que nao tem o campo indicado por 'fieldName'.
	 */
	public ArrayList<Record> getRecordWithoutField(String fieldName)
	{
		ArrayList<Record> records = null;
		boolean contem;

		for(Record record : this.records)
		{	
			contem = false;
			ArrayList<Field> fields = record.getFields();
			if(fields == null)
				continue;
			else
			{
				for(Field field : fields)
				{
					if(field.getFieldName().contentEquals(fieldName))
					{
						contem = true;
					}
				}
				if(!contem)
				{
					if(records == null)
						records = new ArrayList<Record>();					
					records.add(record);
				}
			}
		}
		return records;
	}

	/**
	 * Busca o conjunto de registros que apresentam o valor indicado por 'value' no
	 * campo indicado por 'fieldName'.
	 * @param fieldName
	 * @param value
	 * @return
	 */
	public ArrayList<Record> getRecordWithFieldValue(String fieldName, String value)
	{
		ArrayList<Record> records = null;

		for(Record record : this.records)
		{
			ArrayList<Field> fields = record.getField(fieldName);
			if(fields == null)
				continue;
			else
			{
				for(Field field : fields)
				{
					if(field.getValue().contentEquals(value))
					{
						if(records == null)
							records = new ArrayList<Record>();
						records.add(record);
					}
				}
			}
		}
		return records;
	}
	
	/**
	 * Busca o conjunto de registros que apresentam o valor indicado por 'value' no
	 * campo indicado por 'fieldName'.
	 * @param fieldName
	 * @param value
	 * @return
	 */
	public static ArrayList<Record> getRecordWithFieldValue(ArrayList<Record> recordList, String fieldName, String value)
	{
		ArrayList<Record> records = null;

		for(Record record : recordList)
		{
			ArrayList<Field> fields = record.getField(fieldName);
			if(fields == null)
				continue;
			else
			{
				for(Field field : fields)
				{
					if(field.getValue().contentEquals(value))
					{
						if(records == null)
							records = new ArrayList<Record>();
						records.add(record);
					}
				}
			}
		}
		return records;
	}
	
	/**
	 * Busca o conjunto de registros que apresentam o valor indicado por 'value' no
	 * campo indicado por 'fieldName'.
	 * @param fieldName
	 * @param value
	 * @return
	 */
	public static ArrayList<Record> getRecordWithFieldAndSubfieldValue(ArrayList<Record> recordList, 
			String fieldName, String subfieldName, String value)
	{
		ArrayList<Record> records = null;

		for(Record record : recordList)
		{
			ArrayList<Field> fields = record.getField(fieldName);
			if(fields == null)
				continue;
			else
			{
				for(Field field : fields)
				{
					if(field.hasSubfield(subfieldName))
						for(Subfield subfield : field.getSubfield(subfieldName))
						{
							if(subfield.getValue().toLowerCase().contentEquals(value.toLowerCase()))
							{
								if(records == null)
									records = new ArrayList<Record>();
								records.add(record);
							}
						}
				}
			}
		}
		return records;
	}
	
	/**
	 * Verifica se o marc atual contem o kardex indicado pelo campo 'kardex'.
	 * @param kardex procurado no registro.
	 * @return true se o registro contem o kardex, false se nao contem.
	 */
	public Record getRecordWithKardex(String kardex)
	{
		for(Record record : records)
		{
			if (record.getKardex() == null)
				continue;
			
			String kardexAtual = record.getKardex().getSubfield("a").get(0).getValue();
//			if(kardexAtual == null)
//				continue;
			
			if(kardexAtual.contentEquals(kardex))
				return record;
		}
		return null;
	}
	
	public boolean hasRecordsWithSameValueInField(ArrayList<Record> records, String fieldName)
	{
		for(int r = 0; r < records.size(); r++)
		{
			ArrayList<Field> field = records.get(r).getField(fieldName);
			
			for(int r2 = r + 1; r2 < records.size() - 1; r2++)
			{
				ArrayList<Field> field2 = records.get(r2).getField(fieldName);
				for(Field f : field)
				{
					for(Field f2 : field2)
					{
						if(f.getValue().contentEquals(f2.getValue()))
						{
							System.out.println("AQUI RS");
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Essa funcao retorna um ArrayList<Record> contendo todos
	 * os registros do marc que apresentam o campo especificado
	 * por 'fieldName'
	 * @param fieldName nome do campo procurado.
	 * @return
	 */
	public ArrayList<Record> recordsWithField(String fieldName)
	{
		ArrayList<Record> rec = null;
		for(Record record : records)
		{
			if(record.hasField(fieldName))
			{
				if(rec == null)
					rec = new ArrayList<Record>();
				rec.add(record);
			}
		}
		return rec;
	}

	/**
	 * Essa funcao retorna um ArrayList<Record> contendo todos
	 * os registros do marc que apresentam o campo especificado
	 * por 'fieldName'
	 * @param fieldName nome do campo procurado.
	 * @return
	 */
	public ArrayList<Record> recordsWithFieldAndSubfield(String fieldName, String subfieldName)
	{
		ArrayList<Record> rec = null;
		for(Record record : records)
		{
			if(record.hasField(fieldName))
			{	
				ArrayList<Field> fields = record.getField(fieldName);

				for(Field field : fields)
				{
					ArrayList<Subfield> subfields = field.getSubfield(subfieldName);

					if(subfields != null)
						for(Subfield subfield : subfields)
						{
							if(field.getFieldName().contentEquals(fieldName)
									&& subfield.getSubfieldName().contentEquals(subfieldName))
							{
								if(rec == null)
									rec = new ArrayList<Record>();
								rec.add(record);
							}
						}
				}
			}
		}
		return rec;
	}

	/**
	 * Essa funcao retorna um ArrayList<Record> contendo todos
	 * os registros do marc que apresentam o campo especificado
	 * por 'fieldName' de valor 'value'.
	 * @param fieldName nome do campo procurado.
	 * @return
	 */
	public ArrayList<Record> recordsWithFieldAndSubfieldValue(String fieldName, String subfieldName, String value)
	{
		ArrayList<Record> rec = null;
		for(Record record : records)
		{
			if(record.hasField(fieldName))
			{	
				ArrayList<Field> fields = record.getField(fieldName);

				for(Field field : fields)
				{
					ArrayList<Subfield> subfields = field.getSubfield(subfieldName);

					for(Subfield subfield : subfields)
					{
						if(field.getFieldName().contentEquals(fieldName)
								&& subfield.getSubfieldName().contentEquals(subfieldName)
								&& subfield.getValue().contentEquals(value))
						{
							if(rec == null)
								rec = new ArrayList<Record>();
							rec.add(record);
						}
					}
				}
			}
		}
		return rec;
	}


	
	/**
	 * Essa funcao retorna um ArrayList<Record> contendo todos
	 * os registros do marc que nao apresentam o campo especificado
	 * por 'fieldName'
	 * @param fieldName nome do campo procurado.
	 * @return
	 */
	public ArrayList<Record> recordsWithoutField(String fieldName)
	{
		ArrayList<Record> rec = null;
		for(Record record : records)
		{
			if(!record.hasField(fieldName))
			{
				if(rec == null)
					rec = new ArrayList<Record>();
				rec.add(record);
			}
		}
		return rec;
	}
	
	/**
	 * Remove o campo especificado por 'fieldName' de todos os registros
	 * presentes no catalogo.
	 * @param fieldName
	 * @return ArrayList<Record> registros sem o campo 'fieldName'.
	 */
	public ArrayList<Record> removeField(String fieldName)
	{
		ArrayList<Record> atual = new ArrayList<Record>();
		
		for(Record record : this.records)
		{
			ArrayList<Field> currentField = record.getField(fieldName);
			if(currentField != null)
			{
				for(Field field : currentField)
				{
					record.removeField(field);
				}
			}
			atual.add(record);
		}
		return atual;
	}
	
	/**
	 * Remove o registro 'record' do marc atual.
	 * @param record
	 * @return
	 */
	public boolean removeRecord(Record record)
	{
		return records.remove(record);
	}

	

	public void setRecords(ArrayList<Record> records) {
		this.records = records;
	}

	//------------------------ Funcoes antigas----------------------
	//	/**
	//	 * Este metodo realiza o merge dos arquivos especificados em 'catalogo' e
	//	 * no 'kardex'. Os campos iguais e com valores diferentes serao duplicados. 
	//	 * @param catalogo
	//	 * @param kardex
	//	 * @throws TransformerException 
	//	 * @throws IOException 
	//	 * @throws SAXException 
	//	 * @throws ParserConfigurationException 
	//	 */
	//	public static void mergeArquivos(String catalogo, String kardex) throws ParserConfigurationException, SAXException, IOException, TransformerException
	//	{
	//		Document xmlCatalogo = readMarcXMLFile(catalogo);
	//		Document xmlKardex = readMarcXMLFile(kardex);
	//
	//		NodeList registrosCatalogo = getAllMarcRecords(xmlCatalogo);
	//		NodeList registrosKardex = getAllMarcRecords(xmlKardex);
	//		int tamanhoCatalogo = registrosCatalogo.getLength();
	//		int tamanhoKardex = registrosKardex.getLength();
	//		int cont022Null = 0;
	//		//		camposExistentes(registrosKardex);
	//
	//		for(int i = 0; i < tamanhoCatalogo; i++)
	//		{
	//			Node record = registrosKardex.item(i);
	//			NodeList camposCatalogo = record.getChildNodes();
	//			Node field022 = getField(camposCatalogo, "022");
	//
	//			if(field022 != null) {
	//				String valor = getSubfieldValue(field022, "a");
	//
	//				if(valor.length() == 4)
	//					System.out.println(valor);
	//			}
	//		}
	//
	//		for(int i = 0; i < tamanhoCatalogo; i++)
	//		{
	//			Node record = registrosCatalogo.item(i);
	//			NodeList camposCatalogo = record.getChildNodes();
	//			Node field022 = getField(camposCatalogo, "022");
	//
	//			// se o registro nao apresenta o campo 022, esta com erro
	//			if(field022 == null)
	//				cont022Null++;
	//			else
	//			{	
	//				//busca o registro no kardex pelo campo 022
	//				Node registroCorrespondenteKardex = buscaRegistroCorrespondente(registrosKardex, field022);
	//
	//				// se nao retorna nenhum registro, ha erro
	//				if(registroCorrespondenteKardex == null)
	//					System.err.println("Nao ha correspondencia com o Kardex");
	//				else
	//				{	
	//					// lista de campos do kardex
	//					NodeList camposKardex = registroCorrespondenteKardex.getChildNodes();
	//					for(int c = 0; c < camposKardex.getLength(); c++)
	//					{
	//						// campo atual do kardex
	//						Node campoKardexAvaliado = camposKardex.item(c);
	//						// se o campo atual nao existe no catalogo, ele sera adicionado
	//						if(!hasField(record, getFieldTag(campoKardexAvaliado)))
	//							insertField(record, campoKardexAvaliado);
	//						else	
	//						{	
	//							// se o campo exite, eh buscada a lista desses campos no registro no catalogo
	//							ArrayList<Node> camposDuplicadosExistentes = getDuplicatedFieldInstances(record, getFieldTag(campoKardexAvaliado));
	//							//se nao existe o campo
	//							if(camposDuplicadosExistentes == null)
	//								System.err.println("Erro: registro nao apresenta o campo indicado");
	//							else
	//							{	
	//
	//								boolean contem = false;
	//								//percorre a lista de campo duplicado
	//								for(int y = 0; y < camposDuplicadosExistentes.size(); y++)
	//								{	
	//									// campo duplicado atual
	//									Node campoCatalogoAtual = camposDuplicadosExistentes.get(y);
	//
	//									//lista de subcampos
	//									NodeList subfieldsCatalogo = campoCatalogoAtual.getChildNodes();
	//
	//									for(int subc = 0; subc < subfieldsCatalogo.getLength(); subc++)
	//									{
	//										Node subCampo = subfieldsCatalogo.item(subc);
	//
	//										//										if(isSubfieldRedundantInField(field, subField))
	//									}
	//								}
	//							}
	//						}
	//					}
	//				}
	//			}
	//		}
	//		System.out.println("Total de 022 vazios: " + cont022Null);
	//	}

}
