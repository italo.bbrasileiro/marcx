package boundary.cgti;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import control.XMLHandler;
import entity.Constant;
import entity.Field;
import entity.Record;
import entity.Subfield;

public class Main 
{
	static int cont001 = 0;
	static boolean isFirstAuthor = true;
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		
		String folderPath = "cgti/XML_OMP/", filePath, outputPath = "cgti/resultado.xml";
		File folder = new File(folderPath);
		File[] listOfFiles = folder.listFiles();

		ArrayList<Record> records = new ArrayList<>();
		
		for (int i = 0; i < listOfFiles.length; i++) 
			  if (listOfFiles[i].isFile())
			  {
				  filePath = folderPath + listOfFiles[i].getName();
				  records.addAll(getRecordsFromFile(filePath));
			  }
		
		System.out.println(records.size() + " novos registros criados em " + outputPath);
		XMLHandler.generateXMLFile(records, outputPath);
	}
	
	private static ArrayList<Record> getRecordsFromFile(String filePath) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		Document xmlCatalogo = XMLHandler.readMarcXMLFile(filePath);
		ArrayList<Record> records = instantiateXMLfields(xmlCatalogo);
		return records;
	}

	public static ArrayList<Record> instantiateXMLfields(Document xml)
	{
		ArrayList<Record> recordsArray = new ArrayList<Record>();
		NodeList records = xml.getElementsByTagName("monograph");

		for(int r = 0; r < records.getLength(); r++)
			recordsArray.add(buildRecord(records.item(r)));
		
		return recordsArray;
	}

	private static Record buildRecord(Node originalRecord) 
	{
		NodeList fields = originalRecord.getChildNodes();
		ArrayList<Field> recordFields = new ArrayList<>();
		Record record = new Record(recordFields);
		
		for(int f = 0; f < fields.getLength(); f++)
			createCurrentFields(record, fields.item(f));
		
		addField008(record);
		addMandatoryField(record);

		return record;
	}
	
	private static void addField008(Record record) 
	{
		String year = record.getField("260").get(0).getSubfield("c").get(0).getValue();
		String value = "      s" + year + "    bl-    fq       0 0 por d";
		record.addField(new Field("008", value, Constant.controlFieldDenomination));
	}

	private static void addField260(Record record) 
	{
		ArrayList<Subfield> subfields = new ArrayList<>();
		
		subfields.add(new Subfield("a", "Brasília"));
		subfields.add(new Subfield("b", "Ibict"));
		
		record.addField(new Field("260", "", "", subfields, Constant.dataFieldDenomination));
		
	}

	private static void createCurrentFields(Record record, Node item) 
	{
		String nodeName, fieldContent;
		
		nodeName = item.getNodeName().toString();
		if (nodeName.contentEquals("#text"))
			return;
		
		fieldContent = item.getTextContent();
		
		if(nodeName.contentEquals("authors"))
			addAuthors(record, item);
		else if (nodeName.contentEquals("publication"))
			addSeriesDetail(record, item.getAttributes());
		else
			createFieldFromItem(record, nodeName, fieldContent);
		
		if(item.hasChildNodes())
		{
			NodeList newItems = item.getChildNodes();
			for(int f = 0; f < newItems.getLength(); f++)
				createCurrentFields(record, newItems.item(f));
		}
	}

	private static void addSeriesDetail(Record record, NamedNodeMap attributes) 
	{
		Node seriesNode = attributes.getNamedItem("series");
		
		if (seriesNode == null)
			return;
		
		String series = seriesNode.getNodeValue();
		fillField(record, "090", "", "", "a", series);
	}

	private static void addAuthors(Record record, Node authors) 
	{
		NodeList fields = authors.getChildNodes();
		
		for(int f = 0; f < fields.getLength(); f++)
			addAuthor(record, fields.item(f));
		
		isFirstAuthor = true;
	}

	private static void addAuthor(Record record, Node item) 
	{
		String nodeName;
		nodeName = item.getNodeName().toString();
		NodeList details;
		
		if(nodeName.contentEquals("author"))
		{
			details = item.getChildNodes();
			
			record.addField(addAuthorDetails(isFirstAuthor, details));
		}
	}

	private static Field addAuthorDetails(boolean firstAuthor, NodeList details) 
	{
		String nodeName, fieldContent;
		Node detail;
		String givenName = null, familyName = null, 
				affiliation = null;
		Field field;
		
		for(int i = 0; i < details.getLength(); i++)
		{
			detail = details.item(i);
			nodeName = detail.getNodeName().toString();
			fieldContent = detail.getTextContent();
			
			switch(nodeName)
			{
			case "givenname":
				givenName = fieldContent;
				break;
			case "familyname":
				familyName = fieldContent;
				break;
			case "affiliation":
				affiliation = fieldContent;
				break;
			}
		}
		
		ArrayList<Subfield> subfields = new ArrayList<>();
		
		if(familyName != null && givenName != null)
			subfields.add(new Subfield("a", familyName + ", " + givenName));
		if(affiliation != null)
			subfields.add(new Subfield("g", affiliation));

		if(isFirstAuthor)
		{
			field = new Field("100", "1", "#", subfields , Constant.dataFieldDenomination);
			isFirstAuthor = false;
		}else
		{
			field = new Field("700", "1", "#", subfields , Constant.dataFieldDenomination);
		}
		
		return field;
	}

	private static ArrayList<Field> createFieldFromItem(Record record, String nodeName, String fieldContent) {
		ArrayList<Field> fields = new ArrayList<>();
		
		switch(nodeName)
		{
		case "id":
			String link = "http://labcotec.ibict.br/omp/index.php/edcotec/catalog/book/" + fieldContent;
			if(!hasThisValueIn856(record, link))
				fillField856(record, link);
			break;
		case "title":
			fillField(record, "245", "", "", "a", fieldContent);
			break;
		case "abstract":
			fillField(record, "520", "", "", "a", fieldContent);
			break;
		case "copyrightYear":
			addField260(record);
			fillField(record, "260", "", "", "c", fieldContent);
			break;
		}
		
		return fields;
	}

//	private static void setFamilyName(Record record, String familyName) 
//	{
//		String name = record.getField("100").get(0).getSubfield("a").get(0).getValue();
//		name = familyName + ", " + name;
//		record.getField("100").get(0).getSubfield("a").get(0).setValue(name);
//	}

	private static boolean hasThisValueIn856(Record record, String link) 
	{
		String value;
		
		if(!record.hasField("856"))
			return false;
		
		for(Field field856 : record.getField("856"))
		{
			value = field856.getSubfield("u").get(0).getValue();
			if(value.contentEquals(link))
				return true;
		}
		
		return false;
	}

	/**
	 * Busca dentre a lista de atributos do campo o valor inserido
	 * no campo 'tag'.
	 * @param field Campo no qual eh feito a busca da tag
	 * @return String contendo a tag do campo ou null
	 */
	public static String getFieldTag(Node field) {

		//se 'field' nao tem atributos, retorna null
		if(!field.hasAttributes()) {
			return null;
		}else {
			NamedNodeMap atributos = field.getAttributes();
			for(int a = 0; a < atributos.getLength(); a++) {
				Node atributo = atributos.item(a);
				if(atributo.getNodeName().contains("tag"))
					return atributo.getTextContent();
			}
		}
		return null;
	}
	
	/**
	 * Busca dentre a lista de atributos do campo o valor inserido
	 * no campo 'Constant.recordSubfieldDenomination()' do XML.
	 * @param field Campo no qual eh feito a busca da tag
	 * @return String contendo a tag do campo ou null
	 */
	public static String getXMLSubfieldTag(Node subfield) {
		NamedNodeMap atributos = subfield.getAttributes();

		if(atributos == null)
			return null;

		Node atributo = getXMLAttribute(atributos, Constant.subfieldTagDenomination);
		if(atributo != null)
			return atributo.getTextContent();

		return null;
	}
	
	/**
	 * Busca um atributo dentro de uma lista de atributos.
	 * Recebe uma lista 'NamedNodeMap' de atributos e a String 'at' correspondente ao atributo procurado.
	 * @param atributos Lista de atributos
	 * @param at atributo procurado
	 * @return Node atributo encontrado ou null
	 */
	public static Node getXMLAttribute(NamedNodeMap atributos, String at) {

		//percorre a lista de atributos
		for(int a = 0; a <atributos.getLength(); a++) {
			//atributo = item atual
			Node atributo = atributos.item(a);
			if(atributo.getNodeName().contains(at)) {
				return atributo;
			}
		}
		return null;
	}
	
	private static void fillField(Record record, String fieldName, String ind1, String ind2, String subfield,
			String value) 
	{
		Field destinationField;
		destinationField = defineFieldForValue(record, fieldName, ind1, ind2);
		destinationField.addSubfield(new Subfield(subfield, value));
		
	}
	
	private static void fillField856(Record record,	String value) 
	{
		Field field = new Field("856", "", "", new ArrayList<>(), Constant.dataFieldDenomination);
		record.addField(field);
		field.addSubfield(new Subfield("u", value));
	}

	private static Field defineFieldForValue(Record record, String fieldName, String ind1, String ind2) 
	{
		if(record.hasField(fieldName))
			return record.getField(fieldName).get(0);
		else
		{
			Field field = new Field(fieldName, ind1, ind2, new ArrayList<>(), Constant.dataFieldDenomination);
			record.addField(field);
			return field;
		}
	}
	
	private static void addMandatoryField(Record record) 
	{
		addMaterialField(record);
//		addBranchCode(record);
		addField001(record);
		record.generateLeaderAnd005Field();
		
	}

	private static void addField001(Record record) {
		record.addField(new Field("001", Integer.toString(cont001++), Constant.controlFieldDenomination));
	}
	
	private static void addMaterialField(Record record) 
	{
		record.addField(new Field("942", "c", "LIVRO", Constant.dataFieldDenomination));
		
	}
	
	private static void addBranchCode(Record record) 
	{
		ArrayList<Field> field952 = record.getField("952");
		ArrayList<Subfield> subfields;
		
		if(field952 == null)
		{
			subfields = new ArrayList<>();
			record.addField(new Field("952", "", "", subfields, Constant.dataFieldDenomination));
			
		}
		
		for(Field field : field952)
		{
			field.addSubfield(new Subfield("a", "MHN"));
			field.addSubfield(new Subfield("b", "MHN"));
		}
	}
	
}
