package boundary.ibram;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import control.CSVController;
import control.XMLHandler;
import entity.Constant;
import entity.Field;
import entity.Record;
import entity.Subfield;

public class Republica {

	
	public static void migrate() throws Exception 
	{
		ArrayList<Record> records = new ArrayList<>();
		String outputPath = "ibram/republica/output/acervo.xml";
		
		for(String filepath : getFiles())
			records.addAll(createRecordsFromFile(filepath));
		
		System.out.println(records.size() + " novos registros criados em " + outputPath);
		XMLHandler.generateXMLFile(records, outputPath);
	}

	static int lineNumber = 0;
	
	private static ArrayList<Record> createRecordsFromFile(String filepath) throws Exception 
	{
		ArrayList<String[]> csvContent = CSVController.readCSV(filepath, "\t");
		ArrayList<Record> records = new ArrayList<>();
		
		for(lineNumber = 1; lineNumber < csvContent.size(); lineNumber++)
			records.add(createRecord(csvContent.get(lineNumber)));
		
		return records;
	}
	
	private static Record createRecord(String[] fieldList) 
	{
		Record record = instantiateFields(fieldList);
		addBranchCode(record);
		
		return record;
	}

	private static Record instantiateFields(String[] fieldList) 
	{
		ArrayList<Field> fields = new ArrayList<>();
		Record record = new Record(fields);
		record.generateLeaderAndControlFields();
		
		insertField(record, "035", "z", fieldList[0]); // . Ref
		// insertField(record, "902", "a", fieldList[1]); // MFN - não migrar
		insertField(record, "245", "a", fieldList[2]); // titulo
		insertField(record, "ldr", "07", fieldList[3]); // nivel bibliografico
		insertField(record, "942", "c", fieldList[4]); // tipo
		insertField(record, "obra rara", "", fieldList[5]); // é obra rara
		insertField(record, "533", "a", fieldList[6]); // é fac simile
		insertField(record, "300", "b", fieldList[7]); // possui ilustracao?
		if(fieldList.length >= 43)
			insertField(record, "952", "i", fieldList[42]); // registro
		insertField(record, "952", "z", fieldList[8]); // possui ex-libris
		insertField(record, "300", "b", fieldList[9]); // possui mapa
		insertField(record, "300", "b", fieldList[10]); // possui planta
		insertField(record, "952", "z2", fieldList[11]); // possui dedicatoria
		insertField(record, "952", "8", fieldList[12]); // coleção
		insertField(record, "100", "a", fieldList[13].toLowerCase()); // autores
		if(fieldList.length >= 46)
			insertField(record, "245", "c", fieldList[14].toLowerCase()); // responsabilidade
		insertField(record, "942", "k", fieldList[16]); // chamada
		insertField(record, "008", "35-37", fieldList[17]); // idioma
		insertField(record, "500", "a", fieldList[18]); // referencia
//		insertField(record, "260", "a", fieldList[19]); // local
//		insertField(record, "260", "c", fieldList[20]); // data
		insertField(record, "250", "a", fieldList[21]); // edição
		insertField(record, "260", "abc", fieldList[22]); // imprenta
		insertField(record, "490", "a", fieldList[23]); // série
		insertField(record, "300", "a", fieldList[24]); // colação
		insertField(record, "773", "g", fieldList[25]); // volume
		insertField(record, "773", "g2", fieldList[26]); // número
		insertField(record, "300", "a2", fieldList[27]); // qtde. volumes
		insertField(record, "300", "a3", fieldList[28]); // paginação
		insertField(record, "111", "a", fieldList[29]); // Nome, Número e Cidade do Congresso: 
		insertField(record, "111", "d", fieldList[30]); // data do congresso
		insertField(record, "502", "c", fieldList[31]); // instituição
		insertField(record, "502", "b", fieldList[32]); // grau
		insertField(record, "500", "a", fieldList[33]); // notas
		
		insertField(record, "600", "a", fieldList[34]); // Assuntos (Pessoas Físicas/Entidades Coletivas)
		insertField(record, "651", "a", fieldList[35]); // assuntos locais
		if(fieldList.length >= 44)
			insertField(record, "650", "a", fieldList[43]); // assuntos termos // TODO tratar separação de termos
//		insertField(record, "915", "a", fieldList[37]); // localização
		insertField(record, "599", "a", fieldList[38]); // técnico
		insertField(record, "599", "b", fieldList[39]); // data técnica
		insertField(record, "598", "a", fieldList[40]); // digitador
		insertField(record, "598", "b", fieldList[41]); // data da digitação
		
		return record;
	}



	private static void insertField(Record record, String fieldName, String subfieldName, String value) 
	{
		ArrayList<Field> fields = record.getFields();
		Field tempField;
		value = value.trim();
		String[] nomes;
		String temp;
		
		if(!fieldName.contentEquals("942") 
				&& (value.isBlank() || value.isEmpty() || (value == null)))
			return;
		
		switch(fieldName + "$" + subfieldName) // correct exceptions
		{
		case "obra rara$":
			if(value.trim().contentEquals("Sim"))
				getCorrespondingField(fields, "942").getSubfield("c").get(0).setValue("OBRA");
			break;
		case "ldr$07":
			setBibliographicLevelOnLeader(record, value);
			break;
		case "008$35-37":
			updateField008(record, value.replaceAll("Idiomas[ ]*", "").trim());
			break;
		case "100$a":
			nomes = value.replaceAll("nome[ ]+função[ ]*", "").trim().split("   ");
			fields.add(generateField(fieldName, subfieldName, nomes[0]));
			for(int n = 1; n < nomes.length; n++)
				fields.add(generateField("700", subfieldName, nomes[n]));
			break;
		case "111$a":
			getCorrespondingField(fields, "942").getSubfield("c").get(0).setValue("CON");
			fields.add(generateField(fieldName, subfieldName, value));
			break;
		case "111$d":
			tempField = getCorrespondingField(fields, "111");
			tempField.addSubfield(new Subfield(subfieldName, value));
			break;
		case "245$c":
			temp = value.replaceAll("nome[ ]+função[ ]*", "").trim();
			tempField = getCorrespondingField(fields, "245");
			tempField.addSubfield(new Subfield(subfieldName, temp));
			break;
		case "260$b":
			tempField = getCorrespondingField(fields, "260");
			tempField.addSubfield(new Subfield(subfieldName, value));
			break;
		case "260$c":
			tempField = getCorrespondingField(fields, "260");
			tempField.addSubfield(new Subfield(subfieldName, value));
			break;
		case "260$abc":
			tempField = getCorrespondingField(fields, "260");
			if(tempField == null)
			{
				tempField = new Field("260", Constant.dataFieldDenomination);
				record.addField(tempField);
			}
			editField260info(tempField, value);
			break;
		case "300$a":
			value = value.replaceAll("il.", "").trim();
			if(!value.isBlank())
			{
				tempField = getCorrespondingField(fields, "300");
				tempField.addSubfield(new Subfield(subfieldName, value));
			}
			break;
		case "300$a2":
			tempField = getCorrespondingField(fields, "300");
			addInformationOn300$a(tempField, value);
			break;
		case "300$a3":
			tempField = getCorrespondingField(fields, "300");
			addInformationOn300$a2(tempField, value);
			break;
		case "300$b":
			if(value.contentEquals("Sim"))
				fields.add(generateField(fieldName, subfieldName, "il."));
			break;
		case "533$a":
			if(value.contentEquals("Sim"))
				fields.add(generateField(fieldName, subfieldName, "É fac-símile"));
			break;
		case "598$b":
			tempField = getCorrespondingField(fields, "598");
			tempField.addSubfield(new Subfield(subfieldName, value));
			break;
		case "599$b":
			tempField = getCorrespondingField(fields, "599");
			tempField.addSubfield(new Subfield(subfieldName, value));
			break;
		case "600$a":
			value = value.replaceAll("Nome[ ]+", "").trim();
			fields.add(generateAuthorityField(fieldName, value));
			break;
		case "650$a":
			for(String content : value.split("-"))
				fields.add(generateField(fieldName, subfieldName, content.trim()));
			break;
		case "651$a":
			value = value.replaceAll("Local[ ]+", "").trim();
			fields.add(generateField(fieldName, subfieldName, value));
			break;
		case "773$g":
			fields.add(generateField(fieldName, subfieldName, "v. " + value));
			break;
		case "773$g2":
			tempField = getCorrespondingField(fields, "773");
			updateField773withNumber(tempField, value);
			break;
		case "942$c":
			fields.add(generateField(fieldName, subfieldName, getMaterialType(value)));
			break;
		case "942$k":
			tempField = getCorrespondingField(fields, "942");
			tempField.addSubfield(new Subfield(subfieldName, value));
			break;
		case "952$i":
			for(String tombo : value.split("-"))
				fields.add(generateField(fieldName, subfieldName, tombo));
			break;
		case "952$8":
			if(!record.hasField("952"))
				fields.add(generateField(fieldName, subfieldName, value));
			else
				for(Field field : record.getField("952"))
					field.addSubfield(new Subfield("8", value));
			break;
		case "952$z":
			if(value.contentEquals("Sim"))
			{
				if(!record.hasField("952"))
					fields.add(generateField(fieldName, subfieldName, "Possui ex-libris"));
				else
					for(Field field : record.getField("952"))
						field.addSubfield(new Subfield("z", "Possui ex-libris"));
			}
			break;
		case "952$z2":
			if(value.contentEquals("Sim"))
			{
				if(!record.hasField("952"))
					fields.add(generateField(fieldName, subfieldName, "Possui dedicatória"));
				else
					for(Field field : record.getField("952"))
						field.addSubfield(new Subfield("z", "Possui dedicatória"));
			}
			break;
		default:
			fields.add(generateField(fieldName, subfieldName, value));
			break;
		}
		
	}
	
	
	private static Field generateAuthorityField(String fieldName, String value) 
	{
		Field field;
		if(value.contains(","))
			field = new Field("600", "a", value, Constant.dataFieldDenomination);
		else
			field = new Field("610", "a", value, Constant.dataFieldDenomination);
		return field;
	}

	private static void addInformationOn300$a(Field field300, String value) 
	{
		String content;
		
		if(field300.hasSubfield("a"))
		{
			content = field300.getSubfield("a").get(0).getValue();
			field300.getSubfield("a").get(0).setValue(content + ", " + value);
		} else 
			field300.addSubfield(new Subfield("a", value));
	}
	
	private static void addInformationOn300$a2(Field field300, String value) 
	{
		String content;
		
		if(field300.hasSubfield("a"))
		{
			content = field300.getSubfield("a").get(0).getValue();
			field300.getSubfield("a").get(0).setValue(content + ", p. " + value);
		} else 
			field300.addSubfield(new Subfield("a", "p. " + value));
	}

	private static void updateField773withNumber(Field field773, String value)
	{
		Subfield subG;
		
		if(field773.hasSubfield("g"))
		{
			subG = field773.getSubfield("g").get(0);
			subG.setValue(subG.getValue() + ", n. " + value);
		} else
			field773.addSubfield(new Subfield("g", "n. " + value));
		
	}

	private static void editField260info(Field field260, String value) 
	{
		
		for(String multiple : value.split(";"))
		{
			if(multiple.contains(":"))
			{
				String[] split = multiple.split(":");
				field260.addSubfield(new Subfield("a", split[0]));
				if(split[1].contains(","))
				{
					field260.addSubfield(new Subfield("b", split[1].split(",")[0].trim()));
					field260.addSubfield(new Subfield("c", split[1].split(",")[1].trim()));					
				} else
					field260.addSubfield(new Subfield("b", split[1].trim()));
			} else
			{
				if(multiple.contains(","))
				{
					field260.addSubfield(new Subfield("a", multiple.split(",")[0].trim()));
					field260.addSubfield(new Subfield("c", multiple.split(",")[1].trim()));
				}
			}
			
		}
			
			
		
	}

	private static void setBibliographicLevelOnLeader(Record record, String value) 
	{
		Field leader = record.getLeaderField();
		String leaderContent = leader.getValue();
		
		leader.setValue(leaderContent.substring(0, 7) + getCorrespondence(value.trim()) 
			+ leaderContent.substring(8, leaderContent.length()));
		
		
		
	}

	private static String getCorrespondence(String value) 
	{
		switch(value)
		{
		case "Monografia no todo": return "m";
		case "Parte de monografia": return "a";
		case "Parte de publicação periódica": return "b";
		case "Documento de evento": return "m";
		case "Imagem em movimento": return "m";
		case "Documento sonoro": return "m";
		case "Documento eletrônico": return "m";
		case "Publicação periódica no todo": return "s";
		default: return "m";
		}
	}

	
	private static void updateField008(Record record, String value) 
	{
		if(value.contentEquals("Português"))
		{
			record.editField008("por", "35-37");
		}else if(value.contentEquals("Francês"))
		{
			record.editField008("fre", "35-37");
		}else if(value.contentEquals("Inglês"))
		{
			record.editField008("eng", "35-37");
		}else if(value.contentEquals("Espanhol"))
		{
			record.editField008("esp", "35-37");
		}else if(value.contentEquals("Italiano"))
		{
			record.editField008("ita", "35-37");
		}else if(value.contentEquals("Alemão"))
		{
			record.editField008("ale", "35-37");
		}else if(value.contentEquals("Latim"))
		{
			record.editField008("lat", "35-37");
		}else if(value.contains(" "))
		{
			record.editField008("mul", "35-37");
		}else if(value.contentEquals("Esperanto"))
		{
			record.editField008("esp", "35-37");
		}else
		{
			System.err.println("Idioma nao reconhecido:" + value);
		}
	}

	private static Field getCorrespondingField(ArrayList<Field> fields, String fieldName) 
	{
		for(Field field : fields)
			if(field.getFieldName().contentEquals(fieldName))
				return field;
		
		Field field = new Field(fieldName, Constant.dataFieldDenomination);
		fields.add(field);
		
		return field;
	}


	private static Field generateField(String field, String subfield, String value) 
	{	return new Field(field, subfield, value.trim(), Constant.dataFieldDenomination);	}


	private static String getMaterialType(String valor) 
	{
		String material = "";
		
		switch(valor.trim()) 
		{
		case "Livro": 
			material = "LIV";
			break;
		case "Folheto": 
			material = "FOL";
			break;
		case "Coleção": 
			material = "MX";
			break;
		case "Artigo de Periódico": 
			material = "PER";
			break;
		case "Periódico": 
			material = "PER";
			break;
		case "Tese": 
			material = "TD";
			break;
		case "Anais de congresso": 
			material = "TD";
			break;
		case "CD": 
			material = "CF";
			break;
		case "CD-ROM": 
			material = "CF";
			break;
		case "DVD": 
			material = "CF";
			break;
		case "Vídeo": 
			material = "VM";
			break;
		case "Disquete": 
			material = "CF";
			break;
		case "Capítulo de livro": 
			material = "LIV";
			break;
		case "": 
			material = "LIV";
			break;
		default:
			System.err.println("Material " + valor + " não criado.");
			break;
		}
		return material;
	}


	private static ArrayList<String> getFiles() 
	{
		ArrayList<String> filePaths = new ArrayList<>();
		
		filePaths.add("ibram/republica/input/file1parsed.csv");
		filePaths.add("ibram/republica/input/file2parsed.csv");
		filePaths.add("ibram/republica/input/file3parsed.csv");
		filePaths.add("ibram/republica/input/file4parsed.csv");
		filePaths.add("ibram/republica/input/file5parsed.csv");
		filePaths.add("ibram/republica/input/file6parsed.csv");
		filePaths.add("ibram/republica/input/file7parsed.csv");
		
		return filePaths;
	}

	
	private static void addBranchCode(Record record) 
	{
		ArrayList<Field> field952 = record.getField("952");
		
		if(field952 == null)
			record.addField(new Field("952", "", "", new ArrayList<Subfield>(), Constant.dataFieldDenomination));
		
		for(Field field : record.getField("952"))
		{
			field.addSubfield(new Subfield("a", "MR"));
			field.addSubfield(new Subfield("b", "MR"));
		}
	}
}
