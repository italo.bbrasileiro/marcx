package boundary.ibram;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import control.XMLHandler;
import entity.MARC;
import entity.Record;

public class CastroMaya 
{
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		ArrayList<Record> acervoCM = new MARC("ibram/CENEDOM/marcCastroMaya/temp/koha(2).xml").getRecords();
		
		for (Record record : acervoCM)
			System.out.println(record.getField("999").get(0).getSubfield("c").get(0).getValue());
		
	}
	public static void migrate() throws ParserConfigurationException, SAXException, IOException, TransformerException {
		ArrayList<Record> acervoCM, allRecords = new MARC("ibram/CENEDOM/marcCastroMaya/castroMaya1.xml").getRecords();
		allRecords.addAll(new MARC("ibram/CENEDOM/marcCastroMaya/castroMaya2.xml").getRecords());
		
		acervoCM = getRecordsCastroMaya(allRecords);
		
		String outputPath = "ibram/CENEDOM/ExtractCastroMayaOriginal.xml";
		System.out.println(acervoCM.size() + " novos registros criados em " + outputPath);
		XMLHandler.generateXMLFile(acervoCM, outputPath);
		
	}
	
	public static void changeRecords() throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
//		String[] allFiles = {"Ibram1.xml", "Ibram2.xml", "Ibram3.xml", "Ibram4.xml"};
//		String outputPath = "ibram/IbramAllClean.xml";
//		ArrayList<Record> allRecords = getRecordsCastroMaya(allRecords);
		
//		removeField999(allFiles, allRecords);
//		searchMissingBarcodes();
//		verifyLibraries();
		
		
//		System.out.println(allRecords.size() + " novos registros criados em " + outputPath);
//		XMLHandler.generateXMLFile(allRecords, outputPath);
	}

	private static void removeField999(String[] allFiles, ArrayList<Record> allRecords)
			throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		for(String file : allFiles)
			for(Record record : new MARC("ibram/" + file).getRecords())
			{
				record.removeAllOccurrencesOfField("999");
				allRecords.add(record);
			}
	}
	
	private static ArrayList<Record> getAllRecords(String[] allFiles, ArrayList<Record> allRecords)
			throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		for(String file : allFiles)
			for(Record record : new MARC("ibram/" + file).getRecords())
			{
				allRecords.add(record);
			}
		return allRecords;
	}
	
	private static void verifyLibraries() throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		ArrayList<Record> acervoInconfidencia = new ArrayList<>(), noLibrary = new ArrayList<>();
		acervoInconfidencia.addAll(new MARC("ibram/BelasArtes/belasArtes.xml").getRecords());
//		acervoInconfidencia.addAll(new MARC("ibram/CENEDOM/marcCastroMaya/castroMaya2.xml").getRecords());
		String library;
		
		for(Record record : acervoInconfidencia)
		{
			if(!record.hasAnyOccurrenceOfFieldAndSubfield("040", "a"))
			{
				noLibrary.add(record);
				continue;
			}
				
			library = record.getField("040").get(0).getSubfield("a").get(0).getValue();
			
			if(!library.contentEquals("MCM") && !library.contentEquals("MNBA"))
				System.out.println(library);
		}
			
		
		XMLHandler.generateXMLFile(noLibrary, "ibram/BelasArtes/semBiblioteca.xml" );
	}
	
	private static void searchMissingBarcodes(ArrayList<Record> records) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		ArrayList<Record> acervoCastroMaya = getRecordsCastroMayaOld(), 
				acervoIbram = getRecordsNoBarcode(records),
				foundRecords = new ArrayList<>(), foundRecordsIbram =  new ArrayList<>();
		
		
		String titleCM, titleCenedom;
		int count1 = acervoCastroMaya.size(), count = 0;
				
		for(Record recordCM : acervoCastroMaya)
		{
			count++;
			if(count % 100 == 0)
				System.out.println(count + "/" + count1);
			
			for(Record recordIbram : acervoIbram)
			{
				if (!recordIbram.hasAnyOccurrenceOfFieldAndSubfield("245", "a"))
					continue;
				
				if (!recordCM.hasAnyOccurrenceOfFieldAndSubfield("245", "a"))
					continue;
				
				titleCM = recordCM.getTitle();
				titleCenedom = recordIbram.getTitle();
				
				if(titleCenedom.contentEquals(titleCM))
				{
					foundRecords.add(recordCM);
					foundRecordsIbram.add(recordIbram);
				}
			}
		}
		
		XMLHandler.generateXMLFile(foundRecords, "ibram/CENEDOM/marcCastroMaya/correspondentesCastroMaya.xml" );
		XMLHandler.generateXMLFile(foundRecordsIbram, "ibram/CENEDOM/marcCastroMaya/correspondentesIbram.xml" );
	}

	private static ArrayList<Record> getRecordsCastroMaya(ArrayList<Record> records) 
	{
		ArrayList<Record> recordsCM = new ArrayList<>();
		String library;
		
		for(Record record : records)
		{
			if(!record.hasAnyOccurrenceOfFieldAndSubfield("952", "a"))
				continue;
			
			library = record.getField("952").get(0).getSubfield("a").get(0).getValue();
			if(library.contentEquals("MCM"))
				recordsCM.add(record);
		}
		return recordsCM;
	}

	private static ArrayList<Record> getRecordsCastroMayaOld() throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		ArrayList<Record> acervoCastroMaya = new ArrayList<>(); 
		acervoCastroMaya.addAll(new MARC("ibram/CENEDOM/marcCastroMaya/castroMaya1.xml").getRecords());
		acervoCastroMaya.addAll(new MARC("ibram/CENEDOM/marcCastroMaya/castroMaya2.xml").getRecords());
		return acervoCastroMaya;
	}
	
	private static ArrayList<Record> getRecordsNoBarcode(ArrayList<Record> records) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		ArrayList<Record> noBarcode = new ArrayList<>(); 
		
		for(Record record : records)
			if(!record.hasAnyOccurrenceOfFieldAndSubfield("952", "p"))
				noBarcode.add(record);
		
		return noBarcode;
	}


	
	

}
