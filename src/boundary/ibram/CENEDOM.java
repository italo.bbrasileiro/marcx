package boundary.ibram;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import control.XMLHandler;
import entity.Constant;
import entity.Field;
import entity.MARC;
import entity.Record;

public class CENEDOM 
{
	public static void migrate() throws Exception 
	{
		MARC marc = new MARC("ibram/CENEDOM/original.xml");
		ArrayList<Record> records = marc.getRecords(), noBarcode = new ArrayList<>();
		int cont001 = 1;
		
		for(Record record : records)
		{
			record.removeAllOccurrencesOfField("999");
			checkBranches(record);
			record.addField(new Field("001", "" + cont001++, Constant.controlFieldDenomination));
		}
		
		noBarcode = getRecordsWithoutBarcode(records);
		
		String outputPath = "ibram/CENEDOM/Ibram.xml", noBarcodeOutput = "ibram/CENEDOM/noBarcode.xml";
		System.out.println(records.size() + " novos registros criados em " + outputPath);
		XMLHandler.generateXMLFile(records, outputPath);
		XMLHandler.generateXMLFile(noBarcode, noBarcodeOutput);
	}
	


	private static ArrayList<Record> getRecordsWithoutBarcode(ArrayList<Record> allRecords) 
	{
		ArrayList<Record> noBarcode = new ArrayList<>();
		
		for(Record record : allRecords)
			separateRecordWithoutBarcode(noBarcode, record);
		
		allRecords.removeAll(noBarcode);
		
		return noBarcode;
	}

	private static void separateRecordWithoutBarcode(ArrayList<Record> noBarcode, Record record) 
	{
		String barcode;
		for(Field field952 : record.getField("952"))
		{
			barcode = getBarcode(field952);
			
			if(barcode == null)
				noBarcode.add(record);
		}
	}
	
	private static String getBarcode(Field field952) 
	{
		if(field952.hasSubfield("p"))
			return field952.getSubfield("p").get(0).getValue().trim();

		return null;
	}


	private static void checkBranches(Record record) 
	{
		for(Field field952 : record.getField("952"))
		{
			if(field952 != null)
				branchesAreIncluded(field952);
			else
				System.out.println(record.getField("245").get(0).getSubfield("a").get(0).getValue() + " nao possui exemplares!");
		}
		
	}

	private static void branchesAreIncluded(Field field952) 
	{
		if(!hasBranch(field952))
			return;
		
		String branchA = field952.getSubfield("a").get(0).getValue(),
			branchB = field952.getSubfield("b").get(0).getValue();
		
		printIfNotIncluded(branchA);
		printIfNotIncluded(branchB);
		
	}

	private static boolean hasBranch(Field field952) {
		if(!field952.hasSubfield("a") || !field952.hasSubfield("b"))
		{
			String subfield9 = field952.getSubfield("9").get(0).getValue();
			System.out.println("Exemplar sem branch: " + subfield9);
			return false;
		}
		return true;
	}

	private static void printIfNotIncluded(String branchCODE) 
	{
		switch(branchCODE)
		{
			case "CENEDOM": break;
			case "MAB": break;
			case "MVM": break;
			case "MCM": break;
			case "MHN": break;
			case "MDINC": break;
			default:
				System.out.println(branchCODE + " não incluido!");
				break;
		}
		
	}

	private static void removeItemsNotFromCenedom(Record record) 
	{
		String branchCode;
		ArrayList<Field> fields952 = record.getField("952");
		
		for(Field field952 : record.getField("952"))
			{
				branchCode = field952.getSubfield("a").get(0).getValue();
				if(!branchCode.contentEquals("CENEDOM"))
				{
					record.removeField(field952);
				}
			}
	}
	
	private static void showItemsNotFromCenedom(Record record) 
	{
		String branchCodeA, branchCodeB;
		ArrayList<Field> fields952 = record.getField("952");
		
		for(Field field952 : record.getField("952"))
			{
				branchCodeA = field952.getSubfield("a").get(0).getValue();
				branchCodeB = field952.getSubfield("b").get(0).getValue();
				if(!branchCodeA.contentEquals("CENEDOM") || !branchCodeB.contentEquals("CENEDOM"))
				{
					System.out.print("$a = " + branchCodeA);
					System.out.println(", $b = " + branchCodeB);
				}
			}
	}
}
