package boundary.ibram;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import control.XMLHandler;
import entity.MARC;
import entity.Record;

public class AuxMHN {
	
	static HashMap <String, Integer> titlesInTheCollection;
	
	public static void findMissingRecordsFromMHN() throws Exception 
	{
		ArrayList<Record> currentRecords = new MARC("ibram/MHN/missingRecords/currentRecords.xml").getRecords(); 
		ArrayList<Record> missingRecords = new ArrayList<>();
		String outputPath = "ibram/MHN/missingRecords/missing.xml";
		
//		MHN.createFiles(null); // para gerar um arquivo xml que faltou
		
		fillDataFromTheCollection(currentRecords);
		compareCollection(currentRecords, getListOfXMLs(), missingRecords);
		
		System.out.println(missingRecords.size() + " novos registros criados em " + outputPath);
		XMLHandler.generateXMLFile(missingRecords, outputPath);
		
	}

	
	private static void compareCollection(ArrayList<Record> currentRecords, ArrayList<String> listOfXML,
			ArrayList<Record> missingRecords) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		ArrayList<Record> temp;
		int count = 1;
		
		for(String collection : listOfXML)
		{
			System.out.println("Checking " + collection + " - " + count + "/" + listOfXML.size());
			count++;
			
			temp = getMissingRecords(collection);			
			if(!temp.isEmpty())
				missingRecords.addAll(temp);
		}
	}

	private static void fillDataFromTheCollection(ArrayList<Record> currentRecords) 
	{
		titlesInTheCollection = new HashMap<>();
		
		for(Record record : currentRecords)
			titlesInTheCollection.put(getRecordSignature(record), 0);
	}


	private static String getRecordSignature(Record record) 
	{
		String currentTitle = "";
		
		if(record.hasAnyOccurrenceOfFieldAndSubfield("245", "a"))
			currentTitle = record.getField("245").get(0).getSubfield("a").get(0).getValue();
		else
			return "";
		
		currentTitle += "###";
		
		if(record.hasAnyOccurrenceOfFieldAndSubfield("245", "b"))
			currentTitle += record.getField("245").get(0).getSubfield("b").get(0).getValue();

		currentTitle += "###";
		
		if(record.hasAnyOccurrenceOfFieldAndSubfield("099", "a"))
			currentTitle += record.getField("099").get(0).getSubfield("a").get(0).getValue();

		currentTitle += "###";
		
		if(record.hasAnyOccurrenceOfFieldAndSubfield("100", "a"))
			currentTitle += record.getField("100").get(0).getSubfield("a").get(0).getValue();
		
		return currentTitle;
	}


	private static ArrayList<Record> getMissingRecords(String collectionPath) throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		ArrayList<Record> collection = new MARC(collectionPath).getRecords(); 
		ArrayList<Record> temp = new ArrayList<Record>();
		
		System.out.println("Size: " + collection.size() + " records");
		
		for(Record record : collection)
			if(!isThisRecordInTheCurrentCollection(record))
				temp.add(record);
		
		return temp;
	}

	
	private static boolean isThisRecordInTheCurrentCollection(Record record) 
	{
		if(!record.hasAnyOccurrenceOfFieldAndSubfield("245", "a"))
			return true;
		
		for(String currentRecordSignature : titlesInTheCollection.keySet())
			if(getRecordSignature(record).contentEquals(currentRecordSignature))
				return true;
		
		return false;
	}


	private static ArrayList<String> getListOfXMLs() 
	{
		ArrayList<String> listOfFiles = new ArrayList<>();
		
		listOfFiles.add("ibram/MHN/csv/FT 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/SF 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/BRASIL 500 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/CB 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/COL BRAS 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/COLEGIO DE ARMAS.xml");
		listOfFiles.add("ibram/MHN/csv/FF 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/HM 1 60a.xml");
		listOfFiles.add("ibram/MHN/csv/IM 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/LG 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/ORR FINAL 1 a 60 (1).xml");
		listOfFiles.add("ibram/MHN/csv/AG REFERÊNCIA 1 A 60.xml");
		listOfFiles.add("ibram/MHN/csv/AG REFERÊNCIA 1 A 60-2.xml");
		listOfFiles.add("ibram/MHN/csv/AG 1 TOTAL FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/AG FOLHETOS 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/DOC BRAS FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/Ana Maria Mezquita 1 a 60 mil.xml");
		listOfFiles.add("ibram/MHN/csv/PK FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/CERLUB 1 A 60 MIL FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/EG 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/EH 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/JD 1 60-1.xml");
		listOfFiles.add("ibram/MHN/csv/JD 1 60-2.xml"); 
		listOfFiles.add("ibram/MHN/csv/OR final 1 60.xml"); 
		listOfFiles.add("ibram/MHN/csv/MAGALY CABRAL FINAL Panfleto.xml");
		listOfFiles.add("ibram/MHN/csv/MAGALY CABRAL FINAL Livro.xml");
		listOfFiles.add("ibram/MHN/csv/BRASIL 200.xml");
		listOfFiles.add("ibram/MHN/csv/SM 1 60.xml"); 
		listOfFiles.add("ibram/MHN/csv/SM PANFLETO 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/Publ. MHN Final 1 a 60.xml"); 
		listOfFiles.add("ibram/MHN/csv/MAGALY CABRAL FINAL Folheto.xml"); 
		listOfFiles.add("ibram/MHN/csv/Arq vertical 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/GB 1 60.xml");
		listOfFiles.add("ibram/MHN/csv/AB PERI KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/AM PERI KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/IM PERI KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/MOC peri FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/CA PERI KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/SM PERI  KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/PUBL.MHN PERI KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/CERLUB PERI KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/BRASIL 500 PERI KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/GF PERI KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/NUMIS ORARA KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/NUMIS FOLHETO KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/COLECÃO CEM KOHA FINAL LIVRO.xml");
		listOfFiles.add("ibram/MHN/csv/COLECÃO CEM KOHA FINAL FOLHETO.xml");
		listOfFiles.add("ibram/MHN/csv/COLECÃO CEM KOHA FINAL PANFLETO.xml");
		listOfFiles.add("ibram/MHN/csv/COLECÃO CEM KOHA FINAL PERIODICOS.xml");
		listOfFiles.add("ibram/MHN/csv/NUMIS REFERÊNCIA KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/NUMIS PERI KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/AG OBRA RARA PERI FINAL 2.xml");
		listOfFiles.add("ibram/MHN/csv/AG OBRA RARÍSSIMA KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/EL LIVRO.xml");
		listOfFiles.add("ibram/MHN/csv/EL PERIÓDICO.xml");
		listOfFiles.add("ibram/MHN/csv/EL ÁLBUM.xml");
		listOfFiles.add("ibram/MHN/csv/EL CARTÃO POSTAL.xml");
		listOfFiles.add("ibram/MHN/csv/EL FOLHETO.xml");
		listOfFiles.add("ibram/MHN/csv/AG PERI FINAL KOHA.xml");
		listOfFiles.add("ibram/MHN/csv/AG PERI FINAL KOHA_2.xml");
		listOfFiles.add("ibram/MHN/csv/ACERVO GERAL KOHA FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/MOC peri FINAL.xml");
		listOfFiles.add("ibram/MHN/csv/PANAIR 1 60.xml");
		
		return listOfFiles;
	}

}
