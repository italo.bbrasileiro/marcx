package boundary.ibram;

import java.util.ArrayList;

import control.CSVController;
import entity.MARC;
import entity.Record;

public class Main {
	
	public static void main(String[] args) throws Exception 
	{
		
//		MHN.migrate();
//		AuxMHN.findMissingRecordsFromMHN();
//		CENEDOM.migrate();
//		BelasArtes.migrate();
//		CastroMaya.migrate();
//		Inconfidencia.migrate();
		Republica.migrate();
		
//		generateCSVWithFields();
		
	}

	

	private static void generateCSVWithFields() throws Exception 
	{
		ArrayList<Record> acervo;
		ArrayList<String[]> arrayToWrite = instantiateAndAddHeader();
		
		for(int count = 1; count <= 4; count++)
		{
			System.out.println("Lendo arquivo " + count);
			acervo = new MARC("ibram/pt" + count + ".xml").getRecords();			
			arrayToWrite.addAll(MigrationAux.generateCSVwithAll500Field(acervo));
		}
		
		CSVController.writeCSV("ibram/listaCampos5xx.csv", arrayToWrite, "\t");
	}

	private static ArrayList<String[]> instantiateAndAddHeader() 
	{
		ArrayList<String[]> arrayToWrite = new ArrayList<>();
		String[] header = {"biblionumber", "campo 5xx", "subcampo", "conteudo"};
		arrayToWrite.add(header);
		return arrayToWrite;
	}
}
