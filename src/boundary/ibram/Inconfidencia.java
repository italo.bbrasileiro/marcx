package boundary.ibram;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import control.XMLHandler;
import entity.Constant;
import entity.Field;
import entity.Record;
import entity.Subfield;

public class Inconfidencia 
{
	static HashMap<String, Integer> values952$8 = new HashMap<>();
	
	public static void migrate() throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		
		ArrayList<Record> acervo = readMrkFile("ibram/Inconfidencia/mrkParseado.mrk");
		
//		print952$8values();
		
		for(Record record : acervo)
		{
			addMandatoryField(record);
			
			if(record.hasField("952"))
				if(record.getField("952").get(0).getSubfields().size() < 1)
					System.err.println("ERROOO");

		}
		
		
		
		String outputPath = "ibram/Inconfidencia/outputInconfidencia.xml";
		System.out.println(acervo.size() + " novos registros criados em " + outputPath);
		XMLHandler.generateXMLFile(acervo, outputPath);
	}
	
	private static void addMandatoryField(Record record) 
	{
		addBranchCode(record);
		addMaterialField(record);
//		addField001(record);
	}
	
	
	private static void addBranchCode(Record record) 
	{
		ArrayList<Field> field952 = record.getField("952");
		
		if(field952 == null)
			record.addField(new Field("952", "", "", new ArrayList<>(), Constant.dataFieldDenomination));
		
		for(Field field : record.getField("952"))
		{
			field.addSubfield(new Subfield("a", "MDINC"));
			field.addSubfield(new Subfield("b", "MDINC"));
		}
	}
	
	
	private static void addMaterialField(Record record) 
	{
		String sigla = getSigla(record);
		
		if(record.hasField("952"))
			for(Field field952 : record.getField("952"))
				field952.addSubfield(new Subfield("y", sigla));
		
		if(record.hasField("942"))
			record.getField("942").get(0).addSubfield(new Subfield("c", sigla));
		else
			record.addField(new Field("942", "c", sigla, Constant.dataFieldDenomination));
	}
	

	private static String getSigla(Record record) 
	{
		String sigla = "LIV";
		
		if(record.hasAnyOccurrenceOfFieldAndSubfield("942", "i"))
		{
			String material = record.getField("942").get(0).getSubfield("i").get(0).getValue(); 
			if(material.contentEquals("PERIÓDICO") || material.contains("PERIODICO")
					|| material.contains("ARTIGO DE PERIÓDICO"))
				sigla = "PER";
			else if (material.contains("TESE") || material.contains("MONOGRAFIA"))
				sigla = "TD";
			else if (material.contains("CD - ROM"))
				sigla = "ALI";
			else if (material.contains("LI - ") || material.contains("CONGRESSO")
					|| material.contains("PA - ") || material.contains("ENCICLOPÉDIA")
					|| material.contains("DICIONÁRIO") || material.contains("DICIONARIO")
					|| material.contains("Brasil"))
				sigla = "LIV";
			else
				System.out.println(material);
		}
		
		return sigla;
	}

	private static ArrayList<Record> readMrkFile(String inputPath) throws IOException 
	{
		ArrayList<Record> records = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(new File(inputPath), StandardCharsets.UTF_8)); 
		String line;
		Record record = null;
		ArrayList<Field> fields = new ArrayList<>();
		
		while ((line = br.readLine()) != null)
		{
			if(line.isBlank() || line.isEmpty())
				continue;
			
			if(line.contains("=LDR  "))
			{
				fields = new ArrayList<>();
				record = new Record(fields);
				record.generateControlFields();
				records.add(record);
			}
			
			insertNewField(line, record, fields);
		}
		
		br.close();
		return records;
	}

	private static void insertNewField(String line, Record record, ArrayList<Field> fields)
	{
		String campo = line.substring(1, 4), valor = line.substring(6);
		
		if(campo.contains("LDR"))
			record.generateLeader(valor);
//			fields.add(new Field(Constant.leaderDenomination, valor, Constant.leaderDenomination));
		else if(campo.contains("026"))
			record.editField008("por", "35-37");
		else if(campo.contains("030"))
			add952subfield(record, "o", valor);
		else if(campo.contains("031"))
		{
			fields.add(new Field("942", "i", valor, Constant.dataFieldDenomination));
//			add952subfield(record, "8", valor);
//			values952$8.put(valor, 0);
		}
		else if(campo.contains("035"))
			add952subfield(record, "p", valor);
		else if(campo.contains("260"))
			add260subfield(record, "a", valor);
		else if(campo.contains("261"))
			add260subfield(record, "b", valor);
		else if(campo.contains("262"))
			add260subfield(record, "c", valor);
		else if(campo.contains("300"))
			check300ilustration(record, valor);
		else if(campo.contains("600"))
			defineAuthorityType(record, valor);
		else
			fields.add(new Field(campo, "a", valor, Constant.dataFieldDenomination));
	}


	private static void defineAuthorityType(Record record, String valor) 
	{
		String fieldName = "650";
		
		if(valor.startsWith("[A-Z]+"))
			System.out.println();
		
		if(valor.matches("[.]*[A-Z]+[,][ ][A-Za-z| |.]+"))
			fieldName = "700";
		
		record.addField(new Field(fieldName, "a", valor, ""));
	}


	private static void check300ilustration(Record record, String valor) 
	{
		
		int charPoint = valor.indexOf("il.");
		String subA, subB;
		ArrayList<Subfield> subfields = new ArrayList<>();
		
		if(charPoint >= 0)
		{
			subA = valor.substring(0, charPoint).trim();
			subB = valor.substring(charPoint);
			subfields.add(new Subfield("b", subB));
		}else
			subA = valor;
		
		subfields.add(new Subfield("a", subA));
		record.addField(new Field("300", "", "", subfields, ""));
		
		
	}


	private static void add952subfield(Record record, String subfield, String valor) 
	{	checkField(record, subfield, valor, "952");	}

	
	private static void add260subfield(Record record, String subfield, String valor) 
	{	checkField(record, subfield, valor, "260");	}

	
	private static void checkField(Record record, String subfield, String value, String fieldName) 
	{
		if(!record.hasField(fieldName))
			record.addField(new Field(fieldName, subfield, value, Constant.dataFieldDenomination));
		else
		{
			Field field952 = record.getField(fieldName).get(0);
			field952.addSubfield(new Subfield(subfield, value));
		}
	}


	private static void print952$8values() 
	{
		for(String value : values952$8.keySet())
			System.out.println(value);
	}	


	
	

} 
