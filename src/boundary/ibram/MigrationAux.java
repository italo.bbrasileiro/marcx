package boundary.ibram;

import java.util.ArrayList;

import entity.Field;
import entity.Subfield;
import entity.Record;

public class MigrationAux {
	
	
	public static ArrayList<String[]> generateCSVwithAll500Field(ArrayList<Record> records) throws Exception 
	{
		ArrayList<String[]> arrayToWrite = new ArrayList<>(), temp;

		for(Record record : records)
			for(Field field : record.getFields())
				if(field.getFieldName().matches("[5][0-9][0-9]"))
				{
					temp = getContentForTable(getBiblionumber(record), field);
					if(temp != null)
						arrayToWrite.addAll(temp);
					
				}
		
		return arrayToWrite;
	}

	
	private static String getBiblionumber(Record record) 
	{	return record.getField("999").get(0).getSubfield("c").get(0).getValue();	}

	
	private static ArrayList<String[]> getContentForTable(String biblionumber, Field field) 
	{
		ArrayList<String[]> allSubfields = new ArrayList<>();
		
		for(Subfield subfield : field.getSubfields())
		{
			// estrutura do array: [0] biblionumber, [1] campo 5xx, [2] subcampo, [3] valor
			String[] campoAtual = {biblionumber, field.getFieldName(), subfield.getSubfieldName(), subfield.getValue()}; //new String[4];
			allSubfields.add(campoAtual);
		}
		return allSubfields;
	}
}
