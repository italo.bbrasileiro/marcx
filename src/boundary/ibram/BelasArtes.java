package boundary.ibram;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import control.XMLHandler;
import entity.Field;
import entity.MARC;
import entity.Record;

public class BelasArtes {

	public static void migrate() throws ParserConfigurationException, SAXException, IOException, TransformerException 
	{
		MARC marc = new MARC("ibram/BelasArtes/belasArtes.xml");
		ArrayList<Record> records = marc.getRecords(), outputRecords = new ArrayList<>();
		String outputPath = "ibram/BelasArtes/finalBelasArtes.xml";
		
		for(Record record : records)
		{
			if(isRecordFromBelasArtes(record))
			{
				outputRecords.add(record);
			}
		}
		
		cleanField001(outputRecords);
		correctMaterialCode(outputRecords, "TES", "TD");
		getAllMaterialTypes(outputRecords);
		
		System.out.println(outputRecords.size() + " novos registros criados em " + outputPath);
		XMLHandler.generateXMLFile(outputRecords, outputPath);
	}

	private static void correctMaterialCode(ArrayList<Record> records, String oldCode, 
			String newCode) 
	{
		String material;
		
		for(Record record : records)
		{
			material = record.getField("942").get(0).getSubfield("c").get(0).getValue();
			if(material.contentEquals(oldCode))
				record.getField("942").get(0).getSubfield("c").get(0).setValue(newCode);
		}
		
	}

	private static void getAllMaterialTypes(ArrayList<Record> records) 
	{
		String material;
		HashMap<String, Integer> materials = new HashMap<>();
		
		for (Record record : records)
		{
			material = record.getField("942").get(0).getSubfield("c").get(0).getValue();
			if(!materials.containsKey(material))
				materials.put(material, 0);
			
			materials.put(material, materials.get(material) + 1);			
		}
		
		for(String mat : materials.keySet())
		{
			System.out.println(mat + " occurs: " + materials.get(mat));
		}
		
	}

	private static void cleanField001(ArrayList<Record> records) 
	{
		for(Record record : records)
			if(record.hasField("001"))
				record.removeAllOccurrencesOfField("001");
	}

	private static boolean isRecordFromBelasArtes(Record record) 
	{
		String branch;
		if(record.hasAnyOccurrenceOfFieldAndSubfield("952", "a"))
		{
			for(Field field952 : record.getField("952"))
			{
				branch = field952.getSubfield("a").get(0).getValue();
				if(branch.contentEquals("MNBA"))
					return true;
			}
		}
		return false;
	}

}
