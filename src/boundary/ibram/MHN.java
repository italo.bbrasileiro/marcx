package boundary.ibram;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import control.CSVController;
import control.XMLHandler;
import entity.Constant;
import entity.Field;
import entity.Record;
import entity.Subfield;

public class MHN 
{
	static int cont001 = 0;
	
	public static void migrate() throws Exception 
	{
		ArrayList<Record> allRecords = new ArrayList<>(), duplicatedBarcodes, noBarcode;
		String outputPath = "ibram/MHN/result.xml", 
				outputDuplicated = "ibram/MHN/duplicatedBarcodes.xml",
				outputNoBarcode = "ibram/MHN/noBarcodes.xml";
		
		allRecords = generateRecordsFromMigrationFiles();
		
/** esse trecho de código nao serve para os últimos três arquivos de csv.
 * 		
  		duplicatedBarcodes = removeRecordsWithDuplicatedBarcodes(allRecords);
		noBarcode = getRecordsWithoutBarcode(allRecords);
		
		allRecords.addAll(noBarcode);
		
		correctionOfDuplicated(allRecords, duplicatedBarcodes);
		allRecords.addAll(duplicatedBarcodes);
		
		// feito duas vezes porque depois da primeira ainda aparecem novos conflitos
		duplicatedBarcodes = removeRecordsWithDuplicatedBarcodes(allRecords);
		correctionOfDuplicated(allRecords, duplicatedBarcodes);
		
		allRecords = mergeItensWithSameCallNumber(allRecords);
 **/	
		XMLHandler.generateXMLFile(allRecords, "ibram/MHN/AG PERI FINAL KOHA_2.xml");
		System.out.println("Registros sem erros: " + allRecords.size());

		// to generate list of duplicated records
//		duplicatedBarcodes = removeRecordsWithDuplicatedBarcodes(allRecords);
//		XMLHandler.generateXMLFile(duplicatedBarcodes, outputDuplicated);
//		System.out.println("Duplicados: " + duplicatedBarcodes.size());
		
		// to generate list of records without barcode
//		XMLHandler.generateXMLFile(noBarcode, outputNoBarcode);
//		System.out.println("Sem barcode: " + noBarcode.size());
		
		System.out.println("Terminado");
	}

	private static void correctionOfDuplicated(ArrayList<Record> allRecords, ArrayList<Record> duplicatedBarcodes) 
	{
		HashMap<String, ArrayList<Record>> duplicatedBC = separateRecords(duplicatedBarcodes); new HashMap<>();
		char letter;
		String barcode;
		Record record;
		
		for(String key : duplicatedBC.keySet())
		{
			letter = 'a';
			for(int r = 1; r < duplicatedBC.get(key).size(); r++)
			{
				record = duplicatedBC.get(key).get(r);
				barcode = record.getField("952").get(0).getSubfield("p").get(0).getValue();
				barcode = barcode + (letter++);
				record.getField("952").get(0).getSubfield("p").get(0).setValue(barcode);
			}
		}
	}

	private static HashMap<String, ArrayList<Record>> separateRecords(ArrayList<Record> duplicatedBarcodes) 
	{
		HashMap<String, ArrayList<Record>> duplicatedBC = new HashMap<>();
		String BC;
		
		for(Record record : duplicatedBarcodes)
		{
			if(!record.hasAnyOccurrenceOfFieldAndSubfield("952", "p"))
				continue;
			
			BC = record.getField("952").get(0).getSubfield("p").get(0).getValue();
			if(!duplicatedBC.containsKey(BC))
				duplicatedBC.put(BC, new ArrayList<>());

			duplicatedBC.get(BC).add(record);
		}
		return duplicatedBC;
	}

	private static ArrayList<Record> getRecordsWithoutBarcode(ArrayList<Record> allRecords) 
	{
		ArrayList<Record> noBarcode = new ArrayList<>();
		String barcode;
		
		for(Record record : allRecords)
			for(Field field952 : record.getField("952"))
			{
				barcode = getBarcode(field952);
				
				if(barcode == null)
				{
					noBarcode.add(record);
				}
			}
		
		allRecords.removeAll(noBarcode);
		
		return noBarcode;
	}

	private static ArrayList<Record> removeRecordsWithDuplicatedBarcodes(ArrayList<Record> allRecords) 
	{
		return getDuplicatedBarcodes(fillWithRecords(allRecords), allRecords);
	}

	private static ArrayList<Record> getDuplicatedBarcodes(HashMap<String, ArrayList<Record>> barcodes, ArrayList<Record> allRecords) 
	{
		ArrayList<Record> duplicated = new ArrayList<>(), currentList;
		
		for(String barcode: barcodes.keySet())
		{
			currentList  = barcodes.get(barcode);
			if(currentList.size() == 1)
				continue;
			
			for(Record record : currentList)
			{
				duplicated.add(record);
				allRecords.remove(record);
			}
		}
		return duplicated;
	}

	private static HashMap<String, ArrayList<Record>> fillWithRecords(ArrayList<Record> allRecords) 
	{
		HashMap<String, ArrayList<Record>> barcodes = new HashMap<>();
		String barcode;
		
		for(Record record : allRecords)
			for(Field field952 : record.getField("952"))
			{
				barcode = getBarcode(field952);
				
				if(barcode == null || barcode.contentEquals(""))
					continue;
				
				addRegistroToMap(barcodes, barcode, record);
			}
		return barcodes;
	}

	private static void addRegistroToMap(HashMap<String, ArrayList<Record>> barcodes, String barcode, Record record) 
	{
		if(!barcodes.containsKey(barcode))
			barcodes.put(barcode, new ArrayList<Record>());
		
		barcodes.get(barcode).add(record);
	}

	private static String getBarcode(Field field952) 
	{
		if(field952.hasSubfield("p"))
			return field952.getSubfield("p").get(0).getValue().trim();

		return null;
	}

	private static ArrayList<Record> generateRecordsFromMigrationFiles(String filepath) throws Exception 
	{		return createXMLfromCSV(filepath);	}
	
	private static ArrayList<Record> generateRecordsFromMigrationFiles() throws Exception 
	{
		ArrayList<Record> allRecords = new ArrayList<>();
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/PANAIR 1 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/FT 1 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/SF 1 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/BRASIL 500 1 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/CB 1 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/COL BRAS 1 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/COLEGIO DE ARMAS.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/FF 1 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/HM 1 60a.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/IM 1 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/LG 1 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/ORR FINAL 1 a 60 (1).csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/AG REFERÊNCIA 1 A 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/AG REFERÊNCIA 1 A 60-2.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/AG 1 TOTAL FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/AG FOLHETOS 1 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/DOC BRAS FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/Ana Maria Mezquita 1 a 60 mil.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/PK FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/CERLUB 1 A 60 MIL FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/EG 1 60.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/EH 1 60.csv")); // as tres ultimas linhas apresentam apenas uma coluna preenchida, e algumas entradas apresentam apenas "," na segunda coluna.
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/JD 1 60-2.csv")); // tem duas colunas não preenchidas: volume e página
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/OR final 1 60.csv")); //TIPO DE MATERIAL "xxx" trocado por "Livro"
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/MAGALY CABRAL FINAL Panfleto.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/MAGALY CABRAL FINAL Livro.csv")); // possui tipo de material "MOC", foi trocado por "livro"
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/BRASIL 200.csv")); // tipo de material "Panfleto"
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/SM 1 60.csv")); //TIPO DE MATERIAL "Panfleto"
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/SM PANFLETO 1 60.csv"));// TIPO DE MATERIAL DESCONHECIDO "Panfleto"
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/Publ. MHN Final 1 a 60.csv")); // TIPO DE MATERIAL "Panfleto" DESCONHECIDO
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/MAGALY CABRAL FINAL Folheto.csv")); // faltam informações na última linha do arquivo
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/Arq vertical 1 60.csv")); //tipo de material "Arquivo Vertical" // done: campo "Nacionalidade"
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/GB 1 60.csv")); // tipos de material "obra rara" e "recortes", // done: campo não indicado para "nacionalidade".
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/AB PERI KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/AM PERI KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/IM PERI KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/MOC peri FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/CA PERI KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/SM PERI  KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/PUBL.MHN PERI KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/CERLUB PERI KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/BRASIL 500 PERI KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/GF PERI KOHA FINAL.csv"));
		
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/NUMIS ORARA KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/NUMIS FOLHETO KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/COLECÃO CEM KOHA FINAL LIVRO.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/COLECÃO CEM KOHA FINAL FOLHETO.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/COLECÃO CEM KOHA FINAL PANFLETO.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/COLECÃO CEM KOHA FINAL PERIODICOS.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/NUMIS REFERÊNCIA KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/NUMIS PERI KOHA FINAL.csv"));


//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/AG OBRA RARA PERI FINAL 2.csv"));
//		addItemsInRecords(allRecords, createXMLfromCSV("ibram/MHN/csv/AG OBRA RARA PERI FINAL 1.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/AG OBRA RARÍSSIMA KOHA FINAL.csv"));
		
		
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/EL LIVRO.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/EL PERIÓDICO.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/EL ÁLBUM.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/EL CARTÃO POSTAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/EL FOLHETO.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/AG PERI FINAL KOHA.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/AG PERI FINAL KOHA_2.csv"));
		
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/ACERVO GERAL KOHA FINAL.csv"));
//		allRecords.addAll(createXMLfromCSV("ibram/MHN/csv/MOC peri FINAL.csv"));
		return allRecords;
	}

	

	private static void addItemsInRecords(ArrayList<Record> allRecords, 
			ArrayList<Record> itemsRecords) 
	{
		Record originalRecord;
		String field090a;
		for(Record record : itemsRecords)
		{
			field090a = record.getField("090").get(0).getSubfield("a").get(0).getValue();
			originalRecord = getRecordWith090a(allRecords, field090a);
			if(originalRecord == null)
			{
				fillField(record, "099", "", "", "a", field090a);
				allRecords.add(record);
			}
			else
				originalRecord.addField(record.getField("952").get(0));
		}
		
	}

	private static Record getRecordWith090a(ArrayList<Record> allRecords, String field090a) 
	{
		String field099a;
		for(Record record : allRecords)
		{
			field099a = record.getField("099").get(0).getSubfield("a").get(0).getValue();
			if(field090a.contentEquals(field099a))
				return record;
		}
		return null;
	}

	private static ArrayList<Record> createXMLfromCSV(String filePath) throws Exception 
	{
		ArrayList<String[]> csv = CSVController.readCSV(filePath, "\t");
		String[] fieldName = csv.get(0);
		Record record;
		ArrayList<Field> fields;
		ArrayList<Record> records = new ArrayList<>();
		boolean csvHasException = csvFileHasException(filePath);
		previous090name = "";
		
		for(int linha = 2; linha < csv.size(); linha++)
		{
			// instancia registro e campos
			fields = new ArrayList<>();
			record = new Record(fields);
			records.add(record);
			
			record.generateLeaderAndControlFields();
//			addMaterialField(record);

			for(int colIndex = 0; colIndex < fieldName.length; colIndex++)
			{
				if(colIndex < csv.get(linha).length)
					if(csvHasException)
						createSpecificFieldFromColumn(record, csv.get(linha)[colIndex].trim(), fieldName[colIndex]);
					else
						createFieldFromColumn(record, csv.get(linha)[colIndex].trim(), fieldName[colIndex]);
				
			}
			addMandatoryField(record);

		}
		
		System.out.println(records.size() + " novos registros criados em " + filePath);
//		XMLHandler.generateXMLFile(records, filePath.substring(0, filePath.length() - 4) + ".xml");
		return records;
	}

	private static boolean csvFileHasException(String filePath) 
	{	return (filePath.contentEquals("ibram/MHN/csv/AG PERI FINAL KOHA.csv")
			|| (filePath.contentEquals("ibram/MHN/csv/AG PERI FINAL KOHA_2.csv")));	}

	private static ArrayList<Record> mergeItensWithSameCallNumber(ArrayList<Record> records) 
	{
		HashMap<String, Record> mergedRecords = new HashMap<>();
		
		for(Record record : records)
		{
			if(!record.hasField("099"))
				continue;
			groupDuplicatedCallNumbersAsItem(mergedRecords, record);
		}
		
		ArrayList<Record> result = new ArrayList<>();
		
		for(String key : mergedRecords.keySet())
			result.add(mergedRecords.get(key));
		
		return result;
	}

	private static void groupDuplicatedCallNumbersAsItem(HashMap<String, Record> mergedRecords,
			Record record) 
	{
		String callnumber;
		Record currentRecord;
		
		callnumber = record.getField("099").get(0).getSubfield("a").get(0).getValue();
		if(!mergedRecords.containsKey(callnumber))
			mergedRecords.put(callnumber, record);
		else
		{
			currentRecord = mergedRecords.get(callnumber);
			currentRecord.addField(record.getField("952"));
		}
	}

	private static void addMandatoryField(Record record) 
	{
		addMaterialField(record);
		addBranchCode(record);
		addField001(record);
		
	}

	private static void addField001(Record record) {
		record.addField(new Field("001", Integer.toString(cont001++), Constant.controlFieldDenomination));
	}
	
	
	private static String previous090name; 
	private static void createSpecificFieldFromColumn(Record record, String value, String fieldName)
	{
		switch(fieldName)
		{
		case "090$a / 952$o":
			if(value.isBlank())
				value = previous090name;
			else
				previous090name =  value;
			
			fillField(record, "090", "", "", "a", value);
			fillField(record, "952", "", "", "o", value);
			break;
		default:
			createFieldFromColumn(record, value, fieldName);
		}
	}

	private static void createFieldFromColumn(Record record, String value, String fieldName) 
	{	
		if( (value == null) || value.contentEquals(""))
			return;
		
		switch(fieldName)
		{
		case "008 35-37":
			if(value.contentEquals("Português"))
			{
				record.editField008("por", "35-37");
			}else if(value.contentEquals("Francês"))
			{
				record.editField008("fre", "35-37");
			}else if(value.contentEquals("Inglês"))
			{
				record.editField008("eng", "35-37");
			}else
			{
				System.err.println("Idioma nao reconhecido:" + value);
			}
			break;
		case "008 15-17":
			if(value.contentEquals("Nacional"))
			{
				record.editField008("bl ", "15-17");
				fillField(record, "043", "", "", "a", "bl");
			}else if(value.contentEquals("Estrangeiro"))
			{
				
			}else if(value.contentEquals(""))
			{
				
			}else
			{
				System.err.println("Nacionalidade nao reconhecida:" + value);
			}
			break;
		case "020$a":
			fillField(record, "020", "", "", "a", value);
			break;
		case "022$a":
			fillField(record, "022", "", "", "a", value);
			break;
		case "040$b":
			fillField(record, "040", "", "", "b", value);
			break;
		case "043$a":
			fillField(record, "043", "", "", "a", value);
			break;
		case "082$a":
			fillField(record, "082", "", "", "a", value);
			break;
		case "090$a":
			fillField(record, "090", "", "", "a", value);
			break;
		case "090$a / 952$o":
			fillField(record, "090", "", "", "a", value);
			fillField(record, "952", "", "", "o", value);
			break;
		case "099$a":
			fillField(record, "099", "", "", "a", value);
			break;
		case "100$a":
			fillField(record, "100", "", "", "a", value);
			break;
		case "110$a":
			fillField(record, "110", "", "", "a", value);
			break;
		case "245$a":
			fillField(record, "245", "", "", "a", value);
			break;
		case "245 10$a":
			fillField(record, "245", "1", "0", "a", value);
			break;
		case "245$b":
			fillField(record, "245", "", "", "b", value);
			break;
		case "245$c":
			fillField(record, "245", "", "", "c", value);
			break;
		case "250$a":
			fillField(record, "250", "", "", "a", value);
			break;
		case "254$b":
			fillField(record, "254", "", "", "b", value);
			break;
		case "260$a":
			fillField(record, "260", "", "", "a", value);
			break;
		case "260$b":
			fillField(record, "260", "", "", "b", value);
			break;
		case "260$c":
			fillField(record, "260", "", "", "c", value);
			break;
		case "300$a":
			/* TODO tratar informacoes desse campo:
			 * 300. Subacampo "a" página. Subcampo "b" il. Subcampo "c" cm.
			*/
			fillField(record, "300", "", "", "a", value);
			break;
		case "330$a":
			fillField(record, "330", "", "", "a", value);
			break;
		case "365$a":
			fillField(record, "365", "", "", "a", value);
			break;
		case "365$b":
			fillField(record, "365", "", "", "b", value);
			break;
		case "440$a":
			fillField(record, "440", "", "", "", value);
			break;
		case "490$a":
			fillField(record, "490", "", "", "", value);
			break;
		case "500$a":
			fillField(record, "500", "", "", "a", value);
			break;
		case "505$a":
			fillField(record, "505", "", "", "a", value);
			break;
		case "583$l":
			fillField(record, "583", "", "", "l", value);
			break;
		case "590$a":
			fillField(record, "590", "", "", "a", value);
			break;
		case "600$a":
			fillField(record, "600", "", "", "a", value);
			break;
		case "610$a":
			fillField(record, "610", "", "", "a", value);
			break;
		case "650$a":
			/*
			 * TODO
			 * separar as autoridades por ;
			 */
			fillField(record, "650", "", "", "a", value);
			break;
		case "651$a":
			fillField(record, "651", "", "", "a", value);
			break;
		case "952$8":
			fillField(record, "952", "", "", "8", value);
			break;
		case "952$b":
			fillField(record, "952", "", "", "8", value);
			break;
		case "952$e":
			fillField(record, "952", "", "", "e", value);
			break;
		case "952$f":
			fillField(record, "952", "", "", "f", value);
			break;
		case "952$h":
			fillField(record, "952", "", "", "h", value);
			break;
		case "952$i":
			fillField(record, "952", "", "", "i", value);
			break;
		case "952$o":
			fillField(record, "952", "", "", "o", value);
			break;
		case "952$p":
			fillField(record, "952", "", "", "p", value);
			break;
		case "952$y":
			fillField(record, "952", "", "", "y", value);
			break;
		case "999$c":
			fillField(record, "999", "", "", "c", value);
			break;
		default:
			System.err.println("Campo " + fieldName + " nao definido.");
		}
	}

	private static void fillField(Record record, String fieldName, String ind1, String ind2, String subfield,
			String value) 
	{
		Field destinationField;
		destinationField = defineFieldForValue(record, fieldName, ind1, ind2);
		destinationField.addSubfield(new Subfield(subfield, value));
		
	}

	private static Field defineFieldForValue(Record record, String fieldName, String ind1, String ind2) 
	{
		if(record.hasField(fieldName))
			return record.getField(fieldName).get(0);
		else
		{
			Field field = new Field(fieldName, ind1, ind2, new ArrayList<>(), Constant.dataFieldDenomination);
			record.addField(field);
			return field;
		}
	}

	private static void addBranchCode(Record record) 
	{
		ArrayList<Field> field952 = record.getField("952");
		ArrayList<Subfield> subfields;
		
		if(field952 == null)
		{
			subfields = new ArrayList<>();
			record.addField(new Field("952", "", "", subfields, Constant.dataFieldDenomination));
			
		}
		
		for(Field field : field952)
		{
			field.addSubfield(new Subfield("a", "MHN"));
			field.addSubfield(new Subfield("b", "MHN"));
		}
	}
	
	private static void addMaterialField(Record record) 
	{
		String material = record.getField("952").get(0).getSubfield("y").get(0).getValue();
		String sigla = null;
		
		switch(material.trim())
		{
			case "Álbum":
				sigla = "ALBUM";
				break;
			case "Cartão-postal":
				sigla = "CP";
				break;
			case "Livro":
				sigla = "LIV";
				break;
			case "LIVRO":
				sigla = "LIV";
				break;
			case "Folheto":
				sigla = "FOL";
				break;
			case "Panfleto":
				sigla = "PAN";
				break;
			case "PANFLETO":
				sigla = "PAN";
				break;
			case "Recortes":
				sigla = "REC";
				break;
			case "Arquivo Vertical":
				sigla = "REC";
				break;
			case "Periódico":
				sigla = "PER";
				break;
			case "Obra Rara":
				sigla = "OBRA";
				break;
			default:
				System.err.println("Tipo de material desconhecido: " + material);
		}
		
		record.getField("952").get(0).getSubfield("y").get(0).setValue(sigla);
		record.addField(new Field("942", "c", sigla, Constant.dataFieldDenomination));
	}
	
	/*
	 * Usei isso para criar os arquivos xml que estavam faltando
	 */
	public static void createFiles(ArrayList<String> filePaths) throws Exception
	{
//		for(String file : filePaths)
//			migrate(file);
		migrate("ibram/MHN/csv/JD 1 60-1.csv");
	}
	
	public static void migrate(String outputPath) throws Exception 
	{
		ArrayList<Record> allRecords = generateRecordsFromMigrationFiles(outputPath);
		outputPath = outputPath.replace("/csv/", "/temp/");
		outputPath = outputPath.substring(0, outputPath.length() - 4) + ".xml";
		System.out.println(outputPath);
		XMLHandler.generateXMLFile(allRecords, outputPath);
		System.out.println("Registros sem erros: " + allRecords.size());
		System.out.println("Terminado");
	}
	
	
	}
